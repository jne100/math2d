///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#include <gtest/gtest-all.cc>


//#define MANUAL_CL_ARGS


int main( int argc, char* argv[] )
{
#ifndef MANUAL_CL_ARGS
    testing::InitGoogleTest( &argc, argv );
#else
    int manualArgc = 2;
    char filterArg[] = "--gtest_filter=Polyline.TestCalcTiltStep2";
    char* manualArgv[] = { argv[0], filterArg };
    testing::InitGoogleTest( &manualArgc, manualArgv );
#endif
    int result = RUN_ALL_TESTS();
    return result;
}

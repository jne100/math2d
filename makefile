CC=g++
BINDIR=bin/Math2d_gcc
OBJDIR=$(BINDIR)/obj
DEPSDIR=$(BINDIR)/obj
INCDIRS=-isystem./libs -I./src -I./test
CFLAGS=-c -MMD -Wall -Wextra -O2 -g ${INCDIRS}
LDFLAGS=-lpthread -g
SOURCES= \
	Math2d.cpp \
	$(wildcard src/*.cpp) \
	$(wildcard test/*.cpp)
VPATH=$(dir $(SOURCES))
OBJECTS=$(patsubst %.cpp,$(OBJDIR)/%.o,$(notdir $(SOURCES)))
DEPENDENCIES=$(patsubst %.cpp,$(OBJDIR)/%.d,$(notdir $(SOURCES)))
EXECUTABLE=$(BINDIR)/math2d

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) $(DEPENDENCIES)
	@mkdir -p $(BINDIR)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

$(DEPENDENCIES): $(DEPSDIR)/%.d : %.cpp
	@mkdir -p $(DEPSDIR)
	$(CC) -MM $< > $@

$(OBJECTS): $(OBJDIR)/%.o : %.cpp
	@mkdir -p $(OBJDIR)
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -rf $(BINDIR)

ifneq ($(MAKECMDGOALS),clean)
    -include ${DEPENDENCIES}
endif

#include "m2d_fused.h"


///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


/*---*/

#include <cstring>


namespace m2d {


//-------------------------------------------------------------------------------------------------
// U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


bool LT( double lhsHigh, double lhsLow, double rhsHigh, double rhsLow )
{
    if ( EQ(lhsHigh, rhsHigh) ) {
        // compare minor components
        return LT( lhsLow, rhsLow );
    } else {
        // compare major components
        return LT( lhsHigh, rhsHigh );
    }
}


bool LT( double lhsHigh, double lhsLow, double rhsHigh, double rhsLow, double eps )
{
    if ( EQ(lhsHigh, rhsHigh, eps) ) {
        // compare minor components
        return LT( lhsLow, rhsLow, eps );
    } else {
        // compare major components
        return LT( lhsHigh, rhsHigh, eps );
    }
}


double StrictNormalize( double value, double endValue, double targetEndValue /*= 1.0*/ )
{
    double normValue = Normalize( value, endValue, targetEndValue );
    if ( normValue < 0.0 ) {
        return 0.0;
    } else if ( normValue > targetEndValue ) {
        return targetEndValue;
    } else {
        return normValue;
    }
}


double StrictNormalize( double value, double beginValue, double endValue,
    double targetBeginValue, double targetEndValue )
{
    double normValue = Normalize( value, beginValue, endValue, targetBeginValue, targetEndValue );
    if ( normValue < targetBeginValue ) {
        return targetBeginValue;
    } else if ( normValue > targetEndValue ) {
        return targetEndValue;
    } else {
        return normValue;
    }
}


double CalcDistance( double x1, double y1, double x2, double y2 )
{
    double dx = x2 - x1;
    double dy = y2 - y1;
    return ( ::sqrt((dx * dx) + (dy * dy)) );
}


double CalcDistance( const Point& p1, const Point& p2 )
{
    return CalcDistance( p1.x_, p1.y_, p2.x_, p2.y_ );
}


Point CalcMiddlePoint( const Point& p1, const Point& p2 )
{
    return Point( (p1.x_ + p2.x_) / 2.0, (p1.y_ + p2.y_) / 2.0 );
}


//-------------------------------------------------------------------------------------------------
// P A I R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Pair::Pair()
{
}


Pair::Pair( const Pair& rhs )
    : v1_( rhs.v1_ )
    , v2_( rhs.v2_ )
{
}


Pair::Pair( double v1, double v2 )
    : v1_( v1 )
    , v2_( v2 )
{
}


Pair& Pair::operator =( const Pair& rhs )
{
    v1_ = rhs.v1_;
    v2_ = rhs.v2_;
    return ( *this );
}


void Pair::Init( double v1, double v2 )
{
    v1_ = v1;
    v2_ = v2;
}


void Pair::operator +=( const Pair& rhs )
{
    v1_ += rhs.v1_;
    v2_ += rhs.v2_;
}


void Pair::operator -=( const Pair& rhs )
{
    v1_ -= rhs.v1_;
    v2_ -= rhs.v2_;
}


bool Pair::operator ==( const Pair& rhs ) const
{
    return ( EQ(v1_, rhs.v1_) && EQ(v2_, rhs.v2_) );
}


bool Pair::operator !=( const Pair& rhs ) const
{
    return ( !((*this) == rhs) );
}


bool Pair::operator <( const Pair& rhs ) const
{
    return LT( v2_, v1_, rhs.v2_, rhs.v1_ );
}


std::ostream& operator <<( std::ostream& os, const Pair& rhs )
{
    os << "(" << rhs.v1_ << ", " << rhs.v2_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// I N T E R V A L   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Interval::Interval()
{
}


Interval::Interval( const Interval& rhs )
: begin_( rhs.begin_ )
, end_( rhs.end_ )
{
}


Interval::Interval( double begin, double end )
: begin_( begin )
, end_( end )
{
}


Interval& Interval::operator =( const Interval& rhs )
{
    begin_ = rhs.begin_;
    end_ = rhs.end_;
    return ( *this );
}


void Interval::Init( double begin, double end )
{
    begin_ = begin;
    end_ = end;
}


void Interval::Widen( double beginDelta, double endDelta )
{
    begin_ -= beginDelta;
    end_ += endDelta;
}


void Interval::WidenOnNormalizedValue( double beginNormalizedDelta, double endNormalizedDelta )
{
    double length = CalcLength();
    begin_ -= length * beginNormalizedDelta;
    end_ += length * endNormalizedDelta;
}


double Interval::CalcMiddle() const
{
    assert( LE(begin_, end_) );
    return ( begin_ + (end_ - begin_) / 2.0 );
}


double Interval::CalcLength() const
{
    assert( LE(begin_, end_) );
    return ( end_ - begin_ );
}


double Interval::CalcValueByNormalizedValue( double notmalizedValue ) const
{
    assert( LE(begin_, end_) );
    return ( begin_ + CalcLength() * notmalizedValue );
}


double Interval::CalcNormalizedValue( double value ) const
{
    assert( LE(begin_, end_) );
    return ( (value - begin_) / CalcLength() );
}


bool Interval::IsInclude( double value ) const
{
    assert( LE(begin_, end_) );
    return IsInsideClosedInterval( value, begin_, end_ );
}


bool Interval::operator ==( const Interval& rhs ) const
{
    return ( EQ(begin_, rhs.begin_) && EQ(end_, rhs.end_) );
}


bool Interval::operator !=( const Interval& rhs ) const
{
    return ( !((*this) == rhs) );
}


std::ostream& operator <<( std::ostream& os, const Interval& rhs )
{
    os << "[" << rhs.begin_ << "; " << rhs.end_ << "]";
    return os;
}


//-------------------------------------------------------------------------------------------------
// P O I N T   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Point::Point()
{
}


Point::Point( const Point& rhs )
    : x_( rhs.x_ )
    , y_( rhs.y_ )
{
}


Point::Point( double x, double y )
    : x_( x )
    , y_( y )
{
}


Point& Point::operator =( const Point& rhs )
{
    x_ = rhs.x_;
    y_ = rhs.y_;
    return ( *this );
}


void Point::Zero()
{
    ( *this ) = ZERO_POINT;
}


void Point::Init( double x, double y )
{
    x_ = x;
    y_ = y;
}


void Point::Transfer( const Vector& shift )
{
    x_ += shift.x_;
    y_ += shift.y_;
}


void Point::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    if ( pCenter == 0 ) {
        x_ *= xCoef;
        y_ *= yCoef;

    } else {
        x_ = ( x_ - pCenter->x_ ) * xCoef + pCenter->x_;
        y_ = ( y_ - pCenter->y_ ) * yCoef + pCenter->y_;
    }
}


void Point::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    double angleSin = ::sin( angle );
    double angleCos = ::cos( angle );

    if ( pCenter == 0 ) {
        double x = x_ * angleCos - y_ * angleSin;
        double y = x_ * angleSin + y_ * angleCos;
        x_ = x;
        y_ = y;

    } else {
        double x = x_ - pCenter->x_;
        double y = y_ - pCenter->y_;
        x_ = ( x * angleCos - y * angleSin ) + pCenter->x_;
        y_ = ( x * angleSin + y * angleCos ) + pCenter->y_;
    }
}


void Point::Transform( const Matrix3x3& m )
{
    double oldX = x_;
    double oldY = y_;
    x_ = oldX * m.m_[ 0 ][ 0 ] + oldY * m.m_[ 1 ][ 0 ] + m.m_[ 2 ][ 0 ];
    y_ = oldX * m.m_[ 0 ][ 1 ] + oldY * m.m_[ 1 ][ 1 ] + m.m_[ 2 ][ 1 ];
}


void Point::operator +=( const Point& rhs )
{
    x_ += rhs.x_;
    y_ += rhs.y_;
}


void Point::operator -=( const Point& rhs )
{
    x_ -= rhs.x_;
    y_ -= rhs.y_;
}


void Point::operator *=( double coef )
{
    x_ *= coef;
    y_ *= coef;
}


bool Point::operator ==( const Point& rhs ) const
{
    return ( (EQ(x_, rhs.x_)) && (EQ(y_, rhs.y_)) );
}


bool Point::operator !=( const Point& rhs ) const
{
    return ( !((*this) == rhs) );
}


bool Point::operator <( const Point& rhs ) const
{
    return LT( y_, x_, rhs.y_, rhs.x_ );
}


std::ostream& operator <<( std::ostream& os, const Point& rhs )
{
    os << "(" << rhs.x_ << ", " << rhs.y_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// V E C T O R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Vector::Vector()
{
}


Vector::Vector( const Vector& rhs )
    : x_( rhs.x_ )
    , y_( rhs.y_ )
{
}


Vector::Vector( double x, double y )
    : x_( x )
    , y_( y )
{
}


/*++
    Construction of vector from->to.
--*/
Vector::Vector( const Point& from, const Point& to )
    : x_( to.x_ - from.x_ )
    , y_( to.y_ - from.y_ )
{
}


Vector& Vector::operator =( const Vector& rhs )
{
    x_ = rhs.x_;
    y_ = rhs.y_;
    return ( *this );
}


void Vector::Zero()
{
    ( *this ) = ZERO_VECTOR;
}


void Vector::Init( double x, double y )
{
    x_ = x;
    y_ = y;
}


void Vector::Normalize()
{
    double length = CalcLength();
    if ( NENil(length) ) {
        double lengthInv = 1.0 / length;
        x_ *= lengthInv;
        y_ *= lengthInv;
    }
}


void Vector::Invert()
{
    x_ *= -1.0;
    y_ *= -1.0;
}


void Vector::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    if ( pCenter != 0 ) {
        x_ -= pCenter->x_;
        y_ -= pCenter->y_;
    }

    x_ *= xCoef;
    y_ *= yCoef;

    if ( pCenter != 0 ) {
        x_ += pCenter->x_;
        y_ += pCenter->y_;
    }
}


void Vector::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    double x = x_;
    double y = y_;

    if ( pCenter != 0 ) {
        x -= pCenter->x_;
        y -= pCenter->y_;
    }

    double angleSin = ::sin( angle );
    double angleCos = ::cos( angle );
    x_ = x * angleCos - y * angleSin;
    y_ = x * angleSin + y * angleCos;

    if ( pCenter != 0 ) {
        x_ += pCenter->x_;
        y_ += pCenter->y_;
    }
}


void Vector::Transform( const Matrix3x3& m )
{
    double oldX = x_;
    double oldY = y_;
    x_ = oldX * m.m_[ 0 ][ 0 ] + oldY * m.m_[ 1 ][ 0 ];
    y_ = oldX * m.m_[ 0 ][ 1 ] + oldY * m.m_[ 1 ][ 1 ];
}


double Vector::CalcLength() const
{
    return ( ::sqrt(x_ * x_ + y_ * y_) );
}


/*++
    Calculation of angle of the vector in global space.
--*/
double Vector::CalcAngle() const
{
    return ( ::atan2(y_, x_) );
}


/*++
    Calculation of angle between two vectors. Method return angle in interval [0, PI2] indicating
    which vector is ahead.
--*/
double Vector::CalcAngle( const Vector& v ) const
{
    return ( ::atan2(v.y_, v.x_) - ::atan2(y_, x_) );
}


/*++
    Calculation of relative angle between two vectors in interval [0, PI] ignoring vectors order
    (method is commutative).
--*/
double Vector::CalcRelativeAngle( const Vector& v ) const
{
    return ( ::acos(CalcDot(v) / (CalcLength() * v.CalcLength())) );
}


double Vector::CalcDot( const Vector& v ) const
{
    return ( (x_ * v.x_) + (y_ * v.y_) );
}


Vector Vector::CalcPerpendicular( bool direction /*= true*/ ) const
{
    return (direction) ? Vector( -y_, x_ ) : Vector( y_, -x_ );
}


bool Vector::IsZero() const
{
    return ( EQNil(x_) && EQNil(y_) );
}


bool Vector::IsNormalized() const
{
    return EQ( CalcLength(), 1.0 );
}


/*++
    Two vectors are collinear if their pseudo scalar product equals zero
    (|v1| * |v2| * sin(a) = 0). Formula can be simplified for current condition check.
--*/
bool Vector::IsCollinear( const Vector& v ) const
{
    return EQNil( ::sin(CalcAngle(v)) );
}


bool Vector::IsCodirectional( const Vector& v ) const
{
    return EQAngles( CalcAngle(), v.CalcAngle() );
}


void Vector::operator +=( const Vector& rhs )
{
    x_ += rhs.x_;
    y_ += rhs.y_;
}


void Vector::operator -=( const Vector& rhs )
{
    x_ -= rhs.x_;
    y_ -= rhs.y_;
}


void Vector::operator *=( double coef )
{
    x_ *= coef;
    y_ *= coef;
}


bool Vector::operator ==( const Vector& rhs ) const
{
    return ( (EQ(x_, rhs.x_)) && (EQ(y_, rhs.y_)) );
}


bool Vector::operator !=( const Vector& rhs ) const
{
    return ( !((*this) == rhs) );
}


std::ostream& operator <<( std::ostream& os, const Vector& rhs )
{
    os << "(" << rhs.x_ << ", " << rhs.y_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 1   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Matrix2x1::Matrix2x1()
{
}


Matrix2x1::Matrix2x1( const Matrix2x1& rhs )
{
    ( *this ) = rhs;
}


Matrix2x1::Matrix2x1( double m00, double m10 )
{
    Init( m00, m10 );
}


Matrix2x1& Matrix2x1::operator =( const Matrix2x1& rhs )
{
    ::memcpy( this->m_, rhs.m_, sizeof(this->m_) );
    return ( *this );
}


void Matrix2x1::Init( double m00, double m10 )
{
    m_[ 0 ][ 0 ] = m00;
    m_[ 1 ][ 0 ] = m10;
}


void Matrix2x1::InitZeroMatrix()
{
    ( *this ) = ZERO_MATRIX_2X1;
}


void Matrix2x1::InitIdentityMatrix()
{
    ( *this ) = IDENTITY_MATRIX_2X1;
}


void Matrix2x1::operator +=( const Matrix2x1& rhs )
{
    m_[ 0 ][ 0 ] += rhs.m_[ 0 ][ 0 ];
    m_[ 1 ][ 0 ] += rhs.m_[ 1 ][ 0 ];
}


void Matrix2x1::operator -=( const Matrix2x1& rhs )
{
    m_[ 0 ][ 0 ] -= rhs.m_[ 0 ][ 0 ];
    m_[ 1 ][ 0 ] -= rhs.m_[ 1 ][ 0 ];
}


void Matrix2x1::operator *=( double coef )
{
    m_[ 0 ][ 0 ] *= coef;
    m_[ 1 ][ 0 ] *= coef;
}


std::ostream& operator <<( std::ostream& os, const Matrix2x1& rhs )
{
    os << "((" << rhs.m_[ 0 ][ 0 ] << "), (" << rhs.m_[ 1 ][ 0 ] << "))";
    return os;
}


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 2   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Matrix2x2::Matrix2x2()
{
}


Matrix2x2::Matrix2x2( const Matrix2x2& rhs )
{
    ( *this ) = rhs;
}


Matrix2x2::Matrix2x2( double m00, double m01, double m10, double m11 )
{
    Init( m00, m01, m10, m11 );
}


Matrix2x2& Matrix2x2::operator =( const Matrix2x2& rhs )
{
    ::memcpy( this->m_, rhs.m_, sizeof(this->m_) );
    return ( *this );
}


void Matrix2x2::Init( double m00, double m01, double m10, double m11 )
{
    m_[ 0 ][ 0 ] = m00;
    m_[ 0 ][ 1 ] = m01;
    m_[ 1 ][ 0 ] = m10;
    m_[ 1 ][ 1 ] = m11;
}


void Matrix2x2::InitZeroMatrix()
{
    ( *this ) = ZERO_MATRIX_2X2;
}


void Matrix2x2::InitIdentityMatrix()
{
    ( *this ) = IDENTITY_MATRIX_2X2;
}


void Matrix2x2::Invert()
{
    double det = m_[0][0] * m_[1][1] - m_[0][1] * m_[1][0];

    if ( NENil(det) ) {

        double detInv = 1.0 / det;

        Matrix2x2 result;

        result.m_[ 0 ][ 0 ] = m_[ 1 ][ 1 ] * detInv;
        result.m_[ 0 ][ 1 ] = m_[ 0 ][ 1 ] * -detInv;
        result.m_[ 1 ][ 0 ] = m_[ 1 ][ 0 ] * -detInv;
        result.m_[ 1 ][ 1 ] = m_[ 0 ][ 0 ] * detInv;

        ( *this ) = result;
    }
}


void Matrix2x2::operator +=( const Matrix2x2& rhs )
{
    m_[ 0 ][ 0 ] += rhs.m_[ 0 ][ 0 ];
    m_[ 0 ][ 1 ] += rhs.m_[ 0 ][ 1 ];
    m_[ 1 ][ 0 ] += rhs.m_[ 1 ][ 0 ];
    m_[ 1 ][ 1 ] += rhs.m_[ 1 ][ 1 ];
}


void Matrix2x2::operator -=( const Matrix2x2& rhs )
{
    m_[ 0 ][ 0 ] -= rhs.m_[ 0 ][ 0 ];
    m_[ 0 ][ 1 ] -= rhs.m_[ 0 ][ 1 ];
    m_[ 1 ][ 0 ] -= rhs.m_[ 1 ][ 0 ];
    m_[ 1 ][ 1 ] -= rhs.m_[ 1 ][ 1 ];
}


void Matrix2x2::operator *=( double coef )
{
    m_[ 0 ][ 0 ] *= coef;
    m_[ 0 ][ 1 ] *= coef;
    m_[ 1 ][ 0 ] *= coef;
    m_[ 1 ][ 1 ] *= coef;
}


void Matrix2x2::operator *=( const Matrix2x2& rhs )
{
    Matrix2x2 result;

    result.m_[ 0 ][ 0 ] = ( m_[0][0] * rhs.m_[0][0] + m_[0][1] * rhs.m_[1][0] );
    result.m_[ 0 ][ 1 ] = ( m_[0][0] * rhs.m_[0][1] + m_[0][1] * rhs.m_[1][1] );
    result.m_[ 1 ][ 0 ] = ( m_[1][0] * rhs.m_[0][0] + m_[1][1] * rhs.m_[1][0] );
    result.m_[ 1 ][ 1 ] = ( m_[1][0] * rhs.m_[0][1] + m_[1][1] * rhs.m_[1][1] );

    ( *this ) = result;
}


Matrix2x1 Matrix2x2::operator *( const Matrix2x1& rhs )
{
    Matrix2x1 result;

    result.m_[ 0 ][ 0 ] = ( m_[0][0] * rhs.m_[0][0] + m_[0][1] * rhs.m_[1][0] );
    result.m_[ 1 ][ 0 ] = ( m_[1][0] * rhs.m_[0][0] + m_[1][1] * rhs.m_[1][0] );

    return result;
}


std::ostream& operator <<( std::ostream& os, const Matrix2x2& rhs )
{
    os << "((" << rhs.m_[ 0 ][ 0 ] << ", " << rhs.m_[ 0 ][ 1 ] << "), (" << rhs.m_[ 1 ][ 0 ] <<
        ", " << rhs.m_[ 1 ][ 1 ] << "))";
    return os;
}


//-------------------------------------------------------------------------------------------------
// M A T R I X 3 X 3   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Matrix3x3::Matrix3x3()
{
}


Matrix3x3::Matrix3x3( const Matrix3x3& rhs )
{
    ( *this ) = rhs;
}


Matrix3x3::Matrix3x3( double m00, double m01, double m02,
    double m10, double m11, double m12,
    double m20, double m21, double m22 )
{
    Init( m00, m01, m02,
        m10, m11, m12,
        m20, m21, m22 );
}


Matrix3x3& Matrix3x3::operator =( const Matrix3x3& rhs )
{
    ::memcpy( this->m_, rhs.m_, sizeof(this->m_) );
    return ( *this );
}


void Matrix3x3::Init( double m00, double m01, double m02,
    double m10, double m11, double m12,
    double m20, double m21, double m22 )
{
    ( m_[0][0] = m00 ); ( m_[0][1] = m01 ); ( m_[0][2] = m02 );
    ( m_[1][0] = m10 ); ( m_[1][1] = m11 ); ( m_[1][2] = m12 );
    ( m_[2][0] = m20 ); ( m_[2][1] = m21 ); ( m_[2][2] = m22 );
}


void Matrix3x3::InitZeroMatrix()
{
    ( *this ) = ZERO_MATRIX_3X3;
}


void Matrix3x3::InitIdentityMatrix()
{
    ( *this ) = IDENTITY_MATRIX_3X3;
}


void Matrix3x3::InitTransferMatrix( const Vector& shift )
{
    Init( 1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        shift.x_, shift.y_, 1.0 );
}


void Matrix3x3::InitInvertedTransferMatrix( const Vector& shift )
{
    Init( 1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        -shift.x_, -shift.y_, 1.0 );
}


void Matrix3x3::InitScaleMatrix( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    if ( pCenter == 0 ) {
        Init( xCoef, 0.0, 0.0,
            0.0, yCoef, 0.0,
            0.0, 0.0, 1.0 );

    } else {
        InitTransferMatrix( Vector(*pCenter, ZERO_POINT) );
        Matrix3x3 mtemp;
        mtemp.Init( xCoef, 0.0, 0.0,
            0.0, yCoef, 0.0,
            0.0, 0.0, 1.0 );
        ( *this ) *= mtemp;
        mtemp.InitTransferMatrix( Vector(ZERO_POINT, *pCenter) );
        ( *this ) *= mtemp;
    }
}


void Matrix3x3::InitRotationMatrix( double angle, const Point* pCenter /*= 0*/ )
{
    double costh = ::cos( angle );
    double sinth = ::sin( angle );

    if ( pCenter == 0 ) {
        Init( costh, -sinth, 0.0,
            sinth, costh, 0.0,
            0.0, 0.0, 1.0 );

    } else {
        InitTransferMatrix( Vector(*pCenter, ZERO_POINT) );
        Matrix3x3 mtemp;
        mtemp.Init( costh, -sinth, 0.0,
            sinth, costh, 0.0,
            0.0, 0.0, 1.0 );
        ( *this ) *= mtemp;
        mtemp.InitTransferMatrix( Vector(ZERO_POINT, *pCenter) );
        ( *this ) *= mtemp;
    }
}


void Matrix3x3::Invert()
{
    double det = m_[ 0 ][ 0 ] * ( m_[1][1] * m_[2][2] - m_[2][1] * m_[1][2] ) -
        m_[ 0 ][ 1 ] * ( m_[1][0] * m_[2][2] - m_[2][0] * m_[1][2] ) +
        m_[ 0 ][ 2 ] * ( m_[1][0] * m_[2][1] - m_[2][0] * m_[1][1] );

    if ( NENil(det) ) {

        double detInv = 1.0 / det;

        Matrix3x3 result;

        result.m_[ 0 ][ 0 ] = ( m_[1][1] * m_[2][2] - m_[2][1] * m_[1][2] ) * detInv;
        result.m_[ 1 ][ 0 ] = ( m_[1][0] * m_[2][2] - m_[2][0] * m_[1][2] ) * -detInv;
        result.m_[ 2 ][ 0 ] = ( m_[1][0] * m_[2][1] - m_[2][0] * m_[1][1] ) * detInv;

        result.m_[ 0 ][ 1 ] = ( m_[0][1] * m_[2][2] - m_[2][1] * m_[0][2] ) * -detInv;
        result.m_[ 1 ][ 1 ] = ( m_[0][0] * m_[2][2] - m_[2][0] * m_[0][2] ) * detInv;
        result.m_[ 2 ][ 1 ] = ( m_[0][0] * m_[2][1] - m_[2][0] * m_[0][1] ) * -detInv;

        result.m_[ 0 ][ 2 ] = ( m_[0][1] * m_[1][2] - m_[1][1] * m_[0][2] ) * detInv;
        result.m_[ 1 ][ 2 ] = ( m_[0][0] * m_[1][2] - m_[1][0] * m_[0][2] ) * -detInv;
        result.m_[ 2 ][ 2 ] = ( m_[0][0] * m_[1][1] - m_[1][0] * m_[0][1] ) * detInv;

        ( *this ) = result;
    }
}


void Matrix3x3::operator +=( const Matrix3x3& rhs )
{
    for ( int nRow = 0; nRow < M; ++nRow ) {
        for ( int nColumn = 0; nColumn < N; ++nColumn ) {
            m_[ nRow ][ nColumn ] += rhs.m_[ nRow ][ nColumn ];
        }
    }
}


void Matrix3x3::operator -=( const Matrix3x3& rhs )
{
    for ( int nRow = 0; nRow < M; ++nRow ) {
        for ( int nColumn = 0; nColumn < N; ++nColumn ) {
            m_[ nRow ][ nColumn ] -= rhs.m_[ nRow ][ nColumn ];
        }
    }
}


void Matrix3x3::operator *=( double coef )
{
    for ( int nRow = 0; nRow < M; ++nRow ) {
        for ( int nColumn = 0; nColumn < N; ++nColumn ) {
            m_[ nRow ][ nColumn ] *= coef;
        }
    }
}


void Matrix3x3::operator *=( const Matrix3x3& rhs )
{
    Matrix3x3 result;

    for ( int nRow = 0; nRow < M; ++nRow ) {
        for ( int nColumn = 0; nColumn < N; ++nColumn ) {

            double tempSum = 0;
            for ( int nIndex = 0; nIndex < N; ++nIndex ) {
                tempSum += ( m_[nRow][nIndex] * rhs.m_[nIndex][nColumn] );
            }
            result.m_[ nRow ][ nColumn ] = tempSum;
        }
    }

    ( *this ) = result;
}


std::ostream& operator <<( std::ostream& os, const Matrix3x3& rhs )
{
    os << "(";
    for ( int nRow = 0; nRow < rhs.M; ++nRow ) {
        os << "(" << rhs.m_[ nRow ][ 0 ] << ", " << rhs.m_[ nRow ][ 1 ] << ", " <<
            rhs.m_[ nRow ][ 2 ] << ")";
        if ( nRow != 2 ) {
            os << ", ";
        }
    }
    os << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// C S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


CS::CS()
{
    InitDefaultCS();
}


CS::CS( const CS& rhs )
    : pos_( rhs.pos_ )
    , axisX_( rhs.axisX_ )
    , axisY_( rhs.axisY_ )
{
}


CS::CS( const Point& pos, double angle /*= 0.0*/ )
{
    Init( pos, angle );
}


CS& CS::operator =( const CS& rhs )
{
    pos_ = rhs.pos_;
    axisX_ = rhs.axisX_;
    axisY_ = rhs.axisY_;
    return ( *this );
}


void CS::InitDefaultCS()
{
    pos_.Zero();
    axisX_.Init( 1.0f, 0.0f );
    axisY_.Init( 0.0f, 1.0f );
}


void CS::Init( const Point& pos, const Vector& axisX, const Vector& axisY )
{
    assert( EQ(axisX.CalcLength(), 1.0) );
    assert( EQ(axisY.CalcLength(), 1.0) );
    assert( EQNil(axisX.CalcDot(axisY)) );

    pos_ = pos;
    axisX_ = axisX;
    axisY_ = axisY;
}


void CS::Init( const Point& pos, double angle /*= 0.0*/ )
{
    pos_ = pos;
    axisX_.Init( ::cos(angle), ::sin(angle) );
    axisY_ = axisX_.CalcPerpendicular();
}


void CS::Transfer( const Vector& shift )
{
    pos_.Transfer( shift );
}


void CS::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    pos_.Scale( xCoef, yCoef, pCenter );
}


void CS::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    pos_.Rotate( angle, pCenter );
    //axisX_.Rotate( angle, pCenter );
    //axisY_.Rotate( angle, pCenter );
    axisX_.Rotate( angle );
    axisY_.Rotate( angle );
}


/*++
    Implementation of coordinate system transform by matrix.
    Notice that this method unable to transform coordinate system by scale matrix with different
    x and y axis scale factors correctly!
--*/
void CS::Transform( const Matrix3x3& m )
{
    pos_.Transform( m );
    axisX_.Transform( m );
    axisY_.Transform( m );
    axisX_.Normalize();
    axisY_.Normalize();

    assert( EQNil(axisX_.CalcDot(axisY_)) );
}


void CS::CalcMatrixFromCS( Matrix3x3& m ) const
{
    m.Init( axisX_.x_, axisX_.y_, 0.0,
        axisY_.x_, axisY_.y_, 0.0,
        pos_.x_, pos_.y_, 1.0 );
}


void CS::CalcMatrixIntoCS( Matrix3x3& m ) const
{
    m.Init( axisX_.x_, axisX_.y_, 0.0,
        axisY_.x_, axisY_.y_, 0.0,
        pos_.x_, pos_.y_, 1.0 );
    m.Invert();
}


bool CS::operator ==( const CS& rhs ) const
{
    return ( (pos_ == rhs.pos_) && (axisX_ == rhs.axisX_) && (axisY_ == rhs.axisY_) );
}


bool CS::operator !=( const CS& rhs ) const
{
    return ( !((*this) == rhs) );
}


std::ostream& operator <<( std::ostream& os, const CS& rhs )
{
    os << "(" << rhs.pos_ << ", x is " << rhs.axisX_ << ", x is " << rhs.axisY_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// R E C T   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Rect::Rect()
{
}


Rect::Rect( const Rect& rhs )
    : minPoint_( rhs.minPoint_ )
    , maxPoint_( rhs.maxPoint_ )
{
}


Rect::Rect( const Point& p1, const Point& p2 )
    : minPoint_( CalcMin(p1.x_, p2.x_), CalcMin(p1.y_, p2.y_) )
    , maxPoint_( CalcMax(p1.x_, p2.x_), CalcMax(p1.y_, p2.y_) )
{
}


Rect::Rect( double x1, double y1, double x2, double y2 )
    : minPoint_( CalcMin(x1, x2), CalcMin(y1, y2) )
    , maxPoint_( CalcMax(x1, x2), CalcMax(y1, y2) )
{
}


Rect::Rect( double width, double height )
    : minPoint_( CalcMin(width, 0.0), CalcMin(height, 0.0) )
    , maxPoint_( CalcMax(width, 0.0), CalcMax(height, 0.0) )
{
}


Rect& Rect::operator =( const Rect& rhs )
{
    minPoint_ = rhs.minPoint_;
    maxPoint_ = rhs.maxPoint_;
    return ( *this );
}


void Rect::Init( double x1, double y1, double x2, double y2 )
{
    minPoint_.Init( CalcMin(x1, x2), CalcMin(y1, y2) );
    maxPoint_.Init( CalcMax(x1, x2), CalcMax(y1, y2) );
}


void Rect::Init( double width, double height )
{
    Init( 0.0, 0.0, width, height );
}


void Rect::Transfer( const Vector& shift )
{
    minPoint_.Transfer( shift );
    maxPoint_.Transfer( shift );
}


void Rect::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    minPoint_.Scale( xCoef, yCoef, pCenter );
    maxPoint_.Scale( xCoef, yCoef, pCenter );
}


/*++
    Inflate rects both left and right sides by dWidth and both top and lower sides by dheight.
--*/
void Rect::Inflate( double dWidth, double dHeight )
{
    double x1 = minPoint_.x_ - dWidth;
    double y1 = minPoint_.y_ - dHeight;
    double x2 = maxPoint_.x_ + dWidth;
    double y2 = maxPoint_.y_ + dHeight;

    minPoint_.Init( CalcMin(x1, x2), CalcMin(y1, y2) );
    maxPoint_.Init( CalcMax(x1, x2), CalcMax(y1, y2) );
}


/*++
    Deflate rects both left and right sides by dWidth and both top and lower sides by dheight.
--*/
void Rect::Deflate( double dWidth, double dHeight )
{
    Inflate( -dWidth, -dHeight );
} 


double Rect::CalcWidth() const
{
    return ( maxPoint_.x_ - minPoint_.x_ );
}


double Rect::CalcHeight() const
{
    return ( maxPoint_.y_ - minPoint_.y_ );
}


double Rect::CalcArea() const
{
    return ( CalcWidth() * CalcHeight() );
}


double Rect::CalcAspectRatio() const
{
    return ( CalcWidth() / CalcHeight() );
}


Point Rect::CalcCenter() const
{
    return Point( minPoint_.x_ + CalcWidth() / 2.0, minPoint_.y_ + CalcHeight() / 2.0 );
}


bool Rect::IsInclude( const Point& rhs ) const
{
    return ( (rhs.x_ > minPoint_.x_) && (rhs.x_ < maxPoint_.x_) &&
        (rhs.y_ > minPoint_.y_) && (rhs.y_ < maxPoint_.y_) );
}


bool Rect::operator ==( const Rect& rhs ) const
{
    return ( (minPoint_ == rhs.minPoint_) && (maxPoint_ == rhs.maxPoint_) );
}


bool Rect::operator !=( const Rect& rhs ) const
{
    return ( !((*this) == rhs) );
}


std::ostream& operator <<( std::ostream& os, const Rect& rhs )
{
    os << "(" << rhs.minPoint_ << "-" << rhs.maxPoint_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// B O U N D I N G   B O X   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


BoundingBox::BoundingBox()
{
    SetEmpty();
}


BoundingBox::BoundingBox( const BoundingBox& rhs )
    : rect_( rhs.rect_ )
{
}


BoundingBox& BoundingBox::operator =( const BoundingBox& rhs )
{
    rect_ = rhs.rect_;
    return ( *this );
}


void BoundingBox::SetEmpty()
{
    rect_.minPoint_.Init( 1.0, 1.0 );
    rect_.maxPoint_.Init( -1.0, -1.0 );
}


void BoundingBox::Add( const BoundingBox& rhs )
{
    Add( rhs.rect_.minPoint_ );
    Add( rhs.rect_.maxPoint_ );
}


void BoundingBox::Add( const Point& rhs )
{
    if ( (rect_.minPoint_.x_ > rect_.maxPoint_.x_) || (rect_.minPoint_.y_ > rect_.maxPoint_.y_) ) {
        rect_.minPoint_ = rhs;
        rect_.maxPoint_ = rhs;
    }

    if ( rhs.x_ < rect_.minPoint_.x_ ) {
        rect_.minPoint_.x_ = rhs.x_;
    }

    if ( rhs.y_ < rect_.minPoint_.y_ ) {
        rect_.minPoint_.y_ = rhs.y_;
    }

    if ( rhs.x_ > rect_.maxPoint_.x_ ) {
        rect_.maxPoint_.x_ = rhs.x_;
    }

    if ( rhs.y_ > rect_.maxPoint_.y_ ) {
        rect_.maxPoint_.y_ = rhs.y_;
    }
}


void BoundingBox::Transfer( const Vector& shift )
{
    rect_.Transfer( shift );
}


void BoundingBox::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    rect_.Scale( xCoef, yCoef, pCenter );
}


void BoundingBox::Inflate( double dWidth, double dHeight )
{
    rect_.Inflate( dWidth, dHeight );
}


void BoundingBox::Deflate( double dWidth, double dHeight )
{
    rect_.Deflate( dWidth, dHeight );
}


double BoundingBox::CalcWidth() const
{
    return rect_.CalcWidth();
}


double BoundingBox::CalcHeight() const
{
    return rect_.CalcHeight();
}


double BoundingBox::CalcArea() const
{
    return rect_.CalcArea();
}


double BoundingBox::CalcAspectRatio() const
{
    return rect_.CalcAspectRatio();
}


Point BoundingBox::CalcCenter() const
{
    return rect_.CalcCenter();
}


bool BoundingBox::IsEmpty() const
{
    return ( (rect_.minPoint_.x_ > rect_.maxPoint_.x_) ||
        (rect_.minPoint_.y_ > rect_.maxPoint_.y_) );
}


const Point& BoundingBox::GetMinPoint() const
{
    return rect_.minPoint_;
}


const Point& BoundingBox::GetMaxPoint() const
{
    return rect_.maxPoint_;
}


bool BoundingBox::operator ==( const BoundingBox& rhs ) const
{
    return ( rect_ != rhs.rect_ );
}


bool BoundingBox::operator !=( const BoundingBox& rhs ) const
{
    return ( rect_ != rhs.rect_ );
}


BoundingBox& operator <<( BoundingBox& lhs, const BoundingBox& rhs )
{
    lhs.Add( rhs );
    return lhs;
}


BoundingBox& operator <<( BoundingBox& lhs, const Point& rhs )
{
    lhs.Add( rhs );
    return lhs;
}


//-------------------------------------------------------------------------------------------------
// F A C T O R I A L   C A L C U L A T O R
//-------------------------------------------------------------------------------------------------


FactorialCalculator::FactorialCalculator( int precalcValuesCount /*= 100*/ )
: precalcValuesCount_( precalcValuesCount )
, pPrecalcValues_( new double [precalcValuesCount] )
{
    assert( precalcValuesCount >  0 );
    pPrecalcValues_[ 0 ] = 1.0;
    for ( int i = 1; i < precalcValuesCount_; ++i ) {
        pPrecalcValues_[ i ] = pPrecalcValues_[ i - 1 ] * i;
    }
}


FactorialCalculator::~FactorialCalculator()
{
    delete [] pPrecalcValues_;
}


double FactorialCalculator::Calc( int value ) const
{
    assert( value >= 0 );
    if ( value < precalcValuesCount_ ) {
        return pPrecalcValues_[ value ];
    } else {
        double result = pPrecalcValues_[ precalcValuesCount_ - 1 ];
        for ( int i = precalcValuesCount_; i <= value; ++i ) {
            result *= i;
        }
        return result;
    }
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


/*---*/

/*---*/
/*---*/


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Point Curve::CalcCharacterPoint( ECurveCharacterPoints characterPoint ) const
{
    switch ( characterPoint ) {
    case CHP_BEGIN:
        return CalcPointOnCurve( GetParametricInterval().begin_ );
    case CHP_MIDDLE:
        return CalcPointOnCurve( GetParametricInterval().CalcMiddle() );
    case CHP_END:
        return CalcPointOnCurve( GetParametricInterval().end_ );
    default:
        return Point();
    }
}


Vector Curve::CalcNormal( double t, bool direction /*= true*/ ) const
{
    Vector normal = CalcFirstDerivative( t ).CalcPerpendicular( direction );
    normal.Normalize();
    return normal;
}


double Curve::CalcDistanceToPoint( const Point& p ) const
{
    double t = CalcPointProjection( p );
    Point pointOnCurve = CalcPointOnCurve( t );
    return ( CalcDistance(p, pointOnCurve) );
}


double Curve::CalcPointProjection( const Point& /*p*/ ) const
{
    // stub
    assert( 0 );
    return 0.0;
}


/*++
    Calculation of metric length, generic algorithm.
--*/
double Curve::CalcLength() const
{
    const int PARAMETRIC_STEPS_COUNT = 20;
    const Interval& interval = GetParametricInterval();
    double tStep = ( interval.CalcLength() / PARAMETRIC_STEPS_COUNT );
    double length = 0.0;

    Point previousPoint = CalcPointOnCurve( interval.begin_ );
    for ( int i = 1; i <= PARAMETRIC_STEPS_COUNT; ++i ) {
        Point currentPoint = CalcPointOnCurve( interval.begin_ + i * tStep );
        length += CalcDistance( previousPoint, currentPoint );
        previousPoint = currentPoint;
    }

    return length;
}


/*++
    Calculation of bounding box, generic algorithm.
--*/
void Curve::CalcBoundingBox( BoundingBox& box ) const
{
    const int PARAMETRIC_STEPS_COUNT = 20;
    const Interval& interval = GetParametricInterval();
    double tStep = ( interval.CalcLength() / PARAMETRIC_STEPS_COUNT );

    for ( int i = 0; i <= PARAMETRIC_STEPS_COUNT; ++i ) {
        box.Add( CalcPointOnCurve(interval.begin_ + i * tStep) );
    }
}


/*++
    Calculation of curvature at specified point.
    c = (x'y" - y'x") / ((x'^2 + y'^2)^(3/2))
--*/
double Curve::CalcCurvature( double t ) const
{
    Vector d1 = CalcFirstDerivative( t );
    Vector d2 = CalcSecondDerivative( t );

    if ( !d2.IsZero() ) {
        return ( (d1.x_ * d2.y_ - d1.y_ * d2.x_) / ::pow((d1.x_ * d1.x_ + d1.y_ * d1.y_), 1.5) );
    } else {
        return 0.0;
    }
}


/*++
    Calculation of tilt parametric step - parametric step through curve, which fulfil condition
    that tangent direction of curve in point (t + step) differs from tangent direction of curve in
    point t not more than on angle da. span parameter represents limiting parametric interval and
    cut parameter represents fact of tilt step truncation.
--*/
double Curve::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    assert( GT(da, 0.0) );

    static const double MIN_DT = 0.001;
    static const double DEFAULT_DT_COEF = 0.01;
    double defaultDt = span.CalcLength() * DEFAULT_DT_COEF;

    double tEnd = t;
    Vector dStart = CalcFirstDerivative( t );
    Vector dEnd = CalcFirstDerivative( tEnd + defaultDt );

    if ( ::fabs(dStart.CalcRelativeAngle(dEnd)) < da ) {
        // general case
        while ( ::fabs(dStart.CalcRelativeAngle(dEnd)) < da ) {
            tEnd += defaultDt;
            if ( GE(tEnd, span.end_) ) {
                tEnd = span.end_;
                cut = true;
                break;
            }
            dEnd = CalcFirstDerivative( tEnd + defaultDt );
        }

    } else {
        // special case when even minimal step along curve (defaultDt) is too big to satisfy
        // condition about maximum tangent deviation angle
        double dt = defaultDt;
        do {
            dt /= 2.0;
            dEnd = CalcFirstDerivative( t + dt );
            if ( dt < MIN_DT ) {
                dt = MIN_DT;
                break;
            }
        } while ( ::fabs(dStart.CalcRelativeAngle(dEnd)) > da );
        tEnd = t + dt;
        if ( GE(tEnd, span.end_) ) {
            tEnd = span.end_;
            cut = true;
        }
    }

    return ( (t < tEnd) ? (tEnd - t) : 0.0 );
}


//-------------------------------------------------------------------------------------------------
// A N A L Y T I C A L   C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


AnalyticalCurve::AnalyticalCurve()
{
}


AnalyticalCurve::AnalyticalCurve( const AnalyticalCurve& rhs )
    : cs_( rhs.cs_ )
{
}


AnalyticalCurve::AnalyticalCurve( const CS& cs )
    : cs_( cs )
{
}


AnalyticalCurve::AnalyticalCurve( const Point& pos, double angle /*= 0.0*/ )
    : cs_( pos, angle )
{
}


AnalyticalCurve& AnalyticalCurve::operator =( const AnalyticalCurve& rhs )
{
    if ( &rhs != this ) {
        cs_ = rhs.cs_;
    }
    return ( *this );
}


const CS& AnalyticalCurve::GetCS() const
{
    return cs_;
}


void AnalyticalCurve::GetCS( const CS& cs )
{
    cs_ = cs;
}


const Point& AnalyticalCurve::GetCenter() const
{
    return cs_.pos_;
}


void AnalyticalCurve::SetCenter( const Point& p )
{
    cs_.pos_ = p;
}


void AnalyticalCurve::Transfer( const Vector& shift )
{
    cs_.Transfer( shift );
}


void AnalyticalCurve::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    cs_.Scale( xCoef, yCoef, pCenter );
}


void AnalyticalCurve::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    cs_.Rotate( angle, pCenter );
}


void AnalyticalCurve::Transform( const Matrix3x3& m )
{
    cs_.Transform( m );
}


//-------------------------------------------------------------------------------------------------
// S E G M E N T   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Segment::Segment()
    : p1_( ZERO_POINT )
    , p2_( ZERO_POINT )
{
}


Segment::Segment( const Point& p1, const Point& p2 )
    : p1_( p1 )
    , p2_( p2 )
{
}


Segment::Segment( const Segment& rhs )
{
    ( *this ) = rhs;
}


const Point& Segment::GetP1() const
{
    return p1_;
}


const Point& Segment::GetP2() const
{
    return p2_;
}


void Segment::SetP1( const Point& p )
{
    p1_.Init( p.x_, p.y_ );
}


void Segment::SetP2( const Point& p )
{
    p2_.Init( p.x_, p.y_ );
}


Segment& Segment::operator =( const Segment& rhs )
{
    if ( &rhs != this ) {
        p1_ = rhs.p1_;
        p2_ = rhs.p2_;
    }
    return ( *this );
}


void Segment::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


ECurveTypes Segment::GetType() const
{
    return CT_SEGMENT;
}


const Interval& Segment::GetParametricInterval() const
{
    return INTERVAL;
}


/*++
    Calculation of the specified point on segment.
    C(t) = ((1 - t) * P1 + t * P2)
--*/
Point Segment::CalcPointOnCurve( double t ) const
{
    double x = ( (1.0 - t) * p1_.x_ + t * p2_.x_ );
    double y = ( (1.0 - t) * p1_.y_ + t * p2_.y_ );
    return ( Point(x, y) );
}


/*++
    Calculation of the first derivative of segment at specified point.
    C(t)' = P2 - P1
--*/
Vector Segment::CalcFirstDerivative( double /*t*/ ) const
{
    // C(t)' = ((1 - t) * P1 + t * P2)' =
    // = P1 * (1 - t)'  + P2 * (t)' = 
    // = P1 * (1)' + P1 * (-t)'  + P2 * (t)' =
    // = P1 * 0 + P1 * (-1) + P2 * (1) = 
    // = P2 - P1

    double dx = ( p2_.x_ - p1_.x_ );
    double dy = ( p2_.y_ - p1_.y_ );
    return ( Vector(dx, dy) );
}


/*++
    Calculation of the second derivative of segment at specified point.
    C(t)'' = 0
--*/
Vector Segment::CalcSecondDerivative( double /*t*/ ) const
{
     // C(t)'' = (P2 - P1)' = (P2)' - (P1)' = 0.0 - 0.0 = 0.0

    return ( Vector(0.0, 0.0) );
}


double Segment::CalcPointProjection( const Point& p ) const
{
    double t = INTERVAL.begin_;

    double length = CalcLength();
    if ( NENil(length) ) {
        Vector pp1( p1_, p );
        Vector p1p2( p1_, p2_ );
        t = ( pp1.CalcLength() * ::cos(pp1.CalcAngle(p1p2)) ) / length;
    }

    return t;
}


double Segment::CalcLength() const
{
    return ( CalcDistance(p1_, p2_) );
}


void Segment::CalcBoundingBox( BoundingBox& box ) const
{
    box.Add( p1_ );
    box.Add( p2_ );
}


double Segment::CalcTiltStep( double t, double /*da*/, const Interval& span, bool& cut ) const
{
    cut = true;
    if ( LE(t, span.end_) ) {
        return span.end_ - t;
    } else {
        return 0.0;
    }
}


void Segment::Transfer( const Vector& shift )
{
    p1_.Transfer( shift );
    p2_.Transfer( shift );
}


void Segment::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    p1_.Scale( xCoef, yCoef, pCenter );
    p2_.Scale( xCoef, yCoef, pCenter );
}


void Segment::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    p1_.Rotate( angle, pCenter );
    p2_.Rotate( angle, pCenter );
}


void Segment::Transform( const Matrix3x3& m )
{
    p1_.Transform( m );
    p2_.Transform( m );
}


const Interval Segment::INTERVAL( 0.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// A R C   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


/*++
    Construction of the arc/circle with center in global origin.
--*/
Arc::Arc( double radius, const Interval* pInterval /*= 0*/ )
    : AnalyticalCurve( DEFAULT_CS )
    , radius_( radius )
    , interval_( (pInterval != 0) ? (*pInterval) : (CIRCLE_INTERVAL) )
{
}


/*++
    Construction of the arc/circle with center at specified point.
--*/
Arc::Arc( double radius, const Point& center, const Interval* pInterval /*= 0*/ )
    : AnalyticalCurve( center )
    , radius_( radius )
    , interval_( (pInterval != 0) ? (*pInterval) : (CIRCLE_INTERVAL) )
{
}


/*++
    Construction of the arc/circle with specified coordinate system.
--*/
Arc::Arc( double radius, const CS& cs, const Interval* pInterval /*= 0*/ )
    : AnalyticalCurve( cs )
    , radius_( radius )
    , interval_( (pInterval != 0) ? (*pInterval) : (CIRCLE_INTERVAL) )
{
}


/*++
    Construction of the arc/circle by three points.
--*/
Arc::Arc( const Point& p1, const Point& p2, const Point& p3, bool isClosed /*= false*/ )
{
    Init( p1, p2, p3, isClosed );
}


Arc::Arc( const Arc& rhs )
    : AnalyticalCurve( rhs )
    , radius_( rhs.radius_ )
    , interval_( rhs.interval_ )
{
}


Arc& Arc::operator =( const Arc& rhs )
{
    AnalyticalCurve::operator =( rhs );
    if ( &rhs != this ) {
        radius_ = rhs.radius_;
        interval_ = rhs.interval_;
    }
    return ( *this );
}


void Arc::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


void Arc::Reset()
{
    cs_ = DEFAULT_CS;
    radius_ = 0.0;
    interval_ = ZERO_INTERVAL;
}


/*++
    Initialization of the arc/circle with center in global origin.
--*/
void Arc::Init( double radius, const Interval* pInterval /*= 0*/ )
{
    cs_ = DEFAULT_CS;
    radius_ = radius;
    interval_ = ( pInterval != 0 ) ? ( *pInterval ) : ( CIRCLE_INTERVAL );
}


/*++
    Initialization of the arc/circle with center at specified point.
--*/
void Arc::Init( double radius, const Point& center, const Interval* pInterval /*= 0*/ )
{
    cs_.Init( center );
    radius_ = radius;
    interval_ = ( pInterval != 0 ) ? ( *pInterval ) : ( CIRCLE_INTERVAL );
}


/*++
    Initialization of the arc/circle with specified coordinate system.
--*/
void Arc::Init( double radius, const CS& cs, const Interval* pInterval /*= 0*/ )
{
    cs_ = cs;
    radius_ = radius;
    interval_ = ( pInterval != 0 ) ? ( *pInterval ) : ( CIRCLE_INTERVAL );
}


/*++
    Initialization of the arc/circle by three points.
--*/
void Arc::Init( const Point& p1, const Point& p2, const Point& p3, bool isClosed /*= false*/ )
{
    // calculation of the arc center
    Point center;
    if ( CalcCenterByPoints(p1, p2, p3, center) ) {

        // calculation of the radius
        radius_ = CalcDistance( center, p1 );

        // calculation of the parametric interval
        if ( isClosed ) {
            cs_.Init( center );
            interval_ = CIRCLE_INTERVAL;

        } else {
            Vector p1Vector( center, p1 );
            Vector p3Vector( center, p3 );
            double p1Angle = NormalizeAngle( p1Vector.CalcAngle() );
            double p3Angle = NormalizeAngle( p3Vector.CalcAngle() );

            const Vector& beginVector = ( (p1Angle < p3Angle) ? (p1Vector) : (p3Vector) );
            Vector middleVector( center, p2 );
            const Vector& endVector = ( (p1Angle < p3Angle) ? (p3Vector) : (p1Vector) );

            double middleAngle = NormalizeAngle( beginVector.CalcAngle(middleVector) );
            double endAngle = NormalizeAngle( beginVector.CalcAngle(endVector) );

            if ( endAngle > middleAngle ) {
                cs_.Init( center, beginVector.CalcAngle() );
                interval_.Init( 0.0, endAngle );
            } else {
                cs_.Init( center, endVector.CalcAngle() );
                interval_.Init( 0.0, PI2 - endAngle );
            }
        }

    } else {
        Reset();
    }
}


double Arc::GetRadius() const
{
    return radius_;
}


bool Arc::IsClosed() const
{
    return GE( interval_.CalcLength(), PI2 );
}


void Arc::SetRadius( double radius )
{
    radius_ = radius;
}


void Arc::SetClosed()
{
    interval_ = CIRCLE_INTERVAL;
}


void Arc::SetSector( double start, double end )
{
    assert( LT(start, end) );
    interval_.begin_ = start;
    interval_.end_ = end;
}


ECurveTypes Arc::GetType() const
{
    return CT_ARC;
}


const Interval& Arc::GetParametricInterval() const
{
    return interval_;
}


/*++
    Calculation of the specified point on arc.
    X(t) = r * cos(t)
    Y(t) = r * sin(t)
--*/
Point Arc::CalcPointOnCurve( double t ) const
{
    Point p( radius_ * ::cos(t), radius_ * ::sin(t) );
    TransformFromCS( cs_, p );
    return p;
}


/*++
    Calculation of the first derivative of arc at specified point.
    X(t)' = r * -sin(t)
    Y(t)' = r * cos(t)
--*/
Vector Arc::CalcFirstDerivative( double t ) const
{
    Vector v( radius_ * (-::sin(t)), radius_ * ::cos(t) );
    TransformFromCS( cs_, v );
    return v;
}


/*++
    Calculation of the second derivative of arc at specified point.
    X(t)'' = r * -cos(t)
    Y(t)'' = r * -sin(t)
--*/
Vector Arc::CalcSecondDerivative( double t ) const
{
    Vector v( radius_ * (-::cos(t)), radius_ * (-::sin(t)) );
    TransformFromCS( cs_, v );
    return v;
}


double Arc::CalcLength() const
{
    return ( radius_ * interval_.CalcLength() );
}


double Arc::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    if ( GE((t + da), span.end_) ) {
        cut = true;
        double step = ( span.end_ - t );
        return ( LE(step, 0.0) ? 0.0 : step );
    } else {
        cut = false;
        return da;
    }
}


void Arc::Transfer( const Vector& shift )
{
    cs_.Transfer( shift );
}


void Arc::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    AnalyticalCurve::Scale( xCoef, yCoef, pCenter );
    radius_ = ::fabs( radius_ * xCoef );
}


void Arc::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    cs_.Rotate( angle, pCenter );
}


void Arc::Transform( const Matrix3x3& m )
{
    Point p1 = CalcPointOnCurve( GetParametricInterval().begin_ );
    Point p2 = CalcPointOnCurve( GetParametricInterval().CalcMiddle() );
    Point p3 = CalcPointOnCurve( GetParametricInterval().end_ );
    p1.Transform( m );
    p2.Transform( m );
    p3.Transform( m );
    Init( p1, p2, p3, IsClosed() );
}


/*++
    Center of the arc constructed by three points located in intersection point of the
    perpendicular bisectors of triangle, formed from these three points.
--*/
bool Arc::CalcCenterByPoints( const Point& p1, const Point& p2, const Point& p3, Point& result ) const
{
    Vector perpendicular12( Vector(p1, p2).CalcPerpendicular() );
    Vector perpendicular23( Vector(p2, p3).CalcPerpendicular() );
    Point middle12( (p1.x_ + p2.x_) / 2.0, (p1.y_ + p2.y_) / 2.0 );
    Point middle23( (p2.x_ + p3.x_) / 2.0, (p2.y_ + p3.y_) / 2.0 );
    return CalcLinesIntersectionPoint( middle12, perpendicular12, middle23, perpendicular23, result );
}


const Interval Arc::CIRCLE_INTERVAL( 0.0, PI2 );


//-------------------------------------------------------------------------------------------------
// S P I R A L   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Spiral::Spiral( const CS& cs, double radius, double angle )
    : AnalyticalCurve( cs )
    , radius_( radius )
    , interval_( 0.0, angle )
{
}


Spiral::Spiral( const Spiral& rhs )
    : AnalyticalCurve( rhs )
    , radius_( rhs.radius_ )
    , interval_( rhs.interval_ )
{
}


Spiral& Spiral::operator =( const Spiral& rhs )
{
    AnalyticalCurve::operator =( rhs );
    if ( &rhs != this ) {
        radius_ = rhs.radius_;
        interval_ = rhs.interval_;
    }
    return ( *this );
}


double Spiral::GetRadius() const
{
    return radius_;
}


void Spiral::SetRadius( double radius )
{
    assert( radius >= 0.0 );
    radius_ = radius;
}


double Spiral::GetAngle() const
{
    return interval_.end_;
}


void Spiral::SetAngle( double angle )
{
    assert( angle >= 0.0 );
    interval_.end_ = angle;
}


const Interval& Spiral::GetParametricInterval() const
{
    return interval_;
}


void Spiral::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    AnalyticalCurve::Scale( xCoef, yCoef, pCenter );
    radius_ = ::fabs( radius_ * xCoef );
}


//-------------------------------------------------------------------------------------------------
// A R C H I M E D I A N   S P I R A L   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


ArchimedianSpiral::ArchimedianSpiral( const CS& cs, double radius, double angle )
    : Spiral( cs, radius, angle )
{
}


ArchimedianSpiral::ArchimedianSpiral( const ArchimedianSpiral& rhs )
    : Spiral( rhs )
{
}


ArchimedianSpiral& ArchimedianSpiral::operator =( const ArchimedianSpiral& rhs )
{
    Spiral::operator =( rhs );
    return ( *this );
}


void ArchimedianSpiral::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


ECurveTypes ArchimedianSpiral::GetType() const
{
    return CT_ARCHIMEDIAN_SPIRAL;
}


/*++
    Calculation of the specified point on archimedian spiral.
    X(t) = r * t * cos(t)
    Y(t) = r * t * sin(t)
--*/
Point ArchimedianSpiral::CalcPointOnCurve( double t ) const
{
    Point p( radius_ * t * ::cos(t), radius_ * t * ::sin(t) );
    TransformFromCS( cs_, p );
    return p;
}


/*++
    Calculation of the first derivative of archimedian spiral at specified point.
    X(t)' = r * t * -sin(t)
    Y(t)' = r * t * cos(t)
--*/
Vector ArchimedianSpiral::CalcFirstDerivative( double t ) const
{
    Vector v( radius_ * t * (-::sin(t)), radius_ * t * ::cos(t) );
    TransformFromCS( cs_, v );
    return v;
}


/*++
    Calculation of the second derivative of archimedian spiral at specified point.
    X(t)'' = r * t * -cos(t)
    Y(t)'' = r * t * -sin(t)
--*/
Vector ArchimedianSpiral::CalcSecondDerivative( double t ) const
{
    Vector v( radius_ * t * (-::cos(t)), radius_ * t * (-::sin(t)) );
    TransformFromCS( cs_, v );
    return v;
}


double ArchimedianSpiral::CalcLength() const
{
    double angle = GetAngle();
    double angleSquare = angle * angle;
    return ( ::log(::sqrt(angleSquare + 1.0) + angle) / 2.0 +
        (angle / 2.0) * ::sqrt(angleSquare + 1.0) ) * radius_;
}


//-------------------------------------------------------------------------------------------------
// L O G A R I T H M I C   S P I R A L   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


LogarithmicSpiral::LogarithmicSpiral( const CS& cs, double radius, double angle, double b )
    : Spiral( cs, radius, angle )
    , b_( b )
{
}


LogarithmicSpiral::LogarithmicSpiral( const LogarithmicSpiral& rhs )
    : Spiral( rhs )
    , b_( rhs.b_ )
{
}


LogarithmicSpiral& LogarithmicSpiral::operator =( const LogarithmicSpiral& rhs )
{
    Spiral::operator =( rhs );
    if ( &rhs != this ) {
        b_ = rhs.b_;
    }
    return ( *this );
}


void LogarithmicSpiral::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


double LogarithmicSpiral::GetB() const
{
    return b_;
}


void LogarithmicSpiral::SetB( double b )
{
    b_ = b;
}


ECurveTypes LogarithmicSpiral::GetType() const
{
    return CT_LOGARITHMIC_SPIRAL;
}


/*++
    Calculation of the specified point on logarithmic spiral.
    X(t) = r * exp(b * t) * cos(t)
    Y(t) = r * exp(b * t) * sin(t)
--*/
Point LogarithmicSpiral::CalcPointOnCurve( double t ) const
{
    Point p( radius_ * ::exp(b_ * t) * ::cos(t), radius_ * ::exp(b_ * t) * ::sin(t) );
    TransformFromCS( cs_, p );
    return p;
}


/*++
    Calculation of the first derivative of logarithmic spiral at specified point.
    X(t)' = r * exp(b * t) * (cos(t) - sin(t))
    Y(t)' = r * exp(b * t) * (sin(t) + cos(t))
--*/
Vector LogarithmicSpiral::CalcFirstDerivative( double t ) const
{
    Vector v( radius_ * ::exp(b_ * t) * (::cos(t) - ::sin(t)),
        radius_ * ::exp(b_ * t) * (::sin(t) + ::cos(t)) );
    TransformFromCS( cs_, v );
    return v;
}


/*++
    Calculation of the second derivative of logarithmic spiral at specified point.
    X(t)'' = -2 * r * exp(b * t) * sin(t)
    Y(t)'' = 2 * r * exp(b * t) * cos(t)
--*/
Vector LogarithmicSpiral::CalcSecondDerivative( double t ) const
{
    Vector v( -2 * radius_ * ::exp(b_ * t) * ::sin(t), 2 * radius_ * ::exp(b_ * t) * ::cos(t) );
    TransformFromCS( cs_, v );
    return v;
}


double LogarithmicSpiral::CalcLength() const
{
    return ( (::sqrt(1.0 + b_ * b_) * (::exp(b_ * GetAngle())  - 1.0) * radius_ ) / b_ );
}


//-------------------------------------------------------------------------------------------------
// P O L Y L I N E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Polyline::Polyline()
    : interval_( ZERO_INTERVAL )
{
}


Polyline::Polyline( const std::vector<Point>& vertices )
    : vertices_( vertices )
{
    UpdateInterval();
}


Polyline::Polyline( const Polyline& rhs )
    : vertices_( rhs.vertices_ )
    , interval_( rhs.interval_ )
{
}


Polyline& Polyline::operator =( const Polyline& rhs )
{
    if ( &rhs != this ) {
        vertices_ = rhs.vertices_;
        interval_ = rhs.interval_;
    }
    return ( *this );
}


void Polyline::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


int Polyline::GetVerticesCount() const
{
    return static_cast<int>( vertices_.size() );
}


const Point& Polyline::GetVertex( int index ) const
{
    assert( static_cast<size_t>(index) < vertices_.size() );
    return vertices_[ index ];
}


void Polyline::SetVertex( int index, const Point& vertex )
{
    assert( static_cast<size_t>(index) < vertices_.size() );
    vertices_[ index ] = vertex;
}


void Polyline::AddVertex( const Point& vertex, int index /*= -1*/ )
{
    if ( (index >= 0) && (index < static_cast<int>(vertices_.size())) ) {
        vertices_.insert( vertices_.begin() + index, vertex );
        UpdateInterval();

    } else if ( index == -1 ) {
        vertices_.push_back( vertex );
        UpdateInterval();
    }
}


void Polyline::RemoveVertex( int index )
{
    if ( (index >= 0) && (index < static_cast<int>(vertices_.size())) ) {
        vertices_.erase( vertices_.begin() + index );
        UpdateInterval();
    }
}


/*++
    Remove all vertices from polyline.
--*/
void Polyline::RemoveVertices()
{
    vertices_.clear();
    UpdateInterval();
}


ECurveTypes Polyline::GetType() const
{
    return CT_POLYLINE;
}


const Interval& Polyline::GetParametricInterval() const
{
    return interval_;
}


/*++
    Calculation of the specified point on polyline.
    Used the same algorithms as for line segment for concrete partiion of the polyline.
    C(t) = ((1 - dt) * P[i] + dt * P[k])
--*/
Point Polyline::CalcPointOnCurve( double t ) const
{
    const Point* pStartPoint = 0;
    const Point* pEndPoint = 0;
    double localT;
    GetSegmentPoints( t, pStartPoint, pEndPoint, localT );

    Point result;
    if ( pStartPoint != 0 ) {
        if ( pStartPoint == pEndPoint ) {
            result = ( *pStartPoint );
        } else {
            result.Init( ((1.0 - localT) * pStartPoint->x_ + localT * pEndPoint->x_),
                ((1.0 - localT) * pStartPoint->y_ + localT * pEndPoint->y_) );
        }
    }
    return result;
}


/*++
    Calculation of the first derivative of polyline at specified point.
    Used the same algorithms as for line segment for concrete partiion of the polyline.
    C(t)' = P[k] - P[i]
--*/
Vector Polyline::CalcFirstDerivative( double t ) const
{
    const Point* pStartPoint;
    const Point* pEndPoint;
    double dummy;
    GetSegmentPoints( t, pStartPoint, pEndPoint, dummy );

    Vector result;
    if ( pStartPoint != 0 ) {
        result.Init( (pEndPoint->x_ - pStartPoint->x_), (pEndPoint->y_ - pStartPoint->y_));
    }
    return result;
}


/*++
    Calculation of the second derivative of polyline at specified point.
    C(t)'' = 0
--*/
Vector Polyline::CalcSecondDerivative( double /*t*/ ) const
{
    return ZERO_VECTOR;
}


double Polyline::CalcLength() const
{
    double length = 0.0;
    for ( size_t i = 1; i < vertices_.size(); ++i ) {
        length += CalcDistance( vertices_[i - 1], vertices_[i] );
    }
    return length;
}


void Polyline::CalcBoundingBox( BoundingBox& box ) const
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        box.Add( vertices_[i] );
    }
}


double Polyline::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    assert( GT(da, 0.0) );

    cut = false;

    double tEnd = ::floor( t );
    Vector dStart = CalcFirstDerivative( tEnd );
    Vector dEnd;

    do {
        tEnd += 1.0;
        if ( GE(tEnd, span.end_) ) {
            tEnd = span.end_;
            cut = true;
            break;
        }
        dEnd = CalcFirstDerivative( tEnd );
    } while( ::fabs(dStart.CalcRelativeAngle(dEnd)) < da );

    return ( (t < tEnd) ? (tEnd - t) : 0.0 );
}


void Polyline::Transfer( const Vector& shift )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Transfer( shift );
    }
}


void Polyline::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Scale( xCoef, yCoef, pCenter );
    }
}


void Polyline::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Rotate( angle, pCenter );
    }
}


void Polyline::Transform( const Matrix3x3& m )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Transform( m );
    }
}


void Polyline::UpdateInterval()
{
    if ( vertices_.size() < 2 ) {
        interval_ = ZERO_INTERVAL;
    } else {
        interval_.Init( 0.0, static_cast<double>(vertices_.size()) - 1.0 );
    }
}


void Polyline::GetSegmentPoints( double t, const Point*& pStartPoint, const Point*& pEndPoint,
    double& localT ) const
{
    if ( vertices_.size() >= 2 ) {

        int nStart = static_cast<int>( ::floor(t) );
        int nEnd = static_cast<int>( ::ceil(t) );

        if ( nStart == nEnd ) {
            nEnd = nStart + 1;
        }

        if ( (nStart >= 0) && (nEnd < static_cast<int>(vertices_.size())) ) {
            pStartPoint = &vertices_[ nStart ];
            pEndPoint = &vertices_[ nEnd ];
            localT = t - ::floor( t );
        } else {
            pStartPoint = ( (nStart < 0) ? &vertices_[0] : &vertices_[vertices_.size() - 2] );
            pEndPoint = ( (nStart < 0) ? &vertices_[1] : &vertices_[vertices_.size() - 1] );
            localT = ( (nStart < 0) ? t : ( t - static_cast<double>(vertices_.size()) + 2.0) );
        }

    } else {
        pStartPoint = 0;
        pEndPoint = 0;
    }
}


//-------------------------------------------------------------------------------------------------
// B E Z I E R   C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


/*++
    Construction of the quadratic Bezier curve.
--*/
BezierCurve::BezierCurve( const Point& p0, const Point& p1, const Point& p2 )
    : controlVerticesCount_( 0 )
    , pControlVertices_( 0 )
{
    AllocateControlVertices( 3 );
    pControlVertices_[ 0 ] = p0;
    pControlVertices_[ 1 ] = p1;
    pControlVertices_[ 2 ] = p2;
}


BezierCurve::BezierCurve( const std::vector<Point>& controlVertices )
    : controlVerticesCount_( 0 )
    , pControlVertices_( 0 )
{
    AllocateControlVertices( static_cast<int>(controlVertices.size()) );
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ] = controlVertices[ i ];
    }
}


BezierCurve::BezierCurve( const BezierCurve& rhs )
    : controlVerticesCount_( 0 )
    , pControlVertices_( 0 )
{
    ( *this ) = rhs;
}


BezierCurve::~BezierCurve()
{
    CleanupControlVertices();
}


BezierCurve& BezierCurve::operator =( const BezierCurve& rhs )
{
    if ( &rhs != this ) {
        AllocateControlVertices( rhs.controlVerticesCount_ );
        for ( int i = 0; i < controlVerticesCount_; ++i ) {
            pControlVertices_[ i ] = rhs.pControlVertices_[ i ];
        }
    }
    return ( *this );
}


void BezierCurve::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


int BezierCurve::GetOrder() const
{
    return controlVerticesCount_ - 1;
}


int BezierCurve::GetControlVerticesCount() const
{
    return controlVerticesCount_;
}


const Point& BezierCurve::GetControlVertex( int index ) const
{
    assert( index < controlVerticesCount_ );
    return pControlVertices_[ index ];
}


void BezierCurve::GetControlVertices( std::vector<Point>& vertices ) const
{
    vertices.reserve( controlVerticesCount_ );
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        vertices.push_back( pControlVertices_[i] );
    }
}


void BezierCurve::SetControlVerticesCount( int count )
{
    AllocateControlVertices( count );
}


void BezierCurve::SetControlVertex( int index, const Point& vertex )
{
    assert( index < controlVerticesCount_ );
    pControlVertices_[ index ].Init( vertex.x_, vertex.y_ );
}


void BezierCurve::SetControlVertices( const std::vector<Point>& vertices )
{
    AllocateControlVertices( static_cast<int>(vertices.size()) );
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ] = vertices[ i ];
    }
}


ECurveTypes BezierCurve::GetType() const
{
    return CT_BEZIER_CURVE;
}


const Interval& BezierCurve::GetParametricInterval() const
{
    return INTERVAL;
}


/*++
    Calculation of the specified point on Bezier curve.
    C1(t) = ((1 - t) * P[0] + t * P[1]),
    C2(t) = ((1 - t) ^ 2) * P[0] + 2 * t * (1 - t) * P[1] + (t ^ 2) * P[2],
    etc.
--*/
Point BezierCurve::CalcPointOnCurve( double t ) const
{
    Point result = ZERO_POINT;

    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        double b = B( controlVerticesCount_ - 1, i, t );
        result.x_ += pControlVertices_[ i ].x_ * b;
        result.y_ += pControlVertices_[ i ].y_ * b;
    }

    return result;
}


Vector BezierCurve::CalcFirstDerivative( double t ) const
{
    Vector result = ZERO_VECTOR;
    Vector tempVector;

    for ( int i = 0, degree = controlVerticesCount_ - 1; i < degree; ++i ) {
        D1( i, tempVector );
        tempVector *= B( degree - 1, i, t );
        result += tempVector;
    }

    result *= controlVerticesCount_;
    return result;
}


Vector BezierCurve::CalcSecondDerivative( double t ) const
{
    Vector result = ZERO_VECTOR;
    Vector tempVector;

    for ( int i = 0, degree = controlVerticesCount_ - 2; i < degree; ++i ) {
        D2( i, tempVector );
        tempVector *= B( degree - 1, i, t );
        result += tempVector;
    }

    result *= controlVerticesCount_ * ( controlVerticesCount_ - 1.0 );
    return result;
}


void BezierCurve::Transfer( const Vector& shift )
{
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ].Transfer( shift );
    }
}


void BezierCurve::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ].Scale( xCoef, yCoef, pCenter );
    }
}


void BezierCurve::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ].Rotate( angle, pCenter );
    }
}


void BezierCurve::Transform( const Matrix3x3& m )
{
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ].Transform( m );
    }
}


/*++
    Calculation of the Bernstein polynomial.
--*/
double BezierCurve::B( int n, int i, double t ) const
{
    return ( FACTORIAL_CALCULATOR.Calc(n) /
        (FACTORIAL_CALCULATOR.Calc(i) * FACTORIAL_CALCULATOR.Calc(n - i)) ) *
        ( ::pow(t, i) ) * ( ::pow(1.0 - t, n - i) );
}


void BezierCurve::D1( int i, Vector& result ) const
{
    assert( i + 1 < controlVerticesCount_ );
    result.Init( pControlVertices_[i + 1].x_ - pControlVertices_[i].x_,
        pControlVertices_[i + 1].y_ - pControlVertices_[i].y_ );
}


void BezierCurve::D2( int i, Vector& result ) const
{
    assert( i + 2 < controlVerticesCount_ );
    result.Init( (pControlVertices_[i + 2].x_ - pControlVertices_[i + 1].x_) -
        (pControlVertices_[i + 1].x_ - pControlVertices_[i].x_),
        (pControlVertices_[i + 2].y_ - pControlVertices_[i + 1].y_) -
        (pControlVertices_[i + 1].y_ - pControlVertices_[i].y_) );
}


void BezierCurve::AllocateControlVertices( int count )
{
    assert( count > 0 );
    CleanupControlVertices();
    controlVerticesCount_ = count;
    pControlVertices_ = new Point [ controlVerticesCount_ ];
}


void BezierCurve::CleanupControlVertices()
{
    delete [] pControlVertices_;
    pControlVertices_ = 0;
}


const Interval BezierCurve::INTERVAL( 0.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// H E R M I T E   S P L I N E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


HermiteSpline::HermiteSpline()
    : interval_( ZERO_INTERVAL )
{
}


HermiteSpline::HermiteSpline( const std::vector<Point>& vertices )
    : vertices_( vertices )
{
    UpdateInterval();
}


HermiteSpline::HermiteSpline( const HermiteSpline& rhs )
    : vertices_( rhs.vertices_ )
    , interval_( rhs.interval_ )
{
}


HermiteSpline& HermiteSpline::operator =( const HermiteSpline& rhs )
{
    if ( &rhs != this ) {
        vertices_ = rhs.vertices_;
        interval_ = rhs.interval_;
    }
    return ( *this );
}


void HermiteSpline::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


int HermiteSpline::GetVerticesCount() const
{
    return static_cast<int>( vertices_.size() );
}


const Point& HermiteSpline::GetVertex( int index ) const
{
    assert( static_cast<size_t>(index) < vertices_.size() );
    return vertices_[ index ];
}


void HermiteSpline::SetVertex( int index, const Point& vertex )
{
    assert( static_cast<size_t>(index) < vertices_.size() );
    vertices_[ index ] = vertex;
}


void HermiteSpline::AddVertex( const Point& vertex, int index /*= -1*/ )
{
    if ( (index >= 0) && (index < static_cast<int>(vertices_.size())) ) {
        vertices_.insert( vertices_.begin() + index, vertex );
        UpdateInterval();

    } else if ( index == -1 ) {
        vertices_.push_back( vertex );
        UpdateInterval();
    }
}


void HermiteSpline::RemoveVertex( int index )
{
    if ( (index >= 0) && (index < static_cast<int>(vertices_.size())) ) {
        vertices_.erase( vertices_.begin() + index );
        UpdateInterval();
    }
}


/*++
    Remove all vertices from spline.
--*/
void HermiteSpline::RemoveVertices()
{
    vertices_.clear();
    UpdateInterval();
}


/*++
    Modification of spline vertices count (shape stay the same).
--*/
bool HermiteSpline::RearrageVertices( int newVerticesCount )
{
    if ( (newVerticesCount >= 3) && (vertices_.size() >= 3) ) {
        std::vector<Point> tempPoints;
        CalcUniformPointsOnCurve( *this, newVerticesCount, tempPoints );
        vertices_.swap( tempPoints );
        UpdateInterval();
        return true;

    } else {
        return false;
    }
}


ECurveTypes HermiteSpline::GetType() const
{
    return CT_HERMITE_SPLINE;
}


const Interval& HermiteSpline::GetParametricInterval() const
{
    return interval_;
}


/*++
    Calculation of the specified point on Hermite spline.
    C(t) = a[0](o)*p[i] + a[1](o)*p[i+1] + b[0](o)*q[i] + b[1](o)*q[i+1],
    a[0](o) = 1 - 3*o^2 + 2*o^3,
    a[1](o) = 3*o^2 - 2*o^3,
    b[0](o) = o - 2*o^2 + o^3,
    b[1](o) = -o^2 + o^3,
    o - local interval parametric value (omega),
    q - tangent at specified vertex.
--*/
Point HermiteSpline::CalcPointOnCurve( double t ) const
{
    // calculation of local interval parametric value (omega) and its powers
    double o =  CalcLocalIntervalParametricValue( t );
    double o2 = o * o;
    double o3 = o2 * o;

    // calculation of a[0](o), a[1](o), b[0](o) and b[1](o)
    double a0 = 1 - 3 * o2 + 2 * o3;
    double a1 = 3 * o2 - 2 * o3;
    double b0 = o - 2 * o2 + o3;
    double b1 = -o2 + o3;

    // calculation of point on curve
    Point result;
    CalcRadiusVector( t, a0, a1, b0, b1, result.x_, result.y_ );
    return result;
}


/*++
    Calculation of the first derivative of Hermite spline at specified point.
    C'(t) = a[0](o)' * p[i] + a[1](o)' * p[i+1] + b[0](o)' * q[i] + b[1](o)' * q[i+1],
    a[0](o)' = -6*o + 6*o^2,
    a[1](o)' = 6*o - 6*o^2,
    b[0](o)' = 1 - 4*o + 3*o^2,
    b[1](o)' = -2*o + 3*o^2,
    o - local interval parametric value (omega),
    q - tangent at specified vertex.
--*/
Vector HermiteSpline::CalcFirstDerivative( double t ) const
{
    // calculation of local interval parametric value (omega) and its power
    double o =  CalcLocalIntervalParametricValue( t );
    double o2 = o * o;

    // calculation of a[0](o)', a[1](o)', b[0](o)' and b[1](o)'
    double da0 = -6 * o + 6 * o2;
    double da1 = 6 * o - 6 * o2;
    double db0 = 1 - 4 * o + 3 * o2;
    double db1 = -2 * o + 3 * o2;

    // calculation of first derivative
    Vector result;
    CalcRadiusVector( t, da0, da1, db0, db1, result.x_, result.y_ );
    return result;
}


/*++
    Calculation of the second derivative of Hermite spline at specified point.
    C''(t) = a[0](o)'' * p[i] + a[1](o)'' * p[i+1] + b[0](o)'' * q[i] + b[1](o)'' * q[i+1],
    a[0](o)'' = -6 + 12*o,
    a[1](o)'' = 6 - 12*o,
    b[0](o)'' = - 4 + 6*o,
    b[1](o)'' = -2 + 6*o,
    o - local interval parametric value (omega),
    q - tangent at specified vertex.
--*/
Vector HermiteSpline::CalcSecondDerivative( double t ) const
{
    // calculation of local interval parametric value (omega)
    double o =  CalcLocalIntervalParametricValue( t );

    // calculation of a[0](o)'', a[1](o)'', b[0](o)'' and b[1](o)''
    double dda0 = -6 + 12 * o;
    double dda1 = 6 - 12 * o;
    double ddb0 = - 4 + 6 * o;
    double ddb1 = -2 + 6 * o;

    // calculation of second derivative
    Vector result;
    CalcRadiusVector( t, dda0, dda1, ddb0, ddb1, result.x_, result.y_ );
    return result;
}


void HermiteSpline::Transfer( const Vector& shift )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Transfer( shift );
    }
}


void HermiteSpline::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Scale( xCoef, yCoef, pCenter );
    }
}


void HermiteSpline::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Rotate( angle, pCenter );
    }
}


void HermiteSpline::Transform( const Matrix3x3& m )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Transform( m );
    }
}


void HermiteSpline::UpdateInterval()
{
    if ( vertices_.size() < 2 ) {
        interval_ = ZERO_INTERVAL;
    } else {
        interval_.Init( 0.0, static_cast<double>(vertices_.size()) - 1.0 );
    }
}


/*++
    Calculation of the local interval points (starting and ending points).
--*/
void HermiteSpline::GetLocalIntervalPoints( double t, const Point*& pP0, const Point*& pP1,
    int& nP0 ) const
{
    if ( vertices_.size() >= 2 ) {

        int nTempP0 = static_cast<int>( ::floor(t) );
        int nTempP1 = static_cast<int>( ::ceil(t) );

        // parameter value exactly at vertex case (0.0, 1.0, 2.0 etc.)
        if ( nTempP0 == nTempP1 ) {
            nTempP1 = nTempP0 + 1;
        }

        if ( nTempP0 < 0 ) {
            // left boundary case
            nTempP0 = 0;
            nTempP1 = 1;

        } else if ( nTempP1 >= static_cast<int>(vertices_.size()) ) {
            // right boundary case
            nTempP0 = ( vertices_.size() - 2 );
            nTempP1 = ( vertices_.size() - 1 );
        }

        pP0 = &vertices_[ nTempP0 ];
        pP1 = &vertices_[ nTempP1 ];
        nP0 = nTempP0;

    } else {
        pP0 = 0;
        pP1 = 0;
    }
}


/*++
    Generic method for calculation of Hermite splines radius vector.
    C(t) = a0*p[i] + a1*p[i+1] + b0*q[i] + b1*q[i+1]
--*/
void HermiteSpline::CalcRadiusVector( double t, double a0, double a1, double b0, double b1,
    double& x, double& y ) const
{
    // acquision of local interval points
    const Point* pP0 = 0;
    const Point* pP1 = 0;
    int nP0;
    GetLocalIntervalPoints( t, pP0, pP1, nP0 );

    if ( pP0 != 0 ) {
        Vector q0;
        Vector q1;
        if ( CalcLocalIntervalTangent(nP0, q0) && CalcLocalIntervalTangent(nP0 + 1, q1) ) {
            x = a0 * pP0->x_ + a1 * pP1->x_ + b0 * q0.x_ + b1 * q1.x_;
            y = a0 * pP0->y_ + a1 * pP1->y_ + b0 * q0.y_ + b1 * q1.y_;
        }

    } else {
        x = 0;
        y = 0;
    }
}


/*++
    Calculation of the local interval parametric value (also called omega).
    o = (t - t[i]) / (t[i+1] - t[i]), t[i] <= t <= t[i+1], 0.0 <= o <= 1.0
--*/
double HermiteSpline::CalcLocalIntervalParametricValue( double t ) const
{
    double o = 0.0;

    if ( vertices_.size() >= 2 ) {

        int nP0 = static_cast<int>( ::floor(t) );
        int nP1 = static_cast<int>( ::ceil(t) );

        // parameter value exactly at vertex case (0.0, 1.0, 2.0 etc.)
        if ( nP0 == nP1 ) {
            nP1 = nP0 + 1;
        }

        if ( nP0 < 0 ) {
            // left boundary case
            o = t;

        } else if ( nP1 >= static_cast<int>(vertices_.size()) ) {
            // right boundary case
            o = ( t - static_cast<double>(vertices_.size()) + 2.0 );

        } else {
            // common case
            o = ( t - ::floor(t) );
        }
    }

    return o;
}


/*++
    Calculation of tangent at specified spline vertex (vertex specified by index).
    q[i] = (s[i+1] * (p[i]-p[i-1])) / (s[i] + s[i+1]) + (s[si] * (p[i+1] - p[i])) / (s[i] + s[i+1]),
    q[0] = 2*(p[1] - p[0]) - q[1],
    q[n-1] = 2*(p[n-2] - p[n-1]) - q[n-2],
    s[i] - distance between p[i] and p[i-1],
    s[i + 1] - distance between p[i+1] and p[i].
    Simpler method providing worse support for non-uniform vertices sets:
    q[i] = (p[i+1] - p[i-1]) / (t[i+1] - t[i-1])
--*/
bool HermiteSpline::CalcLocalIntervalTangent( int i, Vector& q ) const
{
    // spline containing at least 3 vertices required
    if ( vertices_.size() >= 3 ) {

        if ( i <= 0 ) {
            // left boundary case
            const Point& p0 = vertices_[ 0 ];
            const Point& p1 = vertices_[ 1 ];
            Vector q1;
            CalcLocalIntervalTangent( 1, q1 );
            q.x_ = 2.0 * ( p1.x_ - p0.x_ ) - q1.x_;
            q.y_ = 2.0 * ( p1.y_ - p0.y_ ) - q1.y_;

        } else if ( i >= (static_cast<int>(vertices_.size() - 1)) ) {
            // right boundary case
            const Point& pff = vertices_[ vertices_.size() - 2 ];
            const Point& p0 = vertices_[ vertices_.size() - 1 ];
            Vector qff;
            CalcLocalIntervalTangent( vertices_.size() - 2, qff );
            q.x_ = 2.0 * ( p0.x_ - pff.x_ ) - qff.x_;
            q.y_ = 2.0 * ( p0.y_ - pff.y_ ) - qff.y_;

        } else {
            // common case
            const Point& pff = vertices_[ i - 1 ];
            const Point& p0 = vertices_[ i ];
            const Point& p1 = vertices_[ i + 1 ];
            double s0 = CalcDistance( pff, p0 );
            double s1 = CalcDistance( p0, p1 );
            q.x_ = ( s0 * (p0.x_ - pff.x_) + s1 * (p1.x_ - p0.x_) ) / ( s0 + s1 );
            q.y_ = ( s0 * (p0.y_ - pff.y_) + s1 * (p1.y_ - p0.y_) ) / ( s0 + s1 );

            // common case, simpler method
            //const Point& pff = vertices_[ i - 1 ];
            //const Point& p1 = vertices_[ i + 1 ];
            //q.x_ = ( p1.x_ - pff.x_ ) / 2.0;
            //q.y_ = ( p1.y_ - pff.y_ ) / 2.0;
        }

        return true;

    } else {
        return false;
    }
}


//-------------------------------------------------------------------------------------------------
// E Q U I D I S T A N T   C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


EquidistantCurve::EquidistantCurve( Curve* pSourceCurve, const Vector& shiftDirection )
    : pSourceCurve_( pSourceCurve )
    , shiftDirection_( shiftDirection )
{
    assert( pSourceCurve != 0 );
    AddReference( pSourceCurve_ );
}


EquidistantCurve::EquidistantCurve( const EquidistantCurve& rhs )
    : pSourceCurve_( rhs.pSourceCurve_ )
    , shiftDirection_( rhs.shiftDirection_ )
{
    assert( rhs.pSourceCurve_ != 0 );
    AddReference( pSourceCurve_ );
}


EquidistantCurve::~EquidistantCurve()
{
    ReleaseReference( pSourceCurve_ );
}


EquidistantCurve& EquidistantCurve::operator =( const EquidistantCurve& rhs )
{
    if ( &rhs != this ) {
        SetSourceCurve( rhs.pSourceCurve_ );
        shiftDirection_ = rhs.shiftDirection_;
    }
    return ( *this );
}


void EquidistantCurve::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


const Curve& EquidistantCurve::GetSourceCurve() const
{
    return ( *pSourceCurve_ );
}


const Vector& EquidistantCurve::GetShiftDirection() const
{
    return shiftDirection_;
}


void EquidistantCurve::SetSourceCurve( Curve* pSourceCurve )
{
    assert( pSourceCurve != 0 );
    ReleaseReference( pSourceCurve_ );
    pSourceCurve_ = pSourceCurve;
    AddReference( pSourceCurve_ );
}


void EquidistantCurve::SetShiftDirection( const Vector& direction )
{
    shiftDirection_ = direction;
}


ECurveTypes EquidistantCurve::GetType() const
{
    return CT_EQUIDISTANT_CURVE;
}


const Interval& EquidistantCurve::GetParametricInterval() const
{
    return ( pSourceCurve_->GetParametricInterval() );
}


Point EquidistantCurve::CalcPointOnCurve( double t ) const
{
    Vector sourceDerivative = pSourceCurve_->CalcFirstDerivative( t );
    Vector shiftDirection = shiftDirection_;
    shiftDirection.Rotate( sourceDerivative.CalcAngle() );

    Point result = pSourceCurve_->CalcPointOnCurve( t );
    result.Transfer( shiftDirection );
    return result;
}


Vector EquidistantCurve::CalcFirstDerivative( double t ) const
{
    return pSourceCurve_->CalcFirstDerivative( t );
}


Vector EquidistantCurve::CalcSecondDerivative( double t ) const
{
    return pSourceCurve_->CalcSecondDerivative( t );
}


double EquidistantCurve::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    return pSourceCurve_->CalcTiltStep( t, da, span, cut );
}


void EquidistantCurve::Transfer( const Vector& shift )
{
    pSourceCurve_->Transfer( shift );
}


void EquidistantCurve::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    pSourceCurve_->Scale( xCoef, yCoef, pCenter );
}


void EquidistantCurve::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    pSourceCurve_->Rotate( angle, pCenter );
}


void EquidistantCurve::Transform( const Matrix3x3& m )
{
    pSourceCurve_->Transform( m );
}


//-------------------------------------------------------------------------------------------------
// P R O X Y   C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


ProxyCurve::ProxyCurve( Curve* pSourceCurve )
    : pSourceCurve_( pSourceCurve )
    , pInterval_( 0 )
{
    assert( pSourceCurve != 0 );
    AddReference( pSourceCurve_ );
}


ProxyCurve::ProxyCurve( Curve* pSourceCurve, const Interval& interval )
    : pSourceCurve_( pSourceCurve )
    , pInterval_( new Interval(interval) )
{
    assert( pSourceCurve != 0 );
    AddReference( pSourceCurve_ );
}


ProxyCurve::ProxyCurve( const ProxyCurve& rhs )
    : pSourceCurve_( rhs.pSourceCurve_ )
    , pInterval_( (rhs.pInterval_ != 0) ? new Interval(*rhs.pInterval_) : 0 )
{
    assert( rhs.pSourceCurve_ != 0 );
    AddReference( pSourceCurve_ );
}


ProxyCurve::~ProxyCurve()
{
    delete pInterval_;
    ReleaseReference( pSourceCurve_ );
}


ProxyCurve& ProxyCurve::operator =( const ProxyCurve& rhs )
{
    if ( &rhs != this ) {
        SetSourceCurve( rhs.pSourceCurve_ );
        delete pInterval_;
        pInterval_ = ( (rhs.pInterval_ != 0) ? new Interval(*rhs.pInterval_) : 0 );
    }
    return ( *this );
}


void ProxyCurve::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


const Curve& ProxyCurve::GetSourceCurve() const
{
    return ( *pSourceCurve_ );
}


void ProxyCurve::SetSourceCurve( Curve* pSourceCurve )
{
    assert( pSourceCurve != 0 );
    ReleaseReference( pSourceCurve_ );
    pSourceCurve_ = pSourceCurve;
    AddReference( pSourceCurve_ );
}


void ProxyCurve::SetParametricInterval( const Interval& interval )
{
    delete pInterval_;
    pInterval_ = new Interval( interval );
}


void ProxyCurve::ResetParametricInterval()
{
    delete pInterval_;
    pInterval_ = 0;
}


ECurveTypes ProxyCurve::GetType() const
{
    return CT_PROXY_CURVE;
}


const Interval& ProxyCurve::GetParametricInterval() const
{
    return ( pInterval_ ? *pInterval_ : pSourceCurve_ ->GetParametricInterval() );
}


Point ProxyCurve::CalcPointOnCurve( double t ) const
{
    return pSourceCurve_->CalcPointOnCurve( t );
}


Vector ProxyCurve::CalcFirstDerivative( double t ) const
{
    return pSourceCurve_->CalcFirstDerivative( t );
}


Vector ProxyCurve::CalcSecondDerivative( double t ) const
{
    return pSourceCurve_->CalcSecondDerivative( t );
}


double ProxyCurve::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    return pSourceCurve_->CalcTiltStep( t, da, span, cut );
}


void ProxyCurve::Transfer( const Vector& shift )
{
    pSourceCurve_->Transfer( shift );
}


void ProxyCurve::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    pSourceCurve_->Scale( xCoef, yCoef, pCenter );
}


void ProxyCurve::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    pSourceCurve_->Rotate( angle, pCenter );
}


void ProxyCurve::Transform( const Matrix3x3& m )
{
    pSourceCurve_->Transform( m );
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


/*---*/


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C U R V E   U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


void CalcUniformPointsOnCurve( const Curve& curve, int pointsCount, std::vector<double>& points,
    const Interval* pInterval /*= 0*/ )
{
    if ( pointsCount > 1 ) {

        const Interval& interval =
            (pInterval) ? (*pInterval) : curve.GetParametricInterval();
        double tStep = ( interval.CalcLength() / (pointsCount - 1) );

        points.reserve( points.size() + pointsCount );
        for ( int i = 0; i < pointsCount; ++i ) {
            points.push_back( interval.begin_ + i * tStep );
        }
    }
}


void CalcUniformPointsOnCurve( const Curve& curve, int pointsCount, std::vector<Point>& points,
    const Interval* pInterval /*= 0*/ )
{
    std::vector<double> tempPoints;
    CalcUniformPointsOnCurve( curve, pointsCount, tempPoints, pInterval );

    points.reserve( points.size() + tempPoints.size() );
    for ( size_t i = 0; i < tempPoints.size(); ++i ) {
        points.push_back( curve.CalcPointOnCurve(tempPoints[i]) );
    }
}


void CalcPointsOnCurve( const Curve& curve, double da, std::vector<double>& points,
                       const Interval* pInterval /*= 0*/ )
{
    if ( GT(da, 0.0) ) {

        const Interval& span =
            (pInterval) ? (*pInterval) : curve.GetParametricInterval();

        points.push_back( span.begin_ );

        bool cut = false;
        for ( double t = span.begin_; !cut; ) {
            t += curve.CalcTiltStep( t, da, span, cut );
            points.push_back( t );
        }
    }
}


void CalcPointsOnCurve( const Curve& curve, double da, std::vector<Point>& points,
    const Interval* pInterval /*= 0*/ )
{
    std::vector<double> tempPoints;
    CalcPointsOnCurve( curve, da, tempPoints, pInterval );

    points.reserve( points.size() + tempPoints.size() );
    for ( size_t i = 0; i < tempPoints.size(); ++i ) {
        points.push_back( curve.CalcPointOnCurve(tempPoints[i]) );
    }
}


//-------------------------------------------------------------------------------------------------
// T E S S E L A T I O N   U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


void CalcCurveUniformTesselation( const Curve& curve, int segmentsCount,
    std::vector<SegmentDescription>& segments, const Interval* pInterval /*= 0*/ )
{
    if ( segmentsCount >= 1 ) {

        int pointsCount = ( segmentsCount + 1 );
        std::vector<double> points;
        CalcUniformPointsOnCurve( curve, pointsCount, points, pInterval );

        SegmentDescription curSegment;
        curSegment.p1_ = curve.CalcPointOnCurve( points[0] );

        segments.reserve( segments.size() + segmentsCount );
        for ( int i = 1; i < pointsCount; ++i ) {
            curSegment.p2_ = curve.CalcPointOnCurve( points[i] );
            segments.push_back( curSegment );
            curSegment.p1_ = curSegment.p2_;
        }
    }
}


void CalcCurveTesselation( const Curve& curve, double da,
    std::vector<SegmentDescription>& segments, const Interval* pInterval /*= 0*/ ) 
{
    std::vector<double> points;
    CalcPointsOnCurve( curve, da, points, pInterval );

    if ( points.size() > 1 ) {

        SegmentDescription curSegment;
        curSegment.p1_ = curve.CalcPointOnCurve( points[0] );

        int pointsCount = static_cast<int>( points.size() );
        segments.reserve( segments.size() + pointsCount - 1 );

        for ( int i = 1; i < pointsCount; ++i ) {
            curSegment.p2_ = curve.CalcPointOnCurve( points[i] );
            segments.push_back( curSegment );
            curSegment.p1_ = curSegment.p2_;
        }
    }
}


void CalcCurveSmoothTesselation( const Curve& curve, int beziersCount,
    std::vector<QuadraticBezierCurveDescription>& beziers,
    const Interval* pInterval /*= 0*/ )
{
    if ( beziersCount > 1 ) {

        int pointsCount = ( beziersCount + 1 );
        std::vector<double> points;
        CalcUniformPointsOnCurve( curve, pointsCount, points, pInterval );

        QuadraticBezierCurveDescription bezier;
        bezier.p1_ = curve.CalcPointOnCurve( points[0] );
        Vector prevDer = curve.CalcFirstDerivative( points[0] );
        Vector curDer;

        beziers.reserve( beziers.size() + beziersCount );
        for ( int i = 1; i < pointsCount; ++i ) {

            bezier.p3_ = curve.CalcPointOnCurve( points[i] );
            curDer = curve.CalcFirstDerivative( points[i] );

            if ( !CalcLinesIntersectionPoint(bezier.p1_, prevDer, bezier.p3_, curDer, bezier.p2_) ) {
                bezier.p2_.Init( (bezier.p1_.x_ + bezier.p3_.x_) / 2.0,
                    (bezier.p1_.y_ + bezier.p3_.y_) / 2.0 );
            }

            beziers.push_back( bezier );
            bezier.p1_ = bezier.p3_;
            prevDer = curDer;
        }
    }
}


void CalcPolylineClosedSmoothTesselation( const Polyline& polyline,
    std::vector<QuadraticBezierCurveDescription>& beziers )
{
    int verticesCount = polyline.GetVerticesCount();
    if ( verticesCount > 2 ) {

        QuadraticBezierCurveDescription tempBezier;
        const Point* pPrevPoint = 0;
        const Point* pNextPoint = 0;

        for ( int i = 0; i < verticesCount; ++i ) {

            // calculate second point of current bezier segment
            tempBezier.p2_ = polyline.GetVertex( i );

            // calculate first and third points of current bezier segment
            if ( i == 0 ) {
                pPrevPoint = &polyline.GetVertex( verticesCount - 1 );
                pNextPoint = &polyline.GetVertex( i + 1 );
            } else if ( i == (verticesCount - 1) ) {
                pPrevPoint = &polyline.GetVertex( i - 1 );
                pNextPoint = &polyline.GetVertex( 0 );
            } else {
                pPrevPoint = &polyline.GetVertex( i - 1 );
                pNextPoint = &polyline.GetVertex( i + 1 );
            }
            tempBezier.p1_ = CalcMiddlePoint( *pPrevPoint, tempBezier.p2_ );
            tempBezier.p3_ = CalcMiddlePoint( tempBezier.p2_, *pNextPoint );

            beziers.push_back( tempBezier );
        }
    }
}


//-------------------------------------------------------------------------------------------------
// I N T E R S E C T I O N   U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


/*++
    t1 = ((x2 - a2) * (y1 - y2) - (x1 - x2) * (y2 - b2)) /
        ((x2 - a2) * (y1 - b1) - (x1 - a1) * (y2 - b2))
    t2 = ((x1 - a1) * (y1 - y2) - (x1 - x2) * (y1 - b1)) /
        ((x2 - a2) * (y1 - b1) - (x1 - a1) * (y2 - b2))
    (a1, b1) is (p1 + v1)
    (a2, b2) is (p2 + v2)
--*/
bool CalcLinesIntersectionPoint( const Point& p1, const Vector& v1, const Point& p2,
    const Vector& v2, double& t1, double& t2 )
{
    if ( !v1.IsCollinear(v2) ) {
        double x1_x2 = ( p1.x_ - p2.x_ );
        double y1_y2 = ( p1.y_ - p2.y_ );
        double x1_a1 = ( p1.x_ - (p1.x_ + v1.x_) );
        double y1_b1 = ( p1.y_ - (p1.y_ + v1.y_) );
        double x2_a2 = ( p2.x_ - (p2.x_ + v2.x_) );
        double y2_b2 = ( p2.y_ - (p2.y_ + v2.y_) );
        double div = ( x2_a2 * y1_b1 - x1_a1 * y2_b2 );
        t1 = ( x2_a2 * y1_y2 - x1_x2 * y2_b2 ) / div;
        t2 = ( x1_a1 * y1_y2 - x1_x2 * y1_b1 ) / div;
        return true;

    } else {
        return false;
    }
}


/*++
    x = ((x1 * b1 - y1 * a1) * (x2 - a2) - (x1 - a1) * (x2 * b2 - y2 * a2)) /
        ((x1 - a1) * (y2 - b2) - (y1 - b1) * (x2 - a2))
    y = ((x1 * b1 - y1 * a1) * (y2 - b2) - (y1 - b1) * (x2 * b2 - y2 * a2)) /
        ((x1 - a1) * (y2 - b2) - (y1 - b1) * (x2 - a2))
    (a1, b1) is p1 + v1
    (a2, b2) is p2 + v2
--*/
bool CalcLinesIntersectionPoint( const Point& p1, const Vector& v1, const Point& p2,
    const Vector& v2, Point& result )
{
    if ( !v1.IsCollinear(v2) ) {
        double a1 = ( p1.x_ + v1.x_ );
        double b1 = ( p1.y_ + v1.y_ );
        double a2 = ( p2.x_ + v2.x_ );
        double b2 = ( p2.y_ + v2.y_ );
        double x1_a1 = ( p1.x_ - a1 );
        double y1_b1 = ( p1.y_ - b1 );
        double x2_a2 = ( p2.x_ - a2 );
        double y2_b2 = ( p2.y_ - b2 );
        double term1 = ( p1.x_ * b1 - p1.y_ * a1 );
        double term2 = ( p2.x_ * b2 - p2.y_ * a2 );
        double div = ( x1_a1 * y2_b2 - y1_b1 * x2_a2 );
        result.x_ = ( term1 * x2_a2 - x1_a1 * term2 ) / div;
        result.y_ = ( term1 * y2_b2 - y1_b1 * term2 ) / div;
        return true;

    } else {
        return false;
    }
}


void CalcCurvesIntersectionPoints( const Curve& curve1, const Curve& curve2,
    std::vector<Pair>& points, const Interval* pInterval1 /*= 0*/,
    const Interval* pInterval2 /*= 0*/ )
{
    CurvesIntersectionFunctor functor;
    functor.CalcIntersections( curve1, curve2, points, pInterval1, pInterval2 );
}


void CalcCurvesIntersectionPoints( const Curve& curve1, const Curve& curve2,
    std::vector<Point>& points, const Interval* pInterval1 /*= 0*/,
    const Interval* pInterval2 /*= 0*/ )
{
    std::vector<Pair> tempPoints;
    CalcCurvesIntersectionPoints( curve1, curve2, tempPoints, pInterval1, pInterval2 );

    points.reserve( points.size() + tempPoints.size() );
    for ( size_t i = 0; i < tempPoints.size(); ++i ) {
        points.push_back( curve1.CalcPointOnCurve(tempPoints[i].v1_) );
    }
}


void CalcCurvesIntersectionPoints( ICurvesIntersectionFunctor& functor, const Curve& curve1,
    const Curve& curve2, std::vector<Pair>& points, const Interval* pInterval1 /*= 0*/,
    const Interval* pInterval2 /*= 0*/ )
{
    functor.CalcIntersections( curve1, curve2, points, pInterval1, pInterval2 );
}


void CalcCurvesIntersectionPoints( ICurvesIntersectionFunctor& functor, const Curve& curve1,
    const Curve& curve2, std::vector<Point>& points, const Interval* pInterval1 /*= 0*/,
    const Interval* pInterval2 /*= 0*/ )
{
    std::vector<Pair> tempPoints;
    CalcCurvesIntersectionPoints( functor, curve1, curve2, tempPoints, pInterval1, pInterval2 );

    points.reserve( points.size() + tempPoints.size() );
    for ( size_t i = 0; i < tempPoints.size(); ++i ) {
        points.push_back( curve1.CalcPointOnCurve(tempPoints[i].v1_) );
    }
}


//-------------------------------------------------------------------------------------------------
// P R O J E C T I O N   U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


void CalcVectorProjection( const Vector& src, const Vector& dest, Vector& result )
{
    double destLength = dest.CalcLength();
    if ( NENil(destLength) ) {
        double resultLength = ( ::cos(src.CalcAngle(dest)) * src.CalcLength() );
        result.x_ = ( dest.x_ * resultLength ) / destLength;
        result.y_ = ( dest.y_ * resultLength ) / destLength;
    } else {
        result.x_ = 0.0;
        result.y_ = 0.0;
    }
}


double CalcVectorProjectionLength( const Vector& src, const Vector& dest )
{
    if ( EQNil(dest.x_) && EQNil(dest.y_) ) {
        return 0.0;
    } else {
        return ::fabs( ::cos(src.CalcAngle(dest)) * src.CalcLength() );
    }
}


//-------------------------------------------------------------------------------------------------
// C U R V E S   I N T E R S E C T I O N   F U N C T O R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


CurvesIntersectionFunctor::CurvesIntersectionFunctor()
    : da_( DEFAULT_DA )
    , metricEps_( DOUBLE_PRECISION )
    , pC1_( 0 )
    , pC2_( 0 )
{
}


void CurvesIntersectionFunctor::CalcIntersections( const Curve& c1, const Curve& c2,
    std::vector<Pair>& points, const Interval* pI1, const Interval* pI2 )
{
    // prepare
    pC1_ = &c1;
    pC2_ = &c2;

    const Interval& i1 = ( pI1 ? *pI1 : c1.GetParametricInterval() );
    const Interval& i2 = ( pI2 ? *pI2 : c2.GetParametricInterval() );
    Pair tNull;
    Pair tDefect;
    Pair tExact;

    // calculation of first curve parametric steps
    std::vector<double> dts1;
    CalcTiltStepsOnCurve( c1, i1, dts1 );

    // calculation of second curve parametric steps
    std::vector<double> dts2;
    CalcTiltStepsOnCurve( c2, i2, dts2 );

    t_.v1_ = i1.begin_;
    for ( size_t nDt1 = 0, s1 = dts1.size(); nDt1 < s1; ++nDt1 ) {

        dt_.v1_ = dts1[ nDt1 ];

        t_.v2_ = i2.begin_;
        for ( size_t nDt2 = 0, s2 = dts2.size(); nDt2 < s2; ++nDt2 ) {

            dt_.v2_ = dts2[ nDt2 ];

            // calculation of null approximation
            if ( CalcNullApproximation(tNull, tDefect) &&
                IsInsideClosedInterval(tNull.v1_, -tDefect.v1_, (dt_.v1_ + tDefect.v1_)) &&
                IsInsideClosedInterval(tNull.v2_, -tDefect.v2_, (dt_.v2_ + tDefect.v2_)) ) {

                // calculation of exact intersection
                tNull += t_;
                if ( CalcExactIntersection(tNull, tDefect, tExact) &&
                    IsInsideClosedInterval(tNull.v1_, t_.v1_, (t_.v1_ + dt_.v1_)) &&
                    IsInsideClosedInterval(tNull.v2_, t_.v2_, (t_.v2_ + dt_.v2_)) ) {

                    if ( pointsSet_.find(tExact) == pointsSet_.end() ) {
                        points.push_back( tExact );
                        pointsSet_.insert( points.back() );
                    }
                }
            }

            t_.v2_ += dt_.v2_;
        }

        t_.v1_ += dt_.v1_;
    }

    // cleanup
    pC1_ = 0;
    pC2_ = 0;
    pointsSet_.clear();
}


void CurvesIntersectionFunctor::CalcTiltStepsOnCurve( const Curve& c, const Interval& span,
    std::vector<double>& steps ) const
{
    if ( GT(da_, 0.0) ) {
        bool cut = false;
        for ( double t = span.begin_; !cut; ) {
            steps.push_back( c.CalcTiltStep(t, da_, span, cut) );
            t += steps.back();
        }
    }
}


bool CurvesIntersectionFunctor::CalcNullApproximation( Pair& tNull, Pair& tDefect ) const
{
    // construction of first and second lines (starting point and tangent)
    Point p1 = pC1_->CalcPointOnCurve( t_.v1_ );
    Point p2 = pC2_->CalcPointOnCurve( t_.v2_ );
    Vector v1 = pC1_->CalcFirstDerivative( t_.v1_ );
    Vector v2 = pC2_->CalcFirstDerivative( t_.v2_ );
    double v1Length = v1.CalcLength();
    double v2Length = v2.CalcLength();

    // normalization of tangents
    Point nextP1 = pC1_->CalcPointOnCurve( t_.v1_ + dt_.v1_ );
    Point nextP2 = pC2_->CalcPointOnCurve( t_.v2_ + dt_.v2_ );
    v1 *= ( CalcDistance(p1, nextP1) / v1Length );
    v2 *= ( CalcDistance(p2, nextP2) / v2Length );

    // intersection of constructed lines
    if ( CalcLinesIntersectionPoint(p1, v1, p2, v2, tNull.v1_, tNull.v2_) ) {

        tNull.v1_ = Normalize( tNull.v1_, 1.0, dt_.v1_ );
        tNull.v2_ = Normalize( tNull.v2_, 1.0, dt_.v2_ );

        // Some parts of intersection algorithm work incorrectly at the present moment. Deviation
        // caused by approximation have to be considered in a some way, but there are some
        // problems with this issue. Mainly, obvious implementations are quite resource-intensive.
        tDefect.Init( 0.0, 0.0 );

        return true;

    } else {
        return false;
    }
}


/*++
    Solution of polynomial equations system by Newton's method.
    t|n + 1| = t|n| - (f'(t)|n|^-1 * f(t)|n|),
    f(t) = |x1(t1) - x2(t2) = 0|
           |y1(t1) - y2(t2) = 0|,
    f'(t) = |x1'(t1), -x2(t2)|
            |y1'(t1), -y2(t2)|.
--*/
bool CurvesIntersectionFunctor::CalcExactIntersection( const Pair& tNull, const Pair& tDefect,
    Pair& tExact ) const
{
    const int MAX_ITER_COUNT = 0x20;

    // convergence limits
    Pair t( t_.v1_ - tDefect.v1_, t_.v2_ - tDefect.v2_ );
    Pair tEnd( t_.v1_ + dt_.v1_ + tDefect.v1_, t_.v2_ + dt_.v2_ + tDefect.v2_ );

    // points and first derivatives on current iteration step
    Point p1;
    Point p2;
    Vector v1;
    Vector v2;

    // points on next iteration step
    Point nextP1;
    Point nextP2;

    // matrices for equations system solution
    Matrix2x1 ts( tNull.v1_, tNull.v2_ );
    Matrix2x2 a;
    Matrix2x1 b;
    Matrix2x1 c;

    bool result = false;
    bool t1Found = false;
    bool t2Found = false;
    int iterCount = 0;

    while ( IsInsideClosedInterval(ts.m_[0][0], t.v1_, tEnd.v1_) &&
        IsInsideClosedInterval(ts.m_[0][1], t.v2_, tEnd.v2_) ) {

        if ( !t1Found ) {
            p1 = pC1_->CalcPointOnCurve( ts.m_[0][0] );
            v1 = pC1_->CalcFirstDerivative( ts.m_[0][0] );
        }
        if ( !t2Found ) {
            p2 = pC2_->CalcPointOnCurve( ts.m_[1][0] );
            v2 = pC2_->CalcFirstDerivative( ts.m_[1][0] );
        }

        a.Init( v1.x_, -v2.x_, v1.y_, -v2.y_ );
        a.Invert();
        b.Init( p1.x_ - p2.x_, p1.y_ - p2.y_ );
        c = a * b;

        if ( !t1Found ) {
            ts.m_[0][0] -= c.m_[0][0];
            nextP1 = pC1_->CalcPointOnCurve( ts.m_[0][0] );
            t1Found = EQ( p1.x_, nextP1.x_, metricEps_ ) && EQ( p1.y_, nextP1.y_, metricEps_ );
        }
        if ( !t2Found ) {
            ts.m_[1][0] -= c.m_[1][0];
            nextP2 = pC2_->CalcPointOnCurve( ts.m_[1][0] );
            t2Found = EQ( p2.x_, nextP2.x_, metricEps_ ) && EQ( p2.y_, nextP2.y_, metricEps_ );
        }

        if ( t1Found && t2Found ) {
            tExact.v1_ = ts.m_[0][0];
            tExact.v2_ = ts.m_[1][0];
            result = true;
            break;
        }

        // consider iterations count limit
        if ( (++iterCount) >= MAX_ITER_COUNT ) {
            break;
        }
    }

    return result;
}


const double CurvesIntersectionFunctor::DEFAULT_DA = Radians( 5.0 );


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


/*---*/


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C O L L E C T   E D I T I N G   P O I N T S   V I S I T O R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


void CollectEditingPointsVisitor::Visit( Segment& curve )
{
    editingPoints_.push_back( curve.GetP1() );
    editingPoints_.push_back( curve.GetP2() );
}


void CollectEditingPointsVisitor::Visit( Arc& curve )
{
    editingPoints_.push_back( curve.GetCS().pos_ );
}


void CollectEditingPointsVisitor::Visit( ArchimedianSpiral& curve )
{
    editingPoints_.push_back( curve.GetCS().pos_ );
}


void CollectEditingPointsVisitor::Visit( LogarithmicSpiral& curve )
{
    editingPoints_.push_back( curve.GetCS().pos_ );
}


void CollectEditingPointsVisitor::Visit( Polyline& curve )
{
    for ( int i = 0, c = curve.GetVerticesCount(); i < c; ++i ) {
        editingPoints_.push_back( curve.GetVertex(i) );
    }
}


void CollectEditingPointsVisitor::Visit( BezierCurve& curve )
{
    for ( int i = 0, c = curve.GetControlVerticesCount(); i < c; ++i ) {
        editingPoints_.push_back( curve.GetControlVertex(i) );
    }
}


void CollectEditingPointsVisitor::Visit( HermiteSpline& curve )
{
    for ( int i = 0, c = curve.GetVerticesCount(); i < c; ++i ) {
        editingPoints_.push_back( curve.GetVertex(i) );
    }
}


void CollectEditingPointsVisitor::Visit( EquidistantCurve& /*curve*/ )
{
}


void CollectEditingPointsVisitor::Visit( ProxyCurve& /*curve*/ )
{
}


//-------------------------------------------------------------------------------------------------
// C H A N G E   E D I T I N G   P O I N T   V I S I T O R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


ChangeEditingPointVisitor::ChangeEditingPointVisitor( int editingPointIndex,
    const Matrix3x3& transform )
    : editingPointIndex_( editingPointIndex )
    , transform_( transform )
{
}


ChangeEditingPointVisitor::ChangeEditingPointVisitor( int editingPointIndex,
    const Vector& shift )
    : editingPointIndex_( editingPointIndex )
{
    transform_.InitTransferMatrix( shift );
}


void ChangeEditingPointVisitor::Visit( Segment& curve )
{
    assert( editingPointIndex_ < 2 );
    if ( editingPointIndex_ < 2 ) {
        Point editingPoint = ( editingPointIndex_ == 0 ) ? ( curve.GetP1() ) : ( curve.GetP2() );
        editingPoint.Transform( transform_ );
        ( editingPointIndex_ == 0 ) ? ( curve.SetP1(editingPoint) ) : ( curve.SetP2
            (editingPoint) );
    }
}


void ChangeEditingPointVisitor::Visit( Arc& curve )
{
    assert( editingPointIndex_ == 0 );
    if ( editingPointIndex_ == 0 ) {
        Point editingPoint = curve.GetCenter();
        editingPoint.Transform( transform_ );
        curve.SetCenter( editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( ArchimedianSpiral& curve )
{
    assert( editingPointIndex_ == 0 );
    if ( editingPointIndex_ == 0 ) {
        Point editingPoint = curve.GetCenter();
        editingPoint.Transform( transform_ );
        curve.SetCenter( editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( LogarithmicSpiral& curve )
{
    assert( editingPointIndex_ == 0 );
    if ( editingPointIndex_ == 0 ) {
        Point editingPoint = curve.GetCenter();
        editingPoint.Transform( transform_ );
        curve.SetCenter( editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( Polyline& curve )
{
    assert( editingPointIndex_ < curve.GetVerticesCount() );
    if ( editingPointIndex_ < curve.GetVerticesCount() ) {
        Point editingPoint = curve.GetVertex( editingPointIndex_ );
        editingPoint.Transform( transform_ );
        curve.SetVertex( editingPointIndex_, editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( BezierCurve& curve )
{
    assert( editingPointIndex_ < curve.GetControlVerticesCount() );
    if ( editingPointIndex_ < curve.GetControlVerticesCount() ) {
        Point editingPoint = curve.GetControlVertex( editingPointIndex_ );
        editingPoint.Transform( transform_ );
        curve.SetControlVertex( editingPointIndex_, editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( HermiteSpline& curve )
{
    assert( editingPointIndex_ < curve.GetVerticesCount() );
    if ( editingPointIndex_ < curve.GetVerticesCount() ) {
        Point editingPoint = curve.GetVertex( editingPointIndex_ );
        editingPoint.Transform( transform_ );
        curve.SetVertex( editingPointIndex_, editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( EquidistantCurve& /*curve*/ )
{
    assert( 0 );
}


void ChangeEditingPointVisitor::Visit( ProxyCurve& /*curve*/ )
{
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d

﻿#!/usr/bin/env python


__author__ = 'jne100@gmail.com (Nikita Spiridonov)'


import glob
import os
import re
import sys


def fuse_files(suitableMask, fusedName):
    suitableMasks = [suitableMask]
    hFusedFile = fusedName + ".h"
    cppFusedFile = fusedName + ".cpp"
    delete_file(hFusedFile)
    delete_file(cppFusedFile)
    files = collect_suitable_fusion_targets(*suitableMasks)
    hFiles = list(filter(lambda x: x.endswith(".h"), files))
    cppFiles = list(filter(lambda x: x.endswith(".cpp"), files))
    hFusedText = fuse_h_files(hFiles)
    cppFusedText = fuse_cpp_files(cppFiles, hFiles, hFusedFile)
    write_file_text(hFusedFile, hFusedText)
    write_file_text(cppFusedFile, cppFusedText)


def collect_suitable_fusion_targets(*suitableMasks):
  targets = []
  for mask in suitableMasks:
    targets += glob.glob(mask)
  return targets


def fuse_h_files(hFiles):
    # discard all fusion-target headers from h files
    hTexts = []
    for hFile in h_files_topsort(hFiles):
        hText = read_file_text(hFile)
        hText = discard_included_headers(hText, hFiles, "/*---*/")
        hTexts.append(hText)
    # fuse texts
    return "\n\n".join(hTexts)


def h_files_topsort(hFiles):

    def find_independent_file(hFilesAndDeps):
        for file, dependencies in hFilesAndDeps.items():
            if not dependencies:
                return file
        return None

    hFilesAndPaths = dict([os.path.split(x)[::-1] for x in hFiles])
    hFilesAndDeps = dict([(os.path.split(x)[1], set()) for x in hFiles])
    for file in hFiles:
        includeFiles = extract_included_headers(file)
        includeFiles = [os.path.split(x)[1] for x in includeFiles]
        for includeFile in includeFiles[:]:
            if includeFile not in hFilesAndPaths.keys():
                includeFiles.remove(includeFile)
        hFilesAndDeps[os.path.split(file)[1]] = set(includeFiles)

    hSortedFiles = []
    while hFilesAndDeps:
        # find next file without dependencies
        name = find_independent_file(hFilesAndDeps)
        if not name:
            print("Error: Cyclic dependencies in headers!")
            return []    
        # remove find file from dictionary
        hFilesAndDeps.pop(name, None)
        # delete name from dependencies sets
        for file in hFilesAndDeps:
            if name in hFilesAndDeps[file]:
                hFilesAndDeps[file].remove(name)
        # append name with path to the sorted array
        hSortedFiles.append(os.path.join(hFilesAndPaths[name], name))
    return hSortedFiles


def fuse_cpp_files(cppFiles, hFiles, hFusedFile):
    # discard all fusion-target headers from cpp files
    cppTexts = []
    for cppFile in cppFiles:
        cppText = read_file_text(cppFile)
        cppText = discard_included_headers(cppText, hFiles, "/*---*/")
        cppTexts.append(cppText)
    # include single fused header file
    hFusedFile = os.path.split(hFusedFile)[1]
    cppTexts.insert(0, '#include \"{0}\"\n'.format(hFusedFile))
    # fuse texts
    return "\n\n".join(cppTexts)


def extract_included_headers(file):
    text = read_file_text(file)
    return re.findall('#include\s+[<"]([^>"]*)', text)

    
def discard_included_headers(text, hFiles, stub):
    for hFile in hFiles:
        pattern = '#include\s+[<"]{0}[>"]'.format(os.path.split(hFile)[1])
        text = re.sub(pattern, stub, text)
    return text
    

def delete_file(file):
    try:
        os.remove(file)
    except:
        pass


def read_file_text(name):
    with open(name, "r") as f:
        return f.read()


def write_file_text(name, text):
    with open(name, "w") as f:
        f.write(text)


if len(sys.argv) == 3:
    suitableMask = sys.argv[1]
    fusedName = sys.argv[2]
    fuse_files(suitableMask, fusedName)
    print("Files succesfully fused to {0}.h and {0}.cpp".format(fusedName))
    exit(0)
else:
    scriptName = os.path.split(sys.argv[0])[1]
    print("Usage: {0} %suitable_mask% %fused_name%".format(scriptName))
    print("({0} \"../src/m2d*.*\" m2d_fused)".format(scriptName))
    exit(1)

///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#ifndef _M2D_h
#define _M2D_h


#include <cassert>
#include <cmath>
#include <ostream>
#include <vector>


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C O N S T A N T S
//-------------------------------------------------------------------------------------------------


const double EPSILON_E3 = 1e-3;
const double EPSILON_E4 = 1e-4;
const double EPSILON_E5 = 1e-5;
const double EPSILON_E6 = 1e-6;
const double DOUBLE_PRECISION = EPSILON_E5;

const double E = 2.718281828;
const double PI = 3.141592654;
const double PI2 = PI * 2.0;
const double PI_DIV_2 = PI / 2.0;
const double PI_DIV_3 = PI / 3.0;
const double PI_DIV_4 = PI / 4.0;
const double PI_DIV_6 = PI / 6.0;
const double PI_INV = 1 / PI;
const double PI2_INV = 1 / PI2;
const double PI_DEG = 180.0;
const double PI2_DEG = 360.0;


//-------------------------------------------------------------------------------------------------
// U T I L I T I E S
//-------------------------------------------------------------------------------------------------


class Point;


/*++
    Check that lhs is equal to rhs.
--*/
inline bool EQ( double lhs, double rhs )
{
    return ( ::fabs(rhs - lhs) < DOUBLE_PRECISION );
}


/*++
    Check that lhs is equal to rhs (with use of specified precision).
--*/
inline bool EQ( double lhs, double rhs, double eps )
{
    return ( ::fabs(rhs - lhs) < eps );
}


/*++
    Check that lhs is not equal to rhs.
--*/
inline bool NE( double lhs, double rhs )
{
    return ( ::fabs(rhs - lhs) > DOUBLE_PRECISION );
}


/*++
    Check that lhs is not equal to rhs (with use of specified precision).
--*/
inline bool NE( double lhs, double rhs, double eps )
{
    return ( ::fabs(rhs - lhs) > eps );
}


/*++
    Check that lhs is less or equal than rhs.
--*/
inline bool LE( double lhs, double rhs )
{
    return ( (lhs - rhs) < DOUBLE_PRECISION );
}


/*++
    Check that lhs is less or equal than rhs (with use of specified precision).
--*/
inline bool LE( double lhs, double rhs, double eps )
{
    return ( (lhs - rhs) < eps );
}


/*++
    Check that lhs is less than rhs.
--*/
inline bool LT( double lhs, double rhs )
{
    return ( (lhs - rhs) < -DOUBLE_PRECISION );
}


/*++
    Check that lhs is less than rhs (with use of specified precision).
--*/
inline bool LT( double lhs, double rhs, double eps )
{
    return ( (lhs - rhs) < -eps );
}


/*++
    Tricky check that pair of values (lhsHigh, lhsLow) is less than pair of values
    (rhsHigh, rhsLow). Function makes it possible to keep compound object like points and vectors
    inside set (data structure).
--*/
bool LT( double lhsHigh, double lhsLow, double rhsHigh, double rhsLow );


/*++
    Tricky check that pair of values (lhsHigh, lhsLow) is less than pair of values
    (rhsHigh, rhsLow) (with use of specified precision).
--*/
bool LT( double lhsHigh, double lhsLow, double rhsHigh, double rhsLow, double eps );


/*++
    Check that lhs is greater or equal than rhs.
--*/
inline bool GE( double lhs, double rhs )
{
    return ( (lhs - rhs) > -DOUBLE_PRECISION );
}


/*++
    Check that lhs is greater or equal than rhs (with use of specified precision).
--*/
inline bool GE( double lhs, double rhs, double eps )
{
    return ( (lhs - rhs) > -eps );
}


/*++
    Check that lhs is greater than rhs.
--*/
inline bool GT( double lhs, double rhs )
{
    return ( (lhs - rhs) > DOUBLE_PRECISION );
}


/*++
    Check that lhs is greater than rhs (with use of specified precision).
--*/
inline bool GT( double lhs, double rhs, double eps )
{
    return ( (lhs - rhs) > eps );
}


/*++
    Check that value equals zero.
--*/
inline bool EQNil( double value )
{
    return ( ::fabs(value) < DOUBLE_PRECISION );
}


/*++
    Check that value equals zero (with use of specified precision).
--*/
inline bool EQNil( double value, double eps )
{
    return ( ::fabs(value) < eps );
}


/*++
    Check that value not equals zero.
--*/
inline bool NENil( double value )
{
    return ( ::fabs(value) > DOUBLE_PRECISION );
}


/*++
    Check that value not equals zero (with use of specified precision).
--*/
inline bool NENil( double value, double eps )
{
    return ( ::fabs(value) > eps );
}


/*++
    Check that lhsAngle is equal to rhsAngle (two angles are equal if cosine of their difference
    is equal to 1.0).
--*/
inline bool EQAngles( double lhsAngle, double rhsAngle )
{
    return EQ( ::cos(lhsAngle - rhsAngle), 1.0 );
}


/*++
    Check that interval [begin, end] include value (begin <= value <= end).
--*/
inline bool IsInsideClosedInterval( double value, double begin, double end )
{
    return ( GE(value, begin) && LE(value, end) );
}


/*++
    Check that interval (begin, end) include value (begin < value < end).
--*/
inline bool IsInsideOpenInterval( double value, double begin, double end )
{
    return ( GT(value, begin) && LT(value, end) );
}


/*++
    Method that round real value by mathematical rules.
--*/
inline double Round( double value )
{
    double valueFloor = ::floor( value );
    return ( ((value - valueFloor) < 0.5) ? (valueFloor) : (valueFloor + 1.0) );
}


/*++
    Method that convert degrees to radians.
--*/
inline double Radians( double degrees )
{
    return ( degrees * (PI / PI_DEG) );
}


/*++
    Method that convert radians to degrees.
--*/
inline double Degrees( double radians )
{
    return ( radians * (PI_DEG / PI) );
}


/*++
    Normalize value from the interval [0, endValue] to the interval [0, targetEndValue].
--*/
inline double Normalize( double value, double endValue, double targetEndValue = 1.0 )
{
    return ( value / endValue ) * targetEndValue;
}


/*++
    Normalize value from the interval [0, endValue] to the interval [0, targetEndValue].
--*/
inline double Normalize( double value, double beginValue, double endValue,
    double targetBeginValue, double targetEndValue )
{
    return ( (value - beginValue) / (endValue - beginValue) ) *
        ( targetEndValue - targetBeginValue ) + targetBeginValue;
}


/*++
    Normalize value from the interval [0, endValue] to the interval [0, targetEndValue] with
    retrenchment by lower and upper bound.
--*/
double StrictNormalize( double value, double endValue, double targetEndValue = 1.0 );


/*++
    Normalize value from the interval [beginValue, endValue] to the interval
    [targetBeginValue, targetEndValue] with retrenchment by lower and upper bound.
--*/
double StrictNormalize( double value, double beginValue, double endValue,
    double targetBeginValue, double targetEndValue );


/*++
    Normalize angle to interval [0, PI2].
--*/
inline double NormalizeAngle( double angle )
{
    double n = ::floor( angle / PI2 );
    return ( angle - n * PI2 );
}


/*++
    Calculation of minimum value.
--*/
inline double CalcMin( double value1, double value2 )
{
    return ( value1 < value2 ? value1 : value2 );
}


/*++
    Calculation of maximum value.
--*/
inline double CalcMax( double value1, double value2 )
{
    return ( value1 > value2 ? value1 : value2 );
}


/*++
    Calculation of distance between two points.
--*/
double CalcDistance( double x1, double y1, double x2, double y2 );


/*++
    Calculation of distance between two points.
--*/
double CalcDistance( const Point& p1, const Point& p2 );


/*++
    Calculation of middle point of two points.
--*/
Point CalcMiddlePoint( const Point& p1, const Point& p2 );


//-------------------------------------------------------------------------------------------------
// P A I R
//-------------------------------------------------------------------------------------------------


/*++
    Pair.
--*/
class Pair {
public:
    Pair();
    Pair( const Pair& rhs );
    Pair( double v1, double v2 );
    Pair& operator =( const Pair& rhs );

    // modifications
    void Init( double v1, double v2 );

    // operators
    void operator +=( const Pair& rhs );
    void operator -=( const Pair& rhs );
    bool operator ==( const Pair& rhs ) const;
    bool operator !=( const Pair& rhs ) const;
    bool operator <( const Pair& rhs ) const;

public:
    double v1_;
    double v2_;
};


std::ostream& operator <<( std::ostream& os, const Pair& rhs );


//-------------------------------------------------------------------------------------------------
// I N T E R V A L
//-------------------------------------------------------------------------------------------------


/*++
    Interval.
--*/
class Interval {
public:
    Interval();
    Interval( const Interval& rhs );
    Interval( double begin, double end );
    Interval& operator =( const Interval& rhs );

    // modifications
    void Init( double begin, double end );
    void Widen( double beginDelta, double endDelta );
    void WidenOnNormalizedValue( double beginNormalizedDelta, double endNormalizedDelta );

    // calculations
    double CalcMiddle() const;
    double CalcLength() const;
    double CalcValueByNormalizedValue( double notmalizedValue ) const;
    double CalcNormalizedValue( double value ) const;
    bool IsInclude( double value ) const;

    // operators
    bool operator ==( const Interval& rhs ) const;
    bool operator !=( const Interval& rhs ) const;

public:
    double begin_;
    double end_;
};


std::ostream& operator <<( std::ostream& os, const Interval& rhs );


const Interval ZERO_INTERVAL( 0.0, 0.0 );
const Interval IDENTITY_INTERVAL( 0.0, 1.0 );
const Interval PI_INTERVAL( 0.0, PI );
const Interval PI2_INTERVAL( 0.0, PI2 );
const Interval PI_DIV_2_INTERVAL( 0.0, PI_DIV_2 );
const Interval PI_DIV_3_INTERVAL( 0.0, PI_DIV_3 );
const Interval PI_DIV_4_INTERVAL( 0.0, PI_DIV_4 );
const Interval PI_DIV_6_INTERVAL( 0.0, PI_DIV_6 );


//-------------------------------------------------------------------------------------------------
// P O I N T
//-------------------------------------------------------------------------------------------------


class Vector;
class Matrix3x3;


/*++
    Mathematical two-dimensional point.
--*/
class Point {
public:
    Point();
    Point( const Point& rhs );
    Point( double x, double y );
    Point& operator =( const Point& rhs );

    // modifications
    void Zero();
    void Init( double x, double y );
    void Transfer( const Vector& shift );
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

    // operators
    void operator +=( const Point& rhs );
    void operator -=( const Point& rhs );
    void operator *=( double coef );
    bool operator ==( const Point& rhs ) const;
    bool operator !=( const Point& rhs ) const;
    bool operator <( const Point& rhs ) const;

public:
    double x_;
    double y_;
};


std::ostream& operator <<( std::ostream& stream, const Point& rhs );


const Point ZERO_POINT( 0.0, 0.0 );


//-------------------------------------------------------------------------------------------------
// V E C T O R
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical two-dimensional vector.
--*/
class Vector {
public:
    Vector();
    Vector( const Vector& rhs );
    Vector( double x, double y );
    Vector( const Point& from, const Point& to );
    Vector& operator =( const Vector& rhs );

    // modifications
    void Zero();
    void Init( double x, double y );
    void Normalize();
    void Invert();
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

    // calculations
    double CalcLength() const;
    double CalcAngle() const;
    double CalcAngle( const Vector& v ) const;
    double CalcRelativeAngle( const Vector& v ) const;
    double CalcDot( const Vector& v ) const;
    Vector CalcPerpendicular( bool direction = true ) const;
    bool IsZero() const;
    bool IsNormalized() const;
    bool IsCollinear( const Vector& v ) const;
    bool IsCodirectional( const Vector& v ) const;

    // operators
    void operator +=( const Vector& rhs );
    void operator -=( const Vector& rhs );
    void operator *=( double coef );
    bool operator ==( const Vector& rhs ) const;
    bool operator !=( const Vector& rhs ) const;

public:
    double x_;
    double y_;
};


std::ostream& operator <<( std::ostream& os, const Vector& rhs );


const Vector ZERO_VECTOR( 0.0, 0.0 );
const Vector GLOBAL_X_AXIS( 1.0, 0.0 );
const Vector GLOBAL_Y_AXIS( 0.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 1
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical 2x1 matrix.
--*/
class Matrix2x1 {
public:
    Matrix2x1();
    Matrix2x1( const Matrix2x1& rhs );
    Matrix2x1( double m00, double m10 );
    Matrix2x1& operator =( const Matrix2x1& rhs );

    // initialization methods
    void Init( double m00, double m10 );
    void InitZeroMatrix();
    void InitIdentityMatrix();

    // operators
    void operator +=( const Matrix2x1& rhs );
    void operator -=( const Matrix2x1& rhs );
    void operator *=( double coef );

public:
    static const int M = 2;
    static const int N = 1;
    double m_[ M ][ N ];
};


std::ostream& operator <<( std::ostream& os, const Matrix2x1& rhs );


const Matrix2x1 ZERO_MATRIX_2X1( 0.0, 0.0 );
const Matrix2x1 IDENTITY_MATRIX_2X1( 1.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 2
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical 2x2 matrix.
--*/
class Matrix2x2 {
public:
    Matrix2x2();
    Matrix2x2( const Matrix2x2& rhs );
    Matrix2x2( double m00, double m01, double m10, double m11 );
    Matrix2x2& operator =( const Matrix2x2& rhs );

    // initialization methods
    void Init( double m00, double m01, double m10, double m11 );
    void InitZeroMatrix();
    void InitIdentityMatrix();

    // modifications
    void Invert();

    // operators
    void operator +=( const Matrix2x2& rhs );
    void operator -=( const Matrix2x2& rhs );
    void operator *=( double coef );
    void operator *=( const Matrix2x2& rhs );
    Matrix2x1 operator *( const Matrix2x1& rhs );

public:
    static const int M = 2;
    static const int N = 2;
    double m_[ M ][ N ];
};


std::ostream& operator <<( std::ostream& os, const Matrix2x2& rhs );


const Matrix2x2 ZERO_MATRIX_2X2( 0.0, 0.0, 0.0, 0.0 );
const Matrix2x2 IDENTITY_MATRIX_2X2( 1.0, 1.0, 1.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// M A T R I X 3 X 3
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical 3x3 matrix.
--*/
class Matrix3x3 {
public:
    Matrix3x3();
    Matrix3x3( const Matrix3x3& rhs );
    Matrix3x3( double m00, double m01, double m02,
        double m10, double m11, double m12,
        double m20, double m21, double m22 );
    Matrix3x3& operator =( const Matrix3x3& rhs );

    // initialization methods
    void Init( double m00, double m01, double m02,
        double m10, double m11, double m12,
        double m20, double m21, double m22 );
    void InitZeroMatrix();
    void InitIdentityMatrix();
    void InitTransferMatrix( const Vector& shift );
    void InitInvertedTransferMatrix( const Vector& shift );
    void InitScaleMatrix( double xCoef, double yCoef, const Point* pCenter = 0 );
    void InitRotationMatrix( double angle, const Point* pCenter = 0 );

    // modifications
    void Invert();

    // operators
    void operator +=( const Matrix3x3& rhs );
    void operator -=( const Matrix3x3& rhs );
    void operator *=( double coef );
    void operator *=( const Matrix3x3& rhs );

public:
    static const int M = 3;
    static const int N = 3;
    double m_[ M ][ N ];
};


std::ostream& operator <<( std::ostream& os, const Matrix3x3& rhs );


const Matrix3x3 ZERO_MATRIX_3X3( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 );
const Matrix3x3 IDENTITY_MATRIX_3X3( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// C S
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical two-dimensional coordinate system.
--*/
class CS {
public:
    CS();
    CS( const CS& rhs );
    CS( const Point& pos, double angle = 0.0 );
    CS& operator =( const CS& rhs );

    // initialization methods
    void InitDefaultCS();
    void Init( const Point& pos, const Vector& axisX, const Vector& axisY );
    void Init( const Point& pos, double angle = 0.0 );

    // modifications
    void Transfer( const Vector& shift );
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

    // calculations
    void CalcMatrixFromCS( Matrix3x3& m ) const;
    void CalcMatrixIntoCS( Matrix3x3& m ) const;

    // operators
    bool operator ==( const CS& rhs ) const;
    bool operator !=( const CS& rhs ) const;

public:
    Point pos_;
    Vector axisX_;
    Vector axisY_;
};


/*++
    Transform something from local coordinate system to global coordinate system.
--*/
template <typename T>
inline void TransformFromCS( const CS& cs, T& item )
{
    Matrix3x3 m;
    cs.CalcMatrixFromCS( m );
    item.Transform( m );
}


/*++
    Transform something from global coordinate system to local coordinate system.
--*/
template <typename T>
inline void TransformIntoCS( const CS& cs, T& item )
{
    Matrix3x3 m;
    cs.CalcMatrixIntoCS( m );
    item.Transform( m );
}


std::ostream& operator <<( std::ostream& os, const CS& rhs );


const CS DEFAULT_CS;


//-------------------------------------------------------------------------------------------------
// R E C T
//-------------------------------------------------------------------------------------------------


/*++
    Rectanlge.
--*/
class Rect {
public:
    Rect();
    Rect( const Rect& rhs );
    Rect( const Point& p1, const Point& p2 );
    Rect( double x1, double y1, double x2, double y2 );
    Rect( double width, double height );
    Rect& operator =( const Rect& rhs );

    // modifications
    void Init( double x1, double y1, double x2, double y2 );
    void Init( double width, double height );
    void Transfer( const Vector& shift );
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Inflate( double dWidth, double dHeight );
    void Deflate( double dWidth, double dHeight );

    // calculations
    double CalcWidth() const;
    double CalcHeight() const;
    double CalcArea() const;
    double CalcAspectRatio() const;
    Point CalcCenter() const;
    bool IsInclude( const Point& rhs ) const;

    // operators
    bool operator ==( const Rect& rhs ) const;
    bool operator !=( const Rect& rhs ) const;

public:
    Point minPoint_;
    Point maxPoint_;
};


std::ostream& operator <<( std::ostream& os, const Rect& rhs );


//-------------------------------------------------------------------------------------------------
// B O U N D I N G   B O X
//-------------------------------------------------------------------------------------------------


/*++
    Bounding box.
--*/
class BoundingBox {
public:
    BoundingBox();
    BoundingBox( const BoundingBox& rhs );
    BoundingBox& operator =( const BoundingBox& rhs );

    // specific methods
    void SetEmpty();
    void Add( const BoundingBox& rhs );
    void Add( const Point& rhs );

    // modifications
    void Transfer( const Vector& shift );
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Inflate( double dWidth, double dHeight );
    void Deflate( double dWidth, double dHeight );

    // calculations
    double CalcWidth() const;
    double CalcHeight() const;
    double CalcArea() const;
    double CalcAspectRatio() const;
    Point CalcCenter() const;
    bool IsEmpty() const;

    // accessors
    const Point& GetMinPoint() const;
    const Point& GetMaxPoint() const;

    // operators
    bool operator ==( const BoundingBox& rhs ) const;
    bool operator !=( const BoundingBox& rhs ) const;

private:
    Rect rect_;
};


BoundingBox& operator <<( BoundingBox& lhs, const BoundingBox& rhs );


BoundingBox& operator <<( BoundingBox& lhs, const Point& rhs );


//-------------------------------------------------------------------------------------------------
// F A C T O R I A L   C A L C U L A T O R
//-------------------------------------------------------------------------------------------------


/*++
    Factorial calculator.
--*/
class FactorialCalculator {
public:
    FactorialCalculator( int precalcValuesCount = 100 );
    ~FactorialCalculator();

    double Calc( int value ) const;

private:
    FactorialCalculator( const FactorialCalculator& rhs );
    FactorialCalculator& operator =( const FactorialCalculator& rhs );

private:
    int precalcValuesCount_;
    double* pPrecalcValues_;
};


const FactorialCalculator FACTORIAL_CALCULATOR;


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


#endif


///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#ifndef _M2D_CURVES_h
#define _M2D_CURVES_h


#include <cassert>
#include <ostream>
#include <vector>
/*---*/


namespace m2d {


//-------------------------------------------------------------------------------------------------
// R E F   E N T I T Y
//-------------------------------------------------------------------------------------------------


/*++
    Base class of entities with reference counting.
--*/
class ReferenceEntity {
public:
    ReferenceEntity();
    virtual ~ReferenceEntity();

    friend void AddReference( const ReferenceEntity* pEntity );
    friend void ReleaseReference( const ReferenceEntity* pEntity );

private:
    void InternalAddReference() const;
    void InternalReleaseReference() const;

private:
    mutable int referenceCount_;
};


inline ReferenceEntity::ReferenceEntity()
    : referenceCount_( 0 )
{
}


inline ReferenceEntity::~ReferenceEntity()
{
    assert( referenceCount_ == 0 );
}


inline void ReferenceEntity::InternalAddReference() const
{
    ++referenceCount_;
}


inline void ReferenceEntity::InternalReleaseReference() const
{
    --referenceCount_;
    assert( referenceCount_ >= 0 );
}


inline void AddReference( const ReferenceEntity* pEntity )
{
    assert( pEntity != 0 );
    pEntity->InternalAddReference();
}


inline void ReleaseReference( const ReferenceEntity* pEntity )
{
    assert( pEntity != 0 );
    pEntity->InternalReleaseReference();
    if ( pEntity->referenceCount_ == 0 ) {
        delete pEntity;
    }
}


inline void SafeAddReference( const ReferenceEntity* pEntity )
{
    if ( pEntity != 0 ) {
        AddReference( pEntity );
    }
}


inline void SafeReleaseReference( const ReferenceEntity* pEntity )
{
    if ( pEntity != 0 ) {
        ReleaseReference( pEntity );
    }
}


//-------------------------------------------------------------------------------------------------
// C U R V E
//-------------------------------------------------------------------------------------------------


class CurveVisitor;


enum ECurveTypes {
    CT_SEGMENT = 0,
    CT_ARC,
    CT_ARCHIMEDIAN_SPIRAL,
    CT_LOGARITHMIC_SPIRAL,
    CT_POLYLINE,
    CT_BEZIER_CURVE,
    CT_HERMITE_SPLINE,
    CT_EQUIDISTANT_CURVE,
    CT_PROXY_CURVE,
    CT_COUNT,
};


enum ECurveCharacterPoints {
    CHP_BEGIN,
    CHP_MIDDLE,
    CHP_END,
};


/*++
    Base class of all mathematical curves.
--*/
class Curve : public ReferenceEntity {
public:
    virtual ~Curve() {}
    virtual Curve* Clone() const = 0;
    virtual void Accept( CurveVisitor& visitor ) = 0;

    //  accessors
    virtual ECurveTypes GetType() const = 0;
    virtual const Interval& GetParametricInterval() const = 0;

    // calculations
    virtual Point CalcCharacterPoint( ECurveCharacterPoints characterPoint ) const;
    virtual Point CalcPointOnCurve( double t ) const = 0;
    virtual Vector CalcFirstDerivative( double t ) const = 0;
    virtual Vector CalcSecondDerivative( double t ) const = 0;
    virtual Vector CalcNormal( double t, bool direction = true ) const;
    virtual double CalcDistanceToPoint( const Point& p ) const;
    virtual double CalcPointProjection( const Point& p ) const;
    virtual double CalcLength() const;
    virtual void CalcBoundingBox( BoundingBox& box ) const;
    virtual double CalcCurvature( double t ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift ) = 0;
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 ) = 0;
    virtual void Rotate( double angle, const Point* pCenter = 0 ) = 0;
    virtual void Transform( const Matrix3x3& m ) = 0;
};


//-------------------------------------------------------------------------------------------------
// A N A L Y T I C A L   C U R V E
//-------------------------------------------------------------------------------------------------


/*++
    Base class of mathematical analytical curve.
    Curve called analytical if curves coordinatees in some coordinate system can be described with
    the help of analytical equations with no use of points, vectors or other curves.
--*/
class AnalyticalCurve : public Curve {
public:
    AnalyticalCurve();
    AnalyticalCurve( const AnalyticalCurve& rhs );
    AnalyticalCurve( const CS& cs );
    AnalyticalCurve( const Point& pos, double angle = 0.0 );
    virtual ~AnalyticalCurve() {}
    AnalyticalCurve& operator =( const AnalyticalCurve& rhs );

    // specific methods
    const CS& GetCS() const;
    void GetCS( const CS& cs );
    const Point& GetCenter() const;
    void SetCenter( const Point& p );

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

protected:
    CS cs_;
};


//-------------------------------------------------------------------------------------------------
// S E G M E N T
//-------------------------------------------------------------------------------------------------


/*++
    Line segment (mathematical curve).
--*/
class Segment : public Curve {
public:
    Segment();
    Segment( const Point& p1, const Point& p2 );
    Segment( const Segment& rhs );
    virtual ~Segment() {}
    Segment& operator =( const Segment& rhs );
    virtual Segment* Clone() const { return new Segment( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    const Point& GetP1() const;
    const Point& GetP2() const;
    void SetP1( const Point& p );
    void SetP2( const Point& p );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcPointProjection( const Point& p ) const;
    virtual double CalcLength() const;
    virtual void CalcBoundingBox( BoundingBox& box ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

private:
    static const Interval INTERVAL;

private:
    Point p1_;
    Point p2_;
};


//-------------------------------------------------------------------------------------------------
// A R C
//-------------------------------------------------------------------------------------------------


/*++
    Arc (mathematical curve).
--*/
class Arc : public AnalyticalCurve {
public:
    Arc( double radius, const Interval* pInterval = 0 );
    Arc( double radius, const Point& center, const Interval* pInterval = 0 );
    Arc( double radius, const CS& cs, const Interval* pInterval = 0 );
    Arc( const Point& p1, const Point& p2, const Point& p3, bool isClosed = false );
    Arc( const Arc& rhs );
    virtual ~Arc() {}
    Arc& operator =( const Arc& rhs );
    virtual Arc* Clone() const { return new Arc( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // initialization methods
    void Reset();
    void Init( double radius, const Interval* pInterval = 0 );
    void Init( double radius, const Point& center, const Interval* pInterval = 0 );
    void Init( double radius, const CS& cs, const Interval* pInterval = 0 );
    void Init( const Point& p1, const Point& p2, const Point& p3, bool isClosed = false );

    // specific methods
    double GetRadius() const;
    bool IsClosed() const;
    void SetRadius( double radius );
    void SetClosed();
    void SetSector( double start, double end );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcLength() const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

private:
    bool CalcCenterByPoints( const Point& p1, const Point& p2, const Point& p3,
        Point& result ) const;

private:
    static const Interval CIRCLE_INTERVAL;

private:
    double radius_;
    Interval interval_;
};


//-------------------------------------------------------------------------------------------------
// S P I R A L
//-------------------------------------------------------------------------------------------------


/*++
    Base class of spirals.
--*/
class Spiral : public AnalyticalCurve {
public:
    Spiral( const CS& cs, double radius, double angle );
    Spiral( const Spiral& rhs );
    virtual ~Spiral() {}
    Spiral& operator =( const Spiral& rhs );

    // specific methods
    double GetRadius() const;
    void SetRadius( double radius );
    double GetAngle() const;
    void SetAngle( double angle );

    // accessors
    virtual const Interval& GetParametricInterval() const;

    // modifications
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );

protected:
    double radius_;
    Interval interval_;
};


//-------------------------------------------------------------------------------------------------
// A R C H I M E D I A N   S P I R A L
//-------------------------------------------------------------------------------------------------


/*++
    Archimedean spiral (mathematical curve).
--*/
class ArchimedianSpiral : public Spiral {
public:
    ArchimedianSpiral( const CS& cs, double radius, double angle );
    ArchimedianSpiral( const ArchimedianSpiral& rhs );
    virtual ~ArchimedianSpiral() {}
    ArchimedianSpiral& operator =( const ArchimedianSpiral& rhs );
    virtual ArchimedianSpiral* Clone() const { return new ArchimedianSpiral( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // accessors
    virtual ECurveTypes GetType() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcLength() const;
};


//-------------------------------------------------------------------------------------------------
// L O G A R I T H M I C   S P I R A L
//-------------------------------------------------------------------------------------------------


/*++
    Logarithmic spiral (mathematical curve).
--*/
class LogarithmicSpiral : public Spiral {
public:
    LogarithmicSpiral( const CS& cs, double radius, double angle, double b );
    LogarithmicSpiral( const LogarithmicSpiral& rhs );
    virtual ~LogarithmicSpiral() {}
    LogarithmicSpiral& operator =( const LogarithmicSpiral& rhs );
    virtual LogarithmicSpiral* Clone() const { return new LogarithmicSpiral( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    double GetB() const;
    void SetB( double b );

    // accessors
    virtual ECurveTypes GetType() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcLength() const;

private:
    double b_;
};


//-------------------------------------------------------------------------------------------------
// P O L Y L I N E
//-------------------------------------------------------------------------------------------------


/*++
    Polyline (mathematical curve).
--*/
class Polyline : public Curve {
public:
    Polyline();
    Polyline( const std::vector<Point>& vertices );
    Polyline( const Polyline& rhs );
    virtual ~Polyline() {}
    Polyline& operator =( const Polyline& rhs );
    virtual Polyline* Clone() const { return new Polyline( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    int GetVerticesCount() const;
    const Point& GetVertex( int index ) const;
    void SetVertex( int index, const Point& vertex );
    void AddVertex( const Point& vertex, int index = -1 );
    void RemoveVertex( int index );
    void RemoveVertices();

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcLength() const;
    virtual void CalcBoundingBox( BoundingBox& box ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

private:
    void UpdateInterval();
    void GetSegmentPoints( double t, const Point*& pStartPoint, const Point*& pEndPoint,
        double& localT) const;

private:
    std::vector<Point> vertices_;
    Interval interval_;
};


//-------------------------------------------------------------------------------------------------
// B E Z I E R   C U R V E
//-------------------------------------------------------------------------------------------------


/*++
    Bezier curve (mathematical curve).
--*/
class BezierCurve : public Curve {
public:
    BezierCurve( const Point& p0, const Point& p1, const Point& p2 );
    BezierCurve( const std::vector<Point>& controlVertices );
    BezierCurve( const BezierCurve& rhs );
    virtual ~BezierCurve();
    BezierCurve& operator =( const BezierCurve& rhs );
    virtual BezierCurve* Clone() const { return new BezierCurve( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    int GetOrder() const;
    int GetControlVerticesCount() const;
    const Point& GetControlVertex( int index ) const;
    void GetControlVertices( std::vector<Point>& vertices ) const;
    void SetControlVerticesCount( int count );
    void SetControlVertex( int index, const Point& vertex );
    void SetControlVertices( const std::vector<Point>& vertices );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

private:
    double B( int n, int i, double t ) const;
    void D1( int i, Vector& result ) const;
    void D2( int i, Vector& result ) const;
    void AllocateControlVertices( int count );
    void CleanupControlVertices();

private:
    static const Interval INTERVAL;

private:
    int controlVerticesCount_;
    Point* pControlVertices_;
};


//-------------------------------------------------------------------------------------------------
// H E R M I T E   S P L I N E
//-------------------------------------------------------------------------------------------------


/*++
    Hermite spline (mathematical curve).
    Interpolated (instead of approximated) cubic Hermite spline to be exact.
--*/
class HermiteSpline : public Curve {
public:
    HermiteSpline();
    HermiteSpline( const std::vector<Point>& vertices );
    HermiteSpline( const HermiteSpline& rhs );
    virtual ~HermiteSpline() {}
    HermiteSpline& operator =( const HermiteSpline& rhs );
    virtual HermiteSpline* Clone() const { return new HermiteSpline( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    int GetVerticesCount() const;
    const Point& GetVertex( int index ) const;
    void SetVertex( int index, const Point& vertex );
    void AddVertex( const Point& vertex, int index = -1 );
    void RemoveVertex( int index );
    void RemoveVertices();
    bool RearrageVertices( int newVerticesCount );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

private:
    void UpdateInterval();
    void GetLocalIntervalPoints( double t, const Point*& pP0, const Point*& pP1, int& nP0 ) const;
    void CalcRadiusVector( double t, double a0, double a1, double b0, double b1, double& x,
        double& y ) const;
    double CalcLocalIntervalParametricValue( double t ) const;
    bool CalcLocalIntervalTangent( int i, Vector& q ) const;

private:
    std::vector<Point> vertices_;
    Interval interval_;
};


//-------------------------------------------------------------------------------------------------
// E Q U I D I S T A N T   C U R V E
//-------------------------------------------------------------------------------------------------


/*++
    Equidistant curve (mathematical curve).
--*/
class EquidistantCurve : public Curve {
public:
    EquidistantCurve( Curve* pSourceCurve, const Vector& shiftDirection );
    EquidistantCurve( const EquidistantCurve& rhs );
    virtual ~EquidistantCurve();
    EquidistantCurve& operator =( const EquidistantCurve& rhs );
    virtual EquidistantCurve* Clone() const { return new EquidistantCurve( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    const Curve& GetSourceCurve() const;
    const Vector& GetShiftDirection() const;
    void SetSourceCurve( Curve* pSourceCurve );
    void SetShiftDirection( const Vector& direction );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

private:
    Curve* pSourceCurve_;
    Vector shiftDirection_;
};


//-------------------------------------------------------------------------------------------------
// P R O X Y   C U R V E
//-------------------------------------------------------------------------------------------------


/*++
    Proxy curve that refers to another curve and can redefine it's parametric domain.
--*/
class ProxyCurve : public Curve {
public:
    ProxyCurve( Curve* pSourceCurve );
    ProxyCurve( Curve* pSourceCurve, const Interval& interval );
    ProxyCurve( const ProxyCurve& rhs );
    virtual ~ProxyCurve();
    ProxyCurve& operator =( const ProxyCurve& rhs );
    virtual ProxyCurve* Clone() const { return new ProxyCurve( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    const Curve& GetSourceCurve() const;
    void SetSourceCurve( Curve* pSourceCurve );
    void SetParametricInterval( const Interval& interval );
    void ResetParametricInterval();

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

private:
    Curve* pSourceCurve_;
    Interval* pInterval_;
};


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


#endif


///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#ifndef _M2D_NUMERICS_h
#define _M2D_NUMERICS_h


#include <cassert>
#include <set>
#include <vector>
/*---*/


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C U R V E   U T I L I T I E S
//-------------------------------------------------------------------------------------------------


/*++
    Calculation of two or more points uniformly distributed on specified curve.
--*/
void CalcUniformPointsOnCurve( const Curve& curve, int pointsCount, std::vector<double>& points,
    const Interval* pInterval = 0 );


/*++
    Calculation of two or more points uniformly distributed on specified curve.
--*/
void CalcUniformPointsOnCurve( const Curve& curve, int pointsCount, std::vector<Point>& points,
    const Interval* pInterval = 0 );


/*++
    Adaptive calculation of points on specified curve (da parameter represents maximum tilt angle
    of tangent directions in adjacent points).
--*/
void CalcPointsOnCurve( const Curve& curve, double da, std::vector<double>& points,
    const Interval* pInterval = 0 );


/*++
    Adaptive calculation of points on specified curve (da parameter represents maximum tilt angle
    of tangent directions in adjacent points).
--*/
void CalcPointsOnCurve( const Curve& curve, double da, std::vector<Point>& points,
    const Interval* pInterval = 0 );


//-------------------------------------------------------------------------------------------------
// T E S S E L A T I O N   U T I L I T I E S
//-------------------------------------------------------------------------------------------------


/*++
--*/
class SegmentDescription {
public:
    Point p1_;
    Point p2_;
};


/*++
--*/
class QuadraticBezierCurveDescription {
public:
    Point p1_;
    Point p2_;
    Point p3_;
};


/*++
    Calculation of approximated representation of mathematical curve using line segments.
--*/
void CalcCurveUniformTesselation( const Curve& curve, int segmentsCount,
    std::vector<SegmentDescription>& segments, const Interval* pInterval = 0 );


/*++
    Adaptive calculation of approximated representation of mathematical curve using line segments
    (da parameter represents maximum tilt angle of tangent directions in adjacent points).
--*/
void CalcCurveTesselation( const Curve& curve, double da,
    std::vector<SegmentDescription>& segments, const Interval* pInterval = 0 );


/*++
    Calculation of approximated representation of mathematical curve using quadratic Bezier
    curves.
--*/
void CalcCurveSmoothTesselation( const Curve& curve, int beziersCount,
    std::vector<QuadraticBezierCurveDescription>& beziers,
    const Interval* pInterval = 0 );


/*++
    Calculation of smooth representation of polyline using quadratic Bezier curves.
--*/
void CalcPolylineClosedSmoothTesselation( const Polyline& polyline,
    std::vector<QuadraticBezierCurveDescription>& beziers );


//-------------------------------------------------------------------------------------------------
// I N T E R S E C T I O N   U T I L I T I E S
//-------------------------------------------------------------------------------------------------


class ICurvesIntersectionFunctor;


/*++
    Calculation of intersection point parametric values of two lines each represented by one
    point and direction vector.
--*/
bool CalcLinesIntersectionPoint( const Point& p1, const Vector& v1, const Point& p2,
    const Vector& v2, double& t1, double& t2 );


/*++
    Calculation of intersection point of two lines each represented by one point and direction
    vector.
--*/
bool CalcLinesIntersectionPoint( const Point& p1, const Vector& v1, const Point& p2,
    const Vector& v2, Point& result );


/*++
    Calculation of all intersection points of two curves inside parametric intervals.
--*/
void CalcCurvesIntersectionPoints( const Curve& curve1, const Curve& curve2,
    std::vector<Pair>& points, const Interval* pInterval1 = 0, const Interval* pInterval2 = 0 );


/*++
    Calculation of all intersection points of two curves inside parametric intervals.
--*/
void CalcCurvesIntersectionPoints( const Curve& curve1, const Curve& curve2,
    std::vector<Point>& points, const Interval* pInterval1 = 0,
    const Interval* pInterval2 = 0 );


/*++
    Calculation of all intersection points of two curves with the use of specified functor.
--*/
void CalcCurvesIntersectionPoints( ICurvesIntersectionFunctor& functor, const Curve& curve1,
    const Curve& curve2, std::vector<Pair>& points, const Interval* pInterval1 = 0,
    const Interval* pInterval2 = 0 );


/*++
    Calculation of all intersection points of two curves with the use of specified functor.
--*/
void CalcCurvesIntersectionPoints( ICurvesIntersectionFunctor& functor, const Curve& curve1,
    const Curve& curve2, std::vector<Point>& points, const Interval* pInterval1 = 0,
    const Interval* pInterval2 = 0 );


//-------------------------------------------------------------------------------------------------
// P R O J E C T I O N   U T I L I T I E S
//-------------------------------------------------------------------------------------------------


/*++
    Calculation of projection of vector src on vector dest.
--*/
void CalcVectorProjection( const Vector& src, const Vector& dest, Vector& result );


/*++
    Calculation of projection length of vector src on vector dest.
--*/
double CalcVectorProjectionLength( const Vector& src, const Vector& dest );


//-------------------------------------------------------------------------------------------------
// I   C U R V E S   I N T E R S E C T I O N   F U N C T O R
//-------------------------------------------------------------------------------------------------


/*++
    Interface aimed calculation of intersection points of two curves.
--*/
class ICurvesIntersectionFunctor {
public:
    virtual ~ICurvesIntersectionFunctor() {}
    virtual void CalcIntersections( const Curve& c1, const Curve& c2, std::vector<Pair>& points,
        const Interval* pI1, const Interval* pI2 ) = 0;
};


//-------------------------------------------------------------------------------------------------
// C U R V E S   I N T E R S E C T I O N   F U N C T O R
//-------------------------------------------------------------------------------------------------


/*++
    Class aimed for serial calculation of intersection points of two curves with the help of
    iterative methods (null approximation) and solution of polynomial equations system by Newton's
    method (exact intersection).
--*/
class CurvesIntersectionFunctor : public ICurvesIntersectionFunctor {
public:
    CurvesIntersectionFunctor();
    virtual ~CurvesIntersectionFunctor() {}
    virtual void CalcIntersections( const Curve& c1, const Curve& c2, std::vector<Pair>& points,
        const Interval* pI1, const Interval* pI2 );

private:
    void CalcTiltStepsOnCurve( const Curve& c, const Interval& span,
        std::vector<double>& steps ) const;
    bool CalcNullApproximation( Pair& tNull, Pair& tDefect ) const;
    bool CalcExactIntersection( const Pair& tNull, const Pair& tDefect, Pair& tExact ) const;

public:
    static const double DEFAULT_DA;
    double da_;
    double metricEps_;

private:
    const Curve* pC1_;
    const Curve* pC2_;
    Pair t_;
    Pair dt_;
    std::set<Pair> pointsSet_;
};


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


#endif


///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#ifndef _M2D_VISITORS_h
#define _M2D_VISITORS_h


#include <cassert>
#include <vector>
/*---*/
/*---*/


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C U R V E   V I S I T O R
//-------------------------------------------------------------------------------------------------


/*++
    Interface of mathematical curve visitor.
--*/
class CurveVisitor {
public:
    virtual ~CurveVisitor() {}
    virtual void Visit( Segment& curve ) = 0;
    virtual void Visit( Arc& curve ) = 0;
    virtual void Visit( ArchimedianSpiral& curve ) = 0;
    virtual void Visit( LogarithmicSpiral& curve ) = 0;
    virtual void Visit( Polyline& curve ) = 0;
    virtual void Visit( BezierCurve& curve ) = 0;
    virtual void Visit( HermiteSpline& curve ) = 0;
    virtual void Visit( EquidistantCurve& curve ) = 0;
    virtual void Visit( ProxyCurve& curve ) = 0;
};


//-------------------------------------------------------------------------------------------------
// C O L L E C T   E D I T I N G   P O I N T S   V I S I T O R
//-------------------------------------------------------------------------------------------------


/*++
    Visitor which collect mathematcal curves editing points.
--*/
class CollectEditingPointsVisitor : public CurveVisitor {
public:
    virtual ~CollectEditingPointsVisitor() {}
    virtual void Visit( Segment& curve );
    virtual void Visit( Arc& curve );
    virtual void Visit( ArchimedianSpiral& curve );
    virtual void Visit( LogarithmicSpiral& curve );
    virtual void Visit( Polyline& curve );
    virtual void Visit( BezierCurve& curve );
    virtual void Visit( HermiteSpline& curve );
    virtual void Visit( EquidistantCurve& curve );
    virtual void Visit( ProxyCurve& curve );

public:
    std::vector<Point> editingPoints_;
};


//-------------------------------------------------------------------------------------------------
// C H A N G E   E D I T I N G   P O I N T   V I S I T O R
//-------------------------------------------------------------------------------------------------


/*++
    Visitor which modify mathematcal curves editing points.
--*/
class ChangeEditingPointVisitor : public CurveVisitor {
public:
    ChangeEditingPointVisitor( int editingPointIndex, const Matrix3x3& transform );
    ChangeEditingPointVisitor( int editingPointIndex, const Vector& shift );
    virtual ~ChangeEditingPointVisitor() {}
    virtual void Visit( Segment& curve );
    virtual void Visit( Arc& curve );
    virtual void Visit( ArchimedianSpiral& curve );
    virtual void Visit( LogarithmicSpiral& curve );
    virtual void Visit( Polyline& curve );
    virtual void Visit( BezierCurve& curve );
    virtual void Visit( HermiteSpline& curve );
    virtual void Visit( EquidistantCurve& curve );
    virtual void Visit( ProxyCurve& curve );

private:
    int editingPointIndex_;
    Matrix3x3 transform_;
};


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


#endif

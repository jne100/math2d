///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#include "m2d_curves_test.h"

#include <m2d_numerics.h>


using namespace m2d;


//-------------------------------------------------------------------------------------------------
// C U R V E S   T E S T   U T I L I T I E S   T E S T S
//-------------------------------------------------------------------------------------------------


Curve* CreateTestCurve( ECurveTypes type )
{
    switch ( type ) {
    case CT_SEGMENT:
        {
        return new Segment( Point(45.0, 53.0), Point(46.0, 23.0) );
        }
    case CT_ARC:
        {
        Interval arcInterval( 0.35, PI );
        return new Arc( 15.0, Point(-10.0, -10.0), &arcInterval );
        }
    case CT_ARCHIMEDIAN_SPIRAL:
        {
        return new ArchimedianSpiral( CS(Point(-2.0, -97.0), 1.87), 1.0, PI2 );
        }
    case CT_LOGARITHMIC_SPIRAL:
        {
        return new LogarithmicSpiral( CS(Point(-2.0, -97.0), 1.87), 1.0, PI_DIV_4, 1.0 );
        }
    case CT_POLYLINE:
        {
        std::vector<Point> points( 5, ZERO_POINT );
        for ( size_t i = 0; i < points.size(); ++i ) {
            points[ i ] = Point( static_cast<double>(i), ::cos(static_cast<double>(i)) );
        }
        return new Polyline( points );
        }
    case CT_BEZIER_CURVE:
        {
        return new BezierCurve( Point(-32.0, -48.0), Point(80.0, -83.0), Point(7.0, -82.0) );
        }
    case CT_HERMITE_SPLINE:
        {
        std::vector<Point> points;
        points.push_back( ZERO_POINT );
        points.push_back( Point(10.0, 10.0) );
        points.push_back( Point(30.0, 0.0) );
        return new HermiteSpline( points );
        }
    case CT_EQUIDISTANT_CURVE:
        {
        return new EquidistantCurve( new Segment(Point(45.0, 53.0), Point(46.0, 23.0)),
            Vector(-10.0, -14.0) );
        }
    case CT_PROXY_CURVE:
        {
        return new ProxyCurve( new Arc(15.0), Interval(PI_DIV_4, PI_DIV_2) );
        }
    default:
        {
        return 0;
        }
    }
}


/*++
    Tricky class aimed to check that creation of all possible curves was implemented in
    CreateTestCurve function.
--*/
template <int T>
class CreateTestCurveVerifier : public CreateTestCurveVerifier<T - 1> {
public:
    bool Verify() const
    {
        Curve * pCurve = CreateTestCurve<T>();
        bool result = ( pCurve != 0 ) && CreateTestCurveVerifier<T - 1>::Verify();
        delete pCurve;
        return result;
    }
};


/*++
    Specified version of CreateTestCurveTestHelper class to break compile time recursion.
--*/
template <>
class CreateTestCurveVerifier<0> {
public:
    bool Verify() const
    {
        Curve * pCurve = CreateTestCurve<0>();
        bool result = ( pCurve != 0 );
        delete pCurve;
        return result;
    }
};


/*++
    Test that CreateTestCurve function able to create all posible types of curves.
--*/
TEST( CurveTestUtilities, TestCreateTestCurve1 )
{
    // given when then
    for ( int i = 0; i < CT_COUNT; ++i ) {
        ASSERT_TRUE( CreateTestCurve(static_cast<ECurveTypes>(i)) != 0 );
    }
}


/*++
    Test that CreateTestCurve function able to create all posible types of curves.
--*/
TEST( CurveTestUtilities, TestCreateTestCurve2 )
{
    // given
    CreateTestCurveVerifier<CT_COUNT - 1> verifier;

    // when then
    EXPECT_TRUE( verifier.Verify() );
}


//-------------------------------------------------------------------------------------------------
// C U R V E   T E S T S
//-------------------------------------------------------------------------------------------------


class CurveTestWithParam : public testing::TestWithParam<ECurveTypes> {
public:
    virtual ~CurveTestWithParam() {}

    virtual void SetUp()
    {
        pCurve_ = CreateTestCurve( GetParam() );
    }

    virtual void TearDown()
    {
        delete pCurve_;
        pCurve_ = NULL;
    }

protected:
    Curve* pCurve_;
};


INSTANTIATE_TEST_CASE_P(
    Common,
    CurveTestWithParam,
    testing::Values(
        CT_SEGMENT,
        CT_ARC,
        CT_ARCHIMEDIAN_SPIRAL,
        CT_LOGARITHMIC_SPIRAL,
        CT_POLYLINE,
        CT_BEZIER_CURVE,
        CT_HERMITE_SPLINE,
        CT_EQUIDISTANT_CURVE,
        CT_PROXY_CURVE) );


/*++
    Parametrized test of the mathematical curve length calculation.
--*/
TEST_P( CurveTestWithParam, TestCalcLength )
{
    ASSERT_TRUE( pCurve_ != 0 );

    double length = pCurve_->CalcLength();
    double approximateLength = 0.0;
    std::vector<Point> points;
    CalcUniformPointsOnCurve( *pCurve_, 100, points );
    for ( size_t i = 1; i < points.size(); ++i ) {
        approximateLength += CalcDistance( points[i - 1], points[i] );
    }

    // ~1 percent calculation mistake is acceptable
    EXPECT_TRUE( ::fabs(length - approximateLength) < (length / 100.0) );
}


/*++
    Parametrized test of the mathematical curve bounding box calculation.
--*/
TEST_P( CurveTestWithParam, TestCalcBoundingBox )
{
    ASSERT_TRUE( pCurve_ != 0 );

    BoundingBox box;
    BoundingBox hugeBox;
    hugeBox.Add( Point(-1000.0, -1000.0) );
    hugeBox.Add( Point(1000.0, 1000.0) );
    pCurve_->CalcBoundingBox( box );
    pCurve_->CalcBoundingBox( hugeBox );

    BoundingBox approximateBox;
    std::vector<Point> points;
    CalcUniformPointsOnCurve( *pCurve_, 100, points );
    for ( size_t i = 0; i < points.size(); ++i ) {
        approximateBox.Add( points[i] );
    }

    // additivity test
    EXPECT_TRUE( EQ(hugeBox.CalcWidth(), 2000.0) );
    EXPECT_TRUE( EQ(hugeBox.CalcHeight(), 2000.0) );

    // ~1 percent calculation mistake is acceptable
    EXPECT_TRUE( ::fabs(box.CalcArea() - approximateBox.CalcArea()) <
        (box.CalcArea() / 100.0) );
}


/*++
    Test of the CalcNormal method of the Curve.
--*/
TEST( Curve, TestCalcNormal )
{
    // given
    Segment s( Point(10.0, 10.0), Point(20.0, 20.0) );

    // when
    Vector n1 = s.CalcNormal( s.GetParametricInterval().begin_, true );
    Vector n2 = s.CalcNormal( s.GetParametricInterval().begin_, false );
    Vector n3 = s.CalcNormal( s.GetParametricInterval().end_, true );
    Vector n4 = s.CalcNormal( s.GetParametricInterval().end_, false );

    // then
    EXPECT_TRUE( n1.IsNormalized() && n2.IsNormalized() && n3.IsNormalized() && n4.IsNormalized() );
    EXPECT_TRUE( n1.IsCodirectional(Vector(-1.0, 1.0)) );
    EXPECT_TRUE( n2.IsCodirectional(Vector(1.0, -1.0)) );
    EXPECT_TRUE( n3.IsCodirectional(Vector(-1.0, 1.0)) );
    EXPECT_TRUE( n4.IsCodirectional(Vector(1.0, -1.0)) );
}


//-------------------------------------------------------------------------------------------------
// A N A L Y T I C A L   C U R V E   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the Tranfer method of the AnalyticalCurve.
--*/
TEST( AnalyticalCurve, TestTransfer )
{
    // given
    ArchimedianSpiral s( DEFAULT_CS, 10.0, PI2 );

    // when
    s.Transfer( Vector(10.0, 10.0) );

    // then
    EXPECT_EQ( s.GetCenter(), Point(10.0, 10.0) );
}


/*++
    Test of the Scale method of the AnalyticalCurve.
--*/
TEST( AnalyticalCurve, TestScale )
{
    // given
    ArchimedianSpiral s( CS(Point(10.0, 10.0)), 10.0, PI2 );

    // when
    s.Scale( -0.5, 0.3 );

    // then
    EXPECT_TRUE( EQ(s.GetRadius(), 5.0) );
    EXPECT_FALSE( EQ(s.GetRadius(), 3.0) );
    EXPECT_EQ( s.GetCenter(), Point(-5.0, 3.0) );
}


/*++
    Test of the Rotate method of the AnalyticalCurve.
--*/
TEST( AnalyticalCurve, TestRotate )
{
    // given
    ArchimedianSpiral s( CS(Point(10.0, 10.0)), 1.0, PI2 );

    // when
    s.Rotate( PI, &s.GetCenter() );

    // then
    EXPECT_EQ( s.GetCenter(), Point(10.0, 10.0) );
    EXPECT_EQ( s.CalcPointOnCurve(PI2), Point(10.0 - PI2, 10.0) );
}


//-------------------------------------------------------------------------------------------------
// S E G M E N T   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the = operator of the Segment.
--*/
TEST( Segment, TestAssign )
{
    // given
    Segment s1( Point(10.0, 10.0), Point(15.0, 5.0) );
    Segment s2( ZERO_POINT, ZERO_POINT );

    // when
    s2 = s1;

    // then
    EXPECT_EQ( s1.CalcCharacterPoint(CHP_BEGIN), s2.CalcCharacterPoint(CHP_BEGIN) );
    EXPECT_EQ( s1.CalcCharacterPoint(CHP_END), s2.CalcCharacterPoint(CHP_END) );
}


/*++
    Test of the points on segment calculation.
--*/
TEST( Segment, TestCalcPointOnCurve )
{
    // given
    Point p1( 10.0, 10.0 );
    Point p2( 15.0, 5.0 );
    Segment s( p1, p2 );

    // when then
    EXPECT_EQ( p1, s.CalcCharacterPoint(CHP_BEGIN) );
    EXPECT_EQ( p2, s.CalcCharacterPoint(CHP_END) );
}


/*++
    Test of the segment first and second derivatives calculation.
--*/
TEST( Segment, TestCalcDerivatives )
{
    // given
    Segment segment( Point(10.0, 10.0), Point(30.0, 40.0));

    // when
    Vector d11 = segment.CalcFirstDerivative( segment.GetParametricInterval().begin_ );
    Vector d12 = segment.CalcFirstDerivative( segment.GetParametricInterval().CalcMiddle() );
    Vector d13 = segment.CalcFirstDerivative( segment.GetParametricInterval().end_ );
    Vector d21 = segment.CalcSecondDerivative( segment.GetParametricInterval().begin_ );
    Vector d22 = segment.CalcSecondDerivative( segment.GetParametricInterval().CalcMiddle() );
    Vector d23 = segment.CalcSecondDerivative( segment.GetParametricInterval().end_ );

    //then
    EXPECT_TRUE( (d11 == d12) && (d11 == d13) && (d12 == d13) );
    EXPECT_TRUE( d21.IsZero() && d22.IsZero() && d23.IsZero() );
}


/*++
    Test of the CalcPointProjection method of the Segment, result inside segment parametric
    interval.
--*/
TEST( Segment, TestCalcPointProjection1 )
{
    // given
    Segment s1( ZERO_POINT, Point(10.0, 10.0) );
    Segment s2( ZERO_POINT, Point(10.0, 0.0) );
    Segment s3( ZERO_POINT, Point(0.0, 10.0) );

    // when
    double t1 = s1.CalcPointProjection( Point(0.0, 10.0) );
    double t2 = s2.CalcPointProjection( Point(5.0, 5.0) );
    double t3 = s3.CalcPointProjection( Point(5.0, 5.0) );

    // then
    EXPECT_EQ( s1.CalcPointOnCurve(t1), Point(5.0, 5.0) );
    EXPECT_EQ( s2.CalcPointOnCurve(t2), Point(5.0, 0.0) );
    EXPECT_EQ( s3.CalcPointOnCurve(t3), Point(0.0, 5.0) );
}


/*++
    Test of the CalcPointProjection method of the Segment, result outside segment parametric
    interval.
--*/
TEST( Segment, TestCalcPointProjection2 )
{
    // given
    Segment s( Point(-5.0, 5.0), Point(5.0, 5.0) );

    // when
    double t1 = s.CalcPointProjection( Point(-100.0, -100.0) );
    double t2 = s.CalcPointProjection( Point(-10.0, -10.0) );
    double t3 = s.CalcPointProjection( Point(10.0, 10.0) );
    double t4 = s.CalcPointProjection( Point(100.0, 100.0) );

    // then
    EXPECT_EQ( s.CalcPointOnCurve(t1), Point(-100.0, 5.0) );
    EXPECT_EQ( s.CalcPointOnCurve(t2), Point(-10.0, 5.0) );
    EXPECT_EQ( s.CalcPointOnCurve(t3), Point(10.0, 5.0) );
    EXPECT_EQ( s.CalcPointOnCurve(t4), Point(100.0, 5.0) );
}


/*++
    Test of the CalcPointProjection method of the Segment, vacuous segment case.
--*/
TEST( Segment, TestCalcPointProjection3 )
{
    // given
    Point p( 7.0, 9.0 );
    Segment s( p, p );

    // when
    double t1 = s.CalcPointProjection( Point(-89.0, -94.0) );
    double t2 = s.CalcPointProjection( Point(63.0, -58.0) );
    double t3 = s.CalcPointProjection( Point(31.0, 77.0) );

    // then
    EXPECT_TRUE( EQ(t1, s.GetParametricInterval().begin_) );
    EXPECT_TRUE( EQ(t2, s.GetParametricInterval().begin_) );
    EXPECT_TRUE( EQ(t3, s.GetParametricInterval().begin_) );
}


/*++
    Test of the segment length calculation.
--*/
TEST( Segment, TestCalcLength )
{
    // given
    Segment segment0( ZERO_POINT, ZERO_POINT );
    Segment segment1( ZERO_POINT, Point(1.0, 0.0) );
    Segment segment2( Point(50.0, 0.0), Point(50.0, 1.0) );
    Segment segment3( ZERO_POINT, Point(1.0, 1.0) );

    // then
    EXPECT_TRUE( EQ(segment0.CalcLength(), 0.0) );
    EXPECT_TRUE( EQ(segment1.CalcLength(), 1.0) );
    EXPECT_TRUE( EQ(segment2.CalcLength(), 1.0) );
    EXPECT_TRUE( EQ(segment3.CalcLength(), ::sqrt(2.0)) );
}


/*++
    Test of the CalcCurvature method of the Segment.
--*/
TEST( Segment, TestCalcCurvature )
{
    // given
    Segment segment( Point(10.0, 10.0), Point(20.0, 20.0) );

    // when
    double c1 = segment.CalcCurvature( segment.GetParametricInterval().begin_ );
    double c2 = segment.CalcCurvature( segment.GetParametricInterval().CalcMiddle() );
    double c3 = segment.CalcCurvature( segment.GetParametricInterval().end_ );

    // then
    EXPECT_TRUE( EQNil(c1) && EQNil(c2) && EQNil(c3) );
}


/*++
    Test of the CalcTiltStep method of the Segment.
--*/
TEST( Segment, TestCalcTiltStep )
{
    // given
    Segment s( ZERO_POINT, Point(10.0, 10.0) );

    // when
    const int COUNT = 3;
    bool cuts[ COUNT ];
    double steps[ COUNT ];
    steps[ 0 ] = s.CalcTiltStep( 0.6, Radians(5.0), s.GetParametricInterval(), cuts[0] );
    steps[ 1 ] = s.CalcTiltStep( 1.0, Radians(5.0), s.GetParametricInterval(), cuts[1] );
    steps[ 2 ] = s.CalcTiltStep( 1.5, Radians(5.0), s.GetParametricInterval(), cuts[2] );

    // then
    EXPECT_TRUE( cuts[0] );
    EXPECT_TRUE( cuts[1] );
    EXPECT_TRUE( cuts[2] );
    EXPECT_TRUE( EQ(steps[0], 0.4) );
    EXPECT_TRUE( EQNil(steps[1]) );
    EXPECT_TRUE( EQNil(steps[2]) );
}


//-------------------------------------------------------------------------------------------------
// A R C   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the arc construction, simple cases.
--*/
TEST( Arc, TestConstructionSimple1 )
{
    // given
    Arc arc( 10.0, &PI_INTERVAL );
    Arc closedArc( 10.0 );

    // when then
    EXPECT_FALSE( arc.IsClosed() );
    EXPECT_TRUE( closedArc.IsClosed() );
    EXPECT_TRUE( EQ(arc.CalcLength(), PI_INTERVAL.CalcLength() * 10.0) );
    EXPECT_TRUE( EQ(closedArc.CalcLength(), PI2 * 10.0) );
    EXPECT_EQ( arc.GetCenter(), ZERO_POINT );
    EXPECT_EQ( closedArc.GetCenter(), ZERO_POINT );
}


/*++
    Test of the arc construction, simple cases.
--*/
TEST( Arc, TestConstructionSimple2 )
{
    // given
    Point p( -500.0, 13000.0 );
    Arc arc( 2500.0, p, &PI_DIV_2_INTERVAL );
    Arc closedArc( 2500.0, p );

    // when then
    EXPECT_FALSE( arc.IsClosed() );
    EXPECT_TRUE( closedArc.IsClosed() );
    EXPECT_TRUE( EQ(arc.CalcLength(), PI_DIV_2_INTERVAL.CalcLength() * 2500.0) );
    EXPECT_TRUE( EQ(closedArc.CalcLength(), PI2 * 2500.0) );
    EXPECT_EQ( arc.GetCenter(), p );
    EXPECT_EQ( closedArc.GetCenter(), p );
}


/*++
    Test of the arc construction, simple cases.
--*/
TEST( Arc, TestConstructionSimple3 )
{
    // given
    CS cs( ZERO_POINT, PI_DIV_2 );
    Arc arc( 0.5, cs, &PI_DIV_4_INTERVAL );
    Arc closedArc( 0.5, cs );

    // when then
    EXPECT_FALSE( arc.IsClosed() );
    EXPECT_TRUE( closedArc.IsClosed() );
    EXPECT_TRUE( EQ(arc.CalcLength(), PI_DIV_4_INTERVAL.CalcLength() * 0.5) );
    EXPECT_TRUE( EQ(closedArc.CalcLength(), PI2 * 0.5) );
    EXPECT_EQ( arc.GetCenter(), cs.pos_ );
    EXPECT_EQ( closedArc.GetCenter(), cs.pos_ );
}


/*++
    Test of the circle construction, construction by three points with the use of curve direction
    inversion.
--*/
TEST( Arc, TestConstructionByThreePoints1 )
{
    // given
    Point p0( 10.0, 0.0);
    Point p90( 0.0, 10.0 );
    Point p180( -10.0, 0.0 );
    Arc circle( p0, p90, p180, true );
    Arc invertedCircle( p180, p90, p0, true );

    // when then
    EXPECT_EQ( circle.GetCenter(), ZERO_POINT );
    EXPECT_EQ( invertedCircle.GetCenter(), ZERO_POINT );
    EXPECT_TRUE( EQ(circle.GetRadius(), 10.0) );
    EXPECT_TRUE( EQ(invertedCircle.GetRadius(), 10.0) );
    EXPECT_TRUE( EQ(circle.GetParametricInterval().CalcLength(), PI2) );
    EXPECT_TRUE( EQ(invertedCircle.GetParametricInterval().CalcLength(), PI2) );
}


/*++
    Test of the arc construction, construction by three points with the use of curve direction
    inversion.
--*/
TEST( Arc, TestConstructionByThreePoints2 )
{
    // given
    Point p0( 10.0, 0.0);
    Point p90( 0.0, 10.0 );
    Point p180( -10.0, 0.0 );
    Arc arc( p0, p90, p180 );
    Arc invertedArc( p180, p90, p0 );

    // when then
    EXPECT_EQ( arc.GetCenter(), ZERO_POINT );
    EXPECT_EQ( invertedArc.GetCenter(), ZERO_POINT );
    EXPECT_TRUE( EQ(arc.GetRadius(), 10.0) );
    EXPECT_TRUE( EQ(invertedArc.GetRadius(), 10.0) );
    EXPECT_TRUE( EQ(arc.GetParametricInterval().CalcLength(), PI) );
    EXPECT_TRUE( EQ(invertedArc.GetParametricInterval().CalcLength(), PI) );
    EXPECT_EQ( arc.CalcCharacterPoint(CHP_BEGIN), p0 );
    EXPECT_EQ( arc.CalcCharacterPoint(CHP_MIDDLE), p90 );
    EXPECT_EQ( arc.CalcCharacterPoint(CHP_END), p180 );
    EXPECT_EQ( invertedArc.CalcCharacterPoint(CHP_BEGIN), p0 );
    EXPECT_EQ( invertedArc.CalcCharacterPoint(CHP_MIDDLE), p90 );
    EXPECT_EQ( invertedArc.CalcCharacterPoint(CHP_END), p180 );
}


/*++
    Test of the arc construction, verification of the second point processing correctness.
--*/
TEST( Arc, TestConstructionByThreePoints3 )
{
    // given
    Point p0( 10.0, 0.0 );
    Point p45( 10.0 / ::sqrt(2.0), 10.0 / ::sqrt(2.0) );
    Point p90( 0.0, 10.0 );
    Point p225( -10.0 / ::sqrt(2.0), -10.0 / ::sqrt(2.0) );
    Arc arc( p0, p45, p90 );
    Arc invertedArc( p0, p225, p90 );

    // when then
    EXPECT_EQ( arc.GetCenter(), ZERO_POINT );
    EXPECT_EQ( invertedArc.GetCenter(), ZERO_POINT );
    EXPECT_TRUE( EQ(arc.GetRadius(), 10.0) );
    EXPECT_TRUE( EQ(invertedArc.GetRadius(), 10.0) );
    EXPECT_TRUE( EQ(arc.GetParametricInterval().CalcLength(), PI_DIV_2) );
    EXPECT_TRUE( EQ(invertedArc.GetParametricInterval().CalcLength(), PI_DIV_2 * 3.0) );
    EXPECT_EQ( arc.CalcCharacterPoint(CHP_BEGIN), p0 );
    EXPECT_EQ( arc.CalcCharacterPoint(CHP_END), p90 );
    EXPECT_EQ( invertedArc.CalcCharacterPoint(CHP_BEGIN), p90 );
    EXPECT_EQ( invertedArc.CalcCharacterPoint(CHP_END), p0 );
}


/*++
    Test of the arc construction by three points, use of precalculated in CAD system data.
--*/
TEST( Arc, TestConstructionByThreePoints4 )
{
    // given
    const int ARC_COUNT = 10;
    Point points[ ARC_COUNT ][ 3 ] = {
        {Point(-309.0, -532.0), Point(-658.0, 449.0), Point(-154.0, 693.0)},
        {Point(-920.0, 263.0), Point(952.0, -209.0), Point(-616.0, -337.0)},
        {Point(699.0, -930.0), Point(-624.0, -304.0), Point(-710.0, 647.0)},
        {Point(901.0, -690.0), Point(857.0, 493.0), Point(-974.0, 293.0)},
        {Point(-16.0, -13.0), Point(100.0, 86.0), Point(-238.0, -92.0)},
        {Point(-136.0, -250.0), Point(-447.0, -51.0), Point(-708.0, -169.0)},
        {Point(-467.0, 575.0), Point(-241.0, 109.0), Point(-121.0, -122.0)},
        {Point(329.0, -842.0), Point(835.0, 586.0), Point(-891.0, -136.0)},
        {Point(-228.0, -557.0), Point(-716.0, -46.0), Point(-903.0, -631.0)},
        {Point(217.0, 900.0), Point(-320.0, 950.0), Point(-645.0, -57.0)},
    };

    Arc arcs[ ARC_COUNT ] = {
        Arc(points[0][0], points[0][1], points[0][2]),
        Arc(points[1][0], points[1][1], points[1][2]),
        Arc(points[2][0], points[2][1], points[2][2]),
        Arc(points[3][0], points[3][1], points[3][2]),
        Arc(points[4][0], points[4][1], points[4][2]),
        Arc(points[5][0], points[5][1], points[5][2]),
        Arc(points[6][0], points[6][1], points[6][2]),
        Arc(points[7][0], points[7][1], points[7][2]),
        Arc(points[8][0], points[8][1], points[8][2]),
        Arc(points[9][0], points[9][1], points[9][2]),
    };

    bool inversions[ ARC_COUNT ] = {
        true,
        true,
        true,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
    };

    double approximateRadiuses[ ARC_COUNT ] = {
        621.0,
        1043.0,
        1227.0,
        1061.0,
        536.0,
        345.0,
        14123.0,
        952.0,
        387.0,
        660.0,
    };

    double approximateLengths[ ARC_COUNT ] = {
        1816.0,
        5866.0,
        2549.0,
        3486.0,
        3128.0,
        685.0,
        778.0,
        4391.0,
        1601.0,
        1783.0,
    };

    Point approximateCenters[ ARC_COUNT ] = {
        Point(-164.0, 72.0),
        Point(112.0, 409.0),
        Point(459.0, 273.0),
        Point(-1.0, -131.0),
        Point(-302.0, 440.0),
        Point(-448.0, -396.0),
        Point(12352.0, 6504.0),
        Point(39.0, 64.0),
        Point(-586.0, -410.0),
        Point(-107.0, 325.0),
    };

    // when then
    for ( int i = 0; i < ARC_COUNT; ++i ) {

        // verify radius
        EXPECT_TRUE( EQ(approximateRadiuses[i], Round(arcs[i].GetRadius())) );

        // verify length
        EXPECT_TRUE( EQ(approximateLengths[i], Round(arcs[i].CalcLength())) );

        // verify center
        const Point& center = arcs[ i ].GetCenter();
        EXPECT_EQ( approximateCenters[i], Point(Round(center.x_), Round(center.y_)) );

        // verify begin and end points
        Point beginPoint = arcs[i].CalcCharacterPoint( CHP_BEGIN );
        Point endPoint = arcs[i].CalcCharacterPoint( CHP_END );
        EXPECT_EQ( (!inversions[i] ? points[i][0] : points[i][2]), Point(Round(beginPoint.x_), Round(beginPoint.y_)) );
        EXPECT_EQ( (!inversions[i] ? points[i][2] : points[i][0]), Point(Round(endPoint.x_), Round(endPoint.y_)) );
    }
}


/*++
    Test of the arc construction, vacuous arc case.
--*/
TEST( Arc, TestConstructionByThreePoints5 )
{
    // given
    Arc a( ZERO_POINT, Point(10.0, 10.0), ZERO_POINT );

    // when then
    EXPECT_TRUE( EQNil(a.GetRadius()) );
    EXPECT_EQ( a.GetParametricInterval(), ZERO_INTERVAL );
}


/*++
    Test of the points on arc calculation, arc with the center in global origin.
--*/
TEST( Arc, TestCalcPointOnCurve1 )
{
    // given
    Arc a( 10.0 );

    // when
    Point p0 = a.CalcPointOnCurve( 0.0 );
    Point p45 = a.CalcPointOnCurve( PI_DIV_4 );
    Point p90 = a.CalcPointOnCurve( PI_DIV_2 );
    Point p180 = a.CalcPointOnCurve( PI );
    Point p270 = a.CalcPointOnCurve( PI_DIV_2 * 3.0 );
    Point p360 = a.CalcPointOnCurve( PI2 );

    // then
    EXPECT_EQ( p0, Point(10.0, 0.0) );
    EXPECT_EQ( p45, Point(10.0 / ::sqrt(2.0), 10.0 / ::sqrt(2.0)) );
    EXPECT_EQ( p90, Point(0.0, 10.0) );
    EXPECT_EQ( p180, Point(-10.0, 0.0) );
    EXPECT_EQ( p270, Point(0.0, -10.0) );
    EXPECT_EQ( p360, p0 );
}


/*++
    Test of the points on arc calculation, arc with the center in arbitrary point.
--*/
TEST( Arc, TestCalcPointOnCurve2 )
{
    // given
    Point p( 100.0, 200.0 );
    Arc a( 10.0, p );

    // when
    Point p0 = a.CalcPointOnCurve( 0.0 );
    Point p45 = a.CalcPointOnCurve( PI_DIV_4 );
    Point p90 = a.CalcPointOnCurve( PI_DIV_2 );
    Point p180 = a.CalcPointOnCurve( PI );
    Point p270 = a.CalcPointOnCurve( PI_DIV_2 * 3.0 );
    Point p360 = a.CalcPointOnCurve( PI2 );

    // then
    EXPECT_EQ( p0, Point(110.0, 200.0) );
    EXPECT_EQ( p45, Point(10.0 / ::sqrt(2.0) + p.x_, 10.0 / ::sqrt(2.0) + p.y_) );
    EXPECT_EQ( p90, Point(100.0, 210.0) );
    EXPECT_EQ( p180, Point(90.0, 200.0) );
    EXPECT_EQ( p270, Point(100.0, 190.0) );
    EXPECT_EQ( p360, p0 );
}


/*++
    Test of the arc first and second derivatives calculation.
--*/
TEST( Arc, TestCalcDerivatives )
{
    // given
    Arc a( 10.0 );

    // when
    Vector v0 = a.CalcFirstDerivative( 0.0 );
    Vector v45 = a.CalcFirstDerivative( PI_DIV_4 );
    Vector v90 = a.CalcFirstDerivative( PI_DIV_2 );
    Vector v180 = a.CalcFirstDerivative( PI );
    Vector v270 = a.CalcFirstDerivative( PI_DIV_2 * 3.0 );
    Vector v360 = a.CalcFirstDerivative( PI2 );
    v0.Normalize();
    v45.Normalize();
    v90.Normalize();
    v180.Normalize();
    v270.Normalize();
    v360.Normalize();

    // then
    EXPECT_EQ( v0, Vector(0.0, 1.0) );
    EXPECT_EQ( v45, Vector(-1.0 / ::sqrt(2.0), 1.0 / ::sqrt(2.0)) );
    EXPECT_EQ( v90, Vector(-1.0, 0.0) );
    EXPECT_EQ( v180, Vector(0.0, -1.0) );
    EXPECT_EQ( v270, Vector(1.0, 0.0) );
    EXPECT_EQ( v360, v0 );
}


/*++
    Test of the CalcTiltStep method of the Arc.
--*/
TEST( Arc, TestCalcTiltStep )
{
    // given
    Arc a( 10.0, &PI_DIV_2_INTERVAL );
    const Interval& i = a.GetParametricInterval();

    // when
    const int COUNT = 4;
    bool cuts[ COUNT ];
    double steps[ COUNT ];
    steps[ 0 ] = a.CalcTiltStep( 0.0, Radians(2.0), i, cuts[0] );
    steps[ 1 ] = a.CalcTiltStep( PI_DIV_2 - Radians(1.0), Radians(1.0), i, cuts[1] );
    steps[ 2 ] = a.CalcTiltStep( PI_DIV_2, Radians(5.0), i, cuts[2] );
    steps[ 3 ] = a.CalcTiltStep( PI, Radians(5.0), i, cuts[3] );

    // then
    EXPECT_FALSE( cuts[0] );
    EXPECT_TRUE( cuts[1] );
    EXPECT_TRUE( cuts[2] );
    EXPECT_TRUE( cuts[3] );
    EXPECT_TRUE( EQ(steps[0], Radians(2.0)) );
    EXPECT_TRUE( EQ(steps[1], Radians(1.0)) );
    EXPECT_TRUE( EQ(steps[2], 0.0) );
    EXPECT_TRUE( EQ(steps[3], 0.0) );
}


/*++
    Test of the Scale method of the Arc.
--*/
TEST( Arc, TestScale )
{
    // given
    Arc a( 10.0 );

    // when
    a.Scale( 2.0, 3.0 );

    // then
    EXPECT_TRUE( EQ(a.GetRadius(), 20.0) );
}


//-------------------------------------------------------------------------------------------------
// S P I R A L   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the Scale method of the Spiral.
--*/
TEST( Spiral, TestScale )
{
    // given
    ArchimedianSpiral s1( CS(Point(10.0, 5.0), PI_DIV_4), 1.0, PI2 );
    ArchimedianSpiral s2( DEFAULT_CS, 1.0, PI2 );
    double oldRadius = s1.GetRadius();
    double oldLength = s1.CalcLength();

    // when
    s1.Scale( 2.0, 2.0 );
    s2.Scale( -2.0, -2.0 );

    // then
    EXPECT_EQ( s1.GetCenter(), Point(20.0, 10.0) );
    EXPECT_TRUE( EQ(s1.GetRadius(), oldRadius * 2.0) );
    EXPECT_TRUE( EQ(s1.CalcLength(), oldLength * 2.0) );
    EXPECT_TRUE( s2.GetRadius() > 0.0 );
}


//-------------------------------------------------------------------------------------------------
// A R C H I M E D I A N   S P I R A L   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the points on archimedian spiral calculation.
--*/
TEST( ArchimedianSpiral, TestCalcPointOnCurve )
{
    // given
    ArchimedianSpiral s( CS(Point(10.0, 5.0)), 1.0, PI2 );

    // when then
    EXPECT_EQ( s.CalcPointOnCurve(0.0), Point(10.0, 5.0) );
    EXPECT_EQ( s.CalcPointOnCurve(PI), Point(-PI + 10.0, 5.0) );
    EXPECT_EQ( s.CalcPointOnCurve(PI2), Point(PI2 + 10.0, 5.0) );
}


/*++
    Test of the archemedian spiral first derivative calculation.
--*/
TEST( ArchimedianSpiral, TestCalcFirstDerivative )
{
    // given
    ArchimedianSpiral s1( CS(Point(10.0, 5.0)), 1.0, PI2 );
    double radius2 = 3.0;
    ArchimedianSpiral s2( CS(Point(10.0, 5.0), PI), radius2, PI2 );

    // when then
    EXPECT_EQ( s1.CalcFirstDerivative(PI_DIV_2), Vector(-PI_DIV_2, 0.0) );
    EXPECT_EQ( s1.CalcFirstDerivative(PI), Vector(0.0, -PI) );
    EXPECT_EQ( s1.CalcFirstDerivative(PI2), Vector(0.0, PI2) );
    EXPECT_EQ( s2.CalcFirstDerivative(PI_DIV_2), Vector(PI_DIV_2  * radius2, 0.0) );
    EXPECT_EQ( s2.CalcFirstDerivative(PI), Vector(0.0, PI * radius2) );
    EXPECT_EQ( s2.CalcFirstDerivative(PI2), Vector(0.0, (-PI2) * radius2) );
}


//-------------------------------------------------------------------------------------------------
// L O G A R I T H M I C   S P I R A L   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the points on logarithmic spiral calculation.
--*/
TEST( LogarithmicSpiral, TestCalcPointOnCurve )
{
    // given
    LogarithmicSpiral s( DEFAULT_CS, 1.0, PI2, 1.0 );

    // when then
    EXPECT_EQ( s.CalcPointOnCurve(0.0), Point(1.0, 0.0) );
}


//-------------------------------------------------------------------------------------------------
// P O L Y L I N E   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the AddVertex and RemoveVertex methods of the Polyline.
--*/
TEST( Polyline, TestAddRemoveVertex )
{
    // given
    Polyline pl;

    // when
    pl.AddVertex( Point(10.0, 10.0) );
    pl.AddVertex( Point(5.0, 5.0), 0 );
    pl.AddVertex( Point(7.5, 7.5), 1 );
    pl.AddVertex( Point(8.5, 8.5), 2 );
    pl.RemoveVertex( 2 );
    pl.SetVertex( 2, Point(11.0, 10.0) );

    // then
    EXPECT_EQ( pl.GetVerticesCount(), 3 );
    EXPECT_EQ( pl.GetVertex(0), Point(5.0, 5.0) );
    EXPECT_EQ( pl.GetVertex(1), Point(7.5, 7.5) );
    EXPECT_EQ( pl.GetVertex(2), Point(11.0, 10.0) );
}


/*++
    Test of the RemoveVertices methods of the Polyline.
--*/
TEST( Polyline, TestRemoveVertices )
{
    // given
    Polyline pl;
    pl.AddVertex( ZERO_POINT );
    pl.AddVertex( Point(45.0, -10.0) );
    pl.AddVertex( Point(-35.0, -39.0) );

    // when
    pl.RemoveVertices();

    // then
    EXPECT_EQ( pl.GetVerticesCount(), 0 );
    EXPECT_EQ( pl.GetParametricInterval(), ZERO_INTERVAL );
}


/*++
    Test of the points on polyline calculation, parametric value inside polyline parametric
    interval cases.
--*/
TEST( Polyline, TestCalcPointOnCurve1 )
{
    // given
    Point p1( ZERO_POINT );
    Point p2( 10.0, 10.0 );
    Point p3( 20.0, 20.0 );
    Polyline pl;
    pl.AddVertex( p1 );
    pl.AddVertex( p2 );
    pl.AddVertex( p3 );

    // when then
    EXPECT_EQ( pl.CalcPointOnCurve(pl.GetParametricInterval().begin_), p1 );
    EXPECT_EQ( pl.CalcPointOnCurve(0.5), Point(5.0, 5.0) );
    EXPECT_EQ( pl.CalcPointOnCurve(1.0), p2 );
    EXPECT_EQ( pl.CalcPointOnCurve(1.5), Point(15.0, 15.0) );
    EXPECT_EQ( pl.CalcPointOnCurve(pl.GetParametricInterval().end_), p3 );
}


/*++
    Test of the points on polyline calculation, parametric value outside polyline parametric
    interval and boundary cases.
--*/
TEST( Polyline, TestCalcPointOnCurve2 )
{
    // given
    Polyline pl;
    pl.AddVertex( ZERO_POINT );
    pl.AddVertex( Point(10.0, 10.0) );

    // when then
    EXPECT_EQ( pl.CalcPointOnCurve(-1.5), Point(-15.0, -15.0) );
    EXPECT_EQ( pl.CalcPointOnCurve(-1.0), Point(-10.0, -10.0) );
    EXPECT_EQ( pl.CalcPointOnCurve(0.0), Point(0.0, 0.0) );
    EXPECT_EQ( pl.CalcPointOnCurve(1.0), Point(10.0, 10.0) );
    EXPECT_EQ( pl.CalcPointOnCurve(2.0), Point(20.0, 20.0) );
    EXPECT_EQ( pl.CalcPointOnCurve(2.5), Point(25.0, 25.0) );
}


/*++
    Test of the polyline first and second derivatives calculation.
--*/
TEST( Polyline, TestCalcDerivatives )
{
    // given
    Polyline pl;
    pl.AddVertex( ZERO_POINT );
    pl.AddVertex( Point(0.0, 10.0) );
    pl.AddVertex( Point(10.0, 10.0) );
    pl.AddVertex( Point(10.0, 0.0) );

    // when
    Vector d00 = pl.CalcFirstDerivative( 0.0 );
    Vector d05 = pl.CalcFirstDerivative( 0.5 );
    Vector d10 = pl.CalcFirstDerivative( 1.0 );
    Vector d15 = pl.CalcFirstDerivative( 1.5 );
    Vector d20 = pl.CalcFirstDerivative( 2.0 );
    Vector d25 = pl.CalcFirstDerivative( 2.5 );
    Vector d30 = pl.CalcFirstDerivative( 3.0 );
    d00.Normalize();
    d05.Normalize();
    d10.Normalize();
    d15.Normalize();
    d20.Normalize();
    d25.Normalize();
    d30.Normalize();
    Vector dd00 = pl.CalcSecondDerivative( 0.0 );
    Vector dd05 = pl.CalcSecondDerivative( 0.5 );
    Vector dd10 = pl.CalcSecondDerivative( 1.0 );
    Vector dd15 = pl.CalcSecondDerivative( 1.5 );
    Vector dd20 = pl.CalcSecondDerivative( 2.0 );
    Vector dd25 = pl.CalcSecondDerivative( 2.5 );
    Vector dd30 = pl.CalcSecondDerivative( 3.0 );

    // then
    EXPECT_EQ( d00, Vector(0.0, 1.0) );
    EXPECT_EQ( d05, Vector(0.0, 1.0) );
    EXPECT_EQ( d10, Vector(1.0, 0.0) );
    EXPECT_EQ( d15, Vector(1.0, 0.0) );
    EXPECT_EQ( d20, Vector(0.0, -1.0) );
    EXPECT_EQ( d25, Vector(0.0, -1.0) );
    EXPECT_EQ( d30, Vector(0.0, -1.0) );
    EXPECT_TRUE( dd00.IsZero() && dd05.IsZero() && dd10.IsZero() && dd15.IsZero() &&
        dd20.IsZero() && dd25.IsZero() && dd30.IsZero() );
}


/*++
    Test of the polyline length calculation.
--*/
TEST( Polyline, TestCalcLength )
{
    // given
    Polyline pl1;
    Polyline pl2;
    pl1.AddVertex( Point(10.0, 10.0) );
    pl1.AddVertex( Point(20.0, 10.0) );
    pl2.AddVertex( ZERO_POINT );
    pl2.AddVertex( Point(0.0, 10.0) );
    pl2.AddVertex( Point(5.0, 10.0) );
    pl2.AddVertex( Point(5.0, 25.0) );
    pl2.AddVertex( Point(10.0, 25.0) );

    // when then
    EXPECT_TRUE( EQ(pl1.CalcLength(), 10.0) );
    EXPECT_TRUE( EQ(pl2.CalcLength(), 35.0) );
}


/*++
    Test of the polyline bounding box calculation.
--*/
TEST( Polyline, TestCalcBoundingBox )
{
    // given
    Polyline pl;
    pl.AddVertex( Point(96.0, -51.0) );
    pl.AddVertex( Point(68.0, -27.0) );
    pl.AddVertex( Point(30.0, -96.0) );
    pl.AddVertex( Point(-24.0, 45.0) );
    pl.AddVertex( Point(31.0, 69.0) );

    // when
    BoundingBox box;
    pl.CalcBoundingBox( box );

    // then
    EXPECT_EQ( box.GetMinPoint(), Point(-24.0, -96.0) );
    EXPECT_EQ( box.GetMaxPoint(), Point(96.0, 69.0) );
}


/*++
    Test of the CalcTiltStep method of the Polyline.
--*/
TEST( Polyline, TestCalcTiltStep1 )
{
    // given
    Polyline pl;
    pl.AddVertex( ZERO_POINT );
    pl.AddVertex( Point(10.0, 10.0) );
    pl.AddVertex( Point(20.0, 0.0) );
    const Interval& i = pl.GetParametricInterval();

    // when
    const int COUNT = 7;
    bool cuts[ COUNT ];
    double steps[ COUNT ];
    steps[ 0 ] = pl.CalcTiltStep( -10.2, Radians(5.0), i, cuts[0] );
    steps[ 1 ] = pl.CalcTiltStep( 0.0, Radians(5.0), i, cuts[1] );
    steps[ 2 ] = pl.CalcTiltStep( 0.6, Radians(5.0), i, cuts[2] );
    steps[ 3 ] = pl.CalcTiltStep( 1.6, Radians(5.0), i, cuts[3] );
    steps[ 4 ] = pl.CalcTiltStep( i.end_, Radians(5.0), i, cuts[4] );
    steps[ 5 ] = pl.CalcTiltStep( i.end_ + 0.5, Radians(5.0), i, cuts[5] );
    steps[ 6 ] = pl.CalcTiltStep( 10.3, Radians(5.0), i, cuts[6] );

    // then
    EXPECT_FALSE( cuts[0] );
    EXPECT_FALSE( cuts[1] );
    EXPECT_FALSE( cuts[2] );
    EXPECT_TRUE( cuts[3] );
    EXPECT_TRUE( cuts[4] );
    EXPECT_TRUE( cuts[5] );
    EXPECT_TRUE( cuts[6] );

    EXPECT_TRUE( EQ(steps[0], 11.2) );
    EXPECT_TRUE( EQ(steps[1], 1.0) );
    EXPECT_TRUE( EQ(steps[2], 0.4) );
    EXPECT_TRUE( EQ(steps[3], 0.4) );
    EXPECT_TRUE( EQ(steps[4], 0.0) );
    EXPECT_TRUE( EQ(steps[5], 0.0) );
    EXPECT_TRUE( EQ(steps[6], 0.0) );
}


/*++
    Test of the CalcTiltStep method of the Polyline.
--*/
TEST( Polyline, TestCalcTiltStep2 )
{
    // given
    Polyline pl;
    pl.AddVertex( ZERO_POINT );
    pl.AddVertex( Point(10.0, 0.0) );
    Interval span( 0.2, 0.8 );

    // when
    const int COUNT = 7;
    bool cuts[ COUNT ];
    double steps[ COUNT ];
    steps[ 0 ] = pl.CalcTiltStep( -3.3, Radians(5.0), span, cuts[0] );
    steps[ 1 ] = pl.CalcTiltStep( -0.3, Radians(5.0), span, cuts[1] );
    steps[ 2 ] = pl.CalcTiltStep( 0.0, Radians(5.0), span, cuts[2] );
    steps[ 3 ] = pl.CalcTiltStep( 0.2, Radians(5.0), span, cuts[3] );
    steps[ 4 ] = pl.CalcTiltStep( 0.4, Radians(5.0), span, cuts[4] );
    steps[ 5 ] = pl.CalcTiltStep( 0.9, Radians(5.0), span, cuts[5] );
    steps[ 6 ] = pl.CalcTiltStep( 1.6, Radians(5.0), span, cuts[6] );

    // then
    EXPECT_TRUE( cuts[0] );
    EXPECT_TRUE( cuts[1] );
    EXPECT_TRUE( cuts[2] );
    EXPECT_TRUE( cuts[3] );
    EXPECT_TRUE( cuts[4] );
    EXPECT_TRUE( cuts[5] );
    EXPECT_TRUE( cuts[6] );

    EXPECT_TRUE( EQ(steps[0], 4.1) );
    EXPECT_TRUE( EQ(steps[1], 1.1) );
    EXPECT_TRUE( EQ(steps[2], 0.8) );
    EXPECT_TRUE( EQ(steps[3], 0.6) );
    EXPECT_TRUE( EQ(steps[4], 0.4) );
    EXPECT_TRUE( EQ(steps[5], 0.0) );
    EXPECT_TRUE( EQ(steps[6], 0.0) );
}


/*++
    Test of the polyline parametric interval correctness after vertices structure (count) change.
--*/
TEST( Polyline, TestUpdateInterval )
{
    // given
    Polyline pl;

    // when
    Interval i1 = pl.GetParametricInterval();

    pl.AddVertex( ZERO_POINT );
    Interval i2 = pl.GetParametricInterval();

    pl.AddVertex( Point(10.0, 10.0) );
    Interval i3 = pl.GetParametricInterval();

    pl.AddVertex( Point(20.0, 20.0) );
    Interval i4 = pl.GetParametricInterval();

    pl.RemoveVertex( 1 );
    Interval i5 = pl.GetParametricInterval();

    pl.RemoveVertex( 0 );
    Interval i6 = pl.GetParametricInterval();

    pl.RemoveVertex( 0 );
    Interval i7 = pl.GetParametricInterval();

    // then
    EXPECT_EQ( i1, ZERO_INTERVAL );
    EXPECT_EQ( i2, ZERO_INTERVAL );
    EXPECT_EQ( i3, Interval(0.0, 1.0) );
    EXPECT_EQ( i4, Interval(0.0, 2.0) );
    EXPECT_EQ( i5, Interval(0.0, 1.0) );
    EXPECT_EQ( i6, ZERO_INTERVAL );
    EXPECT_EQ( i7, ZERO_INTERVAL );
}


//-------------------------------------------------------------------------------------------------
// B E Z I E R   C U R V E   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the Bezier curve construction.
--*/
TEST( BezierCurve, TestConstruction1 )
{
    // given
    BezierCurve c( ZERO_POINT, ZERO_POINT, ZERO_POINT );

    // when then
    EXPECT_EQ( c.GetControlVerticesCount(), 3 );
}


/*++
    Test of the Bezier curve construction.
--*/
TEST( BezierCurve, TestConstruction2 )
{
    // given
    const int POINTS_COUNT = 5;
    Point points[ POINTS_COUNT ] = {
        Point(1.0, -1.0), Point(7.0, 5.0), Point(4.0, 2.0), Point(-11.0, 29.0), Point(31.0, 12.0),
    };

    // when
    BezierCurve c( std::vector<Point>(points, points + POINTS_COUNT) );

    // then
    ASSERT_EQ( c.GetControlVerticesCount(), POINTS_COUNT );
    for ( int i = 0; i < POINTS_COUNT; ++i ) {
        EXPECT_EQ( c.GetControlVertex(i), points[i] );
    }
}


/*++
    Test of the = operator of the BezierCurve.
--*/
TEST( BezierCurve, TestAssign )
{
    // given
    Point dummyPoint;
    BezierCurve c1( Point(10.0, 10.0), Point(30.0, 40.0), Point(40.0, 30.0) );
    BezierCurve c2( dummyPoint, dummyPoint, dummyPoint );
    BezierCurve c3( c1 );

    // when
    c1 = c1;
    c2 = c1;

    // then
    ASSERT_EQ( c1.GetControlVerticesCount(), c2.GetControlVerticesCount() );
    ASSERT_EQ( c1.GetControlVerticesCount(), c3.GetControlVerticesCount() );
    for ( int i = 0; i < c1.GetControlVerticesCount(); ++i ) {
        EXPECT_EQ( c1.GetControlVertex(i), c2.GetControlVertex(i) );
        EXPECT_EQ( c1.GetControlVertex(i), c3.GetControlVertex(i) );
    }
}


/*++
    Test of the GetOrder method of the BezierCurve.
--*/
TEST( BezierCurve, TestGetOrder )
{
    // given
    Point dummyPoint;
    BezierCurve c( dummyPoint, dummyPoint, dummyPoint );

    // when then
    EXPECT_EQ( c.GetOrder(), 2 );
}


/*++
    Test of the SetControlVerticesCount method of the BezierCurve.
--*/
TEST( BezierCurve, TestSetControlVerticesCount )
{
    // given
    BezierCurve c( ZERO_POINT, ZERO_POINT, ZERO_POINT );

    // when
    c.SetControlVerticesCount( 10 );

    //then
    EXPECT_EQ( c.GetControlVerticesCount(), 10 );
}


/*++
    Test of the SetControlVertices method of the BezierCurve.
--*/
TEST( BezierCurve, TestGetSetControlVertices )
{
    // given
    BezierCurve c( ZERO_POINT, ZERO_POINT, ZERO_POINT );

    // when
    Point somePoint( 1.0, 1.0 );
    c.SetControlVertices( std::vector<Point>(5, somePoint) );

    // then
    EXPECT_EQ( c.GetControlVerticesCount(), 5 );
    for ( int i =0; i < c.GetControlVerticesCount(); ++i ) {
        EXPECT_EQ( c.GetControlVertex(i), somePoint );
    }
}


/*++
    Test of the points on Bezier curve calculation.
--*/
TEST( BezierCurve, TestCalcPointOnCurve )
{
    // given
    Point p0( 10.0, 10.0 );
    Point p1( 30.0, 40.0 );
    Point p2( 40.0, 30.0 );
    BezierCurve c( p0, p1, p2 );

    // when then
    EXPECT_EQ( p0, c.CalcCharacterPoint(CHP_BEGIN) );
    EXPECT_EQ( p2, c.CalcCharacterPoint(CHP_END) );
}


/*++
    Test of the Bezier curve first and second derivatives calculation.
--*/
TEST( BezierCurve, TestCalcDerivatives )
{
    // given
    std::vector<Point> points;
    points.push_back( ZERO_POINT );
    points.push_back( Point(10.0, 10.0) );
    points.push_back( Point(20.0, 20.0) );
    points.push_back( Point(30.0, 30.0) );

    BezierCurve bc( points );
    Segment s( ZERO_POINT, Point(30.0, 30.0) );
    const Interval& bci = bc.GetParametricInterval();
    const Interval& si = s.GetParametricInterval();

    // when then
    EXPECT_TRUE( bc.CalcFirstDerivative(bci.begin_).IsCodirectional(
        s.CalcFirstDerivative(si.begin_)) );
    EXPECT_TRUE( bc.CalcFirstDerivative(bci.CalcMiddle()).IsCodirectional(
        s.CalcFirstDerivative(si.CalcMiddle())) );
    EXPECT_TRUE( bc.CalcFirstDerivative(bci.end_).IsCodirectional(
        s.CalcFirstDerivative(si.end_)) );
    EXPECT_TRUE( bc.CalcSecondDerivative(bci.begin_).IsCodirectional(
        s.CalcSecondDerivative(si.begin_)) );
    EXPECT_TRUE( bc.CalcSecondDerivative(bci.CalcMiddle()).IsCodirectional(
        s.CalcSecondDerivative(si.CalcMiddle())) );
    EXPECT_TRUE( bc.CalcSecondDerivative(bci.end_).IsCodirectional(
        s.CalcSecondDerivative(si.end_)) );
}


/*++
    Test of the Bezier curve first and second derivatives calculation, linear Bezier curve case.
--*/
TEST( BezierCurve, TestCalcDerivativesFirstOrder )
{
    // given
    const int POINTS_COUNT = 2;
    Point points[ POINTS_COUNT ] = {
        ZERO_POINT, Point(10.0, 10.0),
    };
    BezierCurve bc( std::vector<Point>(points, points + POINTS_COUNT) );
    Segment s( points[0], points[1] );
    const Interval& bci = bc.GetParametricInterval();
    const Interval& si = s.GetParametricInterval();

    // when then
    EXPECT_TRUE( bc.CalcFirstDerivative(bci.begin_).IsCodirectional(
        s.CalcFirstDerivative(si.begin_)) );
    EXPECT_TRUE( bc.CalcFirstDerivative(bci.CalcMiddle()).IsCodirectional(
        s.CalcFirstDerivative(si.CalcMiddle())) );
    EXPECT_TRUE( bc.CalcFirstDerivative(bci.end_).IsCodirectional(
        s.CalcFirstDerivative(si.end_)) );
    EXPECT_TRUE( bc.CalcSecondDerivative(bci.begin_).IsCodirectional(
        s.CalcSecondDerivative(si.begin_)) );
    EXPECT_TRUE( bc.CalcSecondDerivative(bci.CalcMiddle()).IsCodirectional(
        s.CalcSecondDerivative(si.CalcMiddle())) );
    EXPECT_TRUE( bc.CalcSecondDerivative(bci.end_).IsCodirectional(
        s.CalcSecondDerivative(si.end_)) );
}


/*++
    Test of the Bezier curve first derivative calculation.
--*/
TEST( BezierCurve, TestCalcFirstDerivative )
{
    // given
    BezierCurve c( ZERO_POINT, Point(5.0, 10.0), Point(10.0, 0.0) );

    // when
    Vector dMiddle = c.CalcFirstDerivative( c.GetParametricInterval().CalcMiddle() );

    // then
    EXPECT_TRUE( dMiddle.IsCodirectional(Vector(1.0, 0.0)) );
}


//-------------------------------------------------------------------------------------------------
// H E R M I T E   S P L I N E   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the Hermite spline construction.
--*/
TEST( HermiteSpline, TestConstruction )
{
    // given
    Point p0;
    Point p1;
    Point p2;
    std::vector<Point> ps2;
    ps2.push_back( p0 );
    ps2.push_back( p1 );
    ps2.push_back( p2 );

    // when
    HermiteSpline hs0;
    HermiteSpline hs1( ps2 );
    HermiteSpline hs2( hs1 );
    HermiteSpline hs3;

    hs3.AddVertex( p0 );
    hs3.AddVertex( p1 );
    hs3.AddVertex( p2 );

    // then
    ASSERT_EQ( hs0.GetVerticesCount(), 0 );
    ASSERT_EQ( hs1.GetVerticesCount(), 3 );
    ASSERT_EQ( hs2.GetVerticesCount(), 3 );
    ASSERT_EQ( hs3.GetVerticesCount(), 3 );

    EXPECT_EQ( hs0.GetParametricInterval().CalcLength(), 0.0 );
    EXPECT_EQ( hs1.GetParametricInterval().CalcLength(), 2.0 );
    EXPECT_EQ( hs2.GetParametricInterval().CalcLength(), 2.0 );
    EXPECT_EQ( hs3.GetParametricInterval().CalcLength(), 2.0 );

    for ( int i = 0; i < hs1.GetVerticesCount(); ++i ) {
        const Point& tp1 = hs1.GetVertex( i );
        const Point& tp2 = hs2.GetVertex( i );
        const Point& tp3 = hs3.GetVertex( i );
        ASSERT_TRUE( (tp1 == tp2) && (tp1 == tp3) );
    }
}


/*++
    Test of the AddVertex and RemoveVertex methods of the HermiteSpline.
--*/
TEST( HermiteSpline, TestAddRemoveVertex )
{
    // given
    HermiteSpline hs;

    // when
    hs.AddVertex( ZERO_POINT );
    hs.AddVertex( Point(10.0, 10.0), 0 );
    hs.AddVertex( Point(30.0, 0.0) );
    hs.AddVertex( Point(20.0, 45.0), 1 );
    hs.RemoveVertex( 0 );
    hs.SetVertex( 2, Point(31.0, 30.0) );

    // then
    ASSERT_EQ( hs.GetVerticesCount(), 3 );
    EXPECT_EQ( hs.GetVertex(0), Point(20.0, 45.0) );
    EXPECT_EQ( hs.GetVertex(1), ZERO_POINT );
    EXPECT_EQ( hs.GetVertex(2), Point(31.0, 30.0) );
}


/*++
    Test of the RemoveVertices methods of the HermiteSpline.
--*/
TEST( HermiteSpline, TestRemoveVertices )
{
    // given
    HermiteSpline hs;
    hs.AddVertex( ZERO_POINT );
    hs.AddVertex( Point(79.0, 12.0) );
    hs.AddVertex( Point(14.0, 97.0) );

    // when
    hs.RemoveVertices();

    // then
    EXPECT_EQ( hs.GetVerticesCount(), 0 );
    EXPECT_EQ( hs.GetParametricInterval(), ZERO_INTERVAL );
}


/*++
    Test of SimplifyByVerticesCount method of the HermiteSpline.
--*/
TEST( HermiteSpline, TestRearrageVertices1 )
{
    // given
    HermiteSpline hs0;

    HermiteSpline hs1;
    hs1.AddVertex( ZERO_POINT );
    hs1.AddVertex( Point(10.0, 10.0) );
    hs1.AddVertex( Point(30.0, 0.0) );

    HermiteSpline hs2;
    hs2.AddVertex( ZERO_POINT );
    hs2.AddVertex( Point(10.0, 0.0) );
    hs2.AddVertex( Point(20.0, 20.0) );
    hs2.AddVertex( Point(30.0, 20.0) );
    hs2.AddVertex( Point(50.0, 0.0) );
    hs2.AddVertex( Point(80.0, -10.0) );
    hs2.AddVertex( Point(110.0, -20.0) );

    HermiteSpline hs3( hs2 );
    HermiteSpline hs4( hs2 );

    // when
    bool result0 = hs0.RearrageVertices( 10 );
    bool result1 = hs1.RearrageVertices( 10 );
    bool result2 = hs2.RearrageVertices( 3 );
    bool result3 = hs3.RearrageVertices( 5 );
    bool result4 = hs4.RearrageVertices( 20 );

    // then
    EXPECT_FALSE( result0 );
    EXPECT_TRUE( result1 );
    EXPECT_TRUE( result2 );
    EXPECT_TRUE( result3 );
    EXPECT_TRUE( result4 );

    EXPECT_EQ( hs0.GetVerticesCount(), 0 );
    EXPECT_EQ( hs1.GetVerticesCount(), 10 );
    EXPECT_EQ( hs2.GetVerticesCount(), 3 );
    EXPECT_EQ( hs3.GetVerticesCount(), 5 );
    EXPECT_EQ( hs4.GetVerticesCount(), 20 );
}


/*++
    Test of SimplifyByVerticesCount method of the HermiteSpline.
--*/
TEST( HermiteSpline, TestRearrageVertices2 )
{
    // given
    HermiteSpline hs1;
    hs1.AddVertex( ZERO_POINT );
    hs1.AddVertex( Point(10.0, 10.0) );
    hs1.AddVertex( Point(15.0, 20.0) );
    hs1.AddVertex( Point(18.0, 30.0) );
    hs1.AddVertex( Point(23.0, 40.0) );

    // when
    HermiteSpline hs2( hs1 );
    hs2.RearrageVertices( 4 );

    std::vector<double> hs2Points;
    CalcUniformPointsOnCurve( hs2, hs2.GetVerticesCount(), hs2Points );

    // then
    for ( size_t i = 0; i < hs2Points.size(); ++i ) {
        double t1 = hs1.GetParametricInterval().CalcValueByNormalizedValue(
            hs2.GetParametricInterval().CalcNormalizedValue(hs2Points[i]) );
        Point p1 = hs1.CalcPointOnCurve( t1 );
        Point p2 = hs2.CalcPointOnCurve( hs2Points[i] );
        ASSERT_EQ( p1, p2 );
    }
}


/*++
    Test of the CalcPointOnCurve method of the HermiteSpline, parametric value inside parametric
    interval.
--*/
TEST( HermiteSpline, TestCalcPointOnCurve1 )
{
    // given
    Point p0( ZERO_POINT );
    Point p1( Point(10.0, 10.0) );
    Point p2( Point(30.0, 0.0) );
    HermiteSpline hs;
    hs.AddVertex( p0 );
    hs.AddVertex( p1 );
    hs.AddVertex( p2 );

    // when then
    EXPECT_EQ( p0, hs.CalcCharacterPoint(CHP_BEGIN) );
    EXPECT_EQ( p1, hs.CalcCharacterPoint(CHP_MIDDLE) );
    EXPECT_EQ( p2, hs.CalcCharacterPoint(CHP_END) );
}


/*++
    Test of the CalcPointOnCurve method of the HermiteSpline, linear Hermite spline case
    (parametric value inside and outside parametric interval).
--*/
TEST( HermiteSpline, TestCalcPointOnCurve2 )
{
    // given
    Point p0( Point(0.0, 0.0) );
    Point p1( Point(10.0, 10.0) );
    Point p2( Point(20.0, 20.0) );
    HermiteSpline hs;
    hs.AddVertex( p0 );
    hs.AddVertex( p1 );
    hs.AddVertex( p2 );
    Segment s( p0, p2 );

    // when
    Interval hermitWidenInterval( hs.GetParametricInterval() );
    Interval segmentWidenInterval( s.GetParametricInterval() );
    hermitWidenInterval.WidenOnNormalizedValue( 0.1, 0.2 );
    segmentWidenInterval.WidenOnNormalizedValue( 0.1, 0.2 );

    const int POINTS_COUNT = 27;
    std::vector<Point> hermitPoints;
    std::vector<Point> segmentPoints;
    CalcUniformPointsOnCurve( hs, POINTS_COUNT, hermitPoints, &hermitWidenInterval );
    CalcUniformPointsOnCurve( s, POINTS_COUNT, segmentPoints, &segmentWidenInterval );

    // then
    for ( size_t i = 0; i < hermitPoints.size(); ++i ) {
        ASSERT_EQ( hermitPoints[i], segmentPoints[i] );
    }
}


/*++
    Test of the CalcPointOnCurve method of the HermiteSpline, boundary cases.
--*/
TEST( HermiteSpline, TestCalcPointOnCurve3 )
{
    // given
    HermiteSpline hs;
    hs.AddVertex( Point(-12.0, 5.0) );
    hs.AddVertex( ZERO_POINT );
    hs.AddVertex( Point(10.0, 10.0) );
    hs.AddVertex( Point(20.0, -7.0) );
    hs.AddVertex( Point(30.0, 1.0) );

    // when
    Point beginPoint = hs.CalcPointOnCurve( hs.GetParametricInterval().begin_ );
    Point endPoint = hs.CalcPointOnCurve( hs.GetParametricInterval().end_ );

    // then
    EXPECT_EQ( beginPoint, hs.GetVertex(0) );
    EXPECT_EQ( endPoint, hs.GetVertex(hs.GetVerticesCount() - 1) );
}


/*++
    Test of the CalcPointOnCurve method of the HermiteSpline, vacuous Hermite spline (just try to
    calculate points on empty initialized by default constructor spline and check crashes
    absence).
--*/
TEST( HermiteSpline, TestCalcPointOnCurve4 )
{
    // given
    HermiteSpline hs;

    // when then
    hs.CalcPointOnCurve( 1.1 );
    hs.CalcPointOnCurve( -2.2 );
    hs.CalcPointOnCurve( 1003.0 );
}


/*++
    Test of the Hermite spline first and second derivatives calculation, linear Hermite spline case.
--*/
TEST( HermiteSpline, TestCalcDerivatives1 )
{
    // given
    HermiteSpline hs;
    hs.AddVertex( ZERO_POINT );
    hs.AddVertex( Point(10.0, 10.0) );
    hs.AddVertex( Point(20.0, 20.0) );

    Vector firstDerivative( 1.0, 1.0 );
    firstDerivative.Normalize();

    // when then
    std::vector<double> points;
    CalcUniformPointsOnCurve( hs, 25, points );

    for ( size_t i = 0; i < points.size(); ++i ) {

        Vector d1 = hs.CalcFirstDerivative( points[i] );
        d1.Normalize();
        ASSERT_EQ( d1, firstDerivative );

        Vector d2 = hs.CalcSecondDerivative( points[i] );
        ASSERT_EQ( d2, ZERO_VECTOR );
    }
}


/*++
    Test of the Hermite spline first and second derivatives calculation, circular Hermite spline
    case.
--*/
TEST( HermiteSpline, TestCalcDerivatives2 )
{
    // given
    Arc arc( 10.0 );
    std::vector<double> arcPoints;
    CalcUniformPointsOnCurve( arc, 25, arcPoints );

    HermiteSpline hs;
    for ( size_t i = 0; i < arcPoints.size(); ++i ) {
        hs.AddVertex( arc.CalcPointOnCurve(arcPoints[i]) );
    }

    // when
    std::vector<double> hermitPoints;
    CalcUniformPointsOnCurve( hs, 25, hermitPoints );

    for ( size_t i = 0; i < hermitPoints.size(); ++i ) {

        Point p = hs.CalcPointOnCurve( hermitPoints[i] );
        Vector d1 = hs.CalcFirstDerivative( hermitPoints[i] );

        Vector v1( ZERO_POINT, p );
        Vector v2( d1.CalcPerpendicular(false) );
        v1.Normalize();
        v2.Normalize();

        // 0.01 calculation mistake is acceptable (we have approximated circle, not real one)
        ASSERT_LE( ::fabs(v1.x_ - v2.x_), 0.01 );
        ASSERT_LE( ::fabs(v1.y_ - v2.y_), 0.01 );
    }
}


/*++
    Test of the Hermite spline parametric interval correctness after vertices structure (count)
    change.
--*/
TEST( HermiteSpline, TestUpdateInterval )
{
    // given when then
    HermiteSpline hs;
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 0.0) );

    hs.AddVertex( ZERO_POINT );
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 0.0) );

    hs.AddVertex( ZERO_POINT );
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 1.0) );

    hs.RemoveVertex( 0 );
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 0.0) );

    hs.AddVertex( ZERO_POINT, 0 );
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 1.0) );

    hs.AddVertex( ZERO_POINT );
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 2.0) );

    hs.AddVertex( ZERO_POINT );
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 3.0) );

    hs.RemoveVertex( 1 );
    hs.RemoveVertex( 0 );
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 1.0) );

    hs.RemoveVertex( 0 );
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 0.0) );

    hs.RemoveVertex( 0 );
    EXPECT_EQ( hs.GetParametricInterval(), Interval(0.0, 0.0) );
}


//-------------------------------------------------------------------------------------------------
// E Q U I D I S T A N T   C U R V E   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the equidistant curve construction.
--*/
TEST( EquidistantCurve, TestConstruction )
{
    // given
    Curve* pSourceCurve = new Segment( Point(10.0, 0.0), Point(20.0, 0.0) );
    Vector shiftDirection( 10.0, 10.0 );

    // when
    EquidistantCurve equidistantCurve1( pSourceCurve, shiftDirection );
    EquidistantCurve equidistantCurve2( equidistantCurve1 );

    // then
    EXPECT_EQ( &equidistantCurve1.GetSourceCurve(), pSourceCurve );
    EXPECT_EQ( &equidistantCurve2.GetSourceCurve(), pSourceCurve );
    EXPECT_EQ( equidistantCurve1.GetShiftDirection(), shiftDirection );
    EXPECT_EQ( equidistantCurve2.GetShiftDirection(), shiftDirection );
}


/*++
    Test of the = operator of the equidistant curve.
--*/
TEST( EquidistantCurve, TextAssign )
{
    // given
    Curve* pSourceCurve1 = new Segment( ZERO_POINT, Point(10.0, 10.0) );
    Curve* pSourceCurve2 = new Segment( ZERO_POINT, Point(20.0, 40.0) );
    Vector shiftDirection1( 10.0, 0.0 );
    Vector shiftDirection2( 0.0, 20.0);
    EquidistantCurve equidistantCurve1( pSourceCurve1, shiftDirection1 );
    EquidistantCurve equidistantCurve2( pSourceCurve2, shiftDirection2 );

    // when
    equidistantCurve2 = equidistantCurve1;

    // then
    EXPECT_EQ( &equidistantCurve2.GetSourceCurve(), pSourceCurve1 );
    EXPECT_EQ( equidistantCurve2.GetShiftDirection(), shiftDirection1 );
}


/*++
    Test of the = operator of the equidistant curve, self assignations.
--*/
TEST( EquidistantCurve, TestSelfAssign )
{
    // given
    Curve* pSourceCurve = new Segment( ZERO_POINT, ZERO_POINT );
    EquidistantCurve equidistant( pSourceCurve, ZERO_VECTOR );

    // when
    equidistant = equidistant;

    // then
    EXPECT_EQ( &equidistant.GetSourceCurve(), pSourceCurve );
}


/*++
    Test of the equidistant curve construction correctness.
--*/
TEST( EquidistantCurve, TestCalcPointOnCurve )
{
    // given
    const Vector EQUIDISTANT_SHIFT( 10.0, -10.0 );
    const int CURVES_COUNT = 3;
    Curve* pCurves[ CURVES_COUNT ] = {
        new Segment(Point(10.1, 11.5), Point(-5.8, 21.4)),
        new BezierCurve(Point(-0.5, -0.5), Point(8.5, 8.5), Point(6.5, 0.5)),
        new ArchimedianSpiral(DEFAULT_CS, 10.0, PI2),
    };

    // when then
    for ( int nCurve = 0; nCurve < CURVES_COUNT; ++nCurve ) {

        std::vector<Point> points;
        CalcUniformPointsOnCurve( *pCurves[nCurve], 2, points );

        std::vector<Point> equidistantPoints;
        EquidistantCurve zeroEquidistant( pCurves[nCurve], ZERO_VECTOR );
        CalcUniformPointsOnCurve( zeroEquidistant, points.size(), equidistantPoints );
        for ( size_t i = 0; i < points.size(); ++i ) {
            ASSERT_EQ( points[i], equidistantPoints[i] );
        }

        equidistantPoints.clear();
        EquidistantCurve equidistant( pCurves[nCurve], EQUIDISTANT_SHIFT );
        CalcUniformPointsOnCurve( equidistant, points.size(), equidistantPoints );
        for ( size_t i = 0; i < points.size(); ++i ) {
            ASSERT_TRUE( EQ(CalcDistance(points[i], equidistantPoints[i]),
                EQUIDISTANT_SHIFT.CalcLength()) );
        }
    }
}


/*++
    Test of the equidistant curve first and second derivatives calculation.
--*/
TEST( EquidistantCurve, TestCalcDerivatives )
{
    // given
    const Vector EQUIDISTANT_SHIFT( 10.0, -10.0 );
    const int CURVES_COUNT = 4;
    Curve* pCurves[ CURVES_COUNT ] = {
        new Segment(Point(10.1, 11.5), Point(-5.8, 21.4)),
        new Arc(Point(12.0, -7.0), Point(19.0, 21.0), Point(5.0, -14.0)),
        new ArchimedianSpiral(DEFAULT_CS, 10.0, PI2),
        new BezierCurve(Point(-0.5, -0.5), Point(8.5, 8.5), Point(6.5, 0.5)),
    };

    // when then
    for ( int nCurve = 0; nCurve < CURVES_COUNT; ++nCurve ) {

        AddReference( pCurves[nCurve] );

        EquidistantCurve zeroEquidistant( pCurves[nCurve], ZERO_VECTOR );
        EquidistantCurve someEquidistant( pCurves[nCurve], Vector(0.0, 10.0) );

        std::vector<double> points;
        CalcUniformPointsOnCurve( *pCurves[nCurve], 10, points );

        for ( size_t i = 0; i < points.size(); ++i ) {
            double t = points[ i ];
            ASSERT_EQ( zeroEquidistant.CalcFirstDerivative(t),
                pCurves[nCurve]->CalcFirstDerivative(t) );
            ASSERT_EQ( zeroEquidistant.CalcSecondDerivative(t),
                pCurves[nCurve]->CalcSecondDerivative(t) );
            ASSERT_EQ( someEquidistant.CalcFirstDerivative(t),
                pCurves[nCurve]->CalcFirstDerivative(t) );
            ASSERT_EQ( someEquidistant.CalcSecondDerivative(t),
                pCurves[nCurve]->CalcSecondDerivative(t) );
        }

        ReleaseReference( pCurves[nCurve] );
    }
}


//-------------------------------------------------------------------------------------------------
// P R O X Y   C U R V E   T E S T S
//------------------------------------------------------------------------------------------------


/*++
    Test of the proxy curve construction.
--*/
TEST( ProxyCurve, TestConstruction )
{
    // given
    Curve* pSourceCurve = new Arc( 10.0 );
    Interval proxyInterval( PI_DIV_6, PI_DIV_4 );

    // when
    ProxyCurve proxyCurve1( pSourceCurve );
    ProxyCurve proxyCurve2( pSourceCurve, proxyInterval );
    ProxyCurve proxyCurve3( proxyCurve1 );

    // then
    EXPECT_EQ( &proxyCurve1.GetSourceCurve(), pSourceCurve );
    EXPECT_EQ( &proxyCurve2.GetSourceCurve(), pSourceCurve );
    EXPECT_EQ( &proxyCurve3.GetSourceCurve(), pSourceCurve );
    EXPECT_EQ( proxyCurve1.GetParametricInterval(), pSourceCurve->GetParametricInterval() );
    EXPECT_EQ( proxyCurve2.GetParametricInterval(), proxyInterval );
    EXPECT_EQ( proxyCurve3.GetParametricInterval(), pSourceCurve->GetParametricInterval() );
}


/*++
    Test of the = operator of the proxy curve.
--*/
TEST( ProxyCurve, TextAssign )
{
    // given
    Interval sourceInterval( PI_DIV_6, PI_DIV_2 );
    Curve* pSourceCurve1 = new Arc( 10.0 );
    Curve* pSourceCurve2 = new Arc( 10.0, &sourceInterval );
    Interval proxyInterval( PI_DIV_4, PI_DIV_2 );
    ProxyCurve proxyCurve1( pSourceCurve1, proxyInterval );
    ProxyCurve proxyCurve2( pSourceCurve2 );

    // when
    proxyCurve2 = proxyCurve1;

    // then
    EXPECT_EQ( &proxyCurve2.GetSourceCurve(), pSourceCurve1 );
    EXPECT_EQ( proxyCurve2.GetParametricInterval(), proxyInterval );
}


/*++
    Test of the = operator of the proxy curve, self assignations.
--*/
TEST( ProxyCurve, TextSelfAssign )
{
    // given
    Curve* pSourceCurve = new Arc( 22.0 );
    ProxyCurve proxyCurve( pSourceCurve, Interval(PI_DIV_4, PI_DIV_2) );

    // when
    proxyCurve = proxyCurve;

    // then
    EXPECT_EQ( &proxyCurve.GetSourceCurve(), pSourceCurve );
}


/*++
    Test of the SetParametricInterval and ResetParametricInterval methods of the ProxyCurve.
--*/
TEST( ProxyCurve, TextSetResetParametricInterval )
{
    // given
    Interval sourceInterval( PI_DIV_4, PI_DIV_2 );
    Curve* pSourceCurve = new Arc( 10.0, &sourceInterval );
    ProxyCurve proxyCurve( pSourceCurve );

    // when then
    EXPECT_EQ( proxyCurve.GetParametricInterval(), sourceInterval );
    EXPECT_EQ( &proxyCurve.GetParametricInterval(), &pSourceCurve->GetParametricInterval() );

    proxyCurve.SetParametricInterval( PI_DIV_2_INTERVAL );
    EXPECT_EQ( proxyCurve.GetParametricInterval(), PI_DIV_2_INTERVAL );
    EXPECT_NE( &proxyCurve.GetParametricInterval(), &PI_DIV_2_INTERVAL );

    proxyCurve.ResetParametricInterval();
    EXPECT_EQ( proxyCurve.GetParametricInterval(), pSourceCurve->GetParametricInterval() );
    EXPECT_EQ( &proxyCurve.GetParametricInterval(), &pSourceCurve->GetParametricInterval() );
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------

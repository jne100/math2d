///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#include <vector>
#include <gtest/gtest.h>
#include <m2d_visitors.h>
#include <m2d_curves_test.h>


using namespace m2d;


//-------------------------------------------------------------------------------------------------
// C O L L E C T   E D I T I N G   P O I N T S   V I S I T O R   T E S T S
//-------------------------------------------------------------------------------------------------


class VisitedCurveTestWithParam : public testing::TestWithParam<ECurveTypes> {
public:
    virtual ~VisitedCurveTestWithParam() {}

    virtual void SetUp()
    {
        pCurve_ = CreateTestCurve( GetParam() );
    }

    virtual void TearDown()
    {
        delete pCurve_;
        pCurve_ = NULL;
    }

protected:
    Curve* pCurve_;
};


INSTANTIATE_TEST_CASE_P(
    Common,
    VisitedCurveTestWithParam,
    testing::Values(
        CT_SEGMENT,
        CT_ARC,
        CT_ARCHIMEDIAN_SPIRAL,
        CT_LOGARITHMIC_SPIRAL,
        CT_POLYLINE,
        CT_BEZIER_CURVE,
        CT_HERMITE_SPLINE,
        CT_EQUIDISTANT_CURVE,
        CT_PROXY_CURVE) );


/*++
    Parametrized test of the mathematical curve visitor ChangeEditingPointVisitor.
--*/
TEST_P( VisitedCurveTestWithParam, TestChangeEditingPointVisitor )
{
    ASSERT_TRUE( pCurve_ != 0 );

    // given
    Vector shift( 10.0, -20.0 );

    // when
    CollectEditingPointsVisitor collectVisitor;
    pCurve_->Accept( collectVisitor);

    for ( size_t nPoint = 0; nPoint < collectVisitor.editingPoints_.size(); ++nPoint ) {
        ChangeEditingPointVisitor changeVisitor( nPoint, shift );
        pCurve_->Accept( changeVisitor );
    }

    std::vector<Point> editingPointsBefore;
    collectVisitor.editingPoints_.swap( editingPointsBefore );
    pCurve_->Accept( collectVisitor );

    // then
    ASSERT_EQ( editingPointsBefore.size(), collectVisitor.editingPoints_.size() );
    for ( size_t nPoint = 0; nPoint < editingPointsBefore.size(); ++nPoint ) {
        Point p = editingPointsBefore[ nPoint ];
        p.Transfer( shift );
        EXPECT_EQ( p, collectVisitor.editingPoints_[nPoint] );
    }
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------

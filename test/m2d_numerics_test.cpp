///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#include <algorithm>
#include <gtest/gtest.h>
#include <m2d_numerics.h>
#include <m2d_curves_test.h>


using namespace m2d;


//-------------------------------------------------------------------------------------------------
// C U R V E   U T I L I T I E S   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the CalcUniformPointsOnCurve function, general cases.
--*/
TEST( CurveUtilities, TestCalcUniformPointsOnCurve1 )
{
    // given
    Point p1( 10.0, 10.0 );
    Point p2( 20.0, 10.0 );
    Segment s( p1, p2 );

    // when
    std::vector<Point> points1;
    std::vector<Point> points2;
    std::vector<Point> points3;
    CalcUniformPointsOnCurve( s, 1, points1 );
    CalcUniformPointsOnCurve( s, 2, points2 );
    CalcUniformPointsOnCurve( s, 3, points3 );

    // then
    EXPECT_TRUE( points1.empty() );
    ASSERT_EQ( points2.size(), 2 );
    ASSERT_EQ( points3.size(), 3 );
    EXPECT_TRUE( (points2.front() == p1) && (points3.front() == p1) );
    EXPECT_TRUE( (points2.back() == p2) && (points3.back() == p2) );
    EXPECT_EQ( points3[1], Point(15.0, 10.0) );
}


/*++
    Test of the CalcUniformPointsOnCurve function, general cases.
--*/
TEST( CurveUtilities, TestCalcUniformPointsOnCurve2 )
{
    // given
    Segment s( ZERO_POINT, Point(10.0, 0.0) );

    // when
    std::vector<double> points1;
    std::vector<Point> points2;
    CalcUniformPointsOnCurve( s, 10, points1 );
    CalcUniformPointsOnCurve( s, 10, points2 );

    // then
    ASSERT_TRUE( (points1.size() == 10) && (points2.size() == 10) );
    for ( size_t i = 0; i < points1.size(); ++i ) {
        EXPECT_EQ( points2[i], s.CalcPointOnCurve(points1[i]) );
    }
}


/*++
    Test of the CalcUniformPointsOnCurve function, calculation of the points on the specified parametric
    interval.
--*/
TEST( CurveUtilities, TestCalcUniformPointsOnCurve3 )
{
    // given
    Point p2( 10.0, 10.0 );
    Segment s( ZERO_POINT, p2 );

    // when
    Interval interval( s.GetParametricInterval().CalcMiddle(),
        s.GetParametricInterval().end_ );
    std::vector<Point> points1;
    std::vector<Point> points2;
    std::vector<Point> points3;
    CalcUniformPointsOnCurve( s, 1, points1, &interval );
    CalcUniformPointsOnCurve( s, 2, points2, &interval );
    CalcUniformPointsOnCurve( s, 3, points3, &interval );

    // then
    EXPECT_TRUE( points1.empty() );
    ASSERT_EQ( points2.size(), 2 );
    ASSERT_EQ( points3.size(), 3 );
    EXPECT_TRUE( (points2.front() == Point(5.0, 5.0)) && (points3.front() == Point(5.0, 5.0)) );
    EXPECT_TRUE( (points2.back() == p2) && (points3.back() == p2) );
    EXPECT_EQ( points3[1], Point(7.5, 7.5) );
}


//-------------------------------------------------------------------------------------------------
// T E S S E L A T I O N   U T I L I T I E S   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the CalcCurveUniformTesselation function.
--*/
TEST( TesselationUtilities, TestCalcCurveUniformTesselation )
{
    // given
    Segment s( ZERO_POINT, Point(37.0, -29.0) );

    // when
    std::vector<SegmentDescription> segments1;
    std::vector<SegmentDescription> segments2;
    CalcCurveUniformTesselation( s, 0, segments1 );
    CalcCurveUniformTesselation( s, 20, segments2 );

    // then
    EXPECT_EQ( segments1.size(), 0 );
    EXPECT_EQ( segments2.size(), 20 );
    for ( size_t i = 0; i < segments2.size(); ++i ) {
        ASSERT_TRUE( EQNil(s.CalcDistanceToPoint(segments2[i].p1_)) );
        ASSERT_TRUE( EQNil(s.CalcDistanceToPoint(segments2[i].p2_)) );
    }
}


/*++
    Test of the CalcCurveTesselation function.
--*/
TEST( TesselationUtilities, TestCalcCurveTesselation )
{
    // given
    Segment s( ZERO_POINT, Point(35.0, 95.0) );

    // when
    std::vector<SegmentDescription> segments;
    CalcCurveTesselation( s, Radians(5.0), segments );

    // then
    ASSERT_EQ( segments.size(), 1 );
    ASSERT_TRUE( EQNil(s.CalcDistanceToPoint(segments[0].p1_)) );
    ASSERT_TRUE( EQNil(s.CalcDistanceToPoint(segments[0].p2_)) );
}


/*++
    Test of the CalcCurveSmoothTesselation function.
--*/
TEST( TesselationUtilities, TestCalcCurveSmoothTesselation )
{
    // given
    Segment s( ZERO_POINT, Point(-90.0, -32.0) );

    // when
    std::vector<QuadraticBezierCurveDescription> beziers1;
    std::vector<QuadraticBezierCurveDescription> beziers2;
    CalcCurveSmoothTesselation( s, 0, beziers1 );
    CalcCurveSmoothTesselation( s, 20, beziers2 );

    // then
    EXPECT_EQ( beziers1.size(), 0 );
    EXPECT_EQ( beziers2.size(), 20 );
    for ( size_t i = 0; i < beziers2.size(); ++i ) {
        ASSERT_TRUE( EQNil(s.CalcDistanceToPoint(beziers2[i].p1_)) );
        ASSERT_TRUE( EQNil(s.CalcDistanceToPoint(beziers2[i].p2_)) );
        ASSERT_TRUE( EQNil(s.CalcDistanceToPoint(beziers2[i].p3_)) );
    }
}


/*++
    Test of the CalcPolylineClosedSmoothTesselation function (boundary cases verification).
--*/
TEST( TesselationUtilities, TestCalcPolylineClosedSmoothTesselation )
{
    // given
    std::vector<Point> tp;
    Polyline p0;

    tp.push_back( ZERO_POINT );
    Polyline p1( tp );

    tp.push_back( Point(10.0, 10.0) );
    Polyline p2( tp );

    tp.push_back( Point(20.0, 0.0) );
    Polyline p3( tp );

    // when
    std::vector<QuadraticBezierCurveDescription> r0;
    std::vector<QuadraticBezierCurveDescription> r1;
    std::vector<QuadraticBezierCurveDescription> r2;
    std::vector<QuadraticBezierCurveDescription> r3;
    CalcPolylineClosedSmoothTesselation( p0, r0 );
    CalcPolylineClosedSmoothTesselation( p1, r1 );
    CalcPolylineClosedSmoothTesselation( p2, r2 );
    CalcPolylineClosedSmoothTesselation( p3, r3 );

    // then
    EXPECT_EQ( r0.size(), 0 );
    EXPECT_EQ( r1.size(), 0 );
    EXPECT_EQ( r2.size(), 0 );
    ASSERT_EQ( r3.size(), 3 );

    EXPECT_EQ( r3[0].p2_, p3.GetVertex(0) );
    EXPECT_EQ( r3[1].p2_, p3.GetVertex(1) );
    EXPECT_EQ( r3[2].p2_, p3.GetVertex(2) );
}


//-------------------------------------------------------------------------------------------------
// I N T E R S E C T I O N   U T I L I T I E S   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Helper utility aimed to check commutativity of two curves intersection.
--*/
bool IsCommutativeCurvesIntersection( const Curve& curve1, const Curve& curve2,
    const Interval* pInterval1 = 0, const Interval* pInterval2 = 0 )
{
    std::vector<Point> pts1;
    std::vector<Point> pts2;
    CalcCurvesIntersectionPoints( curve1, curve2, pts1, pInterval1, pInterval2 );
    CalcCurvesIntersectionPoints( curve2, curve1, pts2, pInterval2, pInterval1 );

    bool result = true;
    if ( pts1.size() != pts2.size() ) {
        result = false;
    }

    for ( size_t i = 0; (i < pts1.size()) && (result); ++i ) {
        result = ( std::count(pts1.begin(), pts1.end(), pts1[i]) ==
            std::count(pts2.begin(), pts2.end(), pts1[i]) );
    }

    return result;
}


/*++
    Test of the two lines intersection point calculation function, each line represented by one
    point and direction vector (intersection inside parametric interval).
--*/
TEST( IntersectionUtilities, TestCalcLinesIntersectionPoint1 )
{
    // given
    Point p1 = ZERO_POINT;
    Vector v1( p1, Point(10.0, 10.0) );
    Point p2( 0.0, 10.0 );
    Vector v2( p2, Point(10.0, 0.0) );

    // when
    double t1;
    double t2;
    bool result = CalcLinesIntersectionPoint( p1, v1, p2, v2, t1, t2 );

    // then
    ASSERT_TRUE( result );
    EXPECT_TRUE( EQ(t1, 0.5) );
    EXPECT_TRUE( EQ(t2, 0.5) );
}


/*++
    Test of the two lines intersection point calculation function, each line represented by one
    point and direction vector (intersection outside parametric interval).
--*/
TEST( IntersectionUtilities, TestCalcLinesIntersectionPoint2 )
{
    // given
    Point p1( 0.0, 20.0 );
    Vector v1( p1, Point(20.0, 0.0) );
    Point p2 = ZERO_POINT;
    Vector v2( p2, Point(5.0, 5.0) );

    // when
    double t1;
    double t2;
    bool result = CalcLinesIntersectionPoint( p1, v1, p2, v2, t1, t2 );

    // then
    ASSERT_TRUE( result );
    EXPECT_TRUE( EQ(t1, 0.5) );
    EXPECT_TRUE( EQ(t2, 2.0) );
}


/*++
    Test of the two lines intersection point calculation function, each line represented by one
    point and direction vector (boundary cases).
--*/
TEST( IntersectionUtilities, TestCalcLinesIntersectionPoint3 )
{
    // given
    Point p1( 40.0, -30.0 );
    Point p2( 90.0, 2.0 );
    Point p3( -13.0, -9.0 );

    // when
    double t1;
    double t2;
    bool result = CalcLinesIntersectionPoint( p1, Vector(p1, p2), p2, Vector(p2, p3), t1, t2 );

    // then
    ASSERT_TRUE( result );
    EXPECT_TRUE( EQ(t1, 1.0) );
    EXPECT_TRUE( EQ(t2, 0.0) );
}


/*++
    Test of the two lines intersection point calculation function, parallel lines case.
--*/
TEST( IntersectionUtilities, TestCalcLinesIntersectionPoint4 )
{
    // given
    Point p1 = ZERO_POINT;
    Point p2( 10.0, 0.0 );
    Vector v( 10.0, 10.0 );

    // when then
    double dummy1;
    double dummy2;
    EXPECT_FALSE( CalcLinesIntersectionPoint(p1, v, p2, v, dummy1, dummy2) );
}


/*++
    Test of the two lines intersection point calculation function, each line represented by one
    point and direction vector.
--*/
TEST( IntersectionUtilities, TestCalcLinesIntersectionPoint5 )
{
    // given
    Point p1 = ZERO_POINT;
    Point p2( 4.0, 0.0 );
    Vector v1( 1.0, 1.0 );
    Vector v2( -1.0, 1.0 );

    // when
    Point p;
    bool result = CalcLinesIntersectionPoint( p1, v1, p2, v2, p );

    // then
    ASSERT_TRUE( result );
    EXPECT_EQ( p, Point(2.0, 2.0) );
}


/*++
    Test of the two lines intersection point calculation function, parallel lines case.
--*/
TEST( IntersectionUtilities, TestCalcLinesIntersectionPoint6 )
{
    // given
    Point p1 = ZERO_POINT;
    Point p2( 10.0, 0.0 );
    Vector v( 1.0, 1.0 );

    // when then
    Point dummy;
    EXPECT_FALSE( CalcLinesIntersectionPoint(p1, v, p2, v, dummy) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of two segments.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints1 )
{
    // given
    Segment s1( ZERO_POINT, Point(10.0, 10.0) );
    Segment s2( Point(0.0, 10.0), Point(10.0, 0.0) );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( s1, s2, pts );

    // then
    ASSERT_EQ( pts.size(), 1 );
    EXPECT_EQ( pts[0], Point(5.0, 5.0) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(s1, s2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of two segments.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints2 )
{
    // given
    Point p1( 10.0, 5.0 );
    Segment s1( Point(0.0, 0.0), p1 );
    Segment s2( p1, Point(20.0, 20.0) );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( s1, s2, pts );

    // then
    ASSERT_EQ( pts.size(), 1 );
    EXPECT_EQ( pts[0], p1 );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(s1, s2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of two parallel segments.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints3 )
{
    // given
    Segment s1( ZERO_POINT, Point(100.0, 0.0) );
    Segment s2( s1 );
    s2.Transfer( Vector(50.0, 50.0) );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( s1, s2, pts );

    // then
    EXPECT_EQ( pts.size(), 0 );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(s1, s2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of two arcs.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints4 )
{
    // given
    Arc a1( 10, Point(-10.0, 0.0) );
    Arc a2( 10, Point(10.0, 0.0) );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( a1, a2, pts );

    // then
    ASSERT_EQ( pts.size(), 2 );
    EXPECT_EQ( pts[0], Point(0.0, 0.0) );
    EXPECT_EQ( pts[1], Point(0.0, 0.0) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(a1, a2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of two splines.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints5 )
{
    // given
    Point p1( 5.0, 10.0 );
    Point p2( 10.0, 10.0 );

    HermiteSpline hs1;
    hs1.AddVertex( Point(0.0, 0.0) );
    hs1.AddVertex( p1 );
    hs1.AddVertex( p2 );
    hs1.AddVertex( Point(15.0, 0.0) );

    HermiteSpline hs2;
    hs2.AddVertex( Point(0.0, 20.0) );
    hs2.AddVertex( p1 );
    hs2.AddVertex( p2 );
    hs2.AddVertex( Point(15.0, 20.0) );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( hs1, hs2, pts );

    // then
    ASSERT_EQ( pts.size(), 2 );
    EXPECT_TRUE( std::find(pts.begin(), pts.end(), p1) != pts.end() );
    EXPECT_TRUE( std::find(pts.begin(), pts.end(), p2) != pts.end() );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(hs1, hs2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of two splines.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints6 )
{
    // given
    Point p1( 5.0, 10.0 );
    Point p2( 10.0, 10.0 );

    std::vector<Point> hp1;
    hp1.push_back( Point(0.0, 0.0) );
    hp1.push_back( p1 );
    hp1.push_back( p2 );
    hp1.push_back( Point(15.0, 0.0) );

    std::vector<Point> hp2;
    hp2.push_back( Point(0.0, 20.0) );
    hp2.push_back( p1 );
    hp2.push_back( p2 );
    hp2.push_back( Point(15.0, 20.0) );

    HermiteSpline hs1( hp1 );
    HermiteSpline hs2( hp2 );

    std::reverse( hp1.begin(), hp1.end() );
    std::reverse( hp2.begin(), hp2.end() );
    HermiteSpline rhs1( hp1 );
    HermiteSpline rhs2( hp2 );

    // when
    const int PTS_COUNT = 4;
    std::vector<Point> ptss[ PTS_COUNT ];
    CalcCurvesIntersectionPoints( hs1, hs2, ptss[0] );
    CalcCurvesIntersectionPoints( hs1, rhs2, ptss[1] );
    CalcCurvesIntersectionPoints( rhs1, hs2, ptss[2] );
    CalcCurvesIntersectionPoints( rhs1, rhs2, ptss[3] );

    // then
    for ( int i = 0; i < PTS_COUNT; ++i ) {
        ASSERT_EQ( ptss[i].size(), 2 );
        ASSERT_TRUE( std::find(ptss[i].begin(), ptss[i].end(), p1) != ptss[i].end() );
        ASSERT_TRUE( std::find(ptss[i].begin(), ptss[i].end(), p2) != ptss[i].end() );
    }
    EXPECT_TRUE( IsCommutativeCurvesIntersection(hs1, hs2) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(hs1, rhs2) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(rhs1, hs2) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(rhs1, rhs2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of segment and arc.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints7 )
{
    // given
    Interval i( PI, PI2 );
    Arc a1( 10.0, &i );
    Segment s1( Point(-50.0, 0.0), Point(50.0, 0.0) );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( a1, s1, pts );

    // then
    ASSERT_EQ( pts.size(), 2 );
    EXPECT_TRUE( std::find(pts.begin(), pts.end(), Point(-10.0, 0.0)) != pts.end() );
    EXPECT_TRUE( std::find(pts.begin(), pts.end(), Point(10.0, 0.0)) != pts.end() );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(a1, s1) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of segment and arc.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints8 )
{
    // given
    Arc a1( Point(0.0, 10.0), Point(10.0, 0.0), Point(0.0, -10.0) );
    Segment s1( Point(-50.0, 0.0), Point(50.0, 0.0) );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( a1, s1, pts );

    // then
    ASSERT_EQ( pts.size(), 1 );
    EXPECT_EQ( pts[0], Point(10.0, 0.0) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(a1, s1) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of segment and spline.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints9 )
{
    // given
    Segment s1( Point(-100.0, 50.0), Point(100.0, 50.0) );
    HermiteSpline hs1;
    hs1.AddVertex( Point(-10.0, 0.0) );
    hs1.AddVertex( Point(0.0, 100.0) );
    hs1.AddVertex( Point(10.0, 0.0) );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( hs1, s1, pts );

    // then
    ASSERT_EQ( pts.size(), 2 );
    EXPECT_TRUE( EQ(pts[0].y_, 50.0) );
    EXPECT_TRUE( EQ(pts[1].y_, 50.0) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(hs1, s1) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of segment and spline.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints10 )
{
    // given
    Segment s1( Point(-100.0, 50.0), Point(100.0, 50.0) );
    HermiteSpline hs1;
    hs1.AddVertex( Point(0.0, 0.0) );
    hs1.AddVertex( Point(10.0, 100.0) );
    hs1.AddVertex( Point(20.0, 0.0) );
    hs1.AddVertex( Point(30.0, 100.0) );
    hs1.AddVertex( Point(40.0, 0.0) );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( hs1, s1, pts );

    // then
    ASSERT_EQ( pts.size(), 4 );
    for ( size_t i = 0; i < pts.size(); ++i ) {
        ASSERT_TRUE( EQ(pts[i].y_, 50.0) );
    }
    EXPECT_TRUE( IsCommutativeCurvesIntersection(hs1, s1) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of two polylines.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints11 )
{
    // given
    Polyline pl1;
    Polyline pl2;

    const int COUNT = 100;
    std::vector<Point> intersections;
    for ( int i = 0; i < COUNT; ++i ) {
        pl1.AddVertex( Point(i, (i % 2 == 0) ? -1.0 : 1.0) );
        pl2.AddVertex( Point(i, (i % 2 == 0) ? 1.0 : -1.0) );
        if ( i != 0 ) {
            intersections.push_back( Point(i - 0.5, 0.0) );
        }
    }

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( pl1, pl2, pts );

    // then
    ASSERT_EQ( pts.size(), intersections.size() );
    for ( size_t i = 0; i < pts.size(); ++i ) {
        EXPECT_TRUE( std::find(intersections.begin(), intersections.end(), pts[i]) !=
            intersections.end() );
    }
    EXPECT_TRUE( IsCommutativeCurvesIntersection(pl1, pl2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersections on specified parametric
    intervals.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints12 )
{
    // given
    Segment s1( ZERO_POINT, Point(10.0, 10.0) );
    Segment s2( Point(10.0, 0.0), Point(0.0, 10.0) );

    Interval i100( 0.0, 1.0 );
    Interval i75( 0.0, 0.75 );
    Interval i50( 0.0, 0.5 );
    Interval i25( 0.0, 0.25 );

    // when
    std::vector<Point> pts[ 4 ];
    CalcCurvesIntersectionPoints( s1, s2, pts[0], &i75, &i100 );
    CalcCurvesIntersectionPoints( s1, s2, pts[1], &i25, &i100 );
    CalcCurvesIntersectionPoints( s1, s2, pts[2], &i50, &i100 );
    CalcCurvesIntersectionPoints( s1, s2, pts[3], &i100, &i25 );

    // then
    ASSERT_EQ( pts[0].size(), 1 );
    ASSERT_EQ( pts[1].size(), 0 );
    ASSERT_EQ( pts[2].size(), 1 );
    ASSERT_EQ( pts[3].size(), 0 );
    EXPECT_EQ( pts[0].front(), Point(5.0, 5.0) );
    EXPECT_EQ( pts[2].front(), Point(5.0, 5.0) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(s1, s2, &i75, &i100) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(s1, s2, &i25, &i100) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(s1, s2, &i50, &i100) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(s1, s2, &i100, &i25) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of extra small and extra large
    curves.
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints13 )
{
    // given
    Point sp( 0.01, 0.0 );
    BezierCurve sc1( Point(0.0, 0.0), Point(0.005, 0.02), sp );
    BezierCurve sc2( sp, Point(0.015, 0.02), Point(0.02, 0.0) );

    Point lp( sp );
    BezierCurve lc1( sc1 );
    BezierCurve lc2( sc2 );

    const double COEF = 1e+10;
    lp.Scale( COEF, COEF );
    lc1.Scale( COEF, COEF );
    lc2.Scale( COEF, COEF );

    // when
    std::vector<Point> spts;
    CalcCurvesIntersectionPoints( sc1, sc2, spts );
    std::vector<Point> lpts;
    CalcCurvesIntersectionPoints( lc1, lc2, lpts );

    // then
    ASSERT_EQ( spts.size(), 1 );
    ASSERT_EQ( spts[0], sp );
    ASSERT_EQ( lpts.size(), 1 );
    ASSERT_EQ( lpts[0], lp );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(sc1, sc2) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(lc1, lc2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of completely coincident curves
    (check of crashes and hungs absence).
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints14 )
{
    // given
    HermiteSpline hs1;
    hs1.AddVertex( Point(45.0, 98.0) );
    hs1.AddVertex( Point(116.0, 110.0) );
    hs1.AddVertex( Point(250.0, 280.0) );

    HermiteSpline hs2( hs1 );

    // when
    std::vector<Point> pts1;
    CalcCurvesIntersectionPoints( hs1, hs1, pts1 );
    std::vector<Point> pts2;
    CalcCurvesIntersectionPoints( hs1, hs2, pts1 );

    // then
    ASSERT_EQ( pts1.size(), 0 );
    ASSERT_EQ( pts2.size(), 0 );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(hs1, hs1) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(hs1, hs2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of two uninitialized curves
    (check of crashes and hungs absence).
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints15 )
{
    // given
    HermiteSpline hs1;
    HermiteSpline hs2;
    Segment s1( ZERO_POINT, ZERO_POINT );
    Segment s2( ZERO_POINT, ZERO_POINT );

    // when
    std::vector<Point> ptss[ 3 ];
    CalcCurvesIntersectionPoints( hs1, hs2, ptss[0] );
    CalcCurvesIntersectionPoints( s1, s2, ptss[1] );

    // then
    EXPECT_EQ( ptss[0].size(), 0 );
    EXPECT_EQ( ptss[1].size(), 0 );
    EXPECT_EQ( ptss[2].size(), 0 );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(hs1, hs2) );
    EXPECT_TRUE( IsCommutativeCurvesIntersection(s1, s2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, load testing (check of crashes and hungs
    absence).
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints16 )
{
    // given
    std::vector<Curve*> curves;
    for ( int i = 0; i < CT_COUNT; ++i ) {
        curves.push_back( CreateTestCurve(static_cast<ECurveTypes>(i)) );
    }

    // when then
    for ( size_t k = 0; k < curves.size(); ++k ) {
        for ( size_t i = 0; i < curves.size(); ++i ) {
            if ( k == i ) {
                continue;
            }
            if ( (curves[i] != 0) && (curves[k] != 0) ) {
                std::vector<Point> dummy;
                CalcCurvesIntersectionPoints( *curves[k], *curves[i], dummy );
                EXPECT_TRUE( IsCommutativeCurvesIntersection(*curves[k], *curves[i]) );
            }
        }
    }

    for ( size_t i = 0; i < curves.size(); ++i ) {
        delete curves[ i ];
    }
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of complicated splines (use of
    precalculated data for verification).
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints17 )
{
    // given
    HermiteSpline hs1;
    hs1.AddVertex( Point(44.6, -60.3) );
    hs1.AddVertex( Point(0.0, 82.8) );
    hs1.AddVertex( Point(-4.6, 32.8) );
    hs1.AddVertex( Point(-74.0, -57.6) );

    HermiteSpline hs2;
    hs2.AddVertex( Point(-68.7, -18.8) );
    hs2.AddVertex( Point(12.7, 69.4) );
    hs2.AddVertex( Point(-12.5, 49.8) );
    hs2.AddVertex( Point(24.6, 91.3) );

    const int INTERSECTIONS_COUNT = 6;
    Point intersections[ INTERSECTIONS_COUNT ] = {
        Point(6.943936, 63.024965),
        Point(6.029535, 65.708276),
        Point(4.646025, 69.725583),
        Point(1.274227, 65.988633),
        Point(1.490706, 61.606959),
        Point(1.50152, 57.029361),
    };

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( hs1, hs2, pts );

    // then
    EXPECT_EQ( pts.size(), INTERSECTIONS_COUNT );
    for ( int i = 0; i < INTERSECTIONS_COUNT; ++i ) {
        EXPECT_TRUE( std::find(pts.begin(), pts.end(), intersections[i]) != pts.end() );
    }
    EXPECT_TRUE( IsCommutativeCurvesIntersection(hs1, hs2) );
}


/*++
    Test of the CalcCurvesIntersectionPoints method, intersection of complicated splines (use of
    precalculated data for verification).
--*/
TEST( IntersectionUtilities, TestCalcCurvesIntersectionPoints18 )
{
    // given
    HermiteSpline hs1;
    hs1.AddVertex( Point(285.0, 818.0) );
    hs1.AddVertex( Point(363.2, 730.4) );
    hs1.AddVertex( Point(446.6, 644.7) );
    hs1.AddVertex( Point(529.0, 559.0) );

    HermiteSpline hs2;
    hs2.AddVertex( Point(506.0, 688.0) );
    hs2.AddVertex( Point(507.0, 645.0) );
    hs2.AddVertex( Point(508.7, 600.0) );
    hs2.AddVertex( Point(509.0, 557.0) );

    const Point intersection( 509.016603, 579.879812 );

    // when
    std::vector<Point> pts;
    CalcCurvesIntersectionPoints( hs1, hs2, pts );

    // then
    ASSERT_EQ( pts.size(), 1 );
    EXPECT_EQ( pts[0], intersection );
}


//-------------------------------------------------------------------------------------------------
// P R O J E C T I O N   U T I L I T I E S   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the CalcVectorProjection method (simple cases).
--*/
TEST( ProjectionUtilities, TestCalcVectorProjectionAndLength1 )
{
    // given
    Vector src1( 5.0, 10.0 );
    Vector dest1( 10.0, 20.0 );

    Vector src2( -10.0, 10.0 );
    Vector dest2( 20.0, 20.0 );

    Vector src3( 10.0, 10.0 );
    Vector dest3( 20.0, 0.0 );

    // when
    const int COUNT = 3;
    Vector resultVectors[ COUNT ];
    double resultLengths[ COUNT ];

    CalcVectorProjection( src1, dest1, resultVectors[0] );
    resultLengths[0] = CalcVectorProjectionLength( src1, dest1 );

    CalcVectorProjection( src2, dest2, resultVectors[1] );
    resultLengths[1] = CalcVectorProjectionLength( src2, dest2 );

    CalcVectorProjection( src3, dest3, resultVectors[2] );
    resultLengths[2] = CalcVectorProjectionLength( src3, dest3 );

    // then
    EXPECT_EQ( resultVectors[0], src1 );
    EXPECT_TRUE( EQ(resultLengths[0], src1.CalcLength()) );

    EXPECT_EQ( resultVectors[1], ZERO_VECTOR );
    EXPECT_TRUE( EQNil(resultLengths[1]) );

    EXPECT_EQ( resultVectors[2], Vector(10.0, 0.0) );
    EXPECT_TRUE( EQ(resultLengths[2], 10.0) );
}


/*++
    Test of the CalcVectorProjection method.
--*/
TEST( ProjectionUtilities, TestCalcVectorProjectionAndLength2 )
{
    // given
    Vector src1( 0.0, 10.0 );
    Vector src2( -10.0, 0.0 );
    Vector dest( 10.0, 10.0 );
    const Vector result1( 5.0, 5.0 );
    const Vector result2( -5.0, -5.0 );

    // when
    const int COUNT = 2;
    Vector resultVectors[ COUNT ];
    double resultLengths[ COUNT ];

    CalcVectorProjection( src1, dest, resultVectors[0] );
    resultLengths[0] = CalcVectorProjectionLength( src1, dest );

    CalcVectorProjection( src2, dest, resultVectors[1] );
    resultLengths[1] = CalcVectorProjectionLength( src2, dest );

    // then
    EXPECT_EQ( resultVectors[0], result1 );
    EXPECT_TRUE( EQ(resultLengths[0], result1.CalcLength()) );
    EXPECT_EQ( resultVectors[1], result2 );
    EXPECT_TRUE( EQ(resultLengths[1], result2.CalcLength()) );
}


/*++
    Test of the CalcVectorProjection method (boundary cases verification).
--*/
TEST( ProjectionUtilities, TestCalcVectorProjectionAndLength3 )
{
    // given
    Vector v( 10.0, 20.0 );

    // when
    const int COUNT = 2;
    Vector resultVectors[ COUNT ];
    double resultLengths[ COUNT ];
    CalcVectorProjection( ZERO_VECTOR, v, resultVectors[0] );
    CalcVectorProjection( v, ZERO_VECTOR, resultVectors[1] );
    resultLengths[0] = CalcVectorProjectionLength( ZERO_VECTOR, v );
    resultLengths[1] = CalcVectorProjectionLength( v, ZERO_VECTOR );

    // then
    EXPECT_EQ( resultVectors[0], ZERO_VECTOR );
    EXPECT_EQ( resultVectors[1], ZERO_VECTOR );
    EXPECT_TRUE( EQNil(resultLengths[0]) );
    EXPECT_TRUE( EQNil(resultLengths[1]) );
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------

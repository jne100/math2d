///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#include <gtest/gtest.h>
#include <m2d.h>
#include <m2d_curves.h>
#include <m2d_numerics.h>
#include <set>


using namespace m2d;


//-------------------------------------------------------------------------------------------------
// U T I L I T I E S   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the LE function.
--*/
TEST( MathUtils, TestLE )
{
    // given when then
    EXPECT_TRUE( LE(5.0, 6.0) );
    EXPECT_FALSE( LE(6.0, 5.0) );
    EXPECT_TRUE( LE(-5.0, 6.0) );
    EXPECT_FALSE( LE(6.0, -5.0) );
    EXPECT_TRUE( LE(-DOUBLE_PRECISION, DOUBLE_PRECISION) );
    EXPECT_FALSE( LE(DOUBLE_PRECISION, -DOUBLE_PRECISION) );
    EXPECT_TRUE( LE(5.0, 5.0) );

    EXPECT_TRUE( LE(5.85, 6.0, 0.1) );
    EXPECT_TRUE( LE(6.05, 6.0, 0.1) );
    EXPECT_FALSE( LE(6.15, 6.0, 0.1) );

    EXPECT_TRUE( LE((5.0 - DOUBLE_PRECISION / 2.0), 5.0) );
    EXPECT_TRUE( LE((5.0 + DOUBLE_PRECISION / 2.0), 5.0) );
    EXPECT_FALSE( LE((5.0 + DOUBLE_PRECISION * 2.0), 5.0) );
    EXPECT_TRUE( LE((-8.0 - DOUBLE_PRECISION / 2.0), -8.0) );
    EXPECT_TRUE( LE((-8.0 + DOUBLE_PRECISION / 2.0), -8.0) );
    EXPECT_FALSE( LE((-8.0 + DOUBLE_PRECISION * 2.0), -8.0) );
}


/*++
    Test of the LT function.
--*/
TEST( MathUtils, TestLT )
{
    // given when then
    EXPECT_TRUE( LT(5.0, 6.0) );
    EXPECT_FALSE( LT(6.0, 5.0) );
    EXPECT_TRUE( LT(-6.0, -5.0) );
    EXPECT_FALSE( LT(-5.0, -6.0) );

    EXPECT_TRUE( LT(5.85, 6.0, 0.1) );
    EXPECT_FALSE( LT(5.95, 6.0, 0.1) );

    EXPECT_FALSE( LT((5.0 - DOUBLE_PRECISION / 2.0), 5.0) );
    EXPECT_FALSE( LT((5.0 + DOUBLE_PRECISION / 2.0), 5.0) );
    EXPECT_TRUE( LT((5.0 - DOUBLE_PRECISION * 2.0), 5.0) );
    EXPECT_FALSE( LT((-8.0 - DOUBLE_PRECISION / 2.0), -8.0) );
    EXPECT_FALSE( LT((-8.0 + DOUBLE_PRECISION / 2.0), -8.0) );
    EXPECT_TRUE( LT((-8.0 - DOUBLE_PRECISION * 2.0), -8.0) );
}


/*++
    Test of the LT function for pairs of double values, primitive cases.
--*/
TEST( MathUtils, TestPairLT1 )
{
    // given when then
    Point p1( 10.0, 20.0 );
    EXPECT_FALSE( p1 < p1 );
}


/*++
    Test of the LT function for pairs of double values, function integrity check.
--*/
TEST( MathUtils, TestPairLT2 )
{
    // given
    ArchimedianSpiral spiral( DEFAULT_CS, 25.0, PI2 * 5 );

    // when
    const int POINTS_COUNT = 20;
    std::vector<Point> points;
    CalcUniformPointsOnCurve( spiral, POINTS_COUNT, points );

    // then
    for ( size_t k = 0; k < points.size(); ++k ) {
        for ( size_t i = 0; i < points.size(); ++i ) {

            const Point& p1 = points[ k ];
            const Point& p2 = points[ i ];

            bool lt12 = LT( p1.y_, p1.x_, p2.y_, p2.x_ );
            bool lt21 = LT( p2.y_, p2.x_, p1.y_, p1.x_ );
            ASSERT_TRUE( (lt12 != lt21) || (p1 == p2) );

            lt12 = LT( p1.y_, p1.x_, p2.y_, p2.x_, EPSILON_E3 );
            lt21 = LT( p2.y_, p2.x_, p1.y_, p1.x_, EPSILON_E3 );
            ASSERT_TRUE( (lt12 != lt21) ||
                (EQ(p1.x_, p2.x_, EPSILON_E3) && EQ(p1.y_, p2.y_, EPSILON_E3)) );
        }
    }
}


/*++
    Test of the GE function.
--*/
TEST( MathUtils, TestGE )
{
    // given when then
    EXPECT_TRUE( GE(6.0, 5.0) );
    EXPECT_FALSE( GE(5.0, 6.0) );
    EXPECT_TRUE( GE(6.0, -5.0) );
    EXPECT_FALSE( GE(-5.0, 6.0) );
    EXPECT_TRUE( GE(DOUBLE_PRECISION, -DOUBLE_PRECISION) );
    EXPECT_FALSE( GE(-DOUBLE_PRECISION, DOUBLE_PRECISION) );
    EXPECT_TRUE( GE(5.0, 5.0) );

    EXPECT_FALSE( GE(5.85, 6.0, 0.1) );
    EXPECT_TRUE( GE(5.95, 6.0, 0.1) );
    EXPECT_TRUE( GE(6.05, 6.0, 0.1) );

    EXPECT_TRUE( GE((5.0 + DOUBLE_PRECISION / 2.0), 5.0) );
    EXPECT_TRUE( GE((5.0 - DOUBLE_PRECISION / 2.0), 5.0) );
    EXPECT_FALSE( GE((5.0 - DOUBLE_PRECISION * 2.0), 5.0) );
    EXPECT_TRUE( GE((-8.0 + DOUBLE_PRECISION / 2.0), -8.0) );
    EXPECT_TRUE( GE((-8.0 - DOUBLE_PRECISION / 2.0), -8.0) );
    EXPECT_FALSE( GE((-8.0 - DOUBLE_PRECISION * 2.0), -8.0) );
}


/*++
    Test of the GT function.
--*/
TEST( MathUtils, TestGT )
{
    // given when then
    EXPECT_TRUE( GT(6.0, 5.0) );
    EXPECT_FALSE( GT(5.0, 6.0) );
    EXPECT_TRUE( GT(-5.0, -6.0) );
    EXPECT_FALSE( GT(-6.0, -5.0) );

    EXPECT_FALSE( GT(5.95, 6.0, 0.1) );
    EXPECT_FALSE( GT(6.05, 6.0, 0.1) );
    EXPECT_TRUE( GT(6.15, 6.0, 0.1) );

    EXPECT_FALSE( GT((5.0 - DOUBLE_PRECISION / 2.0), 5.0) );
    EXPECT_FALSE( GT((5.0 + DOUBLE_PRECISION / 2.0), 5.0) );
    EXPECT_TRUE( GT((5.0 + DOUBLE_PRECISION * 2.0), 5.0) );
    EXPECT_FALSE( GT((-8.0 - DOUBLE_PRECISION / 2.0), -8.0) );
    EXPECT_FALSE( GT((-8.0 + DOUBLE_PRECISION / 2.0), -8.0) );
    EXPECT_TRUE( GT((-8.0 + DOUBLE_PRECISION * 2.0), -8.0) );
}


/*++
    Test of the angles equality check.
--*/
TEST( MathUtils, TestEQAngles )
{
    // given when then
    EXPECT_TRUE( EQAngles(Radians(0.0), Radians(0.0)) );
    EXPECT_TRUE( EQAngles(Radians(0.0), Radians(360.0)) );
    EXPECT_TRUE( EQAngles(Radians(225.0), Radians(-135.0)) );
    EXPECT_TRUE( EQAngles(Radians(-180.0), Radians(540.0)) );

    EXPECT_FALSE( EQAngles(Radians(40.0), Radians(130.0)) );
    EXPECT_FALSE( EQAngles(Radians(40.0), Radians(220.0)) );
    EXPECT_FALSE( EQAngles(Radians(40.0), Radians(310.0)) );
    EXPECT_TRUE( EQAngles(Radians(40.0), Radians(400.0)) );
}


/*++
    Test of the IsInsideClosedInterval function.
--*/
TEST( MathUtils, TestIsInsideClosedInterval )
{
    // given when then
    EXPECT_TRUE( IsInsideClosedInterval(5.0, 2.5, 7.5) );
    EXPECT_TRUE( IsInsideClosedInterval(3.0, 3.0, 4.0) );
    EXPECT_TRUE( IsInsideClosedInterval(4.0, 3.0, 4.0) );
    EXPECT_TRUE( IsInsideClosedInterval(11.0 - DOUBLE_PRECISION / 2.0, 11.0, 17.0) );
    EXPECT_TRUE( IsInsideClosedInterval(17.0 + DOUBLE_PRECISION / 2.0, 11.0, 17.0) );
    EXPECT_FALSE( IsInsideClosedInterval(10.0, 2.5, 7.5) );
    EXPECT_FALSE( IsInsideClosedInterval(-5.0, 2.5, 7.5) );
    EXPECT_FALSE( IsInsideClosedInterval(11.0 - DOUBLE_PRECISION * 2.0, 11.0, 17.0) );
    EXPECT_FALSE( IsInsideClosedInterval(17.0 + DOUBLE_PRECISION * 2.0, 11.0, 17.0) );
}


/*++
    Test of the IsInsideOpenInterval function.
--*/
TEST( MathUtils, TestIsInsideOpenInterval )
{
    // given when then
    EXPECT_TRUE( IsInsideOpenInterval(5.0, 2.5, 7.5) );
    EXPECT_TRUE( IsInsideOpenInterval(11.0 + DOUBLE_PRECISION * 2.0, 11.0, 17.0) );
    EXPECT_TRUE( IsInsideOpenInterval(17.0 - DOUBLE_PRECISION * 2.0, 11.0, 17.0) );
    EXPECT_FALSE( IsInsideOpenInterval(10.0, 2.5, 7.5) );
    EXPECT_FALSE( IsInsideOpenInterval(-5.0, 2.5, 7.5) );
    EXPECT_FALSE( IsInsideOpenInterval(3.0, 3.0, 4.0) );
    EXPECT_FALSE( IsInsideOpenInterval(4.0, 3.0, 4.0) );
    EXPECT_FALSE( IsInsideOpenInterval(11.0 + DOUBLE_PRECISION / 2.0, 11.0, 17.0) );
    EXPECT_FALSE( IsInsideOpenInterval(17.0 - DOUBLE_PRECISION / 2.0, 11.0, 17.0) );
}


/*++
    Test of the rounding of real value by meathematical rules.
--*/
TEST( MathUtils, TestRound )
{
    // given when then
    EXPECT_TRUE( EQ(Round(4.51), 5.0) );
    EXPECT_TRUE( EQ(Round(4.49), 4.0) );
    EXPECT_TRUE( EQ(Round(-4.51), -5.0) );
    EXPECT_TRUE( EQ(Round(-4.49), -4.0) );
}


/*++
    Test of the degrees to radians conversion.
--*/
TEST( MathUtils, TestRadians )
{
    // given when then
    EXPECT_TRUE( EQ(Radians(180.0), PI) );
    EXPECT_TRUE( EQ(Radians(360.0), PI2) );
    EXPECT_TRUE( EQ(Radians(90.0), PI_DIV_2) );
    EXPECT_TRUE( EQ(Radians(0.0), 0.0) );
    EXPECT_TRUE( EQ(Radians(-90.0), -PI_DIV_2) );
}


/*++
    Test of the radians to degrees conversion.
--*/
TEST( MathUtils, TestDegrees )
{
    // given when then
    EXPECT_TRUE( EQ(Degrees(PI), 180.0) );
    EXPECT_TRUE( EQ(Degrees(PI2), 360.0) );
    EXPECT_TRUE( EQ(Degrees(PI_DIV_2), 90.0) );
    EXPECT_TRUE( EQ(Degrees(0.0), 0.0) );
    EXPECT_TRUE( EQ(Degrees(-PI_DIV_2), -90.0) );
}


/*++
    Test of the value normalization.
--*/
TEST( MathUtils, TestNormalize )
{
    // given when then
    EXPECT_TRUE( EQ(Normalize(5.0, 10.0), 0.5) );
    EXPECT_TRUE( EQ(Normalize(5.0, 10.0, 0.1), 0.05) );
    EXPECT_TRUE( EQ(Normalize(15.0, 10.0), 1.5) );
    EXPECT_TRUE( EQ(Normalize(-15.0, 10.0), -1.5) );

    EXPECT_TRUE( EQ(Normalize(15.0, 10.0, 20.0, 0.0, 1.0), 0.5) );
    EXPECT_TRUE( EQ(Normalize(25.0, 10.0, 20.0, 0.0, 1.0), 1.5) );
    EXPECT_TRUE( EQ(Normalize(-5.0, 10.0, 20.0, 0.0, 1.0), -1.5) );
    EXPECT_TRUE( EQ(Normalize(15.0, 10.0, 20.0, 120.0, 140.0), 130.0) );
}


/*++
    Test of the value normalization with retrenchment.
--*/
TEST( MathUtils, TestStrictNormalize )
{
    // given when then
    EXPECT_TRUE( EQ(StrictNormalize(5.0, 10.0), 0.5) );
    EXPECT_TRUE( EQ(StrictNormalize(5.0, 10.0, 0.1), 0.05) );
    EXPECT_TRUE( EQ(StrictNormalize(15.0, 10.0), 1.0) );
    EXPECT_TRUE( EQ(StrictNormalize(-15.0, 10.0), 0.0) );

    EXPECT_TRUE( EQ(StrictNormalize(15.0, 10.0, 20.0, 0.0, 1.0), 0.5) );
    EXPECT_TRUE( EQ(StrictNormalize(25.0, 10.0, 20.0, 0.0, 1.0), 1.0) );
    EXPECT_TRUE( EQ(StrictNormalize(-5.0, 10.0, 20.0, 0.0, 1.0), 0.0) );
    EXPECT_TRUE( EQ(StrictNormalize(25.0, 10.0, 20.0, 120.0, 140.0), 140.0) );
    EXPECT_TRUE( EQ(StrictNormalize(5.0, 10.0, 20.0, 120.0, 140.0), 120.0) );
}


/*++
    Test of the angle normalization to interval [0, PI2].
--*/
TEST( MathUtils, TestNormalizeAngle )
{
    // given when then
    EXPECT_TRUE( EQ(NormalizeAngle(0.0), 0.0) );
    EXPECT_TRUE( EQ(NormalizeAngle(PI_DIV_4), PI_DIV_4) );
    EXPECT_TRUE( EQ(NormalizeAngle(-PI_DIV_4), PI2 - PI_DIV_4) );
    EXPECT_TRUE( EQ(NormalizeAngle(PI_DIV_4 + PI2), PI_DIV_4) );
    EXPECT_TRUE( EQ(NormalizeAngle(-PI_DIV_4 - PI2), PI2 - PI_DIV_4) );
    EXPECT_TRUE( EQ(NormalizeAngle(PI_DIV_4 + PI2 * 100), PI_DIV_4) );
    EXPECT_TRUE( EQ(NormalizeAngle(-PI_DIV_4 - PI2 * 100), PI2 - PI_DIV_4) );
}


//-------------------------------------------------------------------------------------------------
// P A I R   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the arithmetical operators of the Pair.
--*/
TEST( Pair, TestArithmeticalOperators )
{
    // given
    Pair p1( 16.0, -3.0 );
    Pair p2( 35.0, 1.0 );

    // when
    p1 += Pair( 67.0, -91.0 );
    p2 -= Pair( -97.0, 8.0 );

    // then
    EXPECT_EQ( p1, Pair(83.0, -94.0) );
    EXPECT_EQ( p2, Pair(132.0, -7.0) );
}


/*++
    Test of the == and != operators of the Pair.
--*/
TEST( Pair, TestComparisonOperators )
{
    // given
    Pair p1( 3.8, 9.3 );
    Pair p2( p1.v1_, p1.v2_ + 0.1 );

    // when then
    EXPECT_TRUE( p1 == p1 );
    EXPECT_FALSE( p1 != p1 );
    EXPECT_FALSE( p1 == p2 );
    EXPECT_TRUE( p1 != p2 );
}


//-------------------------------------------------------------------------------------------------
// I N T E R V A L   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the Widen method of the Interval.
--*/
TEST( Interval, TestWiden )
{
    // given
    Interval i1( -2.0, 12.0 );
    Interval i2 = i1;
    Interval i3( 10.0, 20.0 );
    Interval i4 = i3;

    // when
    i1.Widen( 2.0, 2.0 );
    i2.WidenOnNormalizedValue( 0.1, 0.1 );
    i3.Widen( -3.0, 17.0 );
    i4.WidenOnNormalizedValue( -0.2, 1.0 );

    // then
    EXPECT_EQ( i1, Interval(-4.0, 14.0) );
    EXPECT_EQ( i2, Interval(-3.4, 13.4) );
    EXPECT_EQ( i3, Interval(13.0, 37.0) );
    EXPECT_EQ( i4, Interval(12.0, 30.0) );
}


/*++
    Test of the interval middle value calculation.
--*/
TEST( Interval, TestCalcMiddle )
{
    // given
    Interval i1( 0.1, 2.2 );
    Interval i2( -0.5, 1.3 );
    Interval i3( -2.4, -1.1 );

    // when then
    EXPECT_TRUE( EQ(i1.CalcMiddle(), 1.15) );
    EXPECT_TRUE( EQ(i2.CalcMiddle(), 0.4) );
    EXPECT_TRUE( EQ(i3.CalcMiddle(), -1.75) );
}


/*++
    Test of the interval length calculation.
--*/
TEST( Interval, TestCalcLength )
{
    // given
    Interval i1( 0.4, 1.1 );
    Interval i2( -0.8, 0.8 );
    Interval i3( -4.4, -3.2 );

    // when then
    EXPECT_TRUE( EQ(i1.CalcLength(), 0.7) );
    EXPECT_TRUE( EQ(i2.CalcLength(), 1.6) );
    EXPECT_TRUE( EQ(i3.CalcLength(), 1.2) );
}


/*++
    Test of the CalcValueByNormalizedValue method of the Interval.
--*/
TEST( Interval, TestCalcValueByNormalizedValue )
{
    // given
    Interval i( -1.0, 5.0 );

    // when then
    EXPECT_TRUE( EQ(i.CalcValueByNormalizedValue(0.0), i.begin_) );
    EXPECT_TRUE( EQ(i.CalcValueByNormalizedValue(0.5), 2.0) );
    EXPECT_TRUE( EQ(i.CalcValueByNormalizedValue(1.0), i.end_) );
}


/*++
    Test of the CalcNormalizedValue method of the Interval.
--*/
TEST( Interval, TestCalcNormalizedValue )
{
    // given
    Interval i( -1.0, 5.0 );

    // when then
    EXPECT_TRUE( EQ(i.CalcNormalizedValue(i.begin_), 0.0) );
    EXPECT_TRUE( EQ(i.CalcNormalizedValue(2.0), 0.5) );
    EXPECT_TRUE( EQ(i.CalcNormalizedValue(i.end_), 1.0) );
}


/*++
    Test of the IsInclude method of the Interval.
--*/
TEST( Interval, TestIsInclude )
{
    // given
    Interval i( 0.0, 1.0 );

    // when then
    EXPECT_TRUE( i.IsInclude(0.5) );
    EXPECT_TRUE( i.IsInclude(i.begin_) );
    EXPECT_TRUE( i.IsInclude(i.end_) );
    EXPECT_FALSE( i.IsInclude(-1.5) );
    EXPECT_FALSE( i.IsInclude(1.5) );
}


/*++
    Test of the == and != operators of the Interval.
--*/
TEST( Interval, TestComparisonOperators )
{
    // given
    Interval i1( 0.7, 3.9 );
    Interval i2( i1.begin_, i1.end_ + 0.1 );

    // when then
    EXPECT_TRUE( i1 == i1 );
    EXPECT_FALSE( i1 != i1 );
    EXPECT_FALSE( i1 == i2 );
    EXPECT_TRUE( i1 != i2 );
}


//-------------------------------------------------------------------------------------------------
// P O I N T   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the Transfer method of the Point.
--*/
TEST( Point, TestTransfer )
{
    // given
    Point p1( 5.0, 5.0 );
    Point p2( 5.0, 5.0 );

    // when
    p1.Transfer( Vector(2.0, 1.0) );
    p2.Transfer( Vector(-2.0, -1.0) );

    // then
    EXPECT_EQ( p1, Point(7.0, 6.0) );
    EXPECT_EQ( p2, Point(3.0, 4.0) );
}


/*++
    Test of the point scaling relative to the global origin.
--*/
TEST( Point, TestScale1 )
{
    // given
    Point p1( 1.0, 1.0 );
    Point p2( 1.0, 1.0 );

    // when
    p1.Scale( 2.0, 2.0 );
    p2.Scale( 0.5, -0.5 );

    // then
    EXPECT_EQ( p1, Point(2.0, 2.0) );
    EXPECT_EQ( p2, Point(0.5, -0.5) );
}


/*++
    Test of the point scaling relative to the arbitrary point.
--*/
TEST( Point, TestScale2 )
{
    // given
    Point p( 10.0, 20.0 );
    Point center( 20.0, 10.0 );

    // then
    p.Scale( 0.5, 1.0, &center );

    // when
    EXPECT_EQ( p, Point(15.0, 20.0) );
}


/*++
    Test of the point scaling relative to the itself.
--*/
TEST( Point, TestScale3 )
{
    // given
    Point p( 10.0, 20.0 );

    // when
    p.Scale( 0.5, 0.5, &p );

    // then
    EXPECT_EQ( p, Point(10.0, 20.0) );
}


/*++
    Test of the Rotate method of the Point, rotation around the global origin.
--*/
TEST( Point, TestRotate1 )
{
    // given
    Point p( 10.0, 0.0 );

    // when
    p.Rotate( PI_DIV_2 );

    // then
    EXPECT_EQ( p, Point(0.0, 10.0) );
}


/*++
    Test of the Rotate method of the Point, rotation around the arbitrary point.
--*/
TEST( Point, TestRotate2 )
{
    // given
    Point p1( 5.0, 5.0 );
    Point p2( 20.0, 10.0 );
    Point p3( 20.0, 10.0 );
    Point center( 10.0, 10.0 );

    // when
    p1.Rotate( PI, &center );
    p2.Rotate( PI_DIV_2, &center );
    p3.Rotate( PI2 - PI_DIV_2, &center );

    // then
    EXPECT_EQ( p1, Point(15.0, 15.0) );
    EXPECT_EQ( p2, Point(10.0, 20.0) );
    EXPECT_EQ( p3, Point(10.0, 0.0) );
}


/*++
    Test of the Rotate method of the Point, rotation around the itself.
--*/
TEST( Point, TestRotate3 )
{
    // given
    Point p( 10.0, 20.0 );

    // when
    p.Rotate( PI_DIV_4, &p );

    // then
    EXPECT_EQ( p, Point(10.0, 20.0) );
}


/*++
    Test of the Transform method of the Point, simple cases.
--*/
TEST( Point, TestTransform1 )
{
    // given
    Point p1( 1.0, 0.0 );
    Point p2( 2.0, 5.0 );
    Point p3( 1.0, 2.0 );
    Point p4( 3.0, 3.0 );
    Point p5( 1.0, 0.0 );

    Matrix3x3 m1;
    Matrix3x3 m2;
    Matrix3x3 m3;
    Matrix3x3 m4;
    Matrix3x3 m5;
    m1.InitTransferMatrix( Vector(1.0, 1.0) );
    m2.InitInvertedTransferMatrix( Vector(1.0, 1.3) );
    m3.InitScaleMatrix( 2.0, 2.0 );
    m4.InitScaleMatrix( 0.5, -0.5 );
    m5.InitRotationMatrix( PI_DIV_2 );

    // when
    p1.Transform( m1 );
    p2.Transform( m2 );
    p3.Transform( m3 );
    p4.Transform( m4 );
    p5.Transform( m5 );

    // then
    EXPECT_EQ( p1, Point(2.0, 1.0) );
    EXPECT_EQ( p2, Point(1.0, 3.7) );
    EXPECT_EQ( p3, Point(2.0, 4.0) );
    EXPECT_EQ( p4, Point(1.5, -1.5) );
    EXPECT_EQ( p5, Point(0.0, -1.0) );
}


/*++
    Test of the Transform method of the Point, complex matrix cases.
--*/
TEST( Point, TestTransform2 )
{
    // given
    Point p( 20.0, 10.0 );
    Matrix3x3 m1;
    Matrix3x3 m2;
    Matrix3x3 m3;

    m1.InitTransferMatrix( Vector(0.0, -10.0) );
    m2.InitRotationMatrix( PI );
    m3.InitScaleMatrix( 0.5, 0.5 );

    // when
    m1 *= m2;
    m1 *= m3;
    p.Transform( m1 );

    // then
    EXPECT_EQ( p, Point(-10.0, 0.0) );
}


/*++
    Test of the arithmetical operators of the Point.
--*/
TEST( Point, TestArithmeticalOperators )
{
    // given
    Point p1( 11.0, 9.0 );
    Point p2( 11.0, 9.0 );
    Point p3( 1.0, 10.0 );

    // when
    p1 += Point( 5.0, 5.0 );
    p2 -= Point( 5.0, 5.0 );
    p3 *= 5.0;

    // then
    EXPECT_EQ( p1, Point(16.0, 14.0) );
    EXPECT_EQ( p2, Point(6.0, 4.0) );
    EXPECT_EQ( p3, Point(5.0, 50.0) );
}


/*++
    Test of the == and != operators of the Point.
--*/
TEST( Point, TestEqualityOperators )
{
    // given
    Point p1( 4.0, 12.0 );
    Point p2( 1.0, 16.0 );

    // when then
    EXPECT_TRUE( p1 == p1 );
    EXPECT_FALSE( p1 != p1 );
    EXPECT_FALSE( p1 == p2 );
    EXPECT_TRUE( p1 != p2 );
}


/*++
    Test of the < operator of the Point.
--*/
TEST( Point, TestLessThanOperator )
{
    // given
    ArchimedianSpiral spiral( DEFAULT_CS, 10.0, PI2 * 10 );

    // when
    const int POINTS_COUNT = 50;
    std::vector<Point> points;
    CalcUniformPointsOnCurve( spiral, POINTS_COUNT, points );

    std::set<Point> pointsSet;
    for ( size_t i = 0; i < points.size(); ++i ) {
        pointsSet.insert( points[i] );
        pointsSet.insert( points[i] );
    }

    // then
    EXPECT_EQ( pointsSet.size(), POINTS_COUNT );
}


//-------------------------------------------------------------------------------------------------
// V E C T O R   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the vector scaling relative to the global origin.
--*/
TEST( Vector, TestScale1 )
{
    // given
    Vector v1( 5.0, 6.0 );
    Vector v2( v1.x_, v1.y_ );

    // then
    v1.Scale( 2.0, 2.0, 0 );
    v2.Scale( 0.5, -0.5, 0 );

    // when
    EXPECT_EQ( v1, Vector(10.0, 12.0) );
    EXPECT_EQ( v2, Vector(2.5, -3.0) );
}


/*++
    Test of the vector scaling relative to the arbitrary point.
--*/
TEST( Vector, TestScale2 )
{
    // given
    Vector v( 10.0, 10.0 );
    Point center( -10.0, 20.0 );

    // then
    v.Scale( 0.5, 1.5, &center );

    // when
    EXPECT_EQ( v, Vector(0.0, 5.0) );
}


/*++
    Test of the Rotate method of the Vector, rotation around the global origin.
--*/
TEST( Vector, TestRotate1 )
{
    // given
    Vector v( 10.0, 0.0 );

    // when
    v.Rotate( PI_DIV_2 );

    // then
    EXPECT_EQ( v, Vector(0.0, 10.0) );
}


/*++
    Test of the Rotate method of the Vector, rotation around the arbitrary point.
--*/
TEST( Vector, TestRotate2 )
{
    // given
    Vector v1( 5.0, 5.0 );
    Vector v2( 20.0, 10.0 );
    Vector v3( 20.0, 10.0 );
    Point center( 10.0, 10.0 );

    // when
    v1.Rotate( PI, &center );
    v2.Rotate( PI, &center );
    v3.Rotate( PI2 - PI_DIV_2, &center );

    // then
    EXPECT_EQ( v1, Vector(15.0, 15.0) );
    EXPECT_EQ( v2, Vector(0.0, 10.0) );
    EXPECT_EQ( v3, Vector(10.0, 0.0) );
}


/*++
    Test of the Transform method of the Vector, simple cases.
--*/
TEST( Vector, TestTransform1 )
{
    // given
    Vector v1( 2.0, 3.0 );
    Vector v2( 9.0, 7.0 );
    Vector v3( 4.0, 0.0 );

    Matrix3x3 m1;
    Matrix3x3 m2;
    Matrix3x3 m3;
    m1.InitTransferMatrix( Vector(4.0, 5.0) );
    m2.InitScaleMatrix( 0.5, 2.0 );
    m3.InitRotationMatrix( PI );

    // when
    v1.Transform( m1 );
    v2.Transform( m2 );
    v3.Transform( m3 );

    // then
    EXPECT_EQ( v1, Vector(2.0, 3.0) );
    EXPECT_EQ( v2, Vector(4.5, 14.0) );
    EXPECT_EQ( v3, Vector(-4.0, 0.0) );
}


/*++
    Test of the Transform method of the Vector, complex matrix cases.
--*/
TEST( Vector, TestTransform2 )
{
    // given
    Vector v( 1.0, 1.0 );
    Matrix3x3 m1;
    Matrix3x3 m2;
    Matrix3x3 m3;
    m1.InitTransferMatrix( Vector(10.0, 10.0) );
    m2.InitRotationMatrix( PI );
    m3.InitScaleMatrix( 5.0, 5.0 );

    // when
    m1 *= m2;
    m1 *= m3;
    v.Transform( m1 );

    // then
    EXPECT_EQ( v, Vector(-5.0, -5.0) );
}


/*++
    Test of the CalcAngle method of the Vector, calculation of the angle relative to the global
    X axis.
--*/
TEST( Vector, TestCalcAngle )
{
    // given
    Vector v0( 1.0, 0.0 );
    Vector v1( v0 );
    Vector v2( v0 );
    Vector v3( v0 );
    Vector v4( v0 );
    v1.Rotate( Radians(45.0) );
    v2.Rotate( Radians(135.0) );
    v3.Rotate( Radians(225.0) );
    v4.Rotate( Radians(315.0) );

    // when then
    EXPECT_TRUE( EQAngles(v0.CalcAngle(), Radians(0.0)) );
    EXPECT_TRUE( EQAngles(v1.CalcAngle(), Radians(45.0)) );
    EXPECT_TRUE( EQAngles(v2.CalcAngle(), Radians(135.0)) );
    EXPECT_TRUE( EQAngles(v3.CalcAngle(), Radians(225.0)) );
    EXPECT_TRUE( EQAngles(v4.CalcAngle(), Radians(315.0)) );
}


/*++
    Test of the CalcAngle method of the Vector, calculation of the angle between two vectors.
--*/
TEST( Vector, TestCalcAngleBetweenVectors1 )
{
    // given
    Vector v1( 10.0, 10.0 );
    Vector v2( -10.0, 10.0 );
    Vector v3( -10.0, 0.0 );
    Vector v4( 10.0, -10.0 );

    // when then
    EXPECT_TRUE( EQ(NormalizeAngle(v1.CalcAngle(v2)), PI_DIV_2) );
    EXPECT_TRUE( EQ(NormalizeAngle(v1.CalcAngle(v3)), PI_DIV_2 + PI_DIV_4) );
    EXPECT_TRUE( EQ(NormalizeAngle(v1.CalcAngle(v4)), PI_DIV_2 * 3.0) );
    EXPECT_TRUE( EQAngles(v1.CalcAngle(v1), 0.0) );
}


/*++
    Test of the CalcAngle method of the Vector, operation non-commutativity check.
--*/
TEST( Vector, TestCalcAngleBetweenVectors2 )
{
    // given
    Vector v1( 10.0, 0.0 );
    Vector v2( 10.0, 10.0 );

    // when then
    EXPECT_TRUE( EQAngles(v1.CalcAngle(v2), PI_DIV_4) );
    EXPECT_TRUE( EQAngles(v2.CalcAngle(v1), PI2 - PI_DIV_4) );
}


/*++
    Test of the CalcAngle method of the Vector, calculation of relative angle between vectors.
--*/
TEST( Vector, TestCalcRelativeAngleBetweenVectors )
{
    // given
    Vector v0( 1.0, 0.0 );
    Vector v315( 1.0, -1.0 );

    // when then
    EXPECT_TRUE( EQAngles(v0.CalcRelativeAngle(v315), PI_DIV_4) );
    EXPECT_TRUE( EQAngles(v315.CalcRelativeAngle(v0), PI_DIV_4) );
    EXPECT_TRUE( EQAngles(v315.CalcRelativeAngle(v315), 0.0) );
}


/*++
    Test of the IsNormalized method of the Vector.
--*/
TEST( Vector, TestIsNormalized )
{
    // given when then
    EXPECT_TRUE( Vector(1.0, 0.0).IsNormalized() );
    EXPECT_TRUE( Vector(-1.0, 0.0).IsNormalized() );
    EXPECT_TRUE( Vector(0.0, 1.0).IsNormalized() );
    EXPECT_TRUE( Vector(0.0, -1.0).IsNormalized() );
    EXPECT_TRUE( Vector(::sqrt(0.5), ::sqrt(0.5)).IsNormalized() );
    EXPECT_FALSE( Vector(10.0, 10.0).IsNormalized() );
}


/*++
    Test of the CalcPerpendicular method of the Vector.
--*/
TEST( Vector, TestCalcPerpendicular )
{
    // given
    Vector v( 10.0, 0.0 );

    // when
    Vector p1 = v.CalcPerpendicular( true );
    Vector p2 = v.CalcPerpendicular( false );
    p1.Normalize();
    p2.Normalize();

    // then
    EXPECT_EQ( p1, Vector(0.0, 1.0) );
    EXPECT_EQ( p2, Vector(0.0, -1.0) );
}


/*++
    Test of the IsCollinear method of the Vector.
--*/
TEST( Vector, TestIsCollinear )
{
    // given
    Vector v45( 1.0, 1.0 );
    Vector v90( 0.0, 1.0 );
    Vector v135( -1.0, 1.0 );
    Vector v225( -1.0, -1.0 );

    // when then
    EXPECT_TRUE( v45.IsCollinear(v45) );
    EXPECT_FALSE( v45.IsCollinear(v90) );
    EXPECT_FALSE( v45.IsCollinear(v135) );
    EXPECT_TRUE( v45.IsCollinear(v225) );
}


/*++
    Test of the IsCodirectional method of the Vector.
--*/
TEST( Vector, TestIsCodirectional )
{
    // given
    Vector v1( 1.0, 2.0 );
    Vector v2( -1.0, -2.0 );
    Vector v3( 2.0, 4.0 );
    Vector v4( -2.0, -4.0 );
    Vector v5( 0.0, 1.0 );

    // when then
    EXPECT_TRUE( v1.IsCodirectional(v1) );
    EXPECT_TRUE( v1.IsCodirectional(v3) );
    EXPECT_FALSE( v1.IsCodirectional(v2) );
    EXPECT_FALSE( v1.IsCodirectional(v4) );
    EXPECT_FALSE( v1.IsCodirectional(v5) );
}


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 1   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the matrix initialization.
--*/
TEST( Matrix2x1, TestInitialization )
{
    // given
    Matrix2x1 m1( 23.0, 11.0 );
    Matrix2x1 m2( m1 );
    Matrix2x1 m3;
    Matrix2x1 m4;

    // when
    m3 = m1;
    m4.Init( 52.0, -80.0 );

    // then
    EXPECT_TRUE( EQ(m1.m_[0][0], 23.0) && EQ(m1.m_[1][0], 11.0) );
    EXPECT_TRUE( EQ(m2.m_[0][0], 23.0) && EQ(m2.m_[1][0], 11.0) );
    EXPECT_TRUE( EQ(m3.m_[0][0], 23.0) && EQ(m3.m_[1][0], 11.0) );
    EXPECT_TRUE( EQ(m4.m_[0][0], 52.0) && EQ(m4.m_[1][0], -80.0) );
}


/*++
    Test of the += and -=  operators of the Matrix2x1.
--*/
TEST( Matrix2x1, TestAddSubAssign )
{
    // given
    Matrix2x1 m1( 47.0, 34.0 );
    Matrix2x1 m2 = m1;
    Matrix2x1 m3( 99.0, -15.0 );

    // when
    m1 += m3;
    m2 -= m3;

    // then
    EXPECT_TRUE( (m1.m_[0][0] == 146.0) && (m1.m_[1][0] == 19.0) );
    EXPECT_TRUE( (m2.m_[0][0] == -52.0) && (m2.m_[1][0] == 49.0) );
}


/*++
    Test of the *= operator of the Matrix2x1, multiplication by coefficient.
--*/
TEST( Matrix2x1, TestMulAssignCoef )
{
    // given
    Matrix2x1 m( -5.0, 64.0 );

    // when
    m *= 0.5;

    // then
    EXPECT_TRUE( (m.m_[0][0] == -2.5) && (m.m_[1][0] == 32.0) );
}


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 2   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the matrix initialization.
--*/
TEST( Matrix2x2, TestInitialization )
{
    // given
    Matrix2x2 m1( 89.0, 19.0, -43.0, 26.0 );
    Matrix2x2 m2( m1 );
    Matrix2x2 m3;
    Matrix2x2 m4;

    // when
    m3 = m1;
    m4.Init( 84.0, 23.0, 88.0, 66.0 );

    // then
    EXPECT_TRUE( EQ(m1.m_[0][0], 89.0) && EQ(m1.m_[0][1], 19.0) &&
        EQ(m1.m_[1][0], -43.0) && EQ(m1.m_[1][1], 26.0) );
    EXPECT_TRUE( EQ(m2.m_[0][0], 89.0) && EQ(m2.m_[0][1], 19.0) &&
        EQ(m2.m_[1][0], -43.0) && EQ(m2.m_[1][1], 26.0) );
    EXPECT_TRUE( EQ(m3.m_[0][0], 89.0) && EQ(m3.m_[0][1], 19.0) &&
        EQ(m3.m_[1][0], -43.0) && EQ(m3.m_[1][1], 26.0) );
    EXPECT_TRUE( EQ(m4.m_[0][0], 84.0) && EQ(m4.m_[0][1], 23.0) &&
        EQ(m4.m_[1][0], 88.0) && EQ(m4.m_[1][1], 66.0) );
}


/*++
    Test of the matrix inversion (use of precalculated data as sample).
--*/
TEST( Matrix2x2, TestInversion )
{
    // given
    Matrix2x2 m( 37.0, -82.0, 20.0, -58.0 );

    // when
    m.Invert();

    // then
    EXPECT_TRUE( EQ(m.m_[0][0], 0.114624506) && EQ(m.m_[0][1], -0.162055336) &&
        EQ(m.m_[1][0], 0.039525692) && EQ(m.m_[1][1], -0.073122530) );
}


/*++
    Test of the += and -= operators of the Matrix2x2.
--*/
TEST( Matrix2x2, TestAddSubAssign )
{
    // given
    Matrix2x2 m1( -35.0, -47.0, -16.0, -22.0 );
    Matrix2x2 m2 = m1;
    Matrix2x2 m3( 63.0, 62.0, -9.0, 22.0 );

    // when
    m1 += m3;
    m2 -= m3;

    // then
    EXPECT_TRUE( EQ(m1.m_[0][0], 28.0) && EQ(m1.m_[0][1], 15.0) &&
        EQ(m1.m_[1][0], -25.0) && EQ(m1.m_[1][1], 0.0) );
    EXPECT_TRUE( EQ(m2.m_[0][0], -98.0) && EQ(m2.m_[0][1], -109.0) &&
        EQ(m2.m_[1][0], -7.0) && EQ(m2.m_[1][1], -44.0) );
}


/*++
    Test of the *= operator of the Matrix2x2, multiplication by coefficient.
--*/
TEST( Matrix2x2, TestMulAssignCoef )
{
    // given
    Matrix2x2 m( 49.0, 25.0, 8.0, -9.0 );

    // when
    m *= 0.5;

    // then
    EXPECT_TRUE( EQ(m.m_[0][0], 24.5) && EQ(m.m_[0][1], 12.5) &&
        EQ(m.m_[1][0], 4.0) && EQ(m.m_[1][1], -4.5) );
}


/*++
    Test of the *= operator of the Matrix2x2 (use of precalculated data as sample).
--*/
TEST( Matrix2x2, TestMulAssign2x1 )
{
    // given
    Matrix2x2 m1( -58.0, 70.0, 95.0, 51.0 );
    Matrix2x1 m2( -38.0, -66.0 );

    // when
    Matrix2x1 m3 = m1 * m2;

    // then
    EXPECT_TRUE( (m3.m_[0][0] == -2416.0) && (m3.m_[1][0] == -6976.0) );
}


/*++
    Test of the *= operator of the Matrix2x2 (use of precalculated data as sample).
--*/
TEST( Matrix2x2, TestMulAssign2x2 )
{
    // given
    Matrix2x2 m1( 7.0, 20.0, 70.0, 10.0 );
    Matrix2x2 m2( -22.0, 74.0, -57.0, -78.0 );

    // when
    m1 *= m2;

    // then
    EXPECT_TRUE( EQ(m1.m_[0][0], -1294.0) && EQ(m1.m_[0][1], -1042.0) &&
        EQ(m1.m_[1][0], -2110.0) && EQ(m1.m_[1][1], 4400.0) );
}


//-------------------------------------------------------------------------------------------------
// M A T R I X 3 X 3   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the matrix initialization.
--*/
TEST( Matrix3x3, TestInitialization )
{
    // given
    double data[ Matrix3x3::M ][ Matrix3x3::N ] = {
        {81.0, 31.0, -73.0},
        {66.0, 78.0, 17.0},
        {47.0, -42.0, -32.0}
    };

    Matrix3x3 m1( data[0][0], data[0][1], data[0][2],
        data[1][0], data[1][1], data[1][2],
        data[2][0], data[2][1], data[2][2] );
    Matrix3x3 m2( m1 );
    Matrix3x3 m3;
    Matrix3x3 m4;

    // when
    m3 = m1;
    m4.Init( data[0][0], data[0][1], data[0][2],
        data[1][0], data[1][1], data[1][2],
        data[2][0], data[2][1], data[2][2] );

    // then
    for ( int k = 0; k < Matrix3x3::M; ++k ) {
        for ( int i = 0; i < Matrix3x3::N; ++i ) {
            ASSERT_TRUE( EQ(m1.m_[k][i], data[k][i]) );
            ASSERT_TRUE( EQ(m2.m_[k][i], data[k][i]) );
            ASSERT_TRUE( EQ(m3.m_[k][i], data[k][i]) );
            ASSERT_TRUE( EQ(m4.m_[k][i], data[k][i]) );
        }
    }
}


/*++
    Test of the scale matrix initialization, scaling relative to the global origin.
--*/
TEST( Matrix3x3, TestInitScaleMatrix1 )
{
    // given
    Matrix3x3 m1;
    Matrix3x3 m2;
    Matrix3x3 m3;
    Point p1( 10.0, 10.0 );
    Point p2 = p1;
    Point p3 = p1;

    // when
    m1.InitScaleMatrix( 0.5, 0.8 );
    m2.InitScaleMatrix( 1.0, 1.0 );
    m3.InitScaleMatrix( 2.0, 3.0 );
    p1.Transform( m1 );
    p2.Transform( m2 );
    p3.Transform( m3 );

    // then
    EXPECT_EQ( p1, Point(5.0, 8.0) );
    EXPECT_EQ( p2, Point(10.0, 10.0) );
    EXPECT_EQ( p3, Point(20.0, 30.0) );
}


/*++
    Test of the scale matrix initialization, scaling relative to the arbitrary point.
--*/
TEST( Matrix3x3, TestInitScaleMatrix2 )
{
    // given
    Matrix3x3 m;
    Point p( 5.0, 5.0 );
    Point center( 10.0, 10.0 );

    // when
    m.InitScaleMatrix( 2.0, 2.0, &center );
    p.Transform( m );

    // then
    EXPECT_EQ( p, Point(0.0, 0.0) );
}


/*++
    Test of the rotation matrix initialization, rotation around the global origin.
--*/
TEST( Matrix3x3, TestInitRotationMatrix1 )
{
    // given
    Matrix3x3 m1;
    Matrix3x3 m2;
    Matrix3x3 m3;
    Point p1( 10.0, 10.0 );
    Point p2 = p1;
    Point p3 = p1;

    // when
    m1.InitRotationMatrix( 0.0 );
    m2.InitRotationMatrix( PI );
    m3.InitRotationMatrix( PI2 );
    p1.Transform( m1 );
    p2.Transform( m2 );
    p3.Transform( m3 );

    // then
    EXPECT_EQ( p1, Point(10.0, 10.0) );
    EXPECT_EQ( p2, Point(-10.0, -10.0) );
    EXPECT_EQ( p3, Point(10.0, 10.0) );
}


/*++
    Test of the rotation matrix initialization, rotation around the arbitrary point.
--*/
TEST( Matrix3x3, TestInitRotationMatrix2 )
{
    // given
    Matrix3x3 m;
    Point p( 5.0, 5.0 );
    Point center( 10.0, 10.0 );

    // when
    m.InitRotationMatrix( PI, &center );
    p.Transform( m );

    // then
    EXPECT_EQ( p, Point(15.0, 15.0) );
}


/*++
    Test of the matrix inversion (use of precalculated data as sample).
--*/
TEST( Matrix3x3, TestInversion )
{
    // given
    Matrix3x3 m( -34.0, 92.0, -59.0,
        26.0, 61.0, 17.0,
        78.0, 73.0, -86.0 );

    // when
    m.Invert();

    // then
    double solution[ Matrix3x3::M ][ Matrix3x3::N ] = {
        {-0.009047395, 0.005027880, 0.007200817},
        {0.004967908, 0.010496484, -0.001333330},
        {-0.003988831, 0.013469976,-0.006228713}
    };

    for ( int k = 0; k < Matrix3x3::M; ++k ) {
        for ( int i = 0; i < Matrix3x3::N; ++i ) {
            ASSERT_TRUE( EQ(m.m_[k][i], solution[k][i]) );
        }
    }
}


/*++
    Test of the += and -= operators of the Matrix3x3.
--*/
TEST( Matrix3x3, TestAddSubAssign )
{
    // given
    Matrix3x3 m1( 70.0, 90.0, 54.0,
        74.0, 11.0, 69.0,
        30.0, 28.0, 45.0 );
    Matrix3x3 m2 = m1;
    Matrix3x3 m3( 11.0, 82.0, 22.0,
        32.0, 84.0, 14.0,
        77.0, 70.0, 83.0 );

    // when
    m1 += m3;
    m2 -= m3;

    // then
    double solution1[ Matrix3x3::M ][ Matrix3x3::N ] = {
        {81.0, 172.0, 76.0},
        {106.0, 95.0, 83.0},
        {107.0, 98.0, 128.0}
    };
    double solution2[ Matrix3x3::M ][ Matrix3x3::N ] = {
        {59.0, 8.0, 32.0},
        {42.0, -73.0, 55.0},
        {-47.0, -42.0, -38.0}
    };

    for ( int k = 0; k < Matrix3x3::M; ++k ) {
        for ( int i = 0; i < Matrix3x3::N; ++i ) {
            ASSERT_TRUE( EQ(m1.m_[k][i], solution1[k][i]) );
            ASSERT_TRUE( EQ(m2.m_[k][i], solution2[k][i]) );
        }
    }
}


/*++
    Test of the *= operator of the Matrix3x3, multiplication by coefficient.
--*/
TEST( Matrix3x3, TestMulAssignCoef )
{
    // given
    Matrix3x3 m( 18.0, 28.0, 87.0,
        50.0, 2.0, -42.0,
        96.0, -3.0, 68.0 );

    // when
    m *= 0.5;

    // then
    double solution[ Matrix3x3::M ][ Matrix3x3::N ] = {
        {9.0, 14.0, 43.5},
        {25.0, 1.0, -21.0},
        {48.0, -1.5, 34.0}
    };

    for ( int k = 0; k < Matrix3x3::M; ++k ) {
        for ( int i = 0; i < Matrix3x3::N; ++i ) {
            ASSERT_TRUE( EQ(m.m_[k][i], solution[k][i]) );
        }
    }
}


/*++
    Test of the *= operator of the Matrix3x3 (use of precalculated data as sample).
--*/
TEST( Matrix3x3, TestMulAssign )
{
    // given
    Matrix3x3 m1( 3.0, 9.0, -2.0,
        2.0, -13.0, 3.0,
        11.0, 2.0, 4.0 );
    Matrix3x3 m2( 1.0, 4.0, 11.0,
        4.0, 5.0, 5.0,
        11.0, 3.0, 7.0 );

    // when
    m1 *= m2;

    // then
    double solution[ Matrix3x3::M ][ Matrix3x3::N ] = {
        {17.0, 51.0, 64.0},
        {-17.0, -48.0, -22.0},
        {63.0, 66.0, 159.0}
    };

    for ( int k = 0; k < Matrix3x3::M; ++k ) {
        for ( int i = 0; i < Matrix3x3::N; ++i ) {
            ASSERT_TRUE( EQ(m1.m_[k][i], solution[k][i]) );
        }
    }
}


//-------------------------------------------------------------------------------------------------
// C S   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the coordinate system initialization methods.
--*/
TEST( CS, TestInit )
{
    // given
    CS cs;

    // when
    cs.Init( Point(20.0, 10.0), PI_DIV_4 );

    // then
    ASSERT_TRUE( cs.axisX_.IsNormalized() );
    ASSERT_TRUE( cs.axisY_.IsNormalized() );
    EXPECT_TRUE( EQNil(cs.axisX_.CalcDot(cs.axisY_)) );
    EXPECT_TRUE( EQ(cs.axisX_.CalcDot(Vector(1.0, 0.0)), ::cos(PI_DIV_4)) );
}


/*++
    Test of the InitDefaultCS method of the CS.
--*/
TEST( CS, TestBuildDefaultCS )
{
    // given
    CS cs;

    // when
    cs.InitDefaultCS();

    // then
    ASSERT_TRUE( cs.axisX_.IsNormalized() );
    ASSERT_TRUE( cs.axisY_.IsNormalized() );
    EXPECT_TRUE( EQNil(cs.axisX_.CalcDot(cs.axisY_)) );
    EXPECT_TRUE( EQ(cs.axisX_.CalcDot(Vector(1.0, 0.0)), ::cos(0.0)) );
}


/*++
    Test of the Transfer method of the CS.
--*/
TEST( CS, TestTransfer )
{
    // given
    CS cs;
    cs.Init( ZERO_POINT, 0.0 );
    Vector oldAxisX = cs.axisX_;
    Vector oldAxisY = cs.axisY_;

    // when
    cs.Transfer( Vector(2.5, -5.0) );

    // then
    EXPECT_EQ( cs.pos_, Point(2.5, -5.0) );
    EXPECT_EQ( cs.axisX_, oldAxisX );
    EXPECT_EQ( cs.axisY_, oldAxisY );
}


/*++
    Test of the Rotate method of the CS, rotation around the global origin.
--*/
TEST( CS, TestRotate1 )
{
    // given
    CS cs1;
    CS cs2;
    cs1.Init( Point(10.0, 0.0), 0.0 );
    cs2.Init( Point(17.0, 23.0), 0.0 );
    Vector oldAxisX1 = cs1.axisX_;
    Vector oldAxisY1 = cs1.axisY_;
    Vector oldAxisX2 = cs2.axisX_;
    Vector oldAxisY2 = cs2.axisY_;

    // when
    cs1.Rotate( PI_DIV_2 );
    cs2.Rotate( PI );

    // then
    EXPECT_EQ( cs1.pos_, Point(0.0, 10.0) );
    EXPECT_TRUE( EQNil(cs1.axisX_.CalcDot(cs1.axisY_)) );
    EXPECT_TRUE( EQNil(cs1.axisX_.CalcDot(oldAxisX1)) );
    EXPECT_TRUE( EQNil(cs1.axisY_.CalcDot(oldAxisY1)) );

    EXPECT_EQ( cs2.pos_, Point(-17.0, -23.0) );
    EXPECT_TRUE( EQNil(cs2.axisX_.CalcDot(cs2.axisY_)) );
    oldAxisX2.Invert();
    oldAxisY2.Invert();
    EXPECT_EQ( cs2.axisX_, oldAxisX2 );
    EXPECT_EQ( cs2.axisY_, oldAxisY2 );
}


/*++
    Test of the Rotate method of the CS, rotation around the arbitrary point.
--*/
TEST( CS, TestRotate2 )
{
    // given
    CS cs( Point(10.0, 10.0) );
    Point center( 20.0, 10.0 );

    // when
    cs.Rotate( -PI_DIV_2, &center );

    // then
    EXPECT_EQ( cs.pos_, Point(20.0, 20.0) );
    EXPECT_EQ( cs.axisX_, Vector(0.0, -1.0) );
    EXPECT_EQ( cs.axisY_, Vector(1.0, 0.0) );
}


/*++
    Test of the Transform method of the CS, simple cases.
--*/
TEST( CS, TestTransform1 )
{
    // given
    CS cs1( ZERO_POINT, PI_DIV_4 );
    CS cs2( Point(8.0, -9.0), PI_DIV_6 );
    CS cs3( Point(5.0, 10.0) );

    Matrix3x3 m1;
    Matrix3x3 m2;
    Matrix3x3 m3;
    m1.InitTransferMatrix( Vector(-7.0, 1.0) );
    m2.InitScaleMatrix( 2.0, 2.0 );
    m3.InitRotationMatrix( PI );

    // when
    cs1.Transform( m1 );
    cs2.Transform( m2 );
    cs3.Transform( m3 );

    // then
    EXPECT_EQ( cs1.pos_, Point(-7.0, 1.0) );
    EXPECT_TRUE( cs1.axisX_.IsNormalized() );
    EXPECT_TRUE( cs1.axisY_.IsNormalized() );
    EXPECT_TRUE( EQAngles(cs1.axisX_.CalcAngle(), PI_DIV_4) );
    EXPECT_TRUE( EQAngles(cs1.axisY_.CalcAngle(), PI_DIV_4 + PI_DIV_2) );

    EXPECT_EQ( cs2.pos_, Point(16.0, -18.0) );
    EXPECT_TRUE( cs2.axisX_.IsNormalized() );
    EXPECT_TRUE( cs2.axisY_.IsNormalized() );
    EXPECT_TRUE( EQAngles(cs2.axisX_.CalcAngle(), PI_DIV_6) );
    EXPECT_TRUE( EQAngles(cs2.axisY_.CalcAngle(), PI_DIV_6 + PI_DIV_2) );

    EXPECT_EQ( cs3.pos_, Point(-5.0, -10.0) );
    EXPECT_TRUE( cs3.axisX_.IsNormalized() );
    EXPECT_TRUE( cs3.axisY_.IsNormalized() );
    EXPECT_TRUE( EQAngles(cs3.axisX_.CalcAngle(), PI) );
    EXPECT_TRUE( EQAngles(cs3.axisY_.CalcAngle(), PI + PI_DIV_2) );
}


/*++
    Test of the Transform method of the CS, complex matrix cases.
--*/
TEST( CS, TestTransform2 )
{
    // given
    CS cs( Point(10.0, 0.0) );

    Matrix3x3 m1;
    Matrix3x3 m2;
    Matrix3x3 m3;
    m1.InitTransferMatrix( Vector(10.0, 0.0) );
    m2.InitRotationMatrix( PI );
    m3.InitScaleMatrix( 0.5, 0.5 );

    // when
    m1 *= m2;
    m1 *= m3;
    cs.Transform( m1 );

    // then
    EXPECT_EQ( cs.pos_, Point(-10.0, 0.0) );
    EXPECT_TRUE( cs.axisX_.IsNormalized() );
    EXPECT_TRUE( cs.axisY_.IsNormalized() );
    EXPECT_TRUE( EQAngles(cs.axisX_.CalcAngle(), PI) );
    EXPECT_TRUE( EQAngles(cs.axisY_.CalcAngle(), PI + PI_DIV_2) );
}


/*++
    Test of the CalcMatrixFrom method of the CS, transformation from simple coordinate systems.
--*/
TEST( CS, TestCalcMatrixFrom1 )
{
    // given
    Point p1( 5.0, 5.0 );
    Point p2( 5.0, 5.0 );
    CS cs1( Point(10.0, 20.0) );
    CS cs2( ZERO_POINT, PI_DIV_2 );

    // when
    Matrix3x3 m1;
    Matrix3x3 m2;
    cs1.CalcMatrixFromCS( m1 );
    cs2.CalcMatrixFromCS( m2 );
    p1.Transform( m1 );
    p2.Transform( m2 );

    // then
    EXPECT_EQ( p1, Point(15.0, 25.0) );
    EXPECT_EQ( p2, Point(-5.0, 5.0) );
}


/*++
    Test of the CalcMatrixFrom method of the CS, transformation from complex coordinate systems.
--*/
TEST( CS, TestCalcMatrixFrom2 )
{
    // given
    Point p( 5.0, 0.0 );
    CS cs( Point(10.0, 20.0), PI_DIV_2 );
    
    // when
    Matrix3x3 m;
    cs.CalcMatrixFromCS( m );
    p.Transform( m );

    // then
    EXPECT_EQ( p, Point(10.0, 25.0) );
}


/*++
    Test of the CalcMatrixFrom method of the CS, result matrix correctness check.
--*/
TEST( CS, TestCalcMatrixFrom3 )
{
    // given
    Point p( 4.0, 0.0 );
    CS cs( Point(2.0, 2.0), PI_DIV_2 );
    Vector v( 1.5, 2.5 );

    // when
    Matrix3x3 transformMatrix;
    Matrix3x3 tempMatrix;
    cs.CalcMatrixFromCS( transformMatrix );
    tempMatrix.InitTransferMatrix( v );
    transformMatrix *= tempMatrix;
    p.Transform( transformMatrix );

    // then
    EXPECT_EQ( p, Point(3.5, 8.5) );
}


/*++
    Test of the CalcMatrixInto method of the CS, transformation from simple coordinate systems.
--*/
TEST( CS, TestCalcMatrixInto1 )
{
    // given
    Point p1( 1.0, 3.0 );
    Point p2( -10.0, 0.0 );
    CS cs1( Point(-6.0, 9.0) );
    CS cs2( ZERO_POINT, PI );

    // when
    Matrix3x3 m1;
    Matrix3x3 m2;
    cs1.CalcMatrixFromCS( m1 );
    cs2.CalcMatrixFromCS( m2 );
    p1.Transform( m1 );
    p2.Transform( m2 );

    // then
    EXPECT_EQ( p1, Point(-5.0, 12.0) );
    EXPECT_EQ( p2, Point(10.0, 0.0) );
}


/*++
    Test of the CalcMatrixInto method of the CS, transformation from complex coordinate systems.
--*/
TEST( CS, TestCalcMatrixInto2 )
{
    // given
    Point p( 10.0, 20.0 );
    CS cs( p, PI_DIV_2 );

    // when
    Matrix3x3 m;
    cs.CalcMatrixIntoCS( m );
    p.Transform( m );

    // then
    EXPECT_EQ( p, ZERO_POINT );
}


/*++
    Test of the CalcMatrixInto method of the CS, result matrix correctness check.
--*/
TEST( CS, TestCalcMatrixInto3 )
{
    // given
    Point p( 5.0, 5.0 );
    CS cs( Point(4.0, 4.0), PI_DIV_2 );
    Vector v( 1.0, 1.0 );

    // when
    Matrix3x3 transformMatrix;
    Matrix3x3 tempMatrix;
    cs.CalcMatrixIntoCS( transformMatrix );
    tempMatrix.InitTransferMatrix( v );
    transformMatrix *= tempMatrix;
    p.Transform( transformMatrix );

    // then
    EXPECT_EQ( p, Point(2.0, 0.0) );
}


//-------------------------------------------------------------------------------------------------
// R E C T   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the rectangle construction.
--*/
TEST( Rect, TestConstruction )
{
    // given
    Rect r1( Point(-1.0, -1.0), Point(-2.0, 1.0) );
    Rect r2( 5.0, 4.0, -15.0, -8.0 );
    Rect r3( 10.0, 20.0 );
    Rect r4( -10.0, 20.0 );
    Rect r5( 10.0, -20.0 );
    Rect r6( -10.0, -20.0 );

    // when then
    EXPECT_EQ( r1, Rect(Point(-2.0, -1.0), Point(-1.0, 1.0)) );
    EXPECT_EQ( r2, Rect(Point(-15.0, -8.0), Point(5.0, 4.0)) );
    EXPECT_EQ( r3, Rect(Point(0.0, 0.0), Point(10.0, 20.0)) );
    EXPECT_EQ( r4, Rect(Point(-10.0, 0.0), Point(0.0, 20.0)) );
    EXPECT_EQ( r5, Rect(Point(0.0, -20.0), Point(10.0, 0.0)) );
    EXPECT_EQ( r6, Rect(Point(-10.0, -20.0), Point(0.0, 0.0)) );
}


/*++
    Test of the rectangle initialization.
--*/
TEST( Rect, TestInitialization )
{
    // given
    Rect r1;
    Rect r2;

    // when
    r1.Init( -640.0, 480.0 );
    r2.Init( -20.0, 10.0, -40, -15.0 );

    // then
    EXPECT_EQ( r1, Rect(Point(-640.0, 0.0), Point(0.0, 480)) );
    EXPECT_EQ( r2, Rect(Point(-40.0, -15.0), Point(-20.0, 10.0)) );
}


/*++
    Test of the Transfer method of the Rect.
--*/
TEST( Rect, TestTransfer )
{
    // given
    Rect r( -10, -20, 30, 40 );

    // when
    r.Transfer( Vector(50.0, -50.0) );

    // then
    EXPECT_EQ( r, Rect(Point(40.0, -70.0), Point(80.0, -10.0)) );
}


/*++
    Test of the rectangle scaling relative to the global origin.
--*/
TEST( Rect, TestScale1 )
{
    // given
    Rect r( 0.0, 0.0, 20.0, 10.0 );

    // when
    r.Scale( 2.0, 0.5 );

    // then
    EXPECT_EQ( r, Rect(Point(0.0, 0.0), Point(40.0, 5.0)) );
}


/*++
    Test of the rectangle scaling relative to the arbitrary point.
--*/
TEST( Rect, TestScale2 )
{
    // given
    Point center( 20.0, 20.0 );
    Rect r( Point(10.0, 10.0), center );

    // when
    r.Scale( 2.0, 2.0, &center );

    // then
    EXPECT_EQ( r, Rect(ZERO_POINT, center) );
}


/*++
    Test of the rectangle width and height calculation.
--*/
TEST( Rect, TestCalcWidthAndHeight )
{
    // given
    Rect r1( Point(10.0, 10.0), Point(20.0, 20.0) );
    Rect r2( Point(-10.0, -20.0), Point(10.0, 20.0) );

    // when then
    EXPECT_TRUE( EQ(r1.CalcWidth(), 10.0) );
    EXPECT_TRUE( EQ(r1.CalcHeight(), 10.0) );
    EXPECT_TRUE( EQ(r2.CalcWidth(), 20.0) );
    EXPECT_TRUE( EQ(r2.CalcHeight(), 40.0) );
}


/*++
    Test of the rectangle area calculation.
--*/
TEST( Rect, TestCalcArea )
{
    // given
    Rect r( Point(-10.0, -10.0), Point(10.0, 10.0) );

    // when then
    EXPECT_TRUE( EQ(r.CalcArea(), 400.0) );
}


/*++
    Test of the rectangle aspect ratio calculation.
--*/
TEST( Rect, TestCalcAspectRatio )
{
    // given
    Rect r1( ZERO_POINT, Point(10.0, 20.0) );
    Rect r2( ZERO_POINT, Point(20.0, 10.0) );

    // when then
    EXPECT_TRUE( EQ(r1.CalcAspectRatio(), 0.5) );
    EXPECT_TRUE( EQ(r2.CalcAspectRatio(), 2.0) );
}


/*++
    Test of the rectanle center calculation.
--*/
TEST( Rect, TestCalcCenter )
{
    // given
    Rect r( ZERO_POINT, Point(20.0, 30.0) );

    // when then
    EXPECT_EQ( r.CalcCenter(), Point(10.0, 15.0) );
}


/*++
    Test of the Inflate and Deflate methods of the Rect.
--*/
TEST( Rect, TestDeflateInflate )
{
    // given
    Rect r1( 10.0, 10.5 );
    Rect r2 = r1;
    Rect r3( -1.0, -1.0, 1.0, 1.0 );
    Rect r4 = r3;

    // when
    r1.Inflate( 2.0, 2.5 );
    r2.Deflate( 2.0, 2.5);
    r3.Inflate( -3.0, -3.0 );
    r4.Deflate( 3.0, 3.0 );

    // then
    EXPECT_EQ( r1, Rect(Point(-2.0, -2.5), Point(12.0, 13.0)) );
    EXPECT_EQ( r2, Rect(Point(2.0, 2.5), Point(8.0, 8.0)) );
    EXPECT_EQ( r3, Rect(Point(-2.0, -2.0), Point(2.0, 2.0)) );
    EXPECT_EQ( r4, Rect(Point(-2.0, -2.0), Point(2.0, 2.0)) );
}


/*++
    Test of the IsInclude method of the Rect.
--*/
TEST( Rect, TestInclusion )
{
    // given
    Rect r( Point(-10.0, -10.0), Point(10.0, 10.0) );

    // when then
    EXPECT_TRUE( r.IsInclude(Point(0.0, 0.0)) );
    EXPECT_TRUE( r.IsInclude(Point(-5.0, -5.0)) );
    EXPECT_FALSE( r.IsInclude(Point(-15.0, 15.0)) );
    EXPECT_FALSE( r.IsInclude(Point(0.0, -15.0)) );
}


/*++
    Test of the == and != operators of the Rect.
--*/
TEST( Rect, TestComparisonOperators )
{
    // given
    Rect r1( Point(99.0, 21.0), Point(68.0, 50.0) );
    Rect r2 = r1;
    Rect r3( Point(88.0, 77.0), Point(25.0, 58.0) );

    // when then
    EXPECT_TRUE( r1 == r2 );
    EXPECT_FALSE( r1 != r2 );
    EXPECT_TRUE( r1 != r3 );
    EXPECT_FALSE( r1 == r3 );
}


//-------------------------------------------------------------------------------------------------
// B O U N D I N G   B O X   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the bounding box construction.
--*/
TEST( BoundingBox, TestConstruction )
{
    // given
    BoundingBox b1;
    BoundingBox b2;
    b1.Add( Point(70.0, 34.0) );
    b1.Add( Point(47.0, 67.0) );
    b2.Add( b1 );

    // when then
    EXPECT_EQ( b1.GetMinPoint(), Point(47.0, 34.0) );
    EXPECT_EQ( b1.GetMaxPoint(), Point(70.0, 67.0) );
    EXPECT_TRUE( b1.GetMinPoint() == b2.GetMinPoint() );
    EXPECT_TRUE( b1.GetMaxPoint() == b2.GetMaxPoint() );
}


/*++
    Test of the Add method of the BoundingBox.
--*/
TEST( BoundingBox, TestAdd )
{
    // given
    BoundingBox b1;
    BoundingBox b2;
    BoundingBox b3;
    b1.Add( Point(10.0, 10.0) );
    b1.Add( Point(20.0, 20.0) );
    b2.Add( Point(-10.0, -20.0) );
    b2.Add( Point(10.0, 10.0) );
    b2.Add( Point(5.0, 5.0) );
    b2.Add( Point(10.0, 20.0) );
    b3.Add( Point(5.0, 5.0) );
    b3.Add( b1 );

    // when then
    EXPECT_EQ( b1.CalcCenter(), Point(15.0, 15.0) );
    EXPECT_TRUE( EQ(b1.CalcWidth(), 10.0) );
    EXPECT_TRUE( EQ(b1.CalcHeight(), 10.0) );
    EXPECT_EQ( b2.CalcCenter(), Point(0.0, 0.0) );
    EXPECT_TRUE( EQ(b2.CalcWidth(), 20.0) );
    EXPECT_TRUE( EQ(b2.CalcHeight(), 40.0) );
    EXPECT_EQ( b3.CalcCenter(), Point(12.5, 12.5) );
    EXPECT_TRUE( EQ(b3.CalcWidth(), 15.0) );
    EXPECT_TRUE( EQ(b3.CalcHeight(), 15.0 ) );
}


/*++
    Test of the IsEmpty and SetEmpty methods of the BoundingBox.
--*/
TEST( BoundingBox, TestEmptiness )
{
    // given
    BoundingBox b1;
    BoundingBox b2;
    BoundingBox b3;
    b2.Add( Point(88.0, 29.0) );
    b2.Add( Point(2.0, 8.0) );
    b3 = b2;
    b2.SetEmpty();

    // when then
    EXPECT_TRUE( b1.IsEmpty() );
    EXPECT_TRUE( b2.IsEmpty() );
    EXPECT_FALSE( b3.IsEmpty() );
}


/*++
    Test of the << operator working with points and bounding boxes of the BoundingBox.
--*/
TEST( BoundingBox, TestStreamOperators )
{
    // given
    BoundingBox b1;
    BoundingBox b2;
    b2.Add( Point(10.0, 24.0) );
    b2.Add( Point(20.0, 12.0) );

    // when
    b1 << Point( -5.0, -4.0 ) << b2 << Point( -3.0, -6.0 );

    // then
    EXPECT_EQ( b1.GetMinPoint(), Point(-5.0, -6.0) );
    EXPECT_EQ( b1.GetMaxPoint(), Point(20.0, 24.0) );
}


//-------------------------------------------------------------------------------------------------
// F A C T O R I A L   C A L C U L A T O R   T E S T S
//-------------------------------------------------------------------------------------------------


/*++
    Test of the factorial calculation by FactorialCalculator.
--*/
TEST( FactorialCalculator, TestSimple )
{
    // given
    FactorialCalculator calculator1( 1 );
    FactorialCalculator calculator2( 30 );

    // when then
    EXPECT_TRUE( EQ(calculator1.Calc(0), 1.0) );
    EXPECT_TRUE( EQ(calculator1.Calc(1), 1.0) );
    EXPECT_TRUE( EQ(calculator1.Calc(2), 2.0) );

    EXPECT_TRUE( EQ(calculator2.Calc(0), 1.0) );
    EXPECT_TRUE( EQ(calculator2.Calc(1), 1.0) );
    EXPECT_TRUE( EQ(calculator2.Calc(2), 2.0) );
    EXPECT_TRUE( EQ(calculator2.Calc(3), 6.0) );
    EXPECT_TRUE( EQ(calculator2.Calc(4), 24.0) );
    EXPECT_TRUE( EQ(calculator2.Calc(5), 120.0) );
    EXPECT_TRUE( EQ(calculator2.Calc(6), 720.0) );
    EXPECT_TRUE( EQ(calculator2.Calc(7), 5040.0) );
    EXPECT_TRUE( EQ(calculator2.Calc(50) / 1.0e64, 3.0414093202) );
    EXPECT_TRUE( EQ(calculator2.Calc(100) / 1.0e157, 9.3326215444) );
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------

///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#ifndef _M2D_CURVES_TEST_h
#define _M2D_CURVES_TEST_h


#include <gtest/gtest.h>
#include <m2d_curves.h>


using namespace m2d;


//-------------------------------------------------------------------------------------------------
// C U R V E S   T E S T   U T I L I T I E S
//-------------------------------------------------------------------------------------------------


/*++
    Function that able to create mathematical curve of any possible type.
    ATTANTION!, when new mathematical curve appears current function have to be updated. Special
    test case fails otherwise. Moreover, every corresponding INSTANTIATE_TEST_CASE_P declaration
    have to be updated too.
--*/
Curve* CreateTestCurve( ECurveTypes type );


/*++
    Static wrapper for dynamically parametrized version of CreateTestCurve function.
--*/
template <int T>
Curve* CreateTestCurve()
{
    return CreateTestCurve( static_cast<ECurveTypes>(T) );
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


#endif

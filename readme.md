math2d
============

Math2d is a simple general purpose mathematical library written in C++. Mainly
Math2d contains functions and classes for work with generic mathematical
entities (such as points, vectors and coordinate systems) and parametric
curves (such as segments, arcs or Bezier curves). It also contains real values
comparison functions, round function (missing in standart library), factorial
calculator and bunch of other utilities in addition.

I really doubt you will be able to use it in serious commercial project, it's
more for educational purposes (in spite of it, at least my own windows store
project uses it for parametric curves rendering and modification). The main
goal was to write some code I would be really proud of, through TDD, with unit
test and all the cool stuff, as opposed to typycal production mess.

Version history
============
* v1.0 - generic math classes, utilities, parametric curves;
* v1.1 - Hermit spline, basic numerical analysis, crossplatform compilation.

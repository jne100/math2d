///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#ifndef _M2D_VISITORS_h
#define _M2D_VISITORS_h


#include <cassert>
#include <vector>
#include <m2d.h>
#include <m2d_curves.h>


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C U R V E   V I S I T O R
//-------------------------------------------------------------------------------------------------


/*++
    Interface of mathematical curve visitor.
--*/
class CurveVisitor {
public:
    virtual ~CurveVisitor() {}
    virtual void Visit( Segment& curve ) = 0;
    virtual void Visit( Arc& curve ) = 0;
    virtual void Visit( ArchimedianSpiral& curve ) = 0;
    virtual void Visit( LogarithmicSpiral& curve ) = 0;
    virtual void Visit( Polyline& curve ) = 0;
    virtual void Visit( BezierCurve& curve ) = 0;
    virtual void Visit( HermiteSpline& curve ) = 0;
    virtual void Visit( EquidistantCurve& curve ) = 0;
    virtual void Visit( ProxyCurve& curve ) = 0;
};


//-------------------------------------------------------------------------------------------------
// C O L L E C T   E D I T I N G   P O I N T S   V I S I T O R
//-------------------------------------------------------------------------------------------------


/*++
    Visitor which collect mathematcal curves editing points.
--*/
class CollectEditingPointsVisitor : public CurveVisitor {
public:
    virtual ~CollectEditingPointsVisitor() {}
    virtual void Visit( Segment& curve );
    virtual void Visit( Arc& curve );
    virtual void Visit( ArchimedianSpiral& curve );
    virtual void Visit( LogarithmicSpiral& curve );
    virtual void Visit( Polyline& curve );
    virtual void Visit( BezierCurve& curve );
    virtual void Visit( HermiteSpline& curve );
    virtual void Visit( EquidistantCurve& curve );
    virtual void Visit( ProxyCurve& curve );

public:
    std::vector<Point> editingPoints_;
};


//-------------------------------------------------------------------------------------------------
// C H A N G E   E D I T I N G   P O I N T   V I S I T O R
//-------------------------------------------------------------------------------------------------


/*++
    Visitor which modify mathematcal curves editing points.
--*/
class ChangeEditingPointVisitor : public CurveVisitor {
public:
    ChangeEditingPointVisitor( int editingPointIndex, const Matrix3x3& transform );
    ChangeEditingPointVisitor( int editingPointIndex, const Vector& shift );
    virtual ~ChangeEditingPointVisitor() {}
    virtual void Visit( Segment& curve );
    virtual void Visit( Arc& curve );
    virtual void Visit( ArchimedianSpiral& curve );
    virtual void Visit( LogarithmicSpiral& curve );
    virtual void Visit( Polyline& curve );
    virtual void Visit( BezierCurve& curve );
    virtual void Visit( HermiteSpline& curve );
    virtual void Visit( EquidistantCurve& curve );
    virtual void Visit( ProxyCurve& curve );

private:
    int editingPointIndex_;
    Matrix3x3 transform_;
};


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


#endif

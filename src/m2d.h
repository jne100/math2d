///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#ifndef _M2D_h
#define _M2D_h


#include <cassert>
#include <cmath>
#include <ostream>
#include <vector>


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C O N S T A N T S
//-------------------------------------------------------------------------------------------------


const double EPSILON_E3 = 1e-3;
const double EPSILON_E4 = 1e-4;
const double EPSILON_E5 = 1e-5;
const double EPSILON_E6 = 1e-6;
const double DOUBLE_PRECISION = EPSILON_E5;

const double E = 2.718281828;
const double PI = 3.141592654;
const double PI2 = PI * 2.0;
const double PI_DIV_2 = PI / 2.0;
const double PI_DIV_3 = PI / 3.0;
const double PI_DIV_4 = PI / 4.0;
const double PI_DIV_6 = PI / 6.0;
const double PI_INV = 1 / PI;
const double PI2_INV = 1 / PI2;
const double PI_DEG = 180.0;
const double PI2_DEG = 360.0;


//-------------------------------------------------------------------------------------------------
// U T I L I T I E S
//-------------------------------------------------------------------------------------------------


class Point;


/*++
    Check that lhs is equal to rhs.
--*/
inline bool EQ( double lhs, double rhs )
{
    return ( ::fabs(rhs - lhs) < DOUBLE_PRECISION );
}


/*++
    Check that lhs is equal to rhs (with use of specified precision).
--*/
inline bool EQ( double lhs, double rhs, double eps )
{
    return ( ::fabs(rhs - lhs) < eps );
}


/*++
    Check that lhs is not equal to rhs.
--*/
inline bool NE( double lhs, double rhs )
{
    return ( ::fabs(rhs - lhs) > DOUBLE_PRECISION );
}


/*++
    Check that lhs is not equal to rhs (with use of specified precision).
--*/
inline bool NE( double lhs, double rhs, double eps )
{
    return ( ::fabs(rhs - lhs) > eps );
}


/*++
    Check that lhs is less or equal than rhs.
--*/
inline bool LE( double lhs, double rhs )
{
    return ( (lhs - rhs) < DOUBLE_PRECISION );
}


/*++
    Check that lhs is less or equal than rhs (with use of specified precision).
--*/
inline bool LE( double lhs, double rhs, double eps )
{
    return ( (lhs - rhs) < eps );
}


/*++
    Check that lhs is less than rhs.
--*/
inline bool LT( double lhs, double rhs )
{
    return ( (lhs - rhs) < -DOUBLE_PRECISION );
}


/*++
    Check that lhs is less than rhs (with use of specified precision).
--*/
inline bool LT( double lhs, double rhs, double eps )
{
    return ( (lhs - rhs) < -eps );
}


/*++
    Tricky check that pair of values (lhsHigh, lhsLow) is less than pair of values
    (rhsHigh, rhsLow). Function makes it possible to keep compound object like points and vectors
    inside set (data structure).
--*/
bool LT( double lhsHigh, double lhsLow, double rhsHigh, double rhsLow );


/*++
    Tricky check that pair of values (lhsHigh, lhsLow) is less than pair of values
    (rhsHigh, rhsLow) (with use of specified precision).
--*/
bool LT( double lhsHigh, double lhsLow, double rhsHigh, double rhsLow, double eps );


/*++
    Check that lhs is greater or equal than rhs.
--*/
inline bool GE( double lhs, double rhs )
{
    return ( (lhs - rhs) > -DOUBLE_PRECISION );
}


/*++
    Check that lhs is greater or equal than rhs (with use of specified precision).
--*/
inline bool GE( double lhs, double rhs, double eps )
{
    return ( (lhs - rhs) > -eps );
}


/*++
    Check that lhs is greater than rhs.
--*/
inline bool GT( double lhs, double rhs )
{
    return ( (lhs - rhs) > DOUBLE_PRECISION );
}


/*++
    Check that lhs is greater than rhs (with use of specified precision).
--*/
inline bool GT( double lhs, double rhs, double eps )
{
    return ( (lhs - rhs) > eps );
}


/*++
    Check that value equals zero.
--*/
inline bool EQNil( double value )
{
    return ( ::fabs(value) < DOUBLE_PRECISION );
}


/*++
    Check that value equals zero (with use of specified precision).
--*/
inline bool EQNil( double value, double eps )
{
    return ( ::fabs(value) < eps );
}


/*++
    Check that value not equals zero.
--*/
inline bool NENil( double value )
{
    return ( ::fabs(value) > DOUBLE_PRECISION );
}


/*++
    Check that value not equals zero (with use of specified precision).
--*/
inline bool NENil( double value, double eps )
{
    return ( ::fabs(value) > eps );
}


/*++
    Check that lhsAngle is equal to rhsAngle (two angles are equal if cosine of their difference
    is equal to 1.0).
--*/
inline bool EQAngles( double lhsAngle, double rhsAngle )
{
    return EQ( ::cos(lhsAngle - rhsAngle), 1.0 );
}


/*++
    Check that interval [begin, end] include value (begin <= value <= end).
--*/
inline bool IsInsideClosedInterval( double value, double begin, double end )
{
    return ( GE(value, begin) && LE(value, end) );
}


/*++
    Check that interval (begin, end) include value (begin < value < end).
--*/
inline bool IsInsideOpenInterval( double value, double begin, double end )
{
    return ( GT(value, begin) && LT(value, end) );
}


/*++
    Method that round real value by mathematical rules.
--*/
inline double Round( double value )
{
    double valueFloor = ::floor( value );
    return ( ((value - valueFloor) < 0.5) ? (valueFloor) : (valueFloor + 1.0) );
}


/*++
    Method that convert degrees to radians.
--*/
inline double Radians( double degrees )
{
    return ( degrees * (PI / PI_DEG) );
}


/*++
    Method that convert radians to degrees.
--*/
inline double Degrees( double radians )
{
    return ( radians * (PI_DEG / PI) );
}


/*++
    Normalize value from the interval [0, endValue] to the interval [0, targetEndValue].
--*/
inline double Normalize( double value, double endValue, double targetEndValue = 1.0 )
{
    return ( value / endValue ) * targetEndValue;
}


/*++
    Normalize value from the interval [0, endValue] to the interval [0, targetEndValue].
--*/
inline double Normalize( double value, double beginValue, double endValue,
    double targetBeginValue, double targetEndValue )
{
    return ( (value - beginValue) / (endValue - beginValue) ) *
        ( targetEndValue - targetBeginValue ) + targetBeginValue;
}


/*++
    Normalize value from the interval [0, endValue] to the interval [0, targetEndValue] with
    retrenchment by lower and upper bound.
--*/
double StrictNormalize( double value, double endValue, double targetEndValue = 1.0 );


/*++
    Normalize value from the interval [beginValue, endValue] to the interval
    [targetBeginValue, targetEndValue] with retrenchment by lower and upper bound.
--*/
double StrictNormalize( double value, double beginValue, double endValue,
    double targetBeginValue, double targetEndValue );


/*++
    Normalize angle to interval [0, PI2].
--*/
inline double NormalizeAngle( double angle )
{
    double n = ::floor( angle / PI2 );
    return ( angle - n * PI2 );
}


/*++
    Calculation of minimum value.
--*/
inline double CalcMin( double value1, double value2 )
{
    return ( value1 < value2 ? value1 : value2 );
}


/*++
    Calculation of maximum value.
--*/
inline double CalcMax( double value1, double value2 )
{
    return ( value1 > value2 ? value1 : value2 );
}


/*++
    Calculation of distance between two points.
--*/
double CalcDistance( double x1, double y1, double x2, double y2 );


/*++
    Calculation of distance between two points.
--*/
double CalcDistance( const Point& p1, const Point& p2 );


/*++
    Calculation of middle point of two points.
--*/
Point CalcMiddlePoint( const Point& p1, const Point& p2 );


//-------------------------------------------------------------------------------------------------
// P A I R
//-------------------------------------------------------------------------------------------------


/*++
    Pair.
--*/
class Pair {
public:
    Pair();
    Pair( const Pair& rhs );
    Pair( double v1, double v2 );
    Pair& operator =( const Pair& rhs );

    // modifications
    void Init( double v1, double v2 );

    // operators
    void operator +=( const Pair& rhs );
    void operator -=( const Pair& rhs );
    bool operator ==( const Pair& rhs ) const;
    bool operator !=( const Pair& rhs ) const;
    bool operator <( const Pair& rhs ) const;

public:
    double v1_;
    double v2_;
};


std::ostream& operator <<( std::ostream& os, const Pair& rhs );


//-------------------------------------------------------------------------------------------------
// I N T E R V A L
//-------------------------------------------------------------------------------------------------


/*++
    Interval.
--*/
class Interval {
public:
    Interval();
    Interval( const Interval& rhs );
    Interval( double begin, double end );
    Interval& operator =( const Interval& rhs );

    // modifications
    void Init( double begin, double end );
    void Widen( double beginDelta, double endDelta );
    void WidenOnNormalizedValue( double beginNormalizedDelta, double endNormalizedDelta );

    // calculations
    double CalcMiddle() const;
    double CalcLength() const;
    double CalcValueByNormalizedValue( double notmalizedValue ) const;
    double CalcNormalizedValue( double value ) const;
    bool IsInclude( double value ) const;

    // operators
    bool operator ==( const Interval& rhs ) const;
    bool operator !=( const Interval& rhs ) const;

public:
    double begin_;
    double end_;
};


std::ostream& operator <<( std::ostream& os, const Interval& rhs );


const Interval ZERO_INTERVAL( 0.0, 0.0 );
const Interval IDENTITY_INTERVAL( 0.0, 1.0 );
const Interval PI_INTERVAL( 0.0, PI );
const Interval PI2_INTERVAL( 0.0, PI2 );
const Interval PI_DIV_2_INTERVAL( 0.0, PI_DIV_2 );
const Interval PI_DIV_3_INTERVAL( 0.0, PI_DIV_3 );
const Interval PI_DIV_4_INTERVAL( 0.0, PI_DIV_4 );
const Interval PI_DIV_6_INTERVAL( 0.0, PI_DIV_6 );


//-------------------------------------------------------------------------------------------------
// P O I N T
//-------------------------------------------------------------------------------------------------


class Vector;
class Matrix3x3;


/*++
    Mathematical two-dimensional point.
--*/
class Point {
public:
    Point();
    Point( const Point& rhs );
    Point( double x, double y );
    Point& operator =( const Point& rhs );

    // modifications
    void Zero();
    void Init( double x, double y );
    void Transfer( const Vector& shift );
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

    // operators
    void operator +=( const Point& rhs );
    void operator -=( const Point& rhs );
    void operator *=( double coef );
    bool operator ==( const Point& rhs ) const;
    bool operator !=( const Point& rhs ) const;
    bool operator <( const Point& rhs ) const;

public:
    double x_;
    double y_;
};


std::ostream& operator <<( std::ostream& stream, const Point& rhs );


const Point ZERO_POINT( 0.0, 0.0 );


//-------------------------------------------------------------------------------------------------
// V E C T O R
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical two-dimensional vector.
--*/
class Vector {
public:
    Vector();
    Vector( const Vector& rhs );
    Vector( double x, double y );
    Vector( const Point& from, const Point& to );
    Vector& operator =( const Vector& rhs );

    // modifications
    void Zero();
    void Init( double x, double y );
    void Normalize();
    void Invert();
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

    // calculations
    double CalcLength() const;
    double CalcAngle() const;
    double CalcAngle( const Vector& v ) const;
    double CalcRelativeAngle( const Vector& v ) const;
    double CalcDot( const Vector& v ) const;
    Vector CalcPerpendicular( bool direction = true ) const;
    bool IsZero() const;
    bool IsNormalized() const;
    bool IsCollinear( const Vector& v ) const;
    bool IsCodirectional( const Vector& v ) const;

    // operators
    void operator +=( const Vector& rhs );
    void operator -=( const Vector& rhs );
    void operator *=( double coef );
    bool operator ==( const Vector& rhs ) const;
    bool operator !=( const Vector& rhs ) const;

public:
    double x_;
    double y_;
};


std::ostream& operator <<( std::ostream& os, const Vector& rhs );


const Vector ZERO_VECTOR( 0.0, 0.0 );
const Vector GLOBAL_X_AXIS( 1.0, 0.0 );
const Vector GLOBAL_Y_AXIS( 0.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 1
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical 2x1 matrix.
--*/
class Matrix2x1 {
public:
    Matrix2x1();
    Matrix2x1( const Matrix2x1& rhs );
    Matrix2x1( double m00, double m10 );
    Matrix2x1& operator =( const Matrix2x1& rhs );

    // initialization methods
    void Init( double m00, double m10 );
    void InitZeroMatrix();
    void InitIdentityMatrix();

    // operators
    void operator +=( const Matrix2x1& rhs );
    void operator -=( const Matrix2x1& rhs );
    void operator *=( double coef );

public:
    static const int M = 2;
    static const int N = 1;
    double m_[ M ][ N ];
};


std::ostream& operator <<( std::ostream& os, const Matrix2x1& rhs );


const Matrix2x1 ZERO_MATRIX_2X1( 0.0, 0.0 );
const Matrix2x1 IDENTITY_MATRIX_2X1( 1.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 2
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical 2x2 matrix.
--*/
class Matrix2x2 {
public:
    Matrix2x2();
    Matrix2x2( const Matrix2x2& rhs );
    Matrix2x2( double m00, double m01, double m10, double m11 );
    Matrix2x2& operator =( const Matrix2x2& rhs );

    // initialization methods
    void Init( double m00, double m01, double m10, double m11 );
    void InitZeroMatrix();
    void InitIdentityMatrix();

    // modifications
    void Invert();

    // operators
    void operator +=( const Matrix2x2& rhs );
    void operator -=( const Matrix2x2& rhs );
    void operator *=( double coef );
    void operator *=( const Matrix2x2& rhs );
    Matrix2x1 operator *( const Matrix2x1& rhs );

public:
    static const int M = 2;
    static const int N = 2;
    double m_[ M ][ N ];
};


std::ostream& operator <<( std::ostream& os, const Matrix2x2& rhs );


const Matrix2x2 ZERO_MATRIX_2X2( 0.0, 0.0, 0.0, 0.0 );
const Matrix2x2 IDENTITY_MATRIX_2X2( 1.0, 1.0, 1.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// M A T R I X 3 X 3
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical 3x3 matrix.
--*/
class Matrix3x3 {
public:
    Matrix3x3();
    Matrix3x3( const Matrix3x3& rhs );
    Matrix3x3( double m00, double m01, double m02,
        double m10, double m11, double m12,
        double m20, double m21, double m22 );
    Matrix3x3& operator =( const Matrix3x3& rhs );

    // initialization methods
    void Init( double m00, double m01, double m02,
        double m10, double m11, double m12,
        double m20, double m21, double m22 );
    void InitZeroMatrix();
    void InitIdentityMatrix();
    void InitTransferMatrix( const Vector& shift );
    void InitInvertedTransferMatrix( const Vector& shift );
    void InitScaleMatrix( double xCoef, double yCoef, const Point* pCenter = 0 );
    void InitRotationMatrix( double angle, const Point* pCenter = 0 );

    // modifications
    void Invert();

    // operators
    void operator +=( const Matrix3x3& rhs );
    void operator -=( const Matrix3x3& rhs );
    void operator *=( double coef );
    void operator *=( const Matrix3x3& rhs );

public:
    static const int M = 3;
    static const int N = 3;
    double m_[ M ][ N ];
};


std::ostream& operator <<( std::ostream& os, const Matrix3x3& rhs );


const Matrix3x3 ZERO_MATRIX_3X3( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 );
const Matrix3x3 IDENTITY_MATRIX_3X3( 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// C S
//-------------------------------------------------------------------------------------------------


/*++
    Mathematical two-dimensional coordinate system.
--*/
class CS {
public:
    CS();
    CS( const CS& rhs );
    CS( const Point& pos, double angle = 0.0 );
    CS& operator =( const CS& rhs );

    // initialization methods
    void InitDefaultCS();
    void Init( const Point& pos, const Vector& axisX, const Vector& axisY );
    void Init( const Point& pos, double angle = 0.0 );

    // modifications
    void Transfer( const Vector& shift );
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

    // calculations
    void CalcMatrixFromCS( Matrix3x3& m ) const;
    void CalcMatrixIntoCS( Matrix3x3& m ) const;

    // operators
    bool operator ==( const CS& rhs ) const;
    bool operator !=( const CS& rhs ) const;

public:
    Point pos_;
    Vector axisX_;
    Vector axisY_;
};


/*++
    Transform something from local coordinate system to global coordinate system.
--*/
template <typename T>
inline void TransformFromCS( const CS& cs, T& item )
{
    Matrix3x3 m;
    cs.CalcMatrixFromCS( m );
    item.Transform( m );
}


/*++
    Transform something from global coordinate system to local coordinate system.
--*/
template <typename T>
inline void TransformIntoCS( const CS& cs, T& item )
{
    Matrix3x3 m;
    cs.CalcMatrixIntoCS( m );
    item.Transform( m );
}


std::ostream& operator <<( std::ostream& os, const CS& rhs );


const CS DEFAULT_CS;


//-------------------------------------------------------------------------------------------------
// R E C T
//-------------------------------------------------------------------------------------------------


/*++
    Rectanlge.
--*/
class Rect {
public:
    Rect();
    Rect( const Rect& rhs );
    Rect( const Point& p1, const Point& p2 );
    Rect( double x1, double y1, double x2, double y2 );
    Rect( double width, double height );
    Rect& operator =( const Rect& rhs );

    // modifications
    void Init( double x1, double y1, double x2, double y2 );
    void Init( double width, double height );
    void Transfer( const Vector& shift );
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Inflate( double dWidth, double dHeight );
    void Deflate( double dWidth, double dHeight );

    // calculations
    double CalcWidth() const;
    double CalcHeight() const;
    double CalcArea() const;
    double CalcAspectRatio() const;
    Point CalcCenter() const;
    bool IsInclude( const Point& rhs ) const;

    // operators
    bool operator ==( const Rect& rhs ) const;
    bool operator !=( const Rect& rhs ) const;

public:
    Point minPoint_;
    Point maxPoint_;
};


std::ostream& operator <<( std::ostream& os, const Rect& rhs );


//-------------------------------------------------------------------------------------------------
// B O U N D I N G   B O X
//-------------------------------------------------------------------------------------------------


/*++
    Bounding box.
--*/
class BoundingBox {
public:
    BoundingBox();
    BoundingBox( const BoundingBox& rhs );
    BoundingBox& operator =( const BoundingBox& rhs );

    // specific methods
    void SetEmpty();
    void Add( const BoundingBox& rhs );
    void Add( const Point& rhs );

    // modifications
    void Transfer( const Vector& shift );
    void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    void Inflate( double dWidth, double dHeight );
    void Deflate( double dWidth, double dHeight );

    // calculations
    double CalcWidth() const;
    double CalcHeight() const;
    double CalcArea() const;
    double CalcAspectRatio() const;
    Point CalcCenter() const;
    bool IsEmpty() const;

    // accessors
    const Point& GetMinPoint() const;
    const Point& GetMaxPoint() const;

    // operators
    bool operator ==( const BoundingBox& rhs ) const;
    bool operator !=( const BoundingBox& rhs ) const;

private:
    Rect rect_;
};


BoundingBox& operator <<( BoundingBox& lhs, const BoundingBox& rhs );


BoundingBox& operator <<( BoundingBox& lhs, const Point& rhs );


//-------------------------------------------------------------------------------------------------
// F A C T O R I A L   C A L C U L A T O R
//-------------------------------------------------------------------------------------------------


/*++
    Factorial calculator.
--*/
class FactorialCalculator {
public:
    FactorialCalculator( int precalcValuesCount = 100 );
    ~FactorialCalculator();

    double Calc( int value ) const;

private:
    FactorialCalculator( const FactorialCalculator& rhs );
    FactorialCalculator& operator =( const FactorialCalculator& rhs );

private:
    int precalcValuesCount_;
    double* pPrecalcValues_;
};


const FactorialCalculator FACTORIAL_CALCULATOR;


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


#endif

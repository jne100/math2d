///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#include "m2d.h"

#include <cstring>


namespace m2d {


//-------------------------------------------------------------------------------------------------
// U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


bool LT( double lhsHigh, double lhsLow, double rhsHigh, double rhsLow )
{
    if ( EQ(lhsHigh, rhsHigh) ) {
        // compare minor components
        return LT( lhsLow, rhsLow );
    } else {
        // compare major components
        return LT( lhsHigh, rhsHigh );
    }
}


bool LT( double lhsHigh, double lhsLow, double rhsHigh, double rhsLow, double eps )
{
    if ( EQ(lhsHigh, rhsHigh, eps) ) {
        // compare minor components
        return LT( lhsLow, rhsLow, eps );
    } else {
        // compare major components
        return LT( lhsHigh, rhsHigh, eps );
    }
}


double StrictNormalize( double value, double endValue, double targetEndValue /*= 1.0*/ )
{
    double normValue = Normalize( value, endValue, targetEndValue );
    if ( normValue < 0.0 ) {
        return 0.0;
    } else if ( normValue > targetEndValue ) {
        return targetEndValue;
    } else {
        return normValue;
    }
}


double StrictNormalize( double value, double beginValue, double endValue,
    double targetBeginValue, double targetEndValue )
{
    double normValue = Normalize( value, beginValue, endValue, targetBeginValue, targetEndValue );
    if ( normValue < targetBeginValue ) {
        return targetBeginValue;
    } else if ( normValue > targetEndValue ) {
        return targetEndValue;
    } else {
        return normValue;
    }
}


double CalcDistance( double x1, double y1, double x2, double y2 )
{
    double dx = x2 - x1;
    double dy = y2 - y1;
    return ( ::sqrt((dx * dx) + (dy * dy)) );
}


double CalcDistance( const Point& p1, const Point& p2 )
{
    return CalcDistance( p1.x_, p1.y_, p2.x_, p2.y_ );
}


Point CalcMiddlePoint( const Point& p1, const Point& p2 )
{
    return Point( (p1.x_ + p2.x_) / 2.0, (p1.y_ + p2.y_) / 2.0 );
}


//-------------------------------------------------------------------------------------------------
// P A I R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Pair::Pair()
{
}


Pair::Pair( const Pair& rhs )
    : v1_( rhs.v1_ )
    , v2_( rhs.v2_ )
{
}


Pair::Pair( double v1, double v2 )
    : v1_( v1 )
    , v2_( v2 )
{
}


Pair& Pair::operator =( const Pair& rhs )
{
    v1_ = rhs.v1_;
    v2_ = rhs.v2_;
    return ( *this );
}


void Pair::Init( double v1, double v2 )
{
    v1_ = v1;
    v2_ = v2;
}


void Pair::operator +=( const Pair& rhs )
{
    v1_ += rhs.v1_;
    v2_ += rhs.v2_;
}


void Pair::operator -=( const Pair& rhs )
{
    v1_ -= rhs.v1_;
    v2_ -= rhs.v2_;
}


bool Pair::operator ==( const Pair& rhs ) const
{
    return ( EQ(v1_, rhs.v1_) && EQ(v2_, rhs.v2_) );
}


bool Pair::operator !=( const Pair& rhs ) const
{
    return ( !((*this) == rhs) );
}


bool Pair::operator <( const Pair& rhs ) const
{
    return LT( v2_, v1_, rhs.v2_, rhs.v1_ );
}


std::ostream& operator <<( std::ostream& os, const Pair& rhs )
{
    os << "(" << rhs.v1_ << ", " << rhs.v2_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// I N T E R V A L   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Interval::Interval()
{
}


Interval::Interval( const Interval& rhs )
    : begin_( rhs.begin_ )
    , end_( rhs.end_ )
{
}


Interval::Interval( double begin, double end )
    : begin_( begin )
    , end_( end )
{
}


Interval& Interval::operator =( const Interval& rhs )
{
    begin_ = rhs.begin_;
    end_ = rhs.end_;
    return ( *this );
}


void Interval::Init( double begin, double end )
{
    begin_ = begin;
    end_ = end;
}


void Interval::Widen( double beginDelta, double endDelta )
{
    begin_ -= beginDelta;
    end_ += endDelta;
}


void Interval::WidenOnNormalizedValue( double beginNormalizedDelta, double endNormalizedDelta )
{
    double length = CalcLength();
    begin_ -= length * beginNormalizedDelta;
    end_ += length * endNormalizedDelta;
}


double Interval::CalcMiddle() const
{
    assert( LE(begin_, end_) );
    return ( begin_ + (end_ - begin_) / 2.0 );
}


double Interval::CalcLength() const
{
    assert( LE(begin_, end_) );
    return ( end_ - begin_ );
}


double Interval::CalcValueByNormalizedValue( double notmalizedValue ) const
{
    assert( LE(begin_, end_) );
    return ( begin_ + CalcLength() * notmalizedValue );
}


double Interval::CalcNormalizedValue( double value ) const
{
    assert( LE(begin_, end_) );
    return ( (value - begin_) / CalcLength() );
}


bool Interval::IsInclude( double value ) const
{
    assert( LE(begin_, end_) );
    return IsInsideClosedInterval( value, begin_, end_ );
}


bool Interval::operator ==( const Interval& rhs ) const
{
    return ( EQ(begin_, rhs.begin_) && EQ(end_, rhs.end_) );
}


bool Interval::operator !=( const Interval& rhs ) const
{
    return ( !((*this) == rhs) );
}


std::ostream& operator <<( std::ostream& os, const Interval& rhs )
{
    os << "[" << rhs.begin_ << "; " << rhs.end_ << "]";
    return os;
}


//-------------------------------------------------------------------------------------------------
// P O I N T   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Point::Point()
{
}


Point::Point( const Point& rhs )
    : x_( rhs.x_ )
    , y_( rhs.y_ )
{
}


Point::Point( double x, double y )
    : x_( x )
    , y_( y )
{
}


Point& Point::operator =( const Point& rhs )
{
    x_ = rhs.x_;
    y_ = rhs.y_;
    return ( *this );
}


void Point::Zero()
{
    ( *this ) = ZERO_POINT;
}


void Point::Init( double x, double y )
{
    x_ = x;
    y_ = y;
}


void Point::Transfer( const Vector& shift )
{
    x_ += shift.x_;
    y_ += shift.y_;
}


void Point::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    if ( pCenter == 0 ) {
        x_ *= xCoef;
        y_ *= yCoef;

    } else {
        x_ = ( x_ - pCenter->x_ ) * xCoef + pCenter->x_;
        y_ = ( y_ - pCenter->y_ ) * yCoef + pCenter->y_;
    }
}


void Point::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    double angleSin = ::sin( angle );
    double angleCos = ::cos( angle );

    if ( pCenter == 0 ) {
        double x = x_ * angleCos - y_ * angleSin;
        double y = x_ * angleSin + y_ * angleCos;
        x_ = x;
        y_ = y;

    } else {
        double x = x_ - pCenter->x_;
        double y = y_ - pCenter->y_;
        x_ = ( x * angleCos - y * angleSin ) + pCenter->x_;
        y_ = ( x * angleSin + y * angleCos ) + pCenter->y_;
    }
}


void Point::Transform( const Matrix3x3& m )
{
    double oldX = x_;
    double oldY = y_;
    x_ = oldX * m.m_[ 0 ][ 0 ] + oldY * m.m_[ 1 ][ 0 ] + m.m_[ 2 ][ 0 ];
    y_ = oldX * m.m_[ 0 ][ 1 ] + oldY * m.m_[ 1 ][ 1 ] + m.m_[ 2 ][ 1 ];
}


void Point::operator +=( const Point& rhs )
{
    x_ += rhs.x_;
    y_ += rhs.y_;
}


void Point::operator -=( const Point& rhs )
{
    x_ -= rhs.x_;
    y_ -= rhs.y_;
}


void Point::operator *=( double coef )
{
    x_ *= coef;
    y_ *= coef;
}


bool Point::operator ==( const Point& rhs ) const
{
    return ( (EQ(x_, rhs.x_)) && (EQ(y_, rhs.y_)) );
}


bool Point::operator !=( const Point& rhs ) const
{
    return ( !((*this) == rhs) );
}


bool Point::operator <( const Point& rhs ) const
{
    return LT( y_, x_, rhs.y_, rhs.x_ );
}


std::ostream& operator <<( std::ostream& os, const Point& rhs )
{
    os << "(" << rhs.x_ << ", " << rhs.y_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// V E C T O R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Vector::Vector()
{
}


Vector::Vector( const Vector& rhs )
    : x_( rhs.x_ )
    , y_( rhs.y_ )
{
}


Vector::Vector( double x, double y )
    : x_( x )
    , y_( y )
{
}


/*++
    Construction of vector from->to.
--*/
Vector::Vector( const Point& from, const Point& to )
    : x_( to.x_ - from.x_ )
    , y_( to.y_ - from.y_ )
{
}


Vector& Vector::operator =( const Vector& rhs )
{
    x_ = rhs.x_;
    y_ = rhs.y_;
    return ( *this );
}


void Vector::Zero()
{
    ( *this ) = ZERO_VECTOR;
}


void Vector::Init( double x, double y )
{
    x_ = x;
    y_ = y;
}


void Vector::Normalize()
{
    double length = CalcLength();
    if ( NENil(length) ) {
        double lengthInv = 1.0 / length;
        x_ *= lengthInv;
        y_ *= lengthInv;
    }
}


void Vector::Invert()
{
    x_ *= -1.0;
    y_ *= -1.0;
}


void Vector::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    if ( pCenter != 0 ) {
        x_ -= pCenter->x_;
        y_ -= pCenter->y_;
    }

    x_ *= xCoef;
    y_ *= yCoef;

    if ( pCenter != 0 ) {
        x_ += pCenter->x_;
        y_ += pCenter->y_;
    }
}


void Vector::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    double x = x_;
    double y = y_;

    if ( pCenter != 0 ) {
        x -= pCenter->x_;
        y -= pCenter->y_;
    }

    double angleSin = ::sin( angle );
    double angleCos = ::cos( angle );
    x_ = x * angleCos - y * angleSin;
    y_ = x * angleSin + y * angleCos;

    if ( pCenter != 0 ) {
        x_ += pCenter->x_;
        y_ += pCenter->y_;
    }
}


void Vector::Transform( const Matrix3x3& m )
{
    double oldX = x_;
    double oldY = y_;
    x_ = oldX * m.m_[ 0 ][ 0 ] + oldY * m.m_[ 1 ][ 0 ];
    y_ = oldX * m.m_[ 0 ][ 1 ] + oldY * m.m_[ 1 ][ 1 ];
}


double Vector::CalcLength() const
{
    return ( ::sqrt(x_ * x_ + y_ * y_) );
}


/*++
    Calculation of angle of the vector in global space.
--*/
double Vector::CalcAngle() const
{
    return ( ::atan2(y_, x_) );
}


/*++
    Calculation of angle between two vectors. Method return angle in interval [0, PI2] indicating
    which vector is ahead.
--*/
double Vector::CalcAngle( const Vector& v ) const
{
    return ( ::atan2(v.y_, v.x_) - ::atan2(y_, x_) );
}


/*++
    Calculation of relative angle between two vectors in interval [0, PI] ignoring vectors order
    (method is commutative).
--*/
double Vector::CalcRelativeAngle( const Vector& v ) const
{
    return ( ::acos(CalcDot(v) / (CalcLength() * v.CalcLength())) );
}


double Vector::CalcDot( const Vector& v ) const
{
    return ( (x_ * v.x_) + (y_ * v.y_) );
}


Vector Vector::CalcPerpendicular( bool direction /*= true*/ ) const
{
    return (direction) ? Vector( -y_, x_ ) : Vector( y_, -x_ );
}


bool Vector::IsZero() const
{
    return ( EQNil(x_) && EQNil(y_) );
}


bool Vector::IsNormalized() const
{
    return EQ( CalcLength(), 1.0 );
}


/*++
    Two vectors are collinear if their pseudo scalar product equals zero
    (|v1| * |v2| * sin(a) = 0). Formula can be simplified for current condition check.
--*/
bool Vector::IsCollinear( const Vector& v ) const
{
    return EQNil( ::sin(CalcAngle(v)) );
}


bool Vector::IsCodirectional( const Vector& v ) const
{
    return EQAngles( CalcAngle(), v.CalcAngle() );
}


void Vector::operator +=( const Vector& rhs )
{
    x_ += rhs.x_;
    y_ += rhs.y_;
}


void Vector::operator -=( const Vector& rhs )
{
    x_ -= rhs.x_;
    y_ -= rhs.y_;
}


void Vector::operator *=( double coef )
{
    x_ *= coef;
    y_ *= coef;
}


bool Vector::operator ==( const Vector& rhs ) const
{
    return ( (EQ(x_, rhs.x_)) && (EQ(y_, rhs.y_)) );
}


bool Vector::operator !=( const Vector& rhs ) const
{
    return ( !((*this) == rhs) );
}


std::ostream& operator <<( std::ostream& os, const Vector& rhs )
{
    os << "(" << rhs.x_ << ", " << rhs.y_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 1   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Matrix2x1::Matrix2x1()
{
}


Matrix2x1::Matrix2x1( const Matrix2x1& rhs )
{
    ( *this ) = rhs;
}


Matrix2x1::Matrix2x1( double m00, double m10 )
{
    Init( m00, m10 );
}


Matrix2x1& Matrix2x1::operator =( const Matrix2x1& rhs )
{
    ::memcpy( this->m_, rhs.m_, sizeof(this->m_) );
    return ( *this );
}


void Matrix2x1::Init( double m00, double m10 )
{
    m_[ 0 ][ 0 ] = m00;
    m_[ 1 ][ 0 ] = m10;
}


void Matrix2x1::InitZeroMatrix()
{
    ( *this ) = ZERO_MATRIX_2X1;
}


void Matrix2x1::InitIdentityMatrix()
{
    ( *this ) = IDENTITY_MATRIX_2X1;
}


void Matrix2x1::operator +=( const Matrix2x1& rhs )
{
    m_[ 0 ][ 0 ] += rhs.m_[ 0 ][ 0 ];
    m_[ 1 ][ 0 ] += rhs.m_[ 1 ][ 0 ];
}


void Matrix2x1::operator -=( const Matrix2x1& rhs )
{
    m_[ 0 ][ 0 ] -= rhs.m_[ 0 ][ 0 ];
    m_[ 1 ][ 0 ] -= rhs.m_[ 1 ][ 0 ];
}


void Matrix2x1::operator *=( double coef )
{
    m_[ 0 ][ 0 ] *= coef;
    m_[ 1 ][ 0 ] *= coef;
}


std::ostream& operator <<( std::ostream& os, const Matrix2x1& rhs )
{
    os << "((" << rhs.m_[ 0 ][ 0 ] << "), (" << rhs.m_[ 1 ][ 0 ] << "))";
    return os;
}


//-------------------------------------------------------------------------------------------------
// M A T R I X 2 X 2   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Matrix2x2::Matrix2x2()
{
}


Matrix2x2::Matrix2x2( const Matrix2x2& rhs )
{
    ( *this ) = rhs;
}


Matrix2x2::Matrix2x2( double m00, double m01, double m10, double m11 )
{
    Init( m00, m01, m10, m11 );
}


Matrix2x2& Matrix2x2::operator =( const Matrix2x2& rhs )
{
    ::memcpy( this->m_, rhs.m_, sizeof(this->m_) );
    return ( *this );
}


void Matrix2x2::Init( double m00, double m01, double m10, double m11 )
{
    m_[ 0 ][ 0 ] = m00;
    m_[ 0 ][ 1 ] = m01;
    m_[ 1 ][ 0 ] = m10;
    m_[ 1 ][ 1 ] = m11;
}


void Matrix2x2::InitZeroMatrix()
{
    ( *this ) = ZERO_MATRIX_2X2;
}


void Matrix2x2::InitIdentityMatrix()
{
    ( *this ) = IDENTITY_MATRIX_2X2;
}


void Matrix2x2::Invert()
{
    double det = m_[0][0] * m_[1][1] - m_[0][1] * m_[1][0];

    if ( NENil(det) ) {

        double detInv = 1.0 / det;

        Matrix2x2 result;

        result.m_[ 0 ][ 0 ] = m_[ 1 ][ 1 ] * detInv;
        result.m_[ 0 ][ 1 ] = m_[ 0 ][ 1 ] * -detInv;
        result.m_[ 1 ][ 0 ] = m_[ 1 ][ 0 ] * -detInv;
        result.m_[ 1 ][ 1 ] = m_[ 0 ][ 0 ] * detInv;

        ( *this ) = result;
    }
}


void Matrix2x2::operator +=( const Matrix2x2& rhs )
{
    m_[ 0 ][ 0 ] += rhs.m_[ 0 ][ 0 ];
    m_[ 0 ][ 1 ] += rhs.m_[ 0 ][ 1 ];
    m_[ 1 ][ 0 ] += rhs.m_[ 1 ][ 0 ];
    m_[ 1 ][ 1 ] += rhs.m_[ 1 ][ 1 ];
}


void Matrix2x2::operator -=( const Matrix2x2& rhs )
{
    m_[ 0 ][ 0 ] -= rhs.m_[ 0 ][ 0 ];
    m_[ 0 ][ 1 ] -= rhs.m_[ 0 ][ 1 ];
    m_[ 1 ][ 0 ] -= rhs.m_[ 1 ][ 0 ];
    m_[ 1 ][ 1 ] -= rhs.m_[ 1 ][ 1 ];
}


void Matrix2x2::operator *=( double coef )
{
    m_[ 0 ][ 0 ] *= coef;
    m_[ 0 ][ 1 ] *= coef;
    m_[ 1 ][ 0 ] *= coef;
    m_[ 1 ][ 1 ] *= coef;
}


void Matrix2x2::operator *=( const Matrix2x2& rhs )
{
    Matrix2x2 result;

    result.m_[ 0 ][ 0 ] = ( m_[0][0] * rhs.m_[0][0] + m_[0][1] * rhs.m_[1][0] );
    result.m_[ 0 ][ 1 ] = ( m_[0][0] * rhs.m_[0][1] + m_[0][1] * rhs.m_[1][1] );
    result.m_[ 1 ][ 0 ] = ( m_[1][0] * rhs.m_[0][0] + m_[1][1] * rhs.m_[1][0] );
    result.m_[ 1 ][ 1 ] = ( m_[1][0] * rhs.m_[0][1] + m_[1][1] * rhs.m_[1][1] );

    ( *this ) = result;
}


Matrix2x1 Matrix2x2::operator *( const Matrix2x1& rhs )
{
    Matrix2x1 result;

    result.m_[ 0 ][ 0 ] = ( m_[0][0] * rhs.m_[0][0] + m_[0][1] * rhs.m_[1][0] );
    result.m_[ 1 ][ 0 ] = ( m_[1][0] * rhs.m_[0][0] + m_[1][1] * rhs.m_[1][0] );

    return result;
}


std::ostream& operator <<( std::ostream& os, const Matrix2x2& rhs )
{
    os << "((" << rhs.m_[ 0 ][ 0 ] << ", " << rhs.m_[ 0 ][ 1 ] << "), (" << rhs.m_[ 1 ][ 0 ] <<
        ", " << rhs.m_[ 1 ][ 1 ] << "))";
    return os;
}


//-------------------------------------------------------------------------------------------------
// M A T R I X 3 X 3   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Matrix3x3::Matrix3x3()
{
}


Matrix3x3::Matrix3x3( const Matrix3x3& rhs )
{
    ( *this ) = rhs;
}


Matrix3x3::Matrix3x3( double m00, double m01, double m02,
    double m10, double m11, double m12,
    double m20, double m21, double m22 )
{
    Init( m00, m01, m02,
        m10, m11, m12,
        m20, m21, m22 );
}


Matrix3x3& Matrix3x3::operator =( const Matrix3x3& rhs )
{
    ::memcpy( this->m_, rhs.m_, sizeof(this->m_) );
    return ( *this );
}


void Matrix3x3::Init( double m00, double m01, double m02,
    double m10, double m11, double m12,
    double m20, double m21, double m22 )
{
    ( m_[0][0] = m00 ); ( m_[0][1] = m01 ); ( m_[0][2] = m02 );
    ( m_[1][0] = m10 ); ( m_[1][1] = m11 ); ( m_[1][2] = m12 );
    ( m_[2][0] = m20 ); ( m_[2][1] = m21 ); ( m_[2][2] = m22 );
}


void Matrix3x3::InitZeroMatrix()
{
    ( *this ) = ZERO_MATRIX_3X3;
}


void Matrix3x3::InitIdentityMatrix()
{
    ( *this ) = IDENTITY_MATRIX_3X3;
}


void Matrix3x3::InitTransferMatrix( const Vector& shift )
{
    Init( 1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        shift.x_, shift.y_, 1.0 );
}


void Matrix3x3::InitInvertedTransferMatrix( const Vector& shift )
{
    Init( 1.0, 0.0, 0.0,
        0.0, 1.0, 0.0,
        -shift.x_, -shift.y_, 1.0 );
}


void Matrix3x3::InitScaleMatrix( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    if ( pCenter == 0 ) {
        Init( xCoef, 0.0, 0.0,
            0.0, yCoef, 0.0,
            0.0, 0.0, 1.0 );

    } else {
        InitTransferMatrix( Vector(*pCenter, ZERO_POINT) );
        Matrix3x3 mtemp;
        mtemp.Init( xCoef, 0.0, 0.0,
            0.0, yCoef, 0.0,
            0.0, 0.0, 1.0 );
        ( *this ) *= mtemp;
        mtemp.InitTransferMatrix( Vector(ZERO_POINT, *pCenter) );
        ( *this ) *= mtemp;
    }
}


void Matrix3x3::InitRotationMatrix( double angle, const Point* pCenter /*= 0*/ )
{
    double costh = ::cos( angle );
    double sinth = ::sin( angle );

    if ( pCenter == 0 ) {
        Init( costh, -sinth, 0.0,
            sinth, costh, 0.0,
            0.0, 0.0, 1.0 );

    } else {
        InitTransferMatrix( Vector(*pCenter, ZERO_POINT) );
        Matrix3x3 mtemp;
        mtemp.Init( costh, -sinth, 0.0,
            sinth, costh, 0.0,
            0.0, 0.0, 1.0 );
        ( *this ) *= mtemp;
        mtemp.InitTransferMatrix( Vector(ZERO_POINT, *pCenter) );
        ( *this ) *= mtemp;
    }
}


void Matrix3x3::Invert()
{
    double det = m_[ 0 ][ 0 ] * ( m_[1][1] * m_[2][2] - m_[2][1] * m_[1][2] ) -
        m_[ 0 ][ 1 ] * ( m_[1][0] * m_[2][2] - m_[2][0] * m_[1][2] ) +
        m_[ 0 ][ 2 ] * ( m_[1][0] * m_[2][1] - m_[2][0] * m_[1][1] );

    if ( NENil(det) ) {

        double detInv = 1.0 / det;

        Matrix3x3 result;

        result.m_[ 0 ][ 0 ] = ( m_[1][1] * m_[2][2] - m_[2][1] * m_[1][2] ) * detInv;
        result.m_[ 1 ][ 0 ] = ( m_[1][0] * m_[2][2] - m_[2][0] * m_[1][2] ) * -detInv;
        result.m_[ 2 ][ 0 ] = ( m_[1][0] * m_[2][1] - m_[2][0] * m_[1][1] ) * detInv;

        result.m_[ 0 ][ 1 ] = ( m_[0][1] * m_[2][2] - m_[2][1] * m_[0][2] ) * -detInv;
        result.m_[ 1 ][ 1 ] = ( m_[0][0] * m_[2][2] - m_[2][0] * m_[0][2] ) * detInv;
        result.m_[ 2 ][ 1 ] = ( m_[0][0] * m_[2][1] - m_[2][0] * m_[0][1] ) * -detInv;

        result.m_[ 0 ][ 2 ] = ( m_[0][1] * m_[1][2] - m_[1][1] * m_[0][2] ) * detInv;
        result.m_[ 1 ][ 2 ] = ( m_[0][0] * m_[1][2] - m_[1][0] * m_[0][2] ) * -detInv;
        result.m_[ 2 ][ 2 ] = ( m_[0][0] * m_[1][1] - m_[1][0] * m_[0][1] ) * detInv;

        ( *this ) = result;
    }
}


void Matrix3x3::operator +=( const Matrix3x3& rhs )
{
    for ( int nRow = 0; nRow < M; ++nRow ) {
        for ( int nColumn = 0; nColumn < N; ++nColumn ) {
            m_[ nRow ][ nColumn ] += rhs.m_[ nRow ][ nColumn ];
        }
    }
}


void Matrix3x3::operator -=( const Matrix3x3& rhs )
{
    for ( int nRow = 0; nRow < M; ++nRow ) {
        for ( int nColumn = 0; nColumn < N; ++nColumn ) {
            m_[ nRow ][ nColumn ] -= rhs.m_[ nRow ][ nColumn ];
        }
    }
}


void Matrix3x3::operator *=( double coef )
{
    for ( int nRow = 0; nRow < M; ++nRow ) {
        for ( int nColumn = 0; nColumn < N; ++nColumn ) {
            m_[ nRow ][ nColumn ] *= coef;
        }
    }
}


void Matrix3x3::operator *=( const Matrix3x3& rhs )
{
    Matrix3x3 result;

    for ( int nRow = 0; nRow < M; ++nRow ) {
        for ( int nColumn = 0; nColumn < N; ++nColumn ) {

            double tempSum = 0;
            for ( int nIndex = 0; nIndex < N; ++nIndex ) {
                tempSum += ( m_[nRow][nIndex] * rhs.m_[nIndex][nColumn] );
            }
            result.m_[ nRow ][ nColumn ] = tempSum;
        }
    }

    ( *this ) = result;
}


std::ostream& operator <<( std::ostream& os, const Matrix3x3& rhs )
{
    os << "(";
    for ( int nRow = 0; nRow < rhs.M; ++nRow ) {
        os << "(" << rhs.m_[ nRow ][ 0 ] << ", " << rhs.m_[ nRow ][ 1 ] << ", " <<
            rhs.m_[ nRow ][ 2 ] << ")";
        if ( nRow != 2 ) {
            os << ", ";
        }
    }
    os << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// C S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


CS::CS()
{
    InitDefaultCS();
}


CS::CS( const CS& rhs )
    : pos_( rhs.pos_ )
    , axisX_( rhs.axisX_ )
    , axisY_( rhs.axisY_ )
{
}


CS::CS( const Point& pos, double angle /*= 0.0*/ )
{
    Init( pos, angle );
}


CS& CS::operator =( const CS& rhs )
{
    pos_ = rhs.pos_;
    axisX_ = rhs.axisX_;
    axisY_ = rhs.axisY_;
    return ( *this );
}


void CS::InitDefaultCS()
{
    pos_.Zero();
    axisX_.Init( 1.0f, 0.0f );
    axisY_.Init( 0.0f, 1.0f );
}


void CS::Init( const Point& pos, const Vector& axisX, const Vector& axisY )
{
    assert( EQ(axisX.CalcLength(), 1.0) );
    assert( EQ(axisY.CalcLength(), 1.0) );
    assert( EQNil(axisX.CalcDot(axisY)) );

    pos_ = pos;
    axisX_ = axisX;
    axisY_ = axisY;
}


void CS::Init( const Point& pos, double angle /*= 0.0*/ )
{
    pos_ = pos;
    axisX_.Init( ::cos(angle), ::sin(angle) );
    axisY_ = axisX_.CalcPerpendicular();
}


void CS::Transfer( const Vector& shift )
{
    pos_.Transfer( shift );
}


void CS::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    pos_.Scale( xCoef, yCoef, pCenter );
}


void CS::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    pos_.Rotate( angle, pCenter );
    //axisX_.Rotate( angle, pCenter );
    //axisY_.Rotate( angle, pCenter );
    axisX_.Rotate( angle );
    axisY_.Rotate( angle );
}


/*++
    Implementation of coordinate system transform by matrix.
    Notice that this method unable to transform coordinate system by scale matrix with different
    x and y axis scale factors correctly!
--*/
void CS::Transform( const Matrix3x3& m )
{
    pos_.Transform( m );
    axisX_.Transform( m );
    axisY_.Transform( m );
    axisX_.Normalize();
    axisY_.Normalize();

    assert( EQNil(axisX_.CalcDot(axisY_)) );
}


void CS::CalcMatrixFromCS( Matrix3x3& m ) const
{
    m.Init( axisX_.x_, axisX_.y_, 0.0,
        axisY_.x_, axisY_.y_, 0.0,
        pos_.x_, pos_.y_, 1.0 );
}


void CS::CalcMatrixIntoCS( Matrix3x3& m ) const
{
    m.Init( axisX_.x_, axisX_.y_, 0.0,
        axisY_.x_, axisY_.y_, 0.0,
        pos_.x_, pos_.y_, 1.0 );
    m.Invert();
}


bool CS::operator ==( const CS& rhs ) const
{
    return ( (pos_ == rhs.pos_) && (axisX_ == rhs.axisX_) && (axisY_ == rhs.axisY_) );
}


bool CS::operator !=( const CS& rhs ) const
{
    return ( !((*this) == rhs) );
}


std::ostream& operator <<( std::ostream& os, const CS& rhs )
{
    os << "(" << rhs.pos_ << ", x is " << rhs.axisX_ << ", x is " << rhs.axisY_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// R E C T   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Rect::Rect()
{
}


Rect::Rect( const Rect& rhs )
    : minPoint_( rhs.minPoint_ )
    , maxPoint_( rhs.maxPoint_ )
{
}


Rect::Rect( const Point& p1, const Point& p2 )
    : minPoint_( CalcMin(p1.x_, p2.x_), CalcMin(p1.y_, p2.y_) )
    , maxPoint_( CalcMax(p1.x_, p2.x_), CalcMax(p1.y_, p2.y_) )
{
}


Rect::Rect( double x1, double y1, double x2, double y2 )
    : minPoint_( CalcMin(x1, x2), CalcMin(y1, y2) )
    , maxPoint_( CalcMax(x1, x2), CalcMax(y1, y2) )
{
}


Rect::Rect( double width, double height )
    : minPoint_( CalcMin(width, 0.0), CalcMin(height, 0.0) )
    , maxPoint_( CalcMax(width, 0.0), CalcMax(height, 0.0) )
{
}


Rect& Rect::operator =( const Rect& rhs )
{
    minPoint_ = rhs.minPoint_;
    maxPoint_ = rhs.maxPoint_;
    return ( *this );
}


void Rect::Init( double x1, double y1, double x2, double y2 )
{
    minPoint_.Init( CalcMin(x1, x2), CalcMin(y1, y2) );
    maxPoint_.Init( CalcMax(x1, x2), CalcMax(y1, y2) );
}


void Rect::Init( double width, double height )
{
    Init( 0.0, 0.0, width, height );
}


void Rect::Transfer( const Vector& shift )
{
    minPoint_.Transfer( shift );
    maxPoint_.Transfer( shift );
}


void Rect::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    minPoint_.Scale( xCoef, yCoef, pCenter );
    maxPoint_.Scale( xCoef, yCoef, pCenter );
}


/*++
    Inflate rects both left and right sides by dWidth and both top and lower sides by dheight.
--*/
void Rect::Inflate( double dWidth, double dHeight )
{
    double x1 = minPoint_.x_ - dWidth;
    double y1 = minPoint_.y_ - dHeight;
    double x2 = maxPoint_.x_ + dWidth;
    double y2 = maxPoint_.y_ + dHeight;

    minPoint_.Init( CalcMin(x1, x2), CalcMin(y1, y2) );
    maxPoint_.Init( CalcMax(x1, x2), CalcMax(y1, y2) );
}


/*++
    Deflate rects both left and right sides by dWidth and both top and lower sides by dheight.
--*/
void Rect::Deflate( double dWidth, double dHeight )
{
    Inflate( -dWidth, -dHeight );
} 


double Rect::CalcWidth() const
{
    return ( maxPoint_.x_ - minPoint_.x_ );
}


double Rect::CalcHeight() const
{
    return ( maxPoint_.y_ - minPoint_.y_ );
}


double Rect::CalcArea() const
{
    return ( CalcWidth() * CalcHeight() );
}


double Rect::CalcAspectRatio() const
{
    return ( CalcWidth() / CalcHeight() );
}


Point Rect::CalcCenter() const
{
    return Point( minPoint_.x_ + CalcWidth() / 2.0, minPoint_.y_ + CalcHeight() / 2.0 );
}


bool Rect::IsInclude( const Point& rhs ) const
{
    return ( (rhs.x_ > minPoint_.x_) && (rhs.x_ < maxPoint_.x_) &&
        (rhs.y_ > minPoint_.y_) && (rhs.y_ < maxPoint_.y_) );
}


bool Rect::operator ==( const Rect& rhs ) const
{
    return ( (minPoint_ == rhs.minPoint_) && (maxPoint_ == rhs.maxPoint_) );
}


bool Rect::operator !=( const Rect& rhs ) const
{
    return ( !((*this) == rhs) );
}


std::ostream& operator <<( std::ostream& os, const Rect& rhs )
{
    os << "(" << rhs.minPoint_ << "-" << rhs.maxPoint_ << ")";
    return os;
}


//-------------------------------------------------------------------------------------------------
// B O U N D I N G   B O X   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


BoundingBox::BoundingBox()
{
    SetEmpty();
}


BoundingBox::BoundingBox( const BoundingBox& rhs )
    : rect_( rhs.rect_ )
{
}


BoundingBox& BoundingBox::operator =( const BoundingBox& rhs )
{
    rect_ = rhs.rect_;
    return ( *this );
}


void BoundingBox::SetEmpty()
{
    rect_.minPoint_.Init( 1.0, 1.0 );
    rect_.maxPoint_.Init( -1.0, -1.0 );
}


void BoundingBox::Add( const BoundingBox& rhs )
{
    Add( rhs.rect_.minPoint_ );
    Add( rhs.rect_.maxPoint_ );
}


void BoundingBox::Add( const Point& rhs )
{
    if ( (rect_.minPoint_.x_ > rect_.maxPoint_.x_) || (rect_.minPoint_.y_ > rect_.maxPoint_.y_) ) {
        rect_.minPoint_ = rhs;
        rect_.maxPoint_ = rhs;
    }

    if ( rhs.x_ < rect_.minPoint_.x_ ) {
        rect_.minPoint_.x_ = rhs.x_;
    }

    if ( rhs.y_ < rect_.minPoint_.y_ ) {
        rect_.minPoint_.y_ = rhs.y_;
    }

    if ( rhs.x_ > rect_.maxPoint_.x_ ) {
        rect_.maxPoint_.x_ = rhs.x_;
    }

    if ( rhs.y_ > rect_.maxPoint_.y_ ) {
        rect_.maxPoint_.y_ = rhs.y_;
    }
}


void BoundingBox::Transfer( const Vector& shift )
{
    rect_.Transfer( shift );
}


void BoundingBox::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    rect_.Scale( xCoef, yCoef, pCenter );
}


void BoundingBox::Inflate( double dWidth, double dHeight )
{
    rect_.Inflate( dWidth, dHeight );
}


void BoundingBox::Deflate( double dWidth, double dHeight )
{
    rect_.Deflate( dWidth, dHeight );
}


double BoundingBox::CalcWidth() const
{
    return rect_.CalcWidth();
}


double BoundingBox::CalcHeight() const
{
    return rect_.CalcHeight();
}


double BoundingBox::CalcArea() const
{
    return rect_.CalcArea();
}


double BoundingBox::CalcAspectRatio() const
{
    return rect_.CalcAspectRatio();
}


Point BoundingBox::CalcCenter() const
{
    return rect_.CalcCenter();
}


bool BoundingBox::IsEmpty() const
{
    return ( (rect_.minPoint_.x_ > rect_.maxPoint_.x_) ||
        (rect_.minPoint_.y_ > rect_.maxPoint_.y_) );
}


const Point& BoundingBox::GetMinPoint() const
{
    return rect_.minPoint_;
}


const Point& BoundingBox::GetMaxPoint() const
{
    return rect_.maxPoint_;
}


bool BoundingBox::operator ==( const BoundingBox& rhs ) const
{
    return ( rect_ != rhs.rect_ );
}


bool BoundingBox::operator !=( const BoundingBox& rhs ) const
{
    return ( rect_ != rhs.rect_ );
}


BoundingBox& operator <<( BoundingBox& lhs, const BoundingBox& rhs )
{
    lhs.Add( rhs );
    return lhs;
}


BoundingBox& operator <<( BoundingBox& lhs, const Point& rhs )
{
    lhs.Add( rhs );
    return lhs;
}


//-------------------------------------------------------------------------------------------------
// F A C T O R I A L   C A L C U L A T O R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


FactorialCalculator::FactorialCalculator( int precalcValuesCount /*= 100*/ )
    : precalcValuesCount_( precalcValuesCount )
    , pPrecalcValues_( new double [precalcValuesCount] )
{
    assert( precalcValuesCount >  0 );
    pPrecalcValues_[ 0 ] = 1.0;
    for ( int i = 1; i < precalcValuesCount_; ++i ) {
        pPrecalcValues_[ i ] = pPrecalcValues_[ i - 1 ] * i;
    }
}


FactorialCalculator::~FactorialCalculator()
{
    delete [] pPrecalcValues_;
}


double FactorialCalculator::Calc( int value ) const
{
    assert( value >= 0 );
    if ( value < precalcValuesCount_ ) {
        return pPrecalcValues_[ value ];
    } else {
        double result = pPrecalcValues_[ precalcValuesCount_ - 1 ];
        for ( int i = precalcValuesCount_; i <= value; ++i ) {
            result *= i;
        }
        return result;
    }
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d

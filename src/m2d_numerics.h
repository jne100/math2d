///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#ifndef _M2D_NUMERICS_h
#define _M2D_NUMERICS_h


#include <cassert>
#include <set>
#include <vector>
#include <m2d_curves.h>


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C U R V E   U T I L I T I E S
//-------------------------------------------------------------------------------------------------


/*++
    Calculation of two or more points uniformly distributed on specified curve.
--*/
void CalcUniformPointsOnCurve( const Curve& curve, int pointsCount, std::vector<double>& points,
    const Interval* pInterval = 0 );


/*++
    Calculation of two or more points uniformly distributed on specified curve.
--*/
void CalcUniformPointsOnCurve( const Curve& curve, int pointsCount, std::vector<Point>& points,
    const Interval* pInterval = 0 );


/*++
    Adaptive calculation of points on specified curve (da parameter represents maximum tilt angle
    of tangent directions in adjacent points).
--*/
void CalcPointsOnCurve( const Curve& curve, double da, std::vector<double>& points,
    const Interval* pInterval = 0 );


/*++
    Adaptive calculation of points on specified curve (da parameter represents maximum tilt angle
    of tangent directions in adjacent points).
--*/
void CalcPointsOnCurve( const Curve& curve, double da, std::vector<Point>& points,
    const Interval* pInterval = 0 );


//-------------------------------------------------------------------------------------------------
// T E S S E L A T I O N   U T I L I T I E S
//-------------------------------------------------------------------------------------------------


/*++
--*/
class SegmentDescription {
public:
    Point p1_;
    Point p2_;
};


/*++
--*/
class QuadraticBezierCurveDescription {
public:
    Point p1_;
    Point p2_;
    Point p3_;
};


/*++
    Calculation of approximated representation of mathematical curve using line segments.
--*/
void CalcCurveUniformTesselation( const Curve& curve, int segmentsCount,
    std::vector<SegmentDescription>& segments, const Interval* pInterval = 0 );


/*++
    Adaptive calculation of approximated representation of mathematical curve using line segments
    (da parameter represents maximum tilt angle of tangent directions in adjacent points).
--*/
void CalcCurveTesselation( const Curve& curve, double da,
    std::vector<SegmentDescription>& segments, const Interval* pInterval = 0 );


/*++
    Calculation of approximated representation of mathematical curve using quadratic Bezier
    curves.
--*/
void CalcCurveSmoothTesselation( const Curve& curve, int beziersCount,
    std::vector<QuadraticBezierCurveDescription>& beziers,
    const Interval* pInterval = 0 );


/*++
    Calculation of smooth representation of polyline using quadratic Bezier curves.
--*/
void CalcPolylineClosedSmoothTesselation( const Polyline& polyline,
    std::vector<QuadraticBezierCurveDescription>& beziers );


//-------------------------------------------------------------------------------------------------
// I N T E R S E C T I O N   U T I L I T I E S
//-------------------------------------------------------------------------------------------------


class ICurvesIntersectionFunctor;


/*++
    Calculation of intersection point parametric values of two lines each represented by one
    point and direction vector.
--*/
bool CalcLinesIntersectionPoint( const Point& p1, const Vector& v1, const Point& p2,
    const Vector& v2, double& t1, double& t2 );


/*++
    Calculation of intersection point of two lines each represented by one point and direction
    vector.
--*/
bool CalcLinesIntersectionPoint( const Point& p1, const Vector& v1, const Point& p2,
    const Vector& v2, Point& result );


/*++
    Calculation of all intersection points of two curves inside parametric intervals.
--*/
void CalcCurvesIntersectionPoints( const Curve& curve1, const Curve& curve2,
    std::vector<Pair>& points, const Interval* pInterval1 = 0, const Interval* pInterval2 = 0 );


/*++
    Calculation of all intersection points of two curves inside parametric intervals.
--*/
void CalcCurvesIntersectionPoints( const Curve& curve1, const Curve& curve2,
    std::vector<Point>& points, const Interval* pInterval1 = 0,
    const Interval* pInterval2 = 0 );


/*++
    Calculation of all intersection points of two curves with the use of specified functor.
--*/
void CalcCurvesIntersectionPoints( ICurvesIntersectionFunctor& functor, const Curve& curve1,
    const Curve& curve2, std::vector<Pair>& points, const Interval* pInterval1 = 0,
    const Interval* pInterval2 = 0 );


/*++
    Calculation of all intersection points of two curves with the use of specified functor.
--*/
void CalcCurvesIntersectionPoints( ICurvesIntersectionFunctor& functor, const Curve& curve1,
    const Curve& curve2, std::vector<Point>& points, const Interval* pInterval1 = 0,
    const Interval* pInterval2 = 0 );


//-------------------------------------------------------------------------------------------------
// P R O J E C T I O N   U T I L I T I E S
//-------------------------------------------------------------------------------------------------


/*++
    Calculation of projection of vector src on vector dest.
--*/
void CalcVectorProjection( const Vector& src, const Vector& dest, Vector& result );


/*++
    Calculation of projection length of vector src on vector dest.
--*/
double CalcVectorProjectionLength( const Vector& src, const Vector& dest );


//-------------------------------------------------------------------------------------------------
// I   C U R V E S   I N T E R S E C T I O N   F U N C T O R
//-------------------------------------------------------------------------------------------------


/*++
    Interface aimed calculation of intersection points of two curves.
--*/
class ICurvesIntersectionFunctor {
public:
    virtual ~ICurvesIntersectionFunctor() {}
    virtual void CalcIntersections( const Curve& c1, const Curve& c2, std::vector<Pair>& points,
        const Interval* pI1, const Interval* pI2 ) = 0;
};


//-------------------------------------------------------------------------------------------------
// C U R V E S   I N T E R S E C T I O N   F U N C T O R
//-------------------------------------------------------------------------------------------------


/*++
    Class aimed for serial calculation of intersection points of two curves with the help of
    iterative methods (null approximation) and solution of polynomial equations system by Newton's
    method (exact intersection).
--*/
class CurvesIntersectionFunctor : public ICurvesIntersectionFunctor {
public:
    CurvesIntersectionFunctor();
    virtual ~CurvesIntersectionFunctor() {}
    virtual void CalcIntersections( const Curve& c1, const Curve& c2, std::vector<Pair>& points,
        const Interval* pI1, const Interval* pI2 );

private:
    void CalcTiltStepsOnCurve( const Curve& c, const Interval& span,
        std::vector<double>& steps ) const;
    bool CalcNullApproximation( Pair& tNull, Pair& tDefect ) const;
    bool CalcExactIntersection( const Pair& tNull, const Pair& tDefect, Pair& tExact ) const;

public:
    static const double DEFAULT_DA;
    double da_;
    double metricEps_;

private:
    const Curve* pC1_;
    const Curve* pC2_;
    Pair t_;
    Pair dt_;
    std::set<Pair> pointsSet_;
};


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


#endif

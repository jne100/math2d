///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#include "m2d_numerics.h"


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C U R V E   U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


void CalcUniformPointsOnCurve( const Curve& curve, int pointsCount, std::vector<double>& points,
    const Interval* pInterval /*= 0*/ )
{
    if ( pointsCount > 1 ) {

        const Interval& interval =
            (pInterval) ? (*pInterval) : curve.GetParametricInterval();
        double tStep = ( interval.CalcLength() / (pointsCount - 1) );

        points.reserve( points.size() + pointsCount );
        for ( int i = 0; i < pointsCount; ++i ) {
            points.push_back( interval.begin_ + i * tStep );
        }
    }
}


void CalcUniformPointsOnCurve( const Curve& curve, int pointsCount, std::vector<Point>& points,
    const Interval* pInterval /*= 0*/ )
{
    std::vector<double> tempPoints;
    CalcUniformPointsOnCurve( curve, pointsCount, tempPoints, pInterval );

    points.reserve( points.size() + tempPoints.size() );
    for ( size_t i = 0; i < tempPoints.size(); ++i ) {
        points.push_back( curve.CalcPointOnCurve(tempPoints[i]) );
    }
}


void CalcPointsOnCurve( const Curve& curve, double da, std::vector<double>& points,
                       const Interval* pInterval /*= 0*/ )
{
    if ( GT(da, 0.0) ) {

        const Interval& span =
            (pInterval) ? (*pInterval) : curve.GetParametricInterval();

        points.push_back( span.begin_ );

        bool cut = false;
        for ( double t = span.begin_; !cut; ) {
            t += curve.CalcTiltStep( t, da, span, cut );
            points.push_back( t );
        }
    }
}


void CalcPointsOnCurve( const Curve& curve, double da, std::vector<Point>& points,
    const Interval* pInterval /*= 0*/ )
{
    std::vector<double> tempPoints;
    CalcPointsOnCurve( curve, da, tempPoints, pInterval );

    points.reserve( points.size() + tempPoints.size() );
    for ( size_t i = 0; i < tempPoints.size(); ++i ) {
        points.push_back( curve.CalcPointOnCurve(tempPoints[i]) );
    }
}


//-------------------------------------------------------------------------------------------------
// T E S S E L A T I O N   U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


void CalcCurveUniformTesselation( const Curve& curve, int segmentsCount,
    std::vector<SegmentDescription>& segments, const Interval* pInterval /*= 0*/ )
{
    if ( segmentsCount >= 1 ) {

        int pointsCount = ( segmentsCount + 1 );
        std::vector<double> points;
        CalcUniformPointsOnCurve( curve, pointsCount, points, pInterval );

        SegmentDescription curSegment;
        curSegment.p1_ = curve.CalcPointOnCurve( points[0] );

        segments.reserve( segments.size() + segmentsCount );
        for ( int i = 1; i < pointsCount; ++i ) {
            curSegment.p2_ = curve.CalcPointOnCurve( points[i] );
            segments.push_back( curSegment );
            curSegment.p1_ = curSegment.p2_;
        }
    }
}


void CalcCurveTesselation( const Curve& curve, double da,
    std::vector<SegmentDescription>& segments, const Interval* pInterval /*= 0*/ ) 
{
    std::vector<double> points;
    CalcPointsOnCurve( curve, da, points, pInterval );

    if ( points.size() > 1 ) {

        SegmentDescription curSegment;
        curSegment.p1_ = curve.CalcPointOnCurve( points[0] );

        int pointsCount = static_cast<int>( points.size() );
        segments.reserve( segments.size() + pointsCount - 1 );

        for ( int i = 1; i < pointsCount; ++i ) {
            curSegment.p2_ = curve.CalcPointOnCurve( points[i] );
            segments.push_back( curSegment );
            curSegment.p1_ = curSegment.p2_;
        }
    }
}


void CalcCurveSmoothTesselation( const Curve& curve, int beziersCount,
    std::vector<QuadraticBezierCurveDescription>& beziers,
    const Interval* pInterval /*= 0*/ )
{
    if ( beziersCount > 1 ) {

        int pointsCount = ( beziersCount + 1 );
        std::vector<double> points;
        CalcUniformPointsOnCurve( curve, pointsCount, points, pInterval );

        QuadraticBezierCurveDescription bezier;
        bezier.p1_ = curve.CalcPointOnCurve( points[0] );
        Vector prevDer = curve.CalcFirstDerivative( points[0] );
        Vector curDer;

        beziers.reserve( beziers.size() + beziersCount );
        for ( int i = 1; i < pointsCount; ++i ) {

            bezier.p3_ = curve.CalcPointOnCurve( points[i] );
            curDer = curve.CalcFirstDerivative( points[i] );

            if ( !CalcLinesIntersectionPoint(bezier.p1_, prevDer, bezier.p3_, curDer, bezier.p2_) ) {
                bezier.p2_.Init( (bezier.p1_.x_ + bezier.p3_.x_) / 2.0,
                    (bezier.p1_.y_ + bezier.p3_.y_) / 2.0 );
            }

            beziers.push_back( bezier );
            bezier.p1_ = bezier.p3_;
            prevDer = curDer;
        }
    }
}


void CalcPolylineClosedSmoothTesselation( const Polyline& polyline,
    std::vector<QuadraticBezierCurveDescription>& beziers )
{
    int verticesCount = polyline.GetVerticesCount();
    if ( verticesCount > 2 ) {

        QuadraticBezierCurveDescription tempBezier;
        const Point* pPrevPoint = 0;
        const Point* pNextPoint = 0;

        for ( int i = 0; i < verticesCount; ++i ) {

            // calculate second point of current bezier segment
            tempBezier.p2_ = polyline.GetVertex( i );

            // calculate first and third points of current bezier segment
            if ( i == 0 ) {
                pPrevPoint = &polyline.GetVertex( verticesCount - 1 );
                pNextPoint = &polyline.GetVertex( i + 1 );
            } else if ( i == (verticesCount - 1) ) {
                pPrevPoint = &polyline.GetVertex( i - 1 );
                pNextPoint = &polyline.GetVertex( 0 );
            } else {
                pPrevPoint = &polyline.GetVertex( i - 1 );
                pNextPoint = &polyline.GetVertex( i + 1 );
            }
            tempBezier.p1_ = CalcMiddlePoint( *pPrevPoint, tempBezier.p2_ );
            tempBezier.p3_ = CalcMiddlePoint( tempBezier.p2_, *pNextPoint );

            beziers.push_back( tempBezier );
        }
    }
}


//-------------------------------------------------------------------------------------------------
// I N T E R S E C T I O N   U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


/*++
    t1 = ((x2 - a2) * (y1 - y2) - (x1 - x2) * (y2 - b2)) /
        ((x2 - a2) * (y1 - b1) - (x1 - a1) * (y2 - b2))
    t2 = ((x1 - a1) * (y1 - y2) - (x1 - x2) * (y1 - b1)) /
        ((x2 - a2) * (y1 - b1) - (x1 - a1) * (y2 - b2))
    (a1, b1) is (p1 + v1)
    (a2, b2) is (p2 + v2)
--*/
bool CalcLinesIntersectionPoint( const Point& p1, const Vector& v1, const Point& p2,
    const Vector& v2, double& t1, double& t2 )
{
    if ( !v1.IsCollinear(v2) ) {
        double x1_x2 = ( p1.x_ - p2.x_ );
        double y1_y2 = ( p1.y_ - p2.y_ );
        double x1_a1 = ( p1.x_ - (p1.x_ + v1.x_) );
        double y1_b1 = ( p1.y_ - (p1.y_ + v1.y_) );
        double x2_a2 = ( p2.x_ - (p2.x_ + v2.x_) );
        double y2_b2 = ( p2.y_ - (p2.y_ + v2.y_) );
        double div = ( x2_a2 * y1_b1 - x1_a1 * y2_b2 );
        t1 = ( x2_a2 * y1_y2 - x1_x2 * y2_b2 ) / div;
        t2 = ( x1_a1 * y1_y2 - x1_x2 * y1_b1 ) / div;
        return true;

    } else {
        return false;
    }
}


/*++
    x = ((x1 * b1 - y1 * a1) * (x2 - a2) - (x1 - a1) * (x2 * b2 - y2 * a2)) /
        ((x1 - a1) * (y2 - b2) - (y1 - b1) * (x2 - a2))
    y = ((x1 * b1 - y1 * a1) * (y2 - b2) - (y1 - b1) * (x2 * b2 - y2 * a2)) /
        ((x1 - a1) * (y2 - b2) - (y1 - b1) * (x2 - a2))
    (a1, b1) is p1 + v1
    (a2, b2) is p2 + v2
--*/
bool CalcLinesIntersectionPoint( const Point& p1, const Vector& v1, const Point& p2,
    const Vector& v2, Point& result )
{
    if ( !v1.IsCollinear(v2) ) {
        double a1 = ( p1.x_ + v1.x_ );
        double b1 = ( p1.y_ + v1.y_ );
        double a2 = ( p2.x_ + v2.x_ );
        double b2 = ( p2.y_ + v2.y_ );
        double x1_a1 = ( p1.x_ - a1 );
        double y1_b1 = ( p1.y_ - b1 );
        double x2_a2 = ( p2.x_ - a2 );
        double y2_b2 = ( p2.y_ - b2 );
        double term1 = ( p1.x_ * b1 - p1.y_ * a1 );
        double term2 = ( p2.x_ * b2 - p2.y_ * a2 );
        double div = ( x1_a1 * y2_b2 - y1_b1 * x2_a2 );
        result.x_ = ( term1 * x2_a2 - x1_a1 * term2 ) / div;
        result.y_ = ( term1 * y2_b2 - y1_b1 * term2 ) / div;
        return true;

    } else {
        return false;
    }
}


void CalcCurvesIntersectionPoints( const Curve& curve1, const Curve& curve2,
    std::vector<Pair>& points, const Interval* pInterval1 /*= 0*/,
    const Interval* pInterval2 /*= 0*/ )
{
    CurvesIntersectionFunctor functor;
    functor.CalcIntersections( curve1, curve2, points, pInterval1, pInterval2 );
}


void CalcCurvesIntersectionPoints( const Curve& curve1, const Curve& curve2,
    std::vector<Point>& points, const Interval* pInterval1 /*= 0*/,
    const Interval* pInterval2 /*= 0*/ )
{
    std::vector<Pair> tempPoints;
    CalcCurvesIntersectionPoints( curve1, curve2, tempPoints, pInterval1, pInterval2 );

    points.reserve( points.size() + tempPoints.size() );
    for ( size_t i = 0; i < tempPoints.size(); ++i ) {
        points.push_back( curve1.CalcPointOnCurve(tempPoints[i].v1_) );
    }
}


void CalcCurvesIntersectionPoints( ICurvesIntersectionFunctor& functor, const Curve& curve1,
    const Curve& curve2, std::vector<Pair>& points, const Interval* pInterval1 /*= 0*/,
    const Interval* pInterval2 /*= 0*/ )
{
    functor.CalcIntersections( curve1, curve2, points, pInterval1, pInterval2 );
}


void CalcCurvesIntersectionPoints( ICurvesIntersectionFunctor& functor, const Curve& curve1,
    const Curve& curve2, std::vector<Point>& points, const Interval* pInterval1 /*= 0*/,
    const Interval* pInterval2 /*= 0*/ )
{
    std::vector<Pair> tempPoints;
    CalcCurvesIntersectionPoints( functor, curve1, curve2, tempPoints, pInterval1, pInterval2 );

    points.reserve( points.size() + tempPoints.size() );
    for ( size_t i = 0; i < tempPoints.size(); ++i ) {
        points.push_back( curve1.CalcPointOnCurve(tempPoints[i].v1_) );
    }
}


//-------------------------------------------------------------------------------------------------
// P R O J E C T I O N   U T I L I T I E S   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


void CalcVectorProjection( const Vector& src, const Vector& dest, Vector& result )
{
    double destLength = dest.CalcLength();
    if ( NENil(destLength) ) {
        double resultLength = ( ::cos(src.CalcAngle(dest)) * src.CalcLength() );
        result.x_ = ( dest.x_ * resultLength ) / destLength;
        result.y_ = ( dest.y_ * resultLength ) / destLength;
    } else {
        result.x_ = 0.0;
        result.y_ = 0.0;
    }
}


double CalcVectorProjectionLength( const Vector& src, const Vector& dest )
{
    if ( EQNil(dest.x_) && EQNil(dest.y_) ) {
        return 0.0;
    } else {
        return ::fabs( ::cos(src.CalcAngle(dest)) * src.CalcLength() );
    }
}


//-------------------------------------------------------------------------------------------------
// C U R V E S   I N T E R S E C T I O N   F U N C T O R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


CurvesIntersectionFunctor::CurvesIntersectionFunctor()
    : da_( DEFAULT_DA )
    , metricEps_( DOUBLE_PRECISION )
    , pC1_( 0 )
    , pC2_( 0 )
{
}


void CurvesIntersectionFunctor::CalcIntersections( const Curve& c1, const Curve& c2,
    std::vector<Pair>& points, const Interval* pI1, const Interval* pI2 )
{
    // prepare
    pC1_ = &c1;
    pC2_ = &c2;

    const Interval& i1 = ( pI1 ? *pI1 : c1.GetParametricInterval() );
    const Interval& i2 = ( pI2 ? *pI2 : c2.GetParametricInterval() );
    Pair tNull;
    Pair tDefect;
    Pair tExact;

    // calculation of first curve parametric steps
    std::vector<double> dts1;
    CalcTiltStepsOnCurve( c1, i1, dts1 );

    // calculation of second curve parametric steps
    std::vector<double> dts2;
    CalcTiltStepsOnCurve( c2, i2, dts2 );

    t_.v1_ = i1.begin_;
    for ( size_t nDt1 = 0, s1 = dts1.size(); nDt1 < s1; ++nDt1 ) {

        dt_.v1_ = dts1[ nDt1 ];

        t_.v2_ = i2.begin_;
        for ( size_t nDt2 = 0, s2 = dts2.size(); nDt2 < s2; ++nDt2 ) {

            dt_.v2_ = dts2[ nDt2 ];

            // calculation of null approximation
            if ( CalcNullApproximation(tNull, tDefect) &&
                IsInsideClosedInterval(tNull.v1_, -tDefect.v1_, (dt_.v1_ + tDefect.v1_)) &&
                IsInsideClosedInterval(tNull.v2_, -tDefect.v2_, (dt_.v2_ + tDefect.v2_)) ) {

                // calculation of exact intersection
                tNull += t_;
                if ( CalcExactIntersection(tNull, tDefect, tExact) &&
                    IsInsideClosedInterval(tNull.v1_, t_.v1_, (t_.v1_ + dt_.v1_)) &&
                    IsInsideClosedInterval(tNull.v2_, t_.v2_, (t_.v2_ + dt_.v2_)) ) {

                    if ( pointsSet_.find(tExact) == pointsSet_.end() ) {
                        points.push_back( tExact );
                        pointsSet_.insert( points.back() );
                    }
                }
            }

            t_.v2_ += dt_.v2_;
        }

        t_.v1_ += dt_.v1_;
    }

    // cleanup
    pC1_ = 0;
    pC2_ = 0;
    pointsSet_.clear();
}


void CurvesIntersectionFunctor::CalcTiltStepsOnCurve( const Curve& c, const Interval& span,
    std::vector<double>& steps ) const
{
    if ( GT(da_, 0.0) ) {
        bool cut = false;
        for ( double t = span.begin_; !cut; ) {
            steps.push_back( c.CalcTiltStep(t, da_, span, cut) );
            t += steps.back();
        }
    }
}


bool CurvesIntersectionFunctor::CalcNullApproximation( Pair& tNull, Pair& tDefect ) const
{
    // construction of first and second lines (starting point and tangent)
    Point p1 = pC1_->CalcPointOnCurve( t_.v1_ );
    Point p2 = pC2_->CalcPointOnCurve( t_.v2_ );
    Vector v1 = pC1_->CalcFirstDerivative( t_.v1_ );
    Vector v2 = pC2_->CalcFirstDerivative( t_.v2_ );
    double v1Length = v1.CalcLength();
    double v2Length = v2.CalcLength();

    // normalization of tangents
    Point nextP1 = pC1_->CalcPointOnCurve( t_.v1_ + dt_.v1_ );
    Point nextP2 = pC2_->CalcPointOnCurve( t_.v2_ + dt_.v2_ );
    v1 *= ( CalcDistance(p1, nextP1) / v1Length );
    v2 *= ( CalcDistance(p2, nextP2) / v2Length );

    // intersection of constructed lines
    if ( CalcLinesIntersectionPoint(p1, v1, p2, v2, tNull.v1_, tNull.v2_) ) {

        tNull.v1_ = Normalize( tNull.v1_, 1.0, dt_.v1_ );
        tNull.v2_ = Normalize( tNull.v2_, 1.0, dt_.v2_ );

        // Some parts of intersection algorithm work incorrectly at the present moment. Deviation
        // caused by approximation have to be considered in a some way, but there are some
        // problems with this issue. Mainly, obvious implementations are quite resource-intensive.
        tDefect.Init( 0.0, 0.0 );

        return true;

    } else {
        return false;
    }
}


/*++
    Solution of polynomial equations system by Newton's method.
    t|n + 1| = t|n| - (f'(t)|n|^-1 * f(t)|n|),
    f(t) = |x1(t1) - x2(t2) = 0|
           |y1(t1) - y2(t2) = 0|,
    f'(t) = |x1'(t1), -x2(t2)|
            |y1'(t1), -y2(t2)|.
--*/
bool CurvesIntersectionFunctor::CalcExactIntersection( const Pair& tNull, const Pair& tDefect,
    Pair& tExact ) const
{
    const int MAX_ITER_COUNT = 0x20;

    // convergence limits
    Pair t( t_.v1_ - tDefect.v1_, t_.v2_ - tDefect.v2_ );
    Pair tEnd( t_.v1_ + dt_.v1_ + tDefect.v1_, t_.v2_ + dt_.v2_ + tDefect.v2_ );

    // points and first derivatives on current iteration step
    Point p1;
    Point p2;
    Vector v1;
    Vector v2;

    // points on next iteration step
    Point nextP1;
    Point nextP2;

    // matrices for equations system solution
    Matrix2x1 ts( tNull.v1_, tNull.v2_ );
    Matrix2x2 a;
    Matrix2x1 b;
    Matrix2x1 c;

    bool result = false;
    bool t1Found = false;
    bool t2Found = false;
    int iterCount = 0;

    while ( IsInsideClosedInterval(ts.m_[0][0], t.v1_, tEnd.v1_) &&
        IsInsideClosedInterval(ts.m_[1][0], t.v2_, tEnd.v2_) ) {

        if ( !t1Found ) {
            p1 = pC1_->CalcPointOnCurve( ts.m_[0][0] );
            v1 = pC1_->CalcFirstDerivative( ts.m_[0][0] );
        }
        if ( !t2Found ) {
            p2 = pC2_->CalcPointOnCurve( ts.m_[1][0] );
            v2 = pC2_->CalcFirstDerivative( ts.m_[1][0] );
        }

        a.Init( v1.x_, -v2.x_, v1.y_, -v2.y_ );
        a.Invert();
        b.Init( p1.x_ - p2.x_, p1.y_ - p2.y_ );
        c = a * b;

        if ( !t1Found ) {
            ts.m_[0][0] -= c.m_[0][0];
            nextP1 = pC1_->CalcPointOnCurve( ts.m_[0][0] );
            t1Found = EQ( p1.x_, nextP1.x_, metricEps_ ) && EQ( p1.y_, nextP1.y_, metricEps_ );
        }
        if ( !t2Found ) {
            ts.m_[1][0] -= c.m_[1][0];
            nextP2 = pC2_->CalcPointOnCurve( ts.m_[1][0] );
            t2Found = EQ( p2.x_, nextP2.x_, metricEps_ ) && EQ( p2.y_, nextP2.y_, metricEps_ );
        }

        if ( t1Found && t2Found ) {
            tExact.v1_ = ts.m_[0][0];
            tExact.v2_ = ts.m_[1][0];
            result = true;
            break;
        }

        // consider iterations count limit
        if ( (++iterCount) >= MAX_ITER_COUNT ) {
            break;
        }
    }

    return result;
}


const double CurvesIntersectionFunctor::DEFAULT_DA = Radians( 5.0 );


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d

///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#include "m2d_visitors.h"


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C O L L E C T   E D I T I N G   P O I N T S   V I S I T O R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


void CollectEditingPointsVisitor::Visit( Segment& curve )
{
    editingPoints_.push_back( curve.GetP1() );
    editingPoints_.push_back( curve.GetP2() );
}


void CollectEditingPointsVisitor::Visit( Arc& curve )
{
    editingPoints_.push_back( curve.GetCS().pos_ );
}


void CollectEditingPointsVisitor::Visit( ArchimedianSpiral& curve )
{
    editingPoints_.push_back( curve.GetCS().pos_ );
}


void CollectEditingPointsVisitor::Visit( LogarithmicSpiral& curve )
{
    editingPoints_.push_back( curve.GetCS().pos_ );
}


void CollectEditingPointsVisitor::Visit( Polyline& curve )
{
    for ( int i = 0, c = curve.GetVerticesCount(); i < c; ++i ) {
        editingPoints_.push_back( curve.GetVertex(i) );
    }
}


void CollectEditingPointsVisitor::Visit( BezierCurve& curve )
{
    for ( int i = 0, c = curve.GetControlVerticesCount(); i < c; ++i ) {
        editingPoints_.push_back( curve.GetControlVertex(i) );
    }
}


void CollectEditingPointsVisitor::Visit( HermiteSpline& curve )
{
    for ( int i = 0, c = curve.GetVerticesCount(); i < c; ++i ) {
        editingPoints_.push_back( curve.GetVertex(i) );
    }
}


void CollectEditingPointsVisitor::Visit( EquidistantCurve& /*curve*/ )
{
}


void CollectEditingPointsVisitor::Visit( ProxyCurve& /*curve*/ )
{
}


//-------------------------------------------------------------------------------------------------
// C H A N G E   E D I T I N G   P O I N T   V I S I T O R   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


ChangeEditingPointVisitor::ChangeEditingPointVisitor( int editingPointIndex,
    const Matrix3x3& transform )
    : editingPointIndex_( editingPointIndex )
    , transform_( transform )
{
}


ChangeEditingPointVisitor::ChangeEditingPointVisitor( int editingPointIndex,
    const Vector& shift )
    : editingPointIndex_( editingPointIndex )
{
    transform_.InitTransferMatrix( shift );
}


void ChangeEditingPointVisitor::Visit( Segment& curve )
{
    assert( editingPointIndex_ < 2 );
    if ( editingPointIndex_ < 2 ) {
        Point editingPoint = ( editingPointIndex_ == 0 ) ? ( curve.GetP1() ) : ( curve.GetP2() );
        editingPoint.Transform( transform_ );
        ( editingPointIndex_ == 0 ) ? ( curve.SetP1(editingPoint) ) : ( curve.SetP2
            (editingPoint) );
    }
}


void ChangeEditingPointVisitor::Visit( Arc& curve )
{
    assert( editingPointIndex_ == 0 );
    if ( editingPointIndex_ == 0 ) {
        Point editingPoint = curve.GetCenter();
        editingPoint.Transform( transform_ );
        curve.SetCenter( editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( ArchimedianSpiral& curve )
{
    assert( editingPointIndex_ == 0 );
    if ( editingPointIndex_ == 0 ) {
        Point editingPoint = curve.GetCenter();
        editingPoint.Transform( transform_ );
        curve.SetCenter( editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( LogarithmicSpiral& curve )
{
    assert( editingPointIndex_ == 0 );
    if ( editingPointIndex_ == 0 ) {
        Point editingPoint = curve.GetCenter();
        editingPoint.Transform( transform_ );
        curve.SetCenter( editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( Polyline& curve )
{
    assert( editingPointIndex_ < curve.GetVerticesCount() );
    if ( editingPointIndex_ < curve.GetVerticesCount() ) {
        Point editingPoint = curve.GetVertex( editingPointIndex_ );
        editingPoint.Transform( transform_ );
        curve.SetVertex( editingPointIndex_, editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( BezierCurve& curve )
{
    assert( editingPointIndex_ < curve.GetControlVerticesCount() );
    if ( editingPointIndex_ < curve.GetControlVerticesCount() ) {
        Point editingPoint = curve.GetControlVertex( editingPointIndex_ );
        editingPoint.Transform( transform_ );
        curve.SetControlVertex( editingPointIndex_, editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( HermiteSpline& curve )
{
    assert( editingPointIndex_ < curve.GetVerticesCount() );
    if ( editingPointIndex_ < curve.GetVerticesCount() ) {
        Point editingPoint = curve.GetVertex( editingPointIndex_ );
        editingPoint.Transform( transform_ );
        curve.SetVertex( editingPointIndex_, editingPoint );
    }
}


void ChangeEditingPointVisitor::Visit( EquidistantCurve& /*curve*/ )
{
    assert( 0 );
}


void ChangeEditingPointVisitor::Visit( ProxyCurve& /*curve*/ )
{
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d

///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#ifndef _M2D_CURVES_h
#define _M2D_CURVES_h


#include <cassert>
#include <ostream>
#include <vector>
#include <m2d.h>


namespace m2d {


//-------------------------------------------------------------------------------------------------
// R E F   E N T I T Y
//-------------------------------------------------------------------------------------------------


/*++
    Base class of entities with reference counting.
--*/
class ReferenceEntity {
public:
    ReferenceEntity();
    virtual ~ReferenceEntity();

    friend void AddReference( const ReferenceEntity* pEntity );
    friend void ReleaseReference( const ReferenceEntity* pEntity );

private:
    void InternalAddReference() const;
    void InternalReleaseReference() const;

private:
    mutable int referenceCount_;
};


inline ReferenceEntity::ReferenceEntity()
    : referenceCount_( 0 )
{
}


inline ReferenceEntity::~ReferenceEntity()
{
    assert( referenceCount_ == 0 );
}


inline void ReferenceEntity::InternalAddReference() const
{
    ++referenceCount_;
}


inline void ReferenceEntity::InternalReleaseReference() const
{
    --referenceCount_;
    assert( referenceCount_ >= 0 );
}


inline void AddReference( const ReferenceEntity* pEntity )
{
    assert( pEntity != 0 );
    pEntity->InternalAddReference();
}


inline void ReleaseReference( const ReferenceEntity* pEntity )
{
    assert( pEntity != 0 );
    pEntity->InternalReleaseReference();
    if ( pEntity->referenceCount_ == 0 ) {
        delete pEntity;
    }
}


inline void SafeAddReference( const ReferenceEntity* pEntity )
{
    if ( pEntity != 0 ) {
        AddReference( pEntity );
    }
}


inline void SafeReleaseReference( const ReferenceEntity* pEntity )
{
    if ( pEntity != 0 ) {
        ReleaseReference( pEntity );
    }
}


//-------------------------------------------------------------------------------------------------
// C U R V E
//-------------------------------------------------------------------------------------------------


class CurveVisitor;


enum ECurveTypes {
    CT_SEGMENT = 0,
    CT_ARC,
    CT_ARCHIMEDIAN_SPIRAL,
    CT_LOGARITHMIC_SPIRAL,
    CT_POLYLINE,
    CT_BEZIER_CURVE,
    CT_HERMITE_SPLINE,
    CT_EQUIDISTANT_CURVE,
    CT_PROXY_CURVE,
    CT_COUNT,
};


enum ECurveCharacterPoints {
    CHP_BEGIN,
    CHP_MIDDLE,
    CHP_END,
};


/*++
    Base class of all mathematical curves.
--*/
class Curve : public ReferenceEntity {
public:
    virtual ~Curve() {}
    virtual Curve* Clone() const = 0;
    virtual void Accept( CurveVisitor& visitor ) = 0;

    //  accessors
    virtual ECurveTypes GetType() const = 0;
    virtual const Interval& GetParametricInterval() const = 0;

    // calculations
    virtual Point CalcCharacterPoint( ECurveCharacterPoints characterPoint ) const;
    virtual Point CalcPointOnCurve( double t ) const = 0;
    virtual Vector CalcFirstDerivative( double t ) const = 0;
    virtual Vector CalcSecondDerivative( double t ) const = 0;
    virtual Vector CalcNormal( double t, bool direction = true ) const;
    virtual double CalcDistanceToPoint( const Point& p ) const;
    virtual double CalcPointProjection( const Point& p ) const;
    virtual double CalcLength() const;
    virtual void CalcBoundingBox( BoundingBox& box ) const;
    virtual double CalcCurvature( double t ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift ) = 0;
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 ) = 0;
    virtual void Rotate( double angle, const Point* pCenter = 0 ) = 0;
    virtual void Transform( const Matrix3x3& m ) = 0;
};


//-------------------------------------------------------------------------------------------------
// A N A L Y T I C A L   C U R V E
//-------------------------------------------------------------------------------------------------


/*++
    Base class of mathematical analytical curve.
    Curve called analytical if curves coordinatees in some coordinate system can be described with
    the help of analytical equations with no use of points, vectors or other curves.
--*/
class AnalyticalCurve : public Curve {
public:
    AnalyticalCurve();
    AnalyticalCurve( const AnalyticalCurve& rhs );
    AnalyticalCurve( const CS& cs );
    AnalyticalCurve( const Point& pos, double angle = 0.0 );
    virtual ~AnalyticalCurve() {}
    AnalyticalCurve& operator =( const AnalyticalCurve& rhs );

    // specific methods
    const CS& GetCS() const;
    void GetCS( const CS& cs );
    const Point& GetCenter() const;
    void SetCenter( const Point& p );

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

protected:
    CS cs_;
};


//-------------------------------------------------------------------------------------------------
// S E G M E N T
//-------------------------------------------------------------------------------------------------


/*++
    Line segment (mathematical curve).
--*/
class Segment : public Curve {
public:
    Segment();
    Segment( const Point& p1, const Point& p2 );
    Segment( const Segment& rhs );
    virtual ~Segment() {}
    Segment& operator =( const Segment& rhs );
    virtual Segment* Clone() const { return new Segment( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    const Point& GetP1() const;
    const Point& GetP2() const;
    void SetP1( const Point& p );
    void SetP2( const Point& p );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcPointProjection( const Point& p ) const;
    virtual double CalcLength() const;
    virtual void CalcBoundingBox( BoundingBox& box ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

private:
    static const Interval INTERVAL;

private:
    Point p1_;
    Point p2_;
};


//-------------------------------------------------------------------------------------------------
// A R C
//-------------------------------------------------------------------------------------------------


/*++
    Arc (mathematical curve).
--*/
class Arc : public AnalyticalCurve {
public:
    Arc( double radius, const Interval* pInterval = 0 );
    Arc( double radius, const Point& center, const Interval* pInterval = 0 );
    Arc( double radius, const CS& cs, const Interval* pInterval = 0 );
    Arc( const Point& p1, const Point& p2, const Point& p3, bool isClosed = false );
    Arc( const Arc& rhs );
    virtual ~Arc() {}
    Arc& operator =( const Arc& rhs );
    virtual Arc* Clone() const { return new Arc( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // initialization methods
    void Reset();
    void Init( double radius, const Interval* pInterval = 0 );
    void Init( double radius, const Point& center, const Interval* pInterval = 0 );
    void Init( double radius, const CS& cs, const Interval* pInterval = 0 );
    void Init( const Point& p1, const Point& p2, const Point& p3, bool isClosed = false );

    // specific methods
    double GetRadius() const;
    bool IsClosed() const;
    void SetRadius( double radius );
    void SetClosed();
    void SetSector( double start, double end );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcLength() const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

private:
    bool CalcCenterByPoints( const Point& p1, const Point& p2, const Point& p3,
        Point& result ) const;

private:
    static const Interval CIRCLE_INTERVAL;

private:
    double radius_;
    Interval interval_;
};


//-------------------------------------------------------------------------------------------------
// S P I R A L
//-------------------------------------------------------------------------------------------------


/*++
    Base class of spirals.
--*/
class Spiral : public AnalyticalCurve {
public:
    Spiral( const CS& cs, double radius, double angle );
    Spiral( const Spiral& rhs );
    virtual ~Spiral() {}
    Spiral& operator =( const Spiral& rhs );

    // specific methods
    double GetRadius() const;
    void SetRadius( double radius );
    double GetAngle() const;
    void SetAngle( double angle );

    // accessors
    virtual const Interval& GetParametricInterval() const;

    // modifications
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );

protected:
    double radius_;
    Interval interval_;
};


//-------------------------------------------------------------------------------------------------
// A R C H I M E D I A N   S P I R A L
//-------------------------------------------------------------------------------------------------


/*++
    Archimedean spiral (mathematical curve).
--*/
class ArchimedianSpiral : public Spiral {
public:
    ArchimedianSpiral( const CS& cs, double radius, double angle );
    ArchimedianSpiral( const ArchimedianSpiral& rhs );
    virtual ~ArchimedianSpiral() {}
    ArchimedianSpiral& operator =( const ArchimedianSpiral& rhs );
    virtual ArchimedianSpiral* Clone() const { return new ArchimedianSpiral( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // accessors
    virtual ECurveTypes GetType() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcLength() const;
};


//-------------------------------------------------------------------------------------------------
// L O G A R I T H M I C   S P I R A L
//-------------------------------------------------------------------------------------------------


/*++
    Logarithmic spiral (mathematical curve).
--*/
class LogarithmicSpiral : public Spiral {
public:
    LogarithmicSpiral( const CS& cs, double radius, double angle, double b );
    LogarithmicSpiral( const LogarithmicSpiral& rhs );
    virtual ~LogarithmicSpiral() {}
    LogarithmicSpiral& operator =( const LogarithmicSpiral& rhs );
    virtual LogarithmicSpiral* Clone() const { return new LogarithmicSpiral( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    double GetB() const;
    void SetB( double b );

    // accessors
    virtual ECurveTypes GetType() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcLength() const;

private:
    double b_;
};


//-------------------------------------------------------------------------------------------------
// P O L Y L I N E
//-------------------------------------------------------------------------------------------------


/*++
    Polyline (mathematical curve).
--*/
class Polyline : public Curve {
public:
    Polyline();
    Polyline( const std::vector<Point>& vertices );
    Polyline( const Polyline& rhs );
    virtual ~Polyline() {}
    Polyline& operator =( const Polyline& rhs );
    virtual Polyline* Clone() const { return new Polyline( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    int GetVerticesCount() const;
    const Point& GetVertex( int index ) const;
    void SetVertex( int index, const Point& vertex );
    void AddVertex( const Point& vertex, int index = -1 );
    void RemoveVertex( int index );
    void RemoveVertices();

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcLength() const;
    virtual void CalcBoundingBox( BoundingBox& box ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

private:
    void UpdateInterval();
    void GetSegmentPoints( double t, const Point*& pStartPoint, const Point*& pEndPoint,
        double& localT) const;

private:
    std::vector<Point> vertices_;
    Interval interval_;
};


//-------------------------------------------------------------------------------------------------
// B E Z I E R   C U R V E
//-------------------------------------------------------------------------------------------------


/*++
    Bezier curve (mathematical curve).
--*/
class BezierCurve : public Curve {
public:
    BezierCurve( const Point& p0, const Point& p1, const Point& p2 );
    BezierCurve( const std::vector<Point>& controlVertices );
    BezierCurve( const BezierCurve& rhs );
    virtual ~BezierCurve();
    BezierCurve& operator =( const BezierCurve& rhs );
    virtual BezierCurve* Clone() const { return new BezierCurve( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    int GetOrder() const;
    int GetControlVerticesCount() const;
    const Point& GetControlVertex( int index ) const;
    void GetControlVertices( std::vector<Point>& vertices ) const;
    void SetControlVerticesCount( int count );
    void SetControlVertex( int index, const Point& vertex );
    void SetControlVertices( const std::vector<Point>& vertices );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

private:
    double B( int n, int i, double t ) const;
    void D1( int i, Vector& result ) const;
    void D2( int i, Vector& result ) const;
    void AllocateControlVertices( int count );
    void CleanupControlVertices();

private:
    static const Interval INTERVAL;

private:
    int controlVerticesCount_;
    Point* pControlVertices_;
};


//-------------------------------------------------------------------------------------------------
// H E R M I T E   S P L I N E
//-------------------------------------------------------------------------------------------------


/*++
    Hermite spline (mathematical curve).
    Interpolated (instead of approximated) cubic Hermite spline to be exact.
--*/
class HermiteSpline : public Curve {
public:
    HermiteSpline();
    HermiteSpline( const std::vector<Point>& vertices );
    HermiteSpline( const HermiteSpline& rhs );
    virtual ~HermiteSpline() {}
    HermiteSpline& operator =( const HermiteSpline& rhs );
    virtual HermiteSpline* Clone() const { return new HermiteSpline( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    int GetVerticesCount() const;
    const Point& GetVertex( int index ) const;
    void SetVertex( int index, const Point& vertex );
    void AddVertex( const Point& vertex, int index = -1 );
    void RemoveVertex( int index );
    void RemoveVertices();
    bool RearrageVertices( int newVerticesCount );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    void Transform( const Matrix3x3& m );

private:
    void UpdateInterval();
    void GetLocalIntervalPoints( double t, const Point*& pP0, const Point*& pP1, int& nP0 ) const;
    void CalcRadiusVector( double t, double a0, double a1, double b0, double b1, double& x,
        double& y ) const;
    double CalcLocalIntervalParametricValue( double t ) const;
    bool CalcLocalIntervalTangent( int i, Vector& q ) const;

private:
    std::vector<Point> vertices_;
    Interval interval_;
};


//-------------------------------------------------------------------------------------------------
// E Q U I D I S T A N T   C U R V E
//-------------------------------------------------------------------------------------------------


/*++
    Equidistant curve (mathematical curve).
--*/
class EquidistantCurve : public Curve {
public:
    EquidistantCurve( Curve* pSourceCurve, const Vector& shiftDirection );
    EquidistantCurve( const EquidistantCurve& rhs );
    virtual ~EquidistantCurve();
    EquidistantCurve& operator =( const EquidistantCurve& rhs );
    virtual EquidistantCurve* Clone() const { return new EquidistantCurve( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    const Curve& GetSourceCurve() const;
    const Vector& GetShiftDirection() const;
    void SetSourceCurve( Curve* pSourceCurve );
    void SetShiftDirection( const Vector& direction );

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

private:
    Curve* pSourceCurve_;
    Vector shiftDirection_;
};


//-------------------------------------------------------------------------------------------------
// P R O X Y   C U R V E
//-------------------------------------------------------------------------------------------------


/*++
    Proxy curve that refers to another curve and can redefine it's parametric domain.
--*/
class ProxyCurve : public Curve {
public:
    ProxyCurve( Curve* pSourceCurve );
    ProxyCurve( Curve* pSourceCurve, const Interval& interval );
    ProxyCurve( const ProxyCurve& rhs );
    virtual ~ProxyCurve();
    ProxyCurve& operator =( const ProxyCurve& rhs );
    virtual ProxyCurve* Clone() const { return new ProxyCurve( *this ); }
    virtual void Accept( CurveVisitor& visitor );

    // specific methods
    const Curve& GetSourceCurve() const;
    void SetSourceCurve( Curve* pSourceCurve );
    void SetParametricInterval( const Interval& interval );
    void ResetParametricInterval();

    // accessors
    virtual ECurveTypes GetType() const;
    virtual const Interval& GetParametricInterval() const;

    // calculations
    virtual Point CalcPointOnCurve( double t ) const;
    virtual Vector CalcFirstDerivative( double t ) const;
    virtual Vector CalcSecondDerivative( double t ) const;
    virtual double CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const;

    // modifications
    virtual void Transfer( const Vector& shift );
    virtual void Scale( double xCoef, double yCoef, const Point* pCenter = 0 );
    virtual void Rotate( double angle, const Point* pCenter = 0 );
    virtual void Transform( const Matrix3x3& m );

private:
    Curve* pSourceCurve_;
    Interval* pInterval_;
};


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d


#endif

///////////////////////////////////////////////////////////////////////////////////////////////////
//                    __                        _____ _ _           _                            //
//                 /\ \ \___ _   _ _ __ ___     \_   \ | |_   _ ___(_) ___  _ __                 //
//                /  \/ / _ \ | | | '__/ _ \     / /\/ | | | | / __| |/ _ \| '_ \                //
//               / /\  /  __/ |_| | | | (_) | /\/ /_ | | | |_| \__ \ | (_) | | | |               //
//               \_\ \/ \___|\__,_|_|  \___/  \____/ |_|_|\__,_|___/_|\___/|_| |_|               //
//                                                                                               //
//////////////////////////////// Math2d // From Siberia with love // bitbucket.org/jne100/math2d //


#include "m2d_curves.h"

#include <m2d_visitors.h>
#include <m2d_numerics.h>


namespace m2d {


//-------------------------------------------------------------------------------------------------
// C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Point Curve::CalcCharacterPoint( ECurveCharacterPoints characterPoint ) const
{
    switch ( characterPoint ) {
    case CHP_BEGIN:
        return CalcPointOnCurve( GetParametricInterval().begin_ );
    case CHP_MIDDLE:
        return CalcPointOnCurve( GetParametricInterval().CalcMiddle() );
    case CHP_END:
        return CalcPointOnCurve( GetParametricInterval().end_ );
    default:
        return Point();
    }
}


Vector Curve::CalcNormal( double t, bool direction /*= true*/ ) const
{
    Vector normal = CalcFirstDerivative( t ).CalcPerpendicular( direction );
    normal.Normalize();
    return normal;
}


double Curve::CalcDistanceToPoint( const Point& p ) const
{
    double t = CalcPointProjection( p );
    Point pointOnCurve = CalcPointOnCurve( t );
    return ( CalcDistance(p, pointOnCurve) );
}


double Curve::CalcPointProjection( const Point& /*p*/ ) const
{
    // stub
    assert( 0 );
    return 0.0;
}


/*++
    Calculation of metric length, generic algorithm.
--*/
double Curve::CalcLength() const
{
    const int PARAMETRIC_STEPS_COUNT = 20;
    const Interval& interval = GetParametricInterval();
    double tStep = ( interval.CalcLength() / PARAMETRIC_STEPS_COUNT );
    double length = 0.0;

    Point previousPoint = CalcPointOnCurve( interval.begin_ );
    for ( int i = 1; i <= PARAMETRIC_STEPS_COUNT; ++i ) {
        Point currentPoint = CalcPointOnCurve( interval.begin_ + i * tStep );
        length += CalcDistance( previousPoint, currentPoint );
        previousPoint = currentPoint;
    }

    return length;
}


/*++
    Calculation of bounding box, generic algorithm.
--*/
void Curve::CalcBoundingBox( BoundingBox& box ) const
{
    const int PARAMETRIC_STEPS_COUNT = 20;
    const Interval& interval = GetParametricInterval();
    double tStep = ( interval.CalcLength() / PARAMETRIC_STEPS_COUNT );

    for ( int i = 0; i <= PARAMETRIC_STEPS_COUNT; ++i ) {
        box.Add( CalcPointOnCurve(interval.begin_ + i * tStep) );
    }
}


/*++
    Calculation of curvature at specified point.
    c = (x'y" - y'x") / ((x'^2 + y'^2)^(3/2))
--*/
double Curve::CalcCurvature( double t ) const
{
    Vector d1 = CalcFirstDerivative( t );
    Vector d2 = CalcSecondDerivative( t );

    if ( !d2.IsZero() ) {
        return ( (d1.x_ * d2.y_ - d1.y_ * d2.x_) / ::pow((d1.x_ * d1.x_ + d1.y_ * d1.y_), 1.5) );
    } else {
        return 0.0;
    }
}


/*++
    Calculation of tilt parametric step - parametric step through curve, which fulfil condition
    that tangent direction of curve in point (t + step) differs from tangent direction of curve in
    point t not more than on angle da. span parameter represents limiting parametric interval and
    cut parameter represents fact of tilt step truncation.
--*/
double Curve::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    assert( GT(da, 0.0) );

    static const double MIN_DT = 0.001;
    static const double DEFAULT_DT_COEF = 0.01;
    double defaultDt = span.CalcLength() * DEFAULT_DT_COEF;

    double tEnd = t;
    Vector dStart = CalcFirstDerivative( t );
    Vector dEnd = CalcFirstDerivative( tEnd + defaultDt );

    if ( ::fabs(dStart.CalcRelativeAngle(dEnd)) < da ) {
        // general case
        while ( ::fabs(dStart.CalcRelativeAngle(dEnd)) < da ) {
            tEnd += defaultDt;
            if ( GE(tEnd, span.end_) ) {
                tEnd = span.end_;
                cut = true;
                break;
            }
            dEnd = CalcFirstDerivative( tEnd + defaultDt );
        }

    } else {
        // special case when even minimal step along curve (defaultDt) is too big to satisfy
        // condition about maximum tangent deviation angle
        double dt = defaultDt;
        do {
            dt /= 2.0;
            dEnd = CalcFirstDerivative( t + dt );
            if ( dt < MIN_DT ) {
                dt = MIN_DT;
                break;
            }
        } while ( ::fabs(dStart.CalcRelativeAngle(dEnd)) > da );
        tEnd = t + dt;
        if ( GE(tEnd, span.end_) ) {
            tEnd = span.end_;
            cut = true;
        }
    }

    return ( (t < tEnd) ? (tEnd - t) : 0.0 );
}


//-------------------------------------------------------------------------------------------------
// A N A L Y T I C A L   C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


AnalyticalCurve::AnalyticalCurve()
{
}


AnalyticalCurve::AnalyticalCurve( const AnalyticalCurve& rhs )
    : cs_( rhs.cs_ )
{
}


AnalyticalCurve::AnalyticalCurve( const CS& cs )
    : cs_( cs )
{
}


AnalyticalCurve::AnalyticalCurve( const Point& pos, double angle /*= 0.0*/ )
    : cs_( pos, angle )
{
}


AnalyticalCurve& AnalyticalCurve::operator =( const AnalyticalCurve& rhs )
{
    if ( &rhs != this ) {
        cs_ = rhs.cs_;
    }
    return ( *this );
}


const CS& AnalyticalCurve::GetCS() const
{
    return cs_;
}


void AnalyticalCurve::GetCS( const CS& cs )
{
    cs_ = cs;
}


const Point& AnalyticalCurve::GetCenter() const
{
    return cs_.pos_;
}


void AnalyticalCurve::SetCenter( const Point& p )
{
    cs_.pos_ = p;
}


void AnalyticalCurve::Transfer( const Vector& shift )
{
    cs_.Transfer( shift );
}


void AnalyticalCurve::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    cs_.Scale( xCoef, yCoef, pCenter );
}


void AnalyticalCurve::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    cs_.Rotate( angle, pCenter );
}


void AnalyticalCurve::Transform( const Matrix3x3& m )
{
    cs_.Transform( m );
}


//-------------------------------------------------------------------------------------------------
// S E G M E N T   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Segment::Segment()
    : p1_( ZERO_POINT )
    , p2_( ZERO_POINT )
{
}


Segment::Segment( const Point& p1, const Point& p2 )
    : p1_( p1 )
    , p2_( p2 )
{
}


Segment::Segment( const Segment& rhs )
{
    ( *this ) = rhs;
}


const Point& Segment::GetP1() const
{
    return p1_;
}


const Point& Segment::GetP2() const
{
    return p2_;
}


void Segment::SetP1( const Point& p )
{
    p1_.Init( p.x_, p.y_ );
}


void Segment::SetP2( const Point& p )
{
    p2_.Init( p.x_, p.y_ );
}


Segment& Segment::operator =( const Segment& rhs )
{
    if ( &rhs != this ) {
        p1_ = rhs.p1_;
        p2_ = rhs.p2_;
    }
    return ( *this );
}


void Segment::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


ECurveTypes Segment::GetType() const
{
    return CT_SEGMENT;
}


const Interval& Segment::GetParametricInterval() const
{
    return INTERVAL;
}


/*++
    Calculation of the specified point on segment.
    C(t) = ((1 - t) * P1 + t * P2)
--*/
Point Segment::CalcPointOnCurve( double t ) const
{
    double x = ( (1.0 - t) * p1_.x_ + t * p2_.x_ );
    double y = ( (1.0 - t) * p1_.y_ + t * p2_.y_ );
    return ( Point(x, y) );
}


/*++
    Calculation of the first derivative of segment at specified point.
    C(t)' = P2 - P1
--*/
Vector Segment::CalcFirstDerivative( double /*t*/ ) const
{
    // C(t)' = ((1 - t) * P1 + t * P2)' =
    // = P1 * (1 - t)'  + P2 * (t)' = 
    // = P1 * (1)' + P1 * (-t)'  + P2 * (t)' =
    // = P1 * 0 + P1 * (-1) + P2 * (1) = 
    // = P2 - P1

    double dx = ( p2_.x_ - p1_.x_ );
    double dy = ( p2_.y_ - p1_.y_ );
    return ( Vector(dx, dy) );
}


/*++
    Calculation of the second derivative of segment at specified point.
    C(t)'' = 0
--*/
Vector Segment::CalcSecondDerivative( double /*t*/ ) const
{
     // C(t)'' = (P2 - P1)' = (P2)' - (P1)' = 0.0 - 0.0 = 0.0

    return ( Vector(0.0, 0.0) );
}


double Segment::CalcPointProjection( const Point& p ) const
{
    double t = INTERVAL.begin_;

    double length = CalcLength();
    if ( NENil(length) ) {
        Vector pp1( p1_, p );
        Vector p1p2( p1_, p2_ );
        t = ( pp1.CalcLength() * ::cos(pp1.CalcAngle(p1p2)) ) / length;
    }

    return t;
}


double Segment::CalcLength() const
{
    return ( CalcDistance(p1_, p2_) );
}


void Segment::CalcBoundingBox( BoundingBox& box ) const
{
    box.Add( p1_ );
    box.Add( p2_ );
}


double Segment::CalcTiltStep( double t, double /*da*/, const Interval& span, bool& cut ) const
{
    cut = true;
    if ( LE(t, span.end_) ) {
        return span.end_ - t;
    } else {
        return 0.0;
    }
}


void Segment::Transfer( const Vector& shift )
{
    p1_.Transfer( shift );
    p2_.Transfer( shift );
}


void Segment::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    p1_.Scale( xCoef, yCoef, pCenter );
    p2_.Scale( xCoef, yCoef, pCenter );
}


void Segment::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    p1_.Rotate( angle, pCenter );
    p2_.Rotate( angle, pCenter );
}


void Segment::Transform( const Matrix3x3& m )
{
    p1_.Transform( m );
    p2_.Transform( m );
}


const Interval Segment::INTERVAL( 0.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// A R C   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


/*++
    Construction of the arc/circle with center in global origin.
--*/
Arc::Arc( double radius, const Interval* pInterval /*= 0*/ )
    : AnalyticalCurve( DEFAULT_CS )
    , radius_( radius )
    , interval_( (pInterval != 0) ? (*pInterval) : (CIRCLE_INTERVAL) )
{
}


/*++
    Construction of the arc/circle with center at specified point.
--*/
Arc::Arc( double radius, const Point& center, const Interval* pInterval /*= 0*/ )
    : AnalyticalCurve( center )
    , radius_( radius )
    , interval_( (pInterval != 0) ? (*pInterval) : (CIRCLE_INTERVAL) )
{
}


/*++
    Construction of the arc/circle with specified coordinate system.
--*/
Arc::Arc( double radius, const CS& cs, const Interval* pInterval /*= 0*/ )
    : AnalyticalCurve( cs )
    , radius_( radius )
    , interval_( (pInterval != 0) ? (*pInterval) : (CIRCLE_INTERVAL) )
{
}


/*++
    Construction of the arc/circle by three points.
--*/
Arc::Arc( const Point& p1, const Point& p2, const Point& p3, bool isClosed /*= false*/ )
{
    Init( p1, p2, p3, isClosed );
}


Arc::Arc( const Arc& rhs )
    : AnalyticalCurve( rhs )
    , radius_( rhs.radius_ )
    , interval_( rhs.interval_ )
{
}


Arc& Arc::operator =( const Arc& rhs )
{
    AnalyticalCurve::operator =( rhs );
    if ( &rhs != this ) {
        radius_ = rhs.radius_;
        interval_ = rhs.interval_;
    }
    return ( *this );
}


void Arc::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


void Arc::Reset()
{
    cs_ = DEFAULT_CS;
    radius_ = 0.0;
    interval_ = ZERO_INTERVAL;
}


/*++
    Initialization of the arc/circle with center in global origin.
--*/
void Arc::Init( double radius, const Interval* pInterval /*= 0*/ )
{
    cs_ = DEFAULT_CS;
    radius_ = radius;
    interval_ = ( pInterval != 0 ) ? ( *pInterval ) : ( CIRCLE_INTERVAL );
}


/*++
    Initialization of the arc/circle with center at specified point.
--*/
void Arc::Init( double radius, const Point& center, const Interval* pInterval /*= 0*/ )
{
    cs_.Init( center );
    radius_ = radius;
    interval_ = ( pInterval != 0 ) ? ( *pInterval ) : ( CIRCLE_INTERVAL );
}


/*++
    Initialization of the arc/circle with specified coordinate system.
--*/
void Arc::Init( double radius, const CS& cs, const Interval* pInterval /*= 0*/ )
{
    cs_ = cs;
    radius_ = radius;
    interval_ = ( pInterval != 0 ) ? ( *pInterval ) : ( CIRCLE_INTERVAL );
}


/*++
    Initialization of the arc/circle by three points.
--*/
void Arc::Init( const Point& p1, const Point& p2, const Point& p3, bool isClosed /*= false*/ )
{
    // calculation of the arc center
    Point center;
    if ( CalcCenterByPoints(p1, p2, p3, center) ) {

        // calculation of the radius
        radius_ = CalcDistance( center, p1 );

        // calculation of the parametric interval
        if ( isClosed ) {
            cs_.Init( center );
            interval_ = CIRCLE_INTERVAL;

        } else {
            Vector p1Vector( center, p1 );
            Vector p3Vector( center, p3 );
            double p1Angle = NormalizeAngle( p1Vector.CalcAngle() );
            double p3Angle = NormalizeAngle( p3Vector.CalcAngle() );

            const Vector& beginVector = ( (p1Angle < p3Angle) ? (p1Vector) : (p3Vector) );
            Vector middleVector( center, p2 );
            const Vector& endVector = ( (p1Angle < p3Angle) ? (p3Vector) : (p1Vector) );

            double middleAngle = NormalizeAngle( beginVector.CalcAngle(middleVector) );
            double endAngle = NormalizeAngle( beginVector.CalcAngle(endVector) );

            if ( endAngle > middleAngle ) {
                cs_.Init( center, beginVector.CalcAngle() );
                interval_.Init( 0.0, endAngle );
            } else {
                cs_.Init( center, endVector.CalcAngle() );
                interval_.Init( 0.0, PI2 - endAngle );
            }
        }

    } else {
        Reset();
    }
}


double Arc::GetRadius() const
{
    return radius_;
}


bool Arc::IsClosed() const
{
    return GE( interval_.CalcLength(), PI2 );
}


void Arc::SetRadius( double radius )
{
    radius_ = radius;
}


void Arc::SetClosed()
{
    interval_ = CIRCLE_INTERVAL;
}


void Arc::SetSector( double start, double end )
{
    assert( LT(start, end) );
    interval_.begin_ = start;
    interval_.end_ = end;
}


ECurveTypes Arc::GetType() const
{
    return CT_ARC;
}


const Interval& Arc::GetParametricInterval() const
{
    return interval_;
}


/*++
    Calculation of the specified point on arc.
    X(t) = r * cos(t)
    Y(t) = r * sin(t)
--*/
Point Arc::CalcPointOnCurve( double t ) const
{
    Point p( radius_ * ::cos(t), radius_ * ::sin(t) );
    TransformFromCS( cs_, p );
    return p;
}


/*++
    Calculation of the first derivative of arc at specified point.
    X(t)' = r * -sin(t)
    Y(t)' = r * cos(t)
--*/
Vector Arc::CalcFirstDerivative( double t ) const
{
    Vector v( radius_ * (-::sin(t)), radius_ * ::cos(t) );
    TransformFromCS( cs_, v );
    return v;
}


/*++
    Calculation of the second derivative of arc at specified point.
    X(t)'' = r * -cos(t)
    Y(t)'' = r * -sin(t)
--*/
Vector Arc::CalcSecondDerivative( double t ) const
{
    Vector v( radius_ * (-::cos(t)), radius_ * (-::sin(t)) );
    TransformFromCS( cs_, v );
    return v;
}


double Arc::CalcLength() const
{
    return ( radius_ * interval_.CalcLength() );
}


double Arc::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    if ( GE((t + da), span.end_) ) {
        cut = true;
        double step = ( span.end_ - t );
        return ( LE(step, 0.0) ? 0.0 : step );
    } else {
        cut = false;
        return da;
    }
}


void Arc::Transfer( const Vector& shift )
{
    cs_.Transfer( shift );
}


void Arc::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    AnalyticalCurve::Scale( xCoef, yCoef, pCenter );
    radius_ = ::fabs( radius_ * xCoef );
}


void Arc::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    cs_.Rotate( angle, pCenter );
}


void Arc::Transform( const Matrix3x3& m )
{
    Point p1 = CalcPointOnCurve( GetParametricInterval().begin_ );
    Point p2 = CalcPointOnCurve( GetParametricInterval().CalcMiddle() );
    Point p3 = CalcPointOnCurve( GetParametricInterval().end_ );
    p1.Transform( m );
    p2.Transform( m );
    p3.Transform( m );
    Init( p1, p2, p3, IsClosed() );
}


/*++
    Center of the arc constructed by three points located in intersection point of the
    perpendicular bisectors of triangle, formed from these three points.
--*/
bool Arc::CalcCenterByPoints( const Point& p1, const Point& p2, const Point& p3, Point& result ) const
{
    Vector perpendicular12( Vector(p1, p2).CalcPerpendicular() );
    Vector perpendicular23( Vector(p2, p3).CalcPerpendicular() );
    Point middle12( (p1.x_ + p2.x_) / 2.0, (p1.y_ + p2.y_) / 2.0 );
    Point middle23( (p2.x_ + p3.x_) / 2.0, (p2.y_ + p3.y_) / 2.0 );
    return CalcLinesIntersectionPoint( middle12, perpendicular12, middle23, perpendicular23, result );
}


const Interval Arc::CIRCLE_INTERVAL( 0.0, PI2 );


//-------------------------------------------------------------------------------------------------
// S P I R A L   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Spiral::Spiral( const CS& cs, double radius, double angle )
    : AnalyticalCurve( cs )
    , radius_( radius )
    , interval_( 0.0, angle )
{
}


Spiral::Spiral( const Spiral& rhs )
    : AnalyticalCurve( rhs )
    , radius_( rhs.radius_ )
    , interval_( rhs.interval_ )
{
}


Spiral& Spiral::operator =( const Spiral& rhs )
{
    AnalyticalCurve::operator =( rhs );
    if ( &rhs != this ) {
        radius_ = rhs.radius_;
        interval_ = rhs.interval_;
    }
    return ( *this );
}


double Spiral::GetRadius() const
{
    return radius_;
}


void Spiral::SetRadius( double radius )
{
    assert( radius >= 0.0 );
    radius_ = radius;
}


double Spiral::GetAngle() const
{
    return interval_.end_;
}


void Spiral::SetAngle( double angle )
{
    assert( angle >= 0.0 );
    interval_.end_ = angle;
}


const Interval& Spiral::GetParametricInterval() const
{
    return interval_;
}


void Spiral::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    AnalyticalCurve::Scale( xCoef, yCoef, pCenter );
    radius_ = ::fabs( radius_ * xCoef );
}


//-------------------------------------------------------------------------------------------------
// A R C H I M E D I A N   S P I R A L   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


ArchimedianSpiral::ArchimedianSpiral( const CS& cs, double radius, double angle )
    : Spiral( cs, radius, angle )
{
}


ArchimedianSpiral::ArchimedianSpiral( const ArchimedianSpiral& rhs )
    : Spiral( rhs )
{
}


ArchimedianSpiral& ArchimedianSpiral::operator =( const ArchimedianSpiral& rhs )
{
    Spiral::operator =( rhs );
    return ( *this );
}


void ArchimedianSpiral::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


ECurveTypes ArchimedianSpiral::GetType() const
{
    return CT_ARCHIMEDIAN_SPIRAL;
}


/*++
    Calculation of the specified point on archimedian spiral.
    X(t) = r * t * cos(t)
    Y(t) = r * t * sin(t)
--*/
Point ArchimedianSpiral::CalcPointOnCurve( double t ) const
{
    Point p( radius_ * t * ::cos(t), radius_ * t * ::sin(t) );
    TransformFromCS( cs_, p );
    return p;
}


/*++
    Calculation of the first derivative of archimedian spiral at specified point.
    X(t)' = r * t * -sin(t)
    Y(t)' = r * t * cos(t)
--*/
Vector ArchimedianSpiral::CalcFirstDerivative( double t ) const
{
    Vector v( radius_ * t * (-::sin(t)), radius_ * t * ::cos(t) );
    TransformFromCS( cs_, v );
    return v;
}


/*++
    Calculation of the second derivative of archimedian spiral at specified point.
    X(t)'' = r * t * -cos(t)
    Y(t)'' = r * t * -sin(t)
--*/
Vector ArchimedianSpiral::CalcSecondDerivative( double t ) const
{
    Vector v( radius_ * t * (-::cos(t)), radius_ * t * (-::sin(t)) );
    TransformFromCS( cs_, v );
    return v;
}


double ArchimedianSpiral::CalcLength() const
{
    double angle = GetAngle();
    double angleSquare = angle * angle;
    return ( ::log(::sqrt(angleSquare + 1.0) + angle) / 2.0 +
        (angle / 2.0) * ::sqrt(angleSquare + 1.0) ) * radius_;
}


//-------------------------------------------------------------------------------------------------
// L O G A R I T H M I C   S P I R A L   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


LogarithmicSpiral::LogarithmicSpiral( const CS& cs, double radius, double angle, double b )
    : Spiral( cs, radius, angle )
    , b_( b )
{
}


LogarithmicSpiral::LogarithmicSpiral( const LogarithmicSpiral& rhs )
    : Spiral( rhs )
    , b_( rhs.b_ )
{
}


LogarithmicSpiral& LogarithmicSpiral::operator =( const LogarithmicSpiral& rhs )
{
    Spiral::operator =( rhs );
    if ( &rhs != this ) {
        b_ = rhs.b_;
    }
    return ( *this );
}


void LogarithmicSpiral::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


double LogarithmicSpiral::GetB() const
{
    return b_;
}


void LogarithmicSpiral::SetB( double b )
{
    b_ = b;
}


ECurveTypes LogarithmicSpiral::GetType() const
{
    return CT_LOGARITHMIC_SPIRAL;
}


/*++
    Calculation of the specified point on logarithmic spiral.
    X(t) = r * exp(b * t) * cos(t)
    Y(t) = r * exp(b * t) * sin(t)
--*/
Point LogarithmicSpiral::CalcPointOnCurve( double t ) const
{
    Point p( radius_ * ::exp(b_ * t) * ::cos(t), radius_ * ::exp(b_ * t) * ::sin(t) );
    TransformFromCS( cs_, p );
    return p;
}


/*++
    Calculation of the first derivative of logarithmic spiral at specified point.
    X(t)' = r * exp(b * t) * (cos(t) - sin(t))
    Y(t)' = r * exp(b * t) * (sin(t) + cos(t))
--*/
Vector LogarithmicSpiral::CalcFirstDerivative( double t ) const
{
    Vector v( radius_ * ::exp(b_ * t) * (::cos(t) - ::sin(t)),
        radius_ * ::exp(b_ * t) * (::sin(t) + ::cos(t)) );
    TransformFromCS( cs_, v );
    return v;
}


/*++
    Calculation of the second derivative of logarithmic spiral at specified point.
    X(t)'' = -2 * r * exp(b * t) * sin(t)
    Y(t)'' = 2 * r * exp(b * t) * cos(t)
--*/
Vector LogarithmicSpiral::CalcSecondDerivative( double t ) const
{
    Vector v( -2 * radius_ * ::exp(b_ * t) * ::sin(t), 2 * radius_ * ::exp(b_ * t) * ::cos(t) );
    TransformFromCS( cs_, v );
    return v;
}


double LogarithmicSpiral::CalcLength() const
{
    return ( (::sqrt(1.0 + b_ * b_) * (::exp(b_ * GetAngle())  - 1.0) * radius_ ) / b_ );
}


//-------------------------------------------------------------------------------------------------
// P O L Y L I N E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


Polyline::Polyline()
    : interval_( ZERO_INTERVAL )
{
}


Polyline::Polyline( const std::vector<Point>& vertices )
    : vertices_( vertices )
{
    UpdateInterval();
}


Polyline::Polyline( const Polyline& rhs )
    : vertices_( rhs.vertices_ )
    , interval_( rhs.interval_ )
{
}


Polyline& Polyline::operator =( const Polyline& rhs )
{
    if ( &rhs != this ) {
        vertices_ = rhs.vertices_;
        interval_ = rhs.interval_;
    }
    return ( *this );
}


void Polyline::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


int Polyline::GetVerticesCount() const
{
    return static_cast<int>( vertices_.size() );
}


const Point& Polyline::GetVertex( int index ) const
{
    assert( static_cast<size_t>(index) < vertices_.size() );
    return vertices_[ index ];
}


void Polyline::SetVertex( int index, const Point& vertex )
{
    assert( static_cast<size_t>(index) < vertices_.size() );
    vertices_[ index ] = vertex;
}


void Polyline::AddVertex( const Point& vertex, int index /*= -1*/ )
{
    if ( (index >= 0) && (index < static_cast<int>(vertices_.size())) ) {
        vertices_.insert( vertices_.begin() + index, vertex );
        UpdateInterval();

    } else if ( index == -1 ) {
        vertices_.push_back( vertex );
        UpdateInterval();
    }
}


void Polyline::RemoveVertex( int index )
{
    if ( (index >= 0) && (index < static_cast<int>(vertices_.size())) ) {
        vertices_.erase( vertices_.begin() + index );
        UpdateInterval();
    }
}


/*++
    Remove all vertices from polyline.
--*/
void Polyline::RemoveVertices()
{
    vertices_.clear();
    UpdateInterval();
}


ECurveTypes Polyline::GetType() const
{
    return CT_POLYLINE;
}


const Interval& Polyline::GetParametricInterval() const
{
    return interval_;
}


/*++
    Calculation of the specified point on polyline.
    Used the same algorithms as for line segment for concrete partiion of the polyline.
    C(t) = ((1 - dt) * P[i] + dt * P[k])
--*/
Point Polyline::CalcPointOnCurve( double t ) const
{
    const Point* pStartPoint = 0;
    const Point* pEndPoint = 0;
    double localT;
    GetSegmentPoints( t, pStartPoint, pEndPoint, localT );

    Point result;
    if ( pStartPoint != 0 ) {
        if ( pStartPoint == pEndPoint ) {
            result = ( *pStartPoint );
        } else {
            result.Init( ((1.0 - localT) * pStartPoint->x_ + localT * pEndPoint->x_),
                ((1.0 - localT) * pStartPoint->y_ + localT * pEndPoint->y_) );
        }
    }
    return result;
}


/*++
    Calculation of the first derivative of polyline at specified point.
    Used the same algorithms as for line segment for concrete partiion of the polyline.
    C(t)' = P[k] - P[i]
--*/
Vector Polyline::CalcFirstDerivative( double t ) const
{
    const Point* pStartPoint;
    const Point* pEndPoint;
    double dummy;
    GetSegmentPoints( t, pStartPoint, pEndPoint, dummy );

    Vector result;
    if ( pStartPoint != 0 ) {
        result.Init( (pEndPoint->x_ - pStartPoint->x_), (pEndPoint->y_ - pStartPoint->y_));
    }
    return result;
}


/*++
    Calculation of the second derivative of polyline at specified point.
    C(t)'' = 0
--*/
Vector Polyline::CalcSecondDerivative( double /*t*/ ) const
{
    return ZERO_VECTOR;
}


double Polyline::CalcLength() const
{
    double length = 0.0;
    for ( size_t i = 1; i < vertices_.size(); ++i ) {
        length += CalcDistance( vertices_[i - 1], vertices_[i] );
    }
    return length;
}


void Polyline::CalcBoundingBox( BoundingBox& box ) const
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        box.Add( vertices_[i] );
    }
}


double Polyline::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    assert( GT(da, 0.0) );

    cut = false;

    double tEnd = ::floor( t );
    Vector dStart = CalcFirstDerivative( tEnd );
    Vector dEnd;

    do {
        tEnd += 1.0;
        if ( GE(tEnd, span.end_) ) {
            tEnd = span.end_;
            cut = true;
            break;
        }
        dEnd = CalcFirstDerivative( tEnd );
    } while( ::fabs(dStart.CalcRelativeAngle(dEnd)) < da );

    return ( (t < tEnd) ? (tEnd - t) : 0.0 );
}


void Polyline::Transfer( const Vector& shift )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Transfer( shift );
    }
}


void Polyline::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Scale( xCoef, yCoef, pCenter );
    }
}


void Polyline::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Rotate( angle, pCenter );
    }
}


void Polyline::Transform( const Matrix3x3& m )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Transform( m );
    }
}


void Polyline::UpdateInterval()
{
    if ( vertices_.size() < 2 ) {
        interval_ = ZERO_INTERVAL;
    } else {
        interval_.Init( 0.0, static_cast<double>(vertices_.size()) - 1.0 );
    }
}


void Polyline::GetSegmentPoints( double t, const Point*& pStartPoint, const Point*& pEndPoint,
    double& localT ) const
{
    if ( vertices_.size() >= 2 ) {

        int nStart = static_cast<int>( ::floor(t) );
        int nEnd = static_cast<int>( ::ceil(t) );

        if ( nStart == nEnd ) {
            nEnd = nStart + 1;
        }

        if ( (nStart >= 0) && (nEnd < static_cast<int>(vertices_.size())) ) {
            pStartPoint = &vertices_[ nStart ];
            pEndPoint = &vertices_[ nEnd ];
            localT = t - ::floor( t );
        } else {
            pStartPoint = ( (nStart < 0) ? &vertices_[0] : &vertices_[vertices_.size() - 2] );
            pEndPoint = ( (nStart < 0) ? &vertices_[1] : &vertices_[vertices_.size() - 1] );
            localT = ( (nStart < 0) ? t : ( t - static_cast<double>(vertices_.size()) + 2.0) );
        }

    } else {
        pStartPoint = 0;
        pEndPoint = 0;
    }
}


//-------------------------------------------------------------------------------------------------
// B E Z I E R   C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


/*++
    Construction of the quadratic Bezier curve.
--*/
BezierCurve::BezierCurve( const Point& p0, const Point& p1, const Point& p2 )
    : controlVerticesCount_( 0 )
    , pControlVertices_( 0 )
{
    AllocateControlVertices( 3 );
    pControlVertices_[ 0 ] = p0;
    pControlVertices_[ 1 ] = p1;
    pControlVertices_[ 2 ] = p2;
}


BezierCurve::BezierCurve( const std::vector<Point>& controlVertices )
    : controlVerticesCount_( 0 )
    , pControlVertices_( 0 )
{
    AllocateControlVertices( static_cast<int>(controlVertices.size()) );
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ] = controlVertices[ i ];
    }
}


BezierCurve::BezierCurve( const BezierCurve& rhs )
    : controlVerticesCount_( 0 )
    , pControlVertices_( 0 )
{
    ( *this ) = rhs;
}


BezierCurve::~BezierCurve()
{
    CleanupControlVertices();
}


BezierCurve& BezierCurve::operator =( const BezierCurve& rhs )
{
    if ( &rhs != this ) {
        AllocateControlVertices( rhs.controlVerticesCount_ );
        for ( int i = 0; i < controlVerticesCount_; ++i ) {
            pControlVertices_[ i ] = rhs.pControlVertices_[ i ];
        }
    }
    return ( *this );
}


void BezierCurve::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


int BezierCurve::GetOrder() const
{
    return controlVerticesCount_ - 1;
}


int BezierCurve::GetControlVerticesCount() const
{
    return controlVerticesCount_;
}


const Point& BezierCurve::GetControlVertex( int index ) const
{
    assert( index < controlVerticesCount_ );
    return pControlVertices_[ index ];
}


void BezierCurve::GetControlVertices( std::vector<Point>& vertices ) const
{
    vertices.reserve( controlVerticesCount_ );
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        vertices.push_back( pControlVertices_[i] );
    }
}


void BezierCurve::SetControlVerticesCount( int count )
{
    AllocateControlVertices( count );
}


void BezierCurve::SetControlVertex( int index, const Point& vertex )
{
    assert( index < controlVerticesCount_ );
    pControlVertices_[ index ].Init( vertex.x_, vertex.y_ );
}


void BezierCurve::SetControlVertices( const std::vector<Point>& vertices )
{
    AllocateControlVertices( static_cast<int>(vertices.size()) );
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ] = vertices[ i ];
    }
}


ECurveTypes BezierCurve::GetType() const
{
    return CT_BEZIER_CURVE;
}


const Interval& BezierCurve::GetParametricInterval() const
{
    return INTERVAL;
}


/*++
    Calculation of the specified point on Bezier curve.
    C1(t) = ((1 - t) * P[0] + t * P[1]),
    C2(t) = ((1 - t) ^ 2) * P[0] + 2 * t * (1 - t) * P[1] + (t ^ 2) * P[2],
    etc.
--*/
Point BezierCurve::CalcPointOnCurve( double t ) const
{
    Point result = ZERO_POINT;

    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        double b = B( controlVerticesCount_ - 1, i, t );
        result.x_ += pControlVertices_[ i ].x_ * b;
        result.y_ += pControlVertices_[ i ].y_ * b;
    }

    return result;
}


Vector BezierCurve::CalcFirstDerivative( double t ) const
{
    Vector result = ZERO_VECTOR;
    Vector tempVector;

    for ( int i = 0, degree = controlVerticesCount_ - 1; i < degree; ++i ) {
        D1( i, tempVector );
        tempVector *= B( degree - 1, i, t );
        result += tempVector;
    }

    result *= controlVerticesCount_;
    return result;
}


Vector BezierCurve::CalcSecondDerivative( double t ) const
{
    Vector result = ZERO_VECTOR;
    Vector tempVector;

    for ( int i = 0, degree = controlVerticesCount_ - 2; i < degree; ++i ) {
        D2( i, tempVector );
        tempVector *= B( degree - 1, i, t );
        result += tempVector;
    }

    result *= controlVerticesCount_ * ( controlVerticesCount_ - 1.0 );
    return result;
}


void BezierCurve::Transfer( const Vector& shift )
{
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ].Transfer( shift );
    }
}


void BezierCurve::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ].Scale( xCoef, yCoef, pCenter );
    }
}


void BezierCurve::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ].Rotate( angle, pCenter );
    }
}


void BezierCurve::Transform( const Matrix3x3& m )
{
    for ( int i = 0; i < controlVerticesCount_; ++i ) {
        pControlVertices_[ i ].Transform( m );
    }
}


/*++
    Calculation of the Bernstein polynomial.
--*/
double BezierCurve::B( int n, int i, double t ) const
{
    return ( FACTORIAL_CALCULATOR.Calc(n) /
        (FACTORIAL_CALCULATOR.Calc(i) * FACTORIAL_CALCULATOR.Calc(n - i)) ) *
        ( ::pow(t, i) ) * ( ::pow(1.0 - t, n - i) );
}


void BezierCurve::D1( int i, Vector& result ) const
{
    assert( i + 1 < controlVerticesCount_ );
    result.Init( pControlVertices_[i + 1].x_ - pControlVertices_[i].x_,
        pControlVertices_[i + 1].y_ - pControlVertices_[i].y_ );
}


void BezierCurve::D2( int i, Vector& result ) const
{
    assert( i + 2 < controlVerticesCount_ );
    result.Init( (pControlVertices_[i + 2].x_ - pControlVertices_[i + 1].x_) -
        (pControlVertices_[i + 1].x_ - pControlVertices_[i].x_),
        (pControlVertices_[i + 2].y_ - pControlVertices_[i + 1].y_) -
        (pControlVertices_[i + 1].y_ - pControlVertices_[i].y_) );
}


void BezierCurve::AllocateControlVertices( int count )
{
    assert( count > 0 );
    CleanupControlVertices();
    controlVerticesCount_ = count;
    pControlVertices_ = new Point [ controlVerticesCount_ ];
}


void BezierCurve::CleanupControlVertices()
{
    delete [] pControlVertices_;
    pControlVertices_ = 0;
}


const Interval BezierCurve::INTERVAL( 0.0, 1.0 );


//-------------------------------------------------------------------------------------------------
// H E R M I T E   S P L I N E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


HermiteSpline::HermiteSpline()
    : interval_( ZERO_INTERVAL )
{
}


HermiteSpline::HermiteSpline( const std::vector<Point>& vertices )
    : vertices_( vertices )
{
    UpdateInterval();
}


HermiteSpline::HermiteSpline( const HermiteSpline& rhs )
    : vertices_( rhs.vertices_ )
    , interval_( rhs.interval_ )
{
}


HermiteSpline& HermiteSpline::operator =( const HermiteSpline& rhs )
{
    if ( &rhs != this ) {
        vertices_ = rhs.vertices_;
        interval_ = rhs.interval_;
    }
    return ( *this );
}


void HermiteSpline::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


int HermiteSpline::GetVerticesCount() const
{
    return static_cast<int>( vertices_.size() );
}


const Point& HermiteSpline::GetVertex( int index ) const
{
    assert( static_cast<size_t>(index) < vertices_.size() );
    return vertices_[ index ];
}


void HermiteSpline::SetVertex( int index, const Point& vertex )
{
    assert( static_cast<size_t>(index) < vertices_.size() );
    vertices_[ index ] = vertex;
}


void HermiteSpline::AddVertex( const Point& vertex, int index /*= -1*/ )
{
    if ( (index >= 0) && (index < static_cast<int>(vertices_.size())) ) {
        vertices_.insert( vertices_.begin() + index, vertex );
        UpdateInterval();

    } else if ( index == -1 ) {
        vertices_.push_back( vertex );
        UpdateInterval();
    }
}


void HermiteSpline::RemoveVertex( int index )
{
    if ( (index >= 0) && (index < static_cast<int>(vertices_.size())) ) {
        vertices_.erase( vertices_.begin() + index );
        UpdateInterval();
    }
}


/*++
    Remove all vertices from spline.
--*/
void HermiteSpline::RemoveVertices()
{
    vertices_.clear();
    UpdateInterval();
}


/*++
    Modification of spline vertices count (shape stay the same).
--*/
bool HermiteSpline::RearrageVertices( int newVerticesCount )
{
    if ( (newVerticesCount >= 3) && (vertices_.size() >= 3) ) {
        std::vector<Point> tempPoints;
        CalcUniformPointsOnCurve( *this, newVerticesCount, tempPoints );
        vertices_.swap( tempPoints );
        UpdateInterval();
        return true;

    } else {
        return false;
    }
}


ECurveTypes HermiteSpline::GetType() const
{
    return CT_HERMITE_SPLINE;
}


const Interval& HermiteSpline::GetParametricInterval() const
{
    return interval_;
}


/*++
    Calculation of the specified point on Hermite spline.
    C(t) = a[0](o)*p[i] + a[1](o)*p[i+1] + b[0](o)*q[i] + b[1](o)*q[i+1],
    a[0](o) = 1 - 3*o^2 + 2*o^3,
    a[1](o) = 3*o^2 - 2*o^3,
    b[0](o) = o - 2*o^2 + o^3,
    b[1](o) = -o^2 + o^3,
    o - local interval parametric value (omega),
    q - tangent at specified vertex.
--*/
Point HermiteSpline::CalcPointOnCurve( double t ) const
{
    // calculation of local interval parametric value (omega) and its powers
    double o =  CalcLocalIntervalParametricValue( t );
    double o2 = o * o;
    double o3 = o2 * o;

    // calculation of a[0](o), a[1](o), b[0](o) and b[1](o)
    double a0 = 1 - 3 * o2 + 2 * o3;
    double a1 = 3 * o2 - 2 * o3;
    double b0 = o - 2 * o2 + o3;
    double b1 = -o2 + o3;

    // calculation of point on curve
    Point result;
    CalcRadiusVector( t, a0, a1, b0, b1, result.x_, result.y_ );
    return result;
}


/*++
    Calculation of the first derivative of Hermite spline at specified point.
    C'(t) = a[0](o)' * p[i] + a[1](o)' * p[i+1] + b[0](o)' * q[i] + b[1](o)' * q[i+1],
    a[0](o)' = -6*o + 6*o^2,
    a[1](o)' = 6*o - 6*o^2,
    b[0](o)' = 1 - 4*o + 3*o^2,
    b[1](o)' = -2*o + 3*o^2,
    o - local interval parametric value (omega),
    q - tangent at specified vertex.
--*/
Vector HermiteSpline::CalcFirstDerivative( double t ) const
{
    // calculation of local interval parametric value (omega) and its power
    double o =  CalcLocalIntervalParametricValue( t );
    double o2 = o * o;

    // calculation of a[0](o)', a[1](o)', b[0](o)' and b[1](o)'
    double da0 = -6 * o + 6 * o2;
    double da1 = 6 * o - 6 * o2;
    double db0 = 1 - 4 * o + 3 * o2;
    double db1 = -2 * o + 3 * o2;

    // calculation of first derivative
    Vector result;
    CalcRadiusVector( t, da0, da1, db0, db1, result.x_, result.y_ );
    return result;
}


/*++
    Calculation of the second derivative of Hermite spline at specified point.
    C''(t) = a[0](o)'' * p[i] + a[1](o)'' * p[i+1] + b[0](o)'' * q[i] + b[1](o)'' * q[i+1],
    a[0](o)'' = -6 + 12*o,
    a[1](o)'' = 6 - 12*o,
    b[0](o)'' = - 4 + 6*o,
    b[1](o)'' = -2 + 6*o,
    o - local interval parametric value (omega),
    q - tangent at specified vertex.
--*/
Vector HermiteSpline::CalcSecondDerivative( double t ) const
{
    // calculation of local interval parametric value (omega)
    double o =  CalcLocalIntervalParametricValue( t );

    // calculation of a[0](o)'', a[1](o)'', b[0](o)'' and b[1](o)''
    double dda0 = -6 + 12 * o;
    double dda1 = 6 - 12 * o;
    double ddb0 = - 4 + 6 * o;
    double ddb1 = -2 + 6 * o;

    // calculation of second derivative
    Vector result;
    CalcRadiusVector( t, dda0, dda1, ddb0, ddb1, result.x_, result.y_ );
    return result;
}


void HermiteSpline::Transfer( const Vector& shift )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Transfer( shift );
    }
}


void HermiteSpline::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Scale( xCoef, yCoef, pCenter );
    }
}


void HermiteSpline::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Rotate( angle, pCenter );
    }
}


void HermiteSpline::Transform( const Matrix3x3& m )
{
    for ( size_t i = 0; i < vertices_.size(); ++i ) {
        vertices_[ i ].Transform( m );
    }
}


void HermiteSpline::UpdateInterval()
{
    if ( vertices_.size() < 2 ) {
        interval_ = ZERO_INTERVAL;
    } else {
        interval_.Init( 0.0, static_cast<double>(vertices_.size()) - 1.0 );
    }
}


/*++
    Calculation of the local interval points (starting and ending points).
--*/
void HermiteSpline::GetLocalIntervalPoints( double t, const Point*& pP0, const Point*& pP1,
    int& nP0 ) const
{
    if ( vertices_.size() >= 2 ) {

        int nTempP0 = static_cast<int>( ::floor(t) );
        int nTempP1 = static_cast<int>( ::ceil(t) );

        // parameter value exactly at vertex case (0.0, 1.0, 2.0 etc.)
        if ( nTempP0 == nTempP1 ) {
            nTempP1 = nTempP0 + 1;
        }

        if ( nTempP0 < 0 ) {
            // left boundary case
            nTempP0 = 0;
            nTempP1 = 1;

        } else if ( nTempP1 >= static_cast<int>(vertices_.size()) ) {
            // right boundary case
            nTempP0 = ( vertices_.size() - 2 );
            nTempP1 = ( vertices_.size() - 1 );
        }

        pP0 = &vertices_[ nTempP0 ];
        pP1 = &vertices_[ nTempP1 ];
        nP0 = nTempP0;

    } else {
        pP0 = 0;
        pP1 = 0;
    }
}


/*++
    Generic method for calculation of Hermite splines radius vector.
    C(t) = a0*p[i] + a1*p[i+1] + b0*q[i] + b1*q[i+1]
--*/
void HermiteSpline::CalcRadiusVector( double t, double a0, double a1, double b0, double b1,
    double& x, double& y ) const
{
    // acquision of local interval points
    const Point* pP0 = 0;
    const Point* pP1 = 0;
    int nP0;
    GetLocalIntervalPoints( t, pP0, pP1, nP0 );

    if ( pP0 != 0 ) {
        Vector q0;
        Vector q1;
        if ( CalcLocalIntervalTangent(nP0, q0) && CalcLocalIntervalTangent(nP0 + 1, q1) ) {
            x = a0 * pP0->x_ + a1 * pP1->x_ + b0 * q0.x_ + b1 * q1.x_;
            y = a0 * pP0->y_ + a1 * pP1->y_ + b0 * q0.y_ + b1 * q1.y_;
        }

    } else {
        x = 0;
        y = 0;
    }
}


/*++
    Calculation of the local interval parametric value (also called omega).
    o = (t - t[i]) / (t[i+1] - t[i]), t[i] <= t <= t[i+1], 0.0 <= o <= 1.0
--*/
double HermiteSpline::CalcLocalIntervalParametricValue( double t ) const
{
    double o = 0.0;

    if ( vertices_.size() >= 2 ) {

        int nP0 = static_cast<int>( ::floor(t) );
        int nP1 = static_cast<int>( ::ceil(t) );

        // parameter value exactly at vertex case (0.0, 1.0, 2.0 etc.)
        if ( nP0 == nP1 ) {
            nP1 = nP0 + 1;
        }

        if ( nP0 < 0 ) {
            // left boundary case
            o = t;

        } else if ( nP1 >= static_cast<int>(vertices_.size()) ) {
            // right boundary case
            o = ( t - static_cast<double>(vertices_.size()) + 2.0 );

        } else {
            // common case
            o = ( t - ::floor(t) );
        }
    }

    return o;
}


/*++
    Calculation of tangent at specified spline vertex (vertex specified by index).
    q[i] = (s[i+1] * (p[i]-p[i-1])) / (s[i] + s[i+1]) + (s[si] * (p[i+1] - p[i])) / (s[i] + s[i+1]),
    q[0] = 2*(p[1] - p[0]) - q[1],
    q[n-1] = 2*(p[n-2] - p[n-1]) - q[n-2],
    s[i] - distance between p[i] and p[i-1],
    s[i + 1] - distance between p[i+1] and p[i].
    Simpler method providing worse support for non-uniform vertices sets:
    q[i] = (p[i+1] - p[i-1]) / (t[i+1] - t[i-1])
--*/
bool HermiteSpline::CalcLocalIntervalTangent( int i, Vector& q ) const
{
    // spline containing at least 3 vertices required
    if ( vertices_.size() >= 3 ) {

        if ( i <= 0 ) {
            // left boundary case
            const Point& p0 = vertices_[ 0 ];
            const Point& p1 = vertices_[ 1 ];
            Vector q1;
            CalcLocalIntervalTangent( 1, q1 );
            q.x_ = 2.0 * ( p1.x_ - p0.x_ ) - q1.x_;
            q.y_ = 2.0 * ( p1.y_ - p0.y_ ) - q1.y_;

        } else if ( i >= (static_cast<int>(vertices_.size() - 1)) ) {
            // right boundary case
            const Point& pff = vertices_[ vertices_.size() - 2 ];
            const Point& p0 = vertices_[ vertices_.size() - 1 ];
            Vector qff;
            CalcLocalIntervalTangent( vertices_.size() - 2, qff );
            q.x_ = 2.0 * ( p0.x_ - pff.x_ ) - qff.x_;
            q.y_ = 2.0 * ( p0.y_ - pff.y_ ) - qff.y_;

        } else {
            // common case
            const Point& pff = vertices_[ i - 1 ];
            const Point& p0 = vertices_[ i ];
            const Point& p1 = vertices_[ i + 1 ];
            double s0 = CalcDistance( pff, p0 );
            double s1 = CalcDistance( p0, p1 );
            q.x_ = ( s0 * (p0.x_ - pff.x_) + s1 * (p1.x_ - p0.x_) ) / ( s0 + s1 );
            q.y_ = ( s0 * (p0.y_ - pff.y_) + s1 * (p1.y_ - p0.y_) ) / ( s0 + s1 );

            // common case, simpler method
            //const Point& pff = vertices_[ i - 1 ];
            //const Point& p1 = vertices_[ i + 1 ];
            //q.x_ = ( p1.x_ - pff.x_ ) / 2.0;
            //q.y_ = ( p1.y_ - pff.y_ ) / 2.0;
        }

        return true;

    } else {
        return false;
    }
}


//-------------------------------------------------------------------------------------------------
// E Q U I D I S T A N T   C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


EquidistantCurve::EquidistantCurve( Curve* pSourceCurve, const Vector& shiftDirection )
    : pSourceCurve_( pSourceCurve )
    , shiftDirection_( shiftDirection )
{
    assert( pSourceCurve != 0 );
    AddReference( pSourceCurve_ );
}


EquidistantCurve::EquidistantCurve( const EquidistantCurve& rhs )
    : pSourceCurve_( rhs.pSourceCurve_ )
    , shiftDirection_( rhs.shiftDirection_ )
{
    assert( rhs.pSourceCurve_ != 0 );
    AddReference( pSourceCurve_ );
}


EquidistantCurve::~EquidistantCurve()
{
    ReleaseReference( pSourceCurve_ );
}


EquidistantCurve& EquidistantCurve::operator =( const EquidistantCurve& rhs )
{
    if ( &rhs != this ) {
        SetSourceCurve( rhs.pSourceCurve_ );
        shiftDirection_ = rhs.shiftDirection_;
    }
    return ( *this );
}


void EquidistantCurve::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


const Curve& EquidistantCurve::GetSourceCurve() const
{
    return ( *pSourceCurve_ );
}


const Vector& EquidistantCurve::GetShiftDirection() const
{
    return shiftDirection_;
}


void EquidistantCurve::SetSourceCurve( Curve* pSourceCurve )
{
    assert( pSourceCurve != 0 );
    ReleaseReference( pSourceCurve_ );
    pSourceCurve_ = pSourceCurve;
    AddReference( pSourceCurve_ );
}


void EquidistantCurve::SetShiftDirection( const Vector& direction )
{
    shiftDirection_ = direction;
}


ECurveTypes EquidistantCurve::GetType() const
{
    return CT_EQUIDISTANT_CURVE;
}


const Interval& EquidistantCurve::GetParametricInterval() const
{
    return ( pSourceCurve_->GetParametricInterval() );
}


Point EquidistantCurve::CalcPointOnCurve( double t ) const
{
    Vector sourceDerivative = pSourceCurve_->CalcFirstDerivative( t );
    Vector shiftDirection = shiftDirection_;
    shiftDirection.Rotate( sourceDerivative.CalcAngle() );

    Point result = pSourceCurve_->CalcPointOnCurve( t );
    result.Transfer( shiftDirection );
    return result;
}


Vector EquidistantCurve::CalcFirstDerivative( double t ) const
{
    return pSourceCurve_->CalcFirstDerivative( t );
}


Vector EquidistantCurve::CalcSecondDerivative( double t ) const
{
    return pSourceCurve_->CalcSecondDerivative( t );
}


double EquidistantCurve::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    return pSourceCurve_->CalcTiltStep( t, da, span, cut );
}


void EquidistantCurve::Transfer( const Vector& shift )
{
    pSourceCurve_->Transfer( shift );
}


void EquidistantCurve::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    pSourceCurve_->Scale( xCoef, yCoef, pCenter );
}


void EquidistantCurve::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    pSourceCurve_->Rotate( angle, pCenter );
}


void EquidistantCurve::Transform( const Matrix3x3& m )
{
    pSourceCurve_->Transform( m );
}


//-------------------------------------------------------------------------------------------------
// P R O X Y   C U R V E   I M P L E M E N T A T I O N
//-------------------------------------------------------------------------------------------------


ProxyCurve::ProxyCurve( Curve* pSourceCurve )
    : pSourceCurve_( pSourceCurve )
    , pInterval_( 0 )
{
    assert( pSourceCurve != 0 );
    AddReference( pSourceCurve_ );
}


ProxyCurve::ProxyCurve( Curve* pSourceCurve, const Interval& interval )
    : pSourceCurve_( pSourceCurve )
    , pInterval_( new Interval(interval) )
{
    assert( pSourceCurve != 0 );
    AddReference( pSourceCurve_ );
}


ProxyCurve::ProxyCurve( const ProxyCurve& rhs )
    : pSourceCurve_( rhs.pSourceCurve_ )
    , pInterval_( (rhs.pInterval_ != 0) ? new Interval(*rhs.pInterval_) : 0 )
{
    assert( rhs.pSourceCurve_ != 0 );
    AddReference( pSourceCurve_ );
}


ProxyCurve::~ProxyCurve()
{
    delete pInterval_;
    ReleaseReference( pSourceCurve_ );
}


ProxyCurve& ProxyCurve::operator =( const ProxyCurve& rhs )
{
    if ( &rhs != this ) {
        SetSourceCurve( rhs.pSourceCurve_ );
        delete pInterval_;
        pInterval_ = ( (rhs.pInterval_ != 0) ? new Interval(*rhs.pInterval_) : 0 );
    }
    return ( *this );
}


void ProxyCurve::Accept( CurveVisitor& visitor )
{
    visitor.Visit( *this );
}


const Curve& ProxyCurve::GetSourceCurve() const
{
    return ( *pSourceCurve_ );
}


void ProxyCurve::SetSourceCurve( Curve* pSourceCurve )
{
    assert( pSourceCurve != 0 );
    ReleaseReference( pSourceCurve_ );
    pSourceCurve_ = pSourceCurve;
    AddReference( pSourceCurve_ );
}


void ProxyCurve::SetParametricInterval( const Interval& interval )
{
    delete pInterval_;
    pInterval_ = new Interval( interval );
}


void ProxyCurve::ResetParametricInterval()
{
    delete pInterval_;
    pInterval_ = 0;
}


ECurveTypes ProxyCurve::GetType() const
{
    return CT_PROXY_CURVE;
}


const Interval& ProxyCurve::GetParametricInterval() const
{
    return ( pInterval_ ? *pInterval_ : pSourceCurve_ ->GetParametricInterval() );
}


Point ProxyCurve::CalcPointOnCurve( double t ) const
{
    return pSourceCurve_->CalcPointOnCurve( t );
}


Vector ProxyCurve::CalcFirstDerivative( double t ) const
{
    return pSourceCurve_->CalcFirstDerivative( t );
}


Vector ProxyCurve::CalcSecondDerivative( double t ) const
{
    return pSourceCurve_->CalcSecondDerivative( t );
}


double ProxyCurve::CalcTiltStep( double t, double da, const Interval& span, bool& cut ) const
{
    return pSourceCurve_->CalcTiltStep( t, da, span, cut );
}


void ProxyCurve::Transfer( const Vector& shift )
{
    pSourceCurve_->Transfer( shift );
}


void ProxyCurve::Scale( double xCoef, double yCoef, const Point* pCenter /*= 0*/ )
{
    pSourceCurve_->Scale( xCoef, yCoef, pCenter );
}


void ProxyCurve::Rotate( double angle, const Point* pCenter /*= 0*/ )
{
    pSourceCurve_->Rotate( angle, pCenter );
}


void ProxyCurve::Transform( const Matrix3x3& m )
{
    pSourceCurve_->Transform( m );
}


//-------------------------------------------------------------------------------------------------
// END
//-------------------------------------------------------------------------------------------------


}  // namespace m2d
